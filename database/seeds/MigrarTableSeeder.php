<?php

use Illuminate\Database\Seeder;
use App\Core\Entities\Person;
use App\Core\Entities\PaymentDocumentDetail;

class MigrarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $results =\Excel::load(base_path('ini-files/migrar.xlsx'), function($reader) {
        })->get();


        foreach($results as $item)
        {
            $payment = PaymentDocumentDetail::find($item->payment_id);

            if(isset($payment->id))
            {
                DB::table('payment_document_details')->insert(
                    [
                        'id'=>$item->id,
                        'payment_id'=>$item->name,
                        'payment_document_id'=>$item->payment_document_id,
                        'amount'=>$item->amount,
                        'state'=>$item->state

                    ]
                );
            }else{
                DB::table('payment_document_details')->insert(
                    [
                        'id'=>$item->id,
                        'payment_id'=>$item->name,
                        'amount'=>$item->amount,
                        'state'=>$item->state
                    ]
                );
            }
        }
    }
}
