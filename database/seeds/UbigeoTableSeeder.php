<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 05/03/15
 * Time: 01:17 AM
 */

use Illuminate\Database\Seeder;

class UbigeoTableSeeder extends Seeder {

    public function run()
    {
        $sql = file_get_contents(base_path('SQL/departments.sql'));
        $sql2 = file_get_contents(base_path('SQL/provinces.sql'));
        $sql3 = file_get_contents(base_path('SQL/districts.sql'));


        // split the statements, so DB::statement can execute them.
        $statements = array_filter(array_map('trim', explode(';', $sql)));
        $statements2 = array_filter(array_map('trim', explode(';', $sql2)));
        $statements3 = array_filter(array_map('trim', explode(';', $sql3)));

        foreach ($statements as $stmt) {
            DB::statement($stmt);
        }

        foreach ($statements2 as $stmt) {
            DB::statement($stmt);
        }

        foreach ($statements3 as $stmt) {
            DB::statement($stmt);
        }
    }
} 