<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 05/03/15
 * Time: 01:17 AM
 */

use Illuminate\Database\Seeder;
use App\Core\Entities\Module;
use App\Core\Entities\SubModule;

class ModulesTableSeeder extends Seeder {

    public function run()
    {

        $modules = array();

        $modules[] = [
            "name" => "Registro",
            "icon" => "icon-note",
            'submodules'=>[
                [
                    "name" => "Personas",
                    "uri" => "app/people",
                    "link_menu" => 1
                ]
                ,  [
                    "name" => "Perfiles",
                    "uri" => "app/profiles",
                    "link_menu" => 1
                ]
                ,  [
                    "name" => "Usuarios",
                    "uri" => "app/users",
                    "link_menu" => 1
                ]
                ,  [
                    "name" => "Compañia",
                    "uri" => "app/companies",
                    "link_menu" => 1
                ]
                , [
                    "name" => "Locales",
                    "uri" => "app/sites",
                    "link_menu" => 1
                ]
                ,  [
                    "name" => "Areas",
                    "uri" => "app/areas",
                    "link_menu" => 1
                ]
                , [
                    "name" => "Cargos",
                    "uri" => "app/area-jobs",
                    "link_menu" => 1
                ]
                ,[
                    "name" => "Trabajadores",
                    "uri" => "app/employees",
                    "link_menu" => 1
                ]
                , [
                    "name" => "Empresas",
                    "uri" => "app/enterprises",
                    "link_menu" => 1
                ]
                , [
                    "name" => "Clientes",
                    "uri" => "app/customers",
                    "link_menu" => 1
                ] ,[
                    "name" => "Proveedores",
                    "uri" => "app/suppliers",
                    "link_menu" => 1
                ]
            ]
        ];

        $modules[] = [
            "name" => "Operaciones",
            "icon" => "icon-globe",
            'submodules'=>[
                [
                    "name" => "Garantias",
                    "uri" => "app/warranties",
                    "link_menu" => 1
                ]
                ,  [
                    "name" => "Prestamos",
                    "uri" => "app/loans",
                    "link_menu" => 1
                ]
                ,  [
                    "name" => "Negocios",
                    "uri" => "app/business",
                    "link_menu" => 1
                ]
                ,  [
                    "name" => "Caja",
                    "uri" => "app/cash-desk-details",
                    "link_menu" => 1
                ]
            ]
        ];


        foreach ($modules as $module)
        {
            $module_entity = Module::create(
                [
                    "name" => $module['name'],
                    "icon" => $module['icon']
                ]
            );

            foreach ($module['submodules'] as $submodule)
            {
                SubModule::create(
                    [
                        "name" => $submodule['name'],
                        "uri" => $submodule['uri'],
                        "link_menu" => $submodule['link_menu'],
                        "module_id"=> $module_entity->id
                    ]
                );
            }
        }

    }
} 