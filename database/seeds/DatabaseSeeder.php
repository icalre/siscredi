<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $this->call(DayTableSeeder::class);
        $this->call(DocumentTypeTableSeeder::class);
        $this->call(TypesOfMovementsTableSeeder::class);
        $this->call(UbigeoTableSeeder::class);
        $this->call(PersonTableSeeder::class);
        $this->call(ModulesTableSeeder::class);

    }
}
