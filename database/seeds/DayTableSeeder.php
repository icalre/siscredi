<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 05/03/15
 * Time: 01:17 AM
 */
use App\Core\Entities\Day;
use Illuminate\Database\Seeder;

class DayTableSeeder extends Seeder {

    public function run()
    {
        Day::create(
            [
                'valor'=>1,
                'name'=>'Domingo'
            ]
        );

        Day::create(
            [
                'valor'=>2,
                'name'=>'Lunes'
            ]
        );
        Day::create(
            [
                'valor'=>3,
                'name'=>'Martes'
            ]
        );
        Day::create(
            [
                'valor'=>4,
                'name'=>'Miercoles'
            ]
        );
        Day::create(
            [
                'valor'=>5,
                'name'=>'Jueves'
            ]
        );
        Day::create(
            [
                'valor'=>6,
                'name'=>'Viernes'
            ]
        );
        Day::create(
            [
                'valor'=>7,
                'name'=>'Sabado'
            ]
        );
    }
} 