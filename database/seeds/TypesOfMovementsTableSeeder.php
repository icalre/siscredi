<?php

use Illuminate\Database\Seeder;
use App\Core\Entities\TypesOfMovement;

class TypesOfMovementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $results =\Excel::load(base_path('ini-files/operacion_department_stores.xlsx'), function($reader) {
        })->get();


        foreach($results as $item)
        {
            TypesOfMovement::create([
                'name'=>$item->name,
                'code'=>$item->code,
                'type'=>$item->type
            ]);
        }
    }
}
