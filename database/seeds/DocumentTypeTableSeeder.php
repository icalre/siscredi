<?php

use Illuminate\Database\Seeder;
use App\Core\Entities\DocumentType;

class DocumentTypeTableSeeder extends Seeder
{
    public function run()
    {
        $results =\Excel::load(base_path('ini-files/tabla10.xls'), function($reader) {
        })->get();

        foreach($results as $item)
        {
            DocumentType::create([
                'name'=>$item->descripcion,
                'nro'=>$item->n
            ]);
        }
    }
}
