<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 05/03/15
 * Time: 01:17 AM
 */
use Faker\Factory as Faker;
use App\Core\Entities\Person;
use App\Core\Entities\Profile;
use App\Core\Entities\User;
use Illuminate\Database\Seeder;

class PersonTableSeeder extends Seeder {

    public function run()
    {
        Person::create(['name'=>'Administrador',
            'last_name'=>'Administrador',
            'address'=>'Administrador',
            'email'=>'administrador@email.com',
            'dni'=>12345678,
            'department_id'=>rand(1,24),
            'province_id'=>rand(1,100),
            'district_id'=>rand(1,1000),
            'date_of_birth'=>'28-05-1989']);

        Profile::create(['name'=>'Administrador',
                         'description'=>'Administrador de sistema'
                        ]);

        User::create([
            'name'=>'administrador',
            'person_id'=>1,
            'profile_id'=>1,
            'password'=>123456,
            'online'=>0,
            'state'=>1
        ]);
    }
} 