<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDocumentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_document_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_document_id')->unsigned();
            $table->integer('payment_id')->unsigned()->nullable();
            $table->decimal('amount',10,2);
            $table->enum('state',['Anulled', 'Correct'])->default('Correct');
            $table->timestamps();
            $table->foreign('payment_id')->references('id')->on('payments');
            $table->foreign('payment_document_id')->references('id')->on('payment_documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_document_details');
    }
}
