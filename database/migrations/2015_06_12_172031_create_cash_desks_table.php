<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashDesksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cash_desks', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name');
            $table->integer('site_id')->unsigned();
            $table->boolean('state');
            $table->string('number');
            $table->string('series');
            $table->string('printer_name');
            $table->foreign('site_id')->references('id')->on('sites');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cash_desks');
	}

}
