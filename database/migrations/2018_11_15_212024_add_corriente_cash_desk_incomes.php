<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCorrienteCashDeskIncomes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cash_desk_incomes', function (Blueprint $table) {
            $table->boolean('corriente')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cash_desk_incomes', function (Blueprint $table) {
            $table->dropColumn('corriente');
        });
    }
}
