<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id')->unsigned();
            $table->integer('cash_desk_detail_id')->unsigned();
            $table->decimal('initial_amount', 10,2);
            $table->decimal('final_amount', 10,2);
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('cash_desk_detail_id')->references('id')->on('cash_desk_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('account_details');
    }
}
