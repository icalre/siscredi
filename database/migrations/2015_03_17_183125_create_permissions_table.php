<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('permissions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->integer('sub_module_id')->unsigned();
            $table->foreign('profile_id')->references('id')->on('profiles');
            $table->foreign('sub_module_id')->references('id')->on('sub_modules');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('permissions');
	}

}
