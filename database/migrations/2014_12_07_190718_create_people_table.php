<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('people', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('name');
            $table->string('last_name');
            $table->string('address')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('dni',8)->unique();
            $table->string('telephone')->nullable();
            $table->integer('department_id')->unsigned();
            $table->integer('province_id')->unsigned();
            $table->integer('district_id')->unsigned();
            $table->enum('gender',['Male','Female']);
            $table->date('date_of_birth')->default('0000-00-00')->nullable();
            $table->foreign('department_id')->references('id')->on('departments');
            $table->foreign('province_id')->references('id')->on('provinces');
            $table->foreign('district_id')->references('id')->on('districts');
            $table->softDeletes();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('people');
	}

}
