<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('code');
            $table->boolean('state');
            $table->decimal('salary_amount',8,2);
            $table->decimal('salary_hour',8,2);
            $table->decimal('salary_day',8,2);
            $table->string('hours_day');
            $table->integer('site_id')->unsigned();
            $table->integer('area_job_id')->unsigned();
            $table->integer('person_id')->unsigned();
            $table->integer('area_id')->unsigned();
            $table->foreign('area_id')->references('id')->on('areas');
            $table->foreign('area_job_id')->references('id')->on('area_jobs');
            $table->foreign('person_id')->references('id')->on('people');
            $table->foreign('site_id')->references('id')->on('sites');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
    }

}
