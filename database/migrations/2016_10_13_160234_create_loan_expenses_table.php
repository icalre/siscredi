<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('amount', 10,2);
            $table->integer('loan_id')->unsigned();
            $table->integer('payment_id')->unsigned()->nullable();
            $table->integer('payment_document_id')->unsigned()->nullable();
            $table->text('description');
            $table->enum('state', ['Pendiente', 'Pagado']);
            $table->foreign('loan_id')->references('id')->on('loans');
            $table->foreign('payment_id')->references('id')->on('payments');
            $table->foreign('payment_document_id')->references('id')->on('payment_documents');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('loan_expenses');
    }
}
