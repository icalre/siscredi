<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncomeIdCashDeskExpense extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cash_desk_expenses', function (Blueprint $table) {
            $table->integer('cash_desk_income_id')->unsigned()->nullable();
            $table->foreign('cash_desk_income_id')->references('id')->on('cash_desk_incomes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cash_desk_expenses', function (Blueprint $table) {
            $table->dropColumn('cash_desk_income_id');
        });
    }
}
