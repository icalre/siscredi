<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->decimal('amount',10,2);
            $table->decimal('interest',10,2);
            $table->enum('currency',['Soles','Dolares']);
            $table->enum('warranty_option',['Si','No']);
            $table->enum('endorsement_option',['Si','No']);
            $table->date('date_start')->default('00-00-0000');
            $table->string('code');
            $table->string('quotas');
            $table->enum('period',['Semanal','Mensual']);
            $table->enum('type_payment',['Fija','Libre']);
            $table->integer('employee_id')->unsigned();
            $table->enum('state',['Pre-Aprobado','Aprobado','Vigente','Atrasado','Pagado','Anulado','Perdida','Cancelado','Garantía Ejecutada','Perdida Cancelado','Garantía Ejecutada Cancelado'])->default('Pre-Aprobado');
            $table->date('date_end')->default('00-00-0000');
            $table->string('nro_contrato');
            $table->text('observations');
            $table->date('date_annulment')->default('00-00-0000');
            $table->date('date_disbursement')->default('00-00-0000');
            $table->integer('person_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('person_id')->references('id')->on('people');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('employee_id')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('loans');
    }
}
