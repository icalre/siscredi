<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('companies', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('razon_social');
            $table->string('ruc');
            $table->string('nombre_comercial');
            $table->string('address');
            $table->integer('department_id')->unsigned();
            $table->integer('province_id')->unsigned();
            $table->integer('district_id')->unsigned();
            $table->string('telephone');
            $table->string('email');
            $table->string('website');
            $table->string('operating_license');
            $table->string('permit_fumigation');
            $table->string('health_license');
            $table->string('commercial_permission');
            $table->foreign('department_id')->references('id')->on('departments');
            $table->foreign('province_id')->references('id')->on('provinces');
            $table->foreign('district_id')->references('id')->on('districts');
            $table->softDeletes();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('companies');
	}

}
