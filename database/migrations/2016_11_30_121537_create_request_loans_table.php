<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_loans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('person_id')->unsigned();
            $table->decimal('amount', 10, 2);
            $table->decimal('interest',10,2);
            $table->enum('period',['Semanal','Mensual']);
            $table->enum('type_payment',['Fija','Libre']);
            $table->string('quotas');
            $table->integer('employee_id')->unsigned();
            $table->text('observation');
            $table->foreign('person_id')->references('id')->on('people');
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('request_loans');
    }
}
