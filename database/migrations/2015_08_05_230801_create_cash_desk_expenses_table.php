<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashDeskExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_desk_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cash_desk_detail_id')->unsigned();
            $table->enum('type',['Crediperu','Otros','Adelanto']);
            $table->integer('person_id')->unsigned()->nullable();
            $table->string('expense_type');
            $table->string('expense_id');
            $table->decimal('amount',10,2);
            $table->text('description');
            $table->foreign('cash_desk_detail_id')->references('id')->on('cash_desk_details');
            $table->foreign('person_id')->references('id')->on('people');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cash_desk_expenses');
    }
}
