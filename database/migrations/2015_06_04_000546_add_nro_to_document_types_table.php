<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNroToDocumentTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('document_types', function(Blueprint $table)
		{
			$table->string('nro');
            $table->dropColumn('alias');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('document_types', function(Blueprint $table)
		{
			$table->dropColumn('nro');
            $table->string('alias');
		});
	}

}
