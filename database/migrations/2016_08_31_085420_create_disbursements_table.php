<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisbursementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disbursements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cash_desk_detail_id')->unsigned();
            $table->integer('loan_id')->unsigned();
            $table->decimal('amount',10,2);
            $table->foreign('cash_desk_detail_id')->references('id')->on('cash_desk_details');
            $table->foreign('loan_id')->references('id')->on('loans');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('disbursements');
    }
}
