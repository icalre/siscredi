<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('customers', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('code');
            $table->integer('customer_id')->unsigned();
            $table->string('customer_type');
            $table->string('rate');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('customers');
	}

}
