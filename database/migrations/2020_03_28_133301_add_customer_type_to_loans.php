<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomerTypeToLoans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loans', function (Blueprint $table) {
            $table->enum('customer_type', ['Nuevo', 'Renovación'])->nullable();
            $table->decimal('effective_warranty', 10,2)->nullable();
            $table->boolean('effective_state')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loans', function (Blueprint $table) {
            $table->dropColumn('customer_type');
            $table->dropColumn('effective_warranty');
            $table->dropColumn('effective_state');
        });
    }
}
