<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consignments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cash_desk_detail_id')->unsigned();
            $table->integer('to_cash_desk_detail_id')->unsigned();
            $table->decimal('amount',10,2);
            $table->boolean('state');
            $table->date('date')->default('00-00-0000');
            $table->foreign('cash_desk_detail_id')->references('id')->on('cash_desk_details');
            $table->foreign('to_cash_desk_detail_id')->references('id')->on('cash_desk_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('consignments');
    }
}
