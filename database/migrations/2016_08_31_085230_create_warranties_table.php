<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarrantiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warranties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('loan_id')->unsigned();
            $table->text('Description');
            $table->enum('state',['Custodia','Remate','Rescatado']);
            $table->date('remate_date')->defaults('0000-00-00');
            $table->date('liberado_date')->defaults('0000-00-00');
            $table->decimal('reference_price',10,2);
            $table->enum('type',['Otro','Artefacto', 'Joya'])->default('Otro');
            $table->decimal('pb', 10,2);
            $table->decimal('pn',10,2);
            $table->foreign('loan_id')->references('id')->on('loans');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('warranties');
    }
}
