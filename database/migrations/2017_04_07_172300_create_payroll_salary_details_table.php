<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollSalaryDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_salary_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payroll_salary_id')->unsigned();
            $table->integer('employee_id')->unsigned();
            $table->decimal('amount',10,2);
            $table->decimal('discounts',10,2);
            $table->decimal('amount_pay');
            $table->foreign('payroll_salary_id')->references('id')->on('payroll_salaries');
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payroll_salary_details');
    }
}
