<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashDeskDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cash_desk_details', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('cash_desk_id')->unsigned();
            $table->decimal('initial_amount',10,2);
            $table->decimal('arching',10,2);
            $table->decimal('difference',10,2);
            $table->decimal('total_amount',10,2);
            $table->boolean('state');
            $table->dateTime('date_start');
            $table->dateTime('date_end');
            $table->integer('user_id')->unsigned();
            $table->foreign('cash_desk_id')->references('id')->on('cash_desks');
            $table->foreign('user_id')->references('id')->on('users');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cash_desk_details');
	}

}
