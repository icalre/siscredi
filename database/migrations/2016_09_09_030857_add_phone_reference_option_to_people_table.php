<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhoneReferenceOptionToPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('people', function (Blueprint $table) {
            $table->string('phone_reference');
            $table->string('activity');
            $table->boolean('file');
            $table->string('business_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('people', function (Blueprint $table) {
            $table->dropColumn('phone_reference');
            $table->dropColumn('file');
            $table->dropColumn('activity');
            $table->dropColumn('business_address');
        });
    }
}
