<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypePaymentToCashDeskDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cash_desk_details', function (Blueprint $table) {
            $table->decimal('efectivo', 10,2)->default(0);
            $table->decimal('cuenta_corriente', 10,2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cash_desk_details', function (Blueprint $table) {
            $table->dropColumn('efectivo');
            $table->dropColumn('cuenta_corriente');
        });
    }
}
