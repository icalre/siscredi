<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('amount',10,2);
            $table->integer('cash_desk_detail_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->integer('document_type_id')->unsigned();
            $table->enum('state',['Anulled', 'Correct'])->default('Correct');
            $table->foreign('cash_desk_detail_id')->references('id')->on('cash_desk_details');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('document_type_id')->references('id')->on('document_types');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_documents');
    }
}
