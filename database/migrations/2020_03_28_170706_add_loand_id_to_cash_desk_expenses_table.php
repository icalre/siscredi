<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoandIdToCashDeskExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cash_desk_expenses', function (Blueprint $table) {
            $table->integer('loan_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cash_desk_expenses', function (Blueprint $table) {
            $table->dropColumn('loan_id');
        });
    }
}
