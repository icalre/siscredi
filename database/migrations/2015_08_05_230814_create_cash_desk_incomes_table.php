<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashDeskIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_desk_incomes', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type',['Crediperu','Otros']);
            $table->integer('cash_desk_detail_id')->unsigned();
            $table->text('description');
            $table->decimal('amount',10,2);
            $table->foreign('cash_desk_detail_id')->references('id')->on('cash_desk_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cash_desk_incomes');
    }
}
