<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type',['Adelanto','Descuento']);
            $table->boolean('state');
            $table->decimal('amount',10,2);
            $table->integer('employee_id')->unsigned();
            $table->text('description');
            $table->integer('payroll_salary_detail_id')->unsigned()->nullable();
            $table->integer('cash_desk_expense_id')->unsigned()->nullable();
            $table->foreign('payroll_salary_detail_id')->references('id')->on('payroll_salary_details');
            $table->foreign('cash_desk_expense_id')->references('id')->on('cash_desk_expenses');
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('salary_discounts');
    }
}
