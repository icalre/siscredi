<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('loan_id')->unsigned();
            $table->decimal('amount',10,2);
            $table->decimal('interest',10,2);
            $table->decimal('capital',10,2);
            $table->date('expiration')->default('00-00-0000');
            $table->timestamp('date_pay');
            $table->decimal('mora',10,2);
            $table->string('number');
            $table->enum('state',['Pagado','Atrasado','Pendiente','Anulado','Garantia','Perdida']);
            $table->foreign('loan_id')->references('id')->on('loans');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}
