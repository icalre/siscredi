<?php namespace
App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redis;
use App\Core\Components\MainMenu;

class ModuleAccessMiddleware {

    protected $mainMenu;
    public function __construct(MainMenu $mainMenu){
        $this->mainMenu = $mainMenu;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $date = Redis::get('session-date');

        if(env('HOURCHECK') == 1 && date('Y-m-d') != $date)
        {
            shell_exec('sudo ntpdate -u hora.roa.es');
            Redis::set('session-date', date('Y-m-d'));
        }

        if(Redis::ttl('session-date') <= 0)
        {
            Redis::expire('session-date', 3600);
        }

        $menu = $this->mainMenu->menu(\Auth::user()->profile_id);

        $sub_modules = $menu['sub_modules'];

       $segment = explode('-',\Request::segment(2));
        $uri = '';

        if(count($segment) >= 2 )
        {
            if($segment[1] == 'create' ||  $segment[1] == 'edit' ||  $segment[1] == 'show')
            {
                $uri = $segment[0];
            }else{
                $uri = $segment[0].'-'.$segment[1];

                if($segment[1] == 'desk')
                {
                    $uri = $segment[0].'-'.$segment[1].'-'.$segment[2];
                }
            }
        }else{
            if(isset($segment[1]))
            {
                $uri = $segment[0].'-'.$segment[1];

                if($segment[1] == 'desk')
                {
                    $uri = $segment[0].'-'.$segment[1].'-'.$segment[2];
                }
            }else{
                $uri = $segment[0];
            }
        }

        if(in_array(\Request::segment(1).'/'.$uri, $sub_modules))
        {
            return $next($request);
        }elseif(\Request::segment(1).'/'.$uri == 'app/modules' && \Auth::user()->profile_id == 1) {
            return $next($request);
        }else {
            if ($request->wantsJson())
            {
                return \Response::json(403);
            }else if ($request->ajax())
            {
                return redirect()->to('/pages/403');
            }
            else
            {
                return redirect()->to('/pages/403?v=2');
            }
        }
    }

}
