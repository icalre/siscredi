<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/valid-session', function () {
    $response = false;
    if(\Auth::check())
    {
        $response = true;
    }

    return \Response::json(['response'=>$response]);

});

Route::get('/log-out', function () {

    \Auth::logout();

    return \Response::json(['response'=>true]);

});

Route::get('/', function () {
    return view('layouts.master');
    // return view('welcome');
});

Route::get('/print-lose-loans', ['as' => 'print-lose-loans', 'uses' => 'ReportsController@printLoseLoans'] );

Route::get('/pages/{id}', ['as' => 'pages', 'uses' => 'HomeController@pages']);

Route::group(['middleware' => 'auth'], function()
{
    Route::get('/get-menu', ['as' => 'get_menu', 'uses' => 'HomeController@getMenu']);

    Route::group(['prefix' => 'app'], function()
    {
        Route::get('/', ['as' => 'app', 'uses' => 'HomeController@basePath']);

        Route::get('/user-profile', ['as' => 'user-profile', 'uses' => 'HomeController@basePath']);
        Route::get('/wall', ['as' => 'wall', 'uses' => 'HomeController@basePath']);

        Route::post('/user/change-password',['as'=>'user/change-password','uses'=>'UsersController@changePassword']);
        Route::post('/user/upload-photo',['as'=>'user/upload-photo','uses'=>'UsersController@uploadPhoto']);
        Route::post('/user/change-color',['as'=>'user/change-color','uses'=>'UsersController@changeColor']);

        Route::group(['prefix' => 'home'], function()
        {
            Route::get('/', ['as' => 'home', 'uses' => 'HomeController@basePath']);
            Route::get('/index', ['as' => 'home-index', 'uses' => 'HomeController@index']);
            Route::get('/user-profile', ['as' => 'user-profile-index', 'uses' => 'HomeController@profile']);
            Route::get('/ini', ['as' => 'home-index', 'uses' => 'HomeController@home']);
            Route::get('/pagadas', ['as' => 'home-pagadas', 'uses' => 'HomeController@printPayed']);
            Route::get('/ingresos', ['as' => 'home-pagadas', 'uses' => 'HomeController@printIncomes']);
            Route::get('/desembolsos', ['as' => 'home-pagadas', 'uses' => 'HomeController@printDesembolsos']);
            Route::get('/gastos', ['as' => 'home-pagadas', 'uses' => 'HomeController@printGastos']);
            Route::get('/egresos', ['as' => 'home-pagadas', 'uses' => 'HomeController@printEgresos']);

        });

        Route::group(['prefix'=>'flujo'], function()
        {
            Route::get('/targets-values', ['as' => 'hreports-targets-values', 'uses' => 'HomeController@getReportTargets']);
            Route::get('/report-lines-numbers', ['as' => 'hreports-targets-values', 'uses' => 'HomeController@getReportLinesNumbers']);
            Route::get('/report-lines-amounts', ['as' => 'hreports-targets-values', 'uses' => 'HomeController@getReportLinesAmounts']);
            Route::get('/capital', ['as' => 'reports-capital', 'uses' => 'HomeController@capital']);
        });

        //gestion de permisos
        Route::group(['prefix' => 'modules', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'module','uses'=>'HomeController@basePath']);
            Route::get('/form-create',['as'=>'module_form_create','uses'=>'ModulesController@formCreate']);
            Route::get('/form-edit',['as'=>'module_form_edit','uses'=>'ModulesController@formEdit']);
            Route::get('/all',['as'=>'modules_all','uses'=>'ModulesController@all']);
            Route::get('/find/{id}',['as'=>'module_find','uses'=>'ModulesController@find']);
        });
        Route::get('modules-create',['as'=>'module_create','uses'=>'HomeController@basePath']);
        Route::post('modules-create',['as'=>'module_create','uses'=>'ModulesController@create']);
        Route::get('modules-edit/{id?}',['as'=>'module_edit','uses'=>'HomeController@basePath']);
        Route::put('modules-edit/{id}',['as'=>'module_edit', 'uses'=>'ModulesController@edit']);
        Route::delete('modules-delete/{id}',['as'=>'module_delete', 'uses'=>'ModulesController@delete']);
        //end gestion de permisos

        //gestion de personas
        Route::group(['prefix' => 'people', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'people','uses'=>'HomeController@basePath']);
            Route::get('/form-create',['as'=>'people_form_create','uses'=>'PeopleController@formCreate']);
            Route::get('/form-edit',['as'=>'people_form_edit','uses'=>'PeopleController@formEdit']);
            Route::get('/all',['as'=>'people_all','uses'=>'PeopleController@all']);
            Route::get('/find/{id}',['as'=>'people_find','uses'=>'PeopleController@find']);
            Route::get('/file',['as'=>'people_find','uses'=>'PeopleController@printFile']);
        });
        Route::get('people-create',['as'=>'people_create','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('people-create',['as'=>'people_create','middleware' => 'modules','uses'=>'PeopleController@create']);
        Route::get('people-edit/{id?}',['as'=>'people_edit','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::put('people-edit/{id}',['as'=>'people_edit','middleware' => 'modules', 'uses'=>'PeopleController@edit']);
        Route::delete('people-delete/{id}',['as'=>'people_delete','middleware' => 'modules', 'uses'=>'PeopleController@delete']);
        //end gestion de personas


        //gestion de perfiles
        Route::group(['prefix' => 'profiles', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'profiles','uses'=>'HomeController@basePath']);
            Route::get('/form-create',['as'=>'profiles_form_create','uses'=>'ProfilesController@formCreate']);
            Route::get('/form-edit',['as'=>'profiles_form_edit','uses'=>'ProfilesController@formEdit']);
            Route::get('/all',['as'=>'profiles_all','uses'=>'ProfilesController@all']);
            Route::get('/find/{id}',['as'=>'profiles_find','uses'=>'ProfilesController@find']);
        });
        Route::get('profiles-create',['as'=>'profiles_create','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('profiles-create',['as'=>'profiles_create','middleware' => 'modules','uses'=>'ProfilesController@create']);
        Route::get('profiles-edit/{id?}',['as'=>'profiles_edit','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::put('profiles-edit/{id}',['as'=>'profiles_edit','middleware' => 'modules', 'uses'=>'ProfilesController@edit']);
        Route::delete('profiles-delete/{id}',['as'=>'profiles_delete','middleware' => 'modules', 'uses'=>'ProfilesController@delete']);
        //end gestion de perfiles

        //gestion de usuarios
        Route::group(['prefix' => 'users', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'users','uses'=>'HomeController@basePath']);
            Route::get('/form-create',['as'=>'users_form_create','uses'=>'UsersController@formCreate']);
            Route::get('/form-edit',['as'=>'users_form_edit','uses'=>'UsersController@formEdit']);
            Route::get('/all',['as'=>'users_all','uses'=>'UsersController@all']);
            Route::get('/find/{id}',['as'=>'users_find','uses'=>'UsersController@find']);
        });
        Route::get('users-create',['as'=>'users_create','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('users-create',['as'=>'users_create','middleware' => 'modules','uses'=>'UsersController@create']);
        Route::get('users-edit/{id?}',['as'=>'users_edit','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::put('users-edit/{id}',['as'=>'users_edit','middleware' => 'modules', 'uses'=>'UsersController@edit']);
        Route::delete('users-delete/{id}',['as'=>'users_delete','middleware' => 'modules', 'uses'=>'UsersController@delete']);
        //end gestion de usuarios

        //gestion de usuarios
        Route::group(['prefix' => 'companies', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'companies','uses'=>'HomeController@basePath']);
            Route::get('/form-create',['as'=>'companies_form_create','uses'=>'CompaniesController@formCreate']);
            Route::get('/form-edit',['as'=>'companies_form_edit','uses'=>'CompaniesController@formEdit']);
            Route::get('/all',['as'=>'companies_all','uses'=>'CompaniesController@all']);
            Route::get('/find/{id}',['as'=>'companies_find','uses'=>'CompaniesController@find']);
        });
        Route::get('companies-create',['as'=>'companies_create','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('companies-create',['as'=>'companies_create','middleware' => 'modules','uses'=>'CompaniesController@create']);
        Route::get('companies-edit/{id?}',['as'=>'companies_edit','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::put('companies-edit/{id}',['as'=>'companies_edit','middleware' => 'modules', 'uses'=>'CompaniesController@edit']);
        Route::delete('companies-delete/{id}',['as'=>'companies_delete','middleware' => 'modules', 'uses'=>'CompaniesController@delete']);
        //end gestion de usuarios

        //gestion de usuarios
        Route::group(['prefix' => 'sites', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'sites','uses'=>'HomeController@basePath']);
            Route::get('/form-create',['as'=>'sites_form_create','uses'=>'SitesController@formCreate']);
            Route::get('/form-edit',['as'=>'sites_form_edit','uses'=>'SitesController@formEdit']);
            Route::get('/all',['as'=>'sites_all','uses'=>'SitesController@all']);
            Route::get('/find/{id}',['as'=>'sites_find','uses'=>'SitesController@find']);
        });
        Route::get('sites-create',['as'=>'sites_create','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('sites-create',['as'=>'sites_create','middleware' => 'modules','uses'=>'SitesController@create']);
        Route::get('sites-edit/{id?}',['as'=>'sites_edit','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::put('sites-edit/{id}',['as'=>'sites_edit','middleware' => 'modules', 'uses'=>'SitesController@edit']);
        Route::delete('sites-delete/{id}',['as'=>'sites_delete','middleware' => 'modules', 'uses'=>'SitesController@delete']);
        //end gestion de usuarios

        //gestion de usuarios
        Route::group(['prefix' => 'employees', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'employees','uses'=>'HomeController@basePath']);
            Route::get('/form-create',['as'=>'employees_form_create','uses'=>'EmployeesController@formCreate']);
            Route::get('/form-edit',['as'=>'employees_form_edit','uses'=>'EmployeesController@formEdit']);
            Route::get('/all',['as'=>'employees_all','uses'=>'EmployeesController@all']);
            Route::get('/find/{id}',['as'=>'employees_find','uses'=>'EmployeesController@find']);
        });
        Route::get('employees-create',['as'=>'employees_create','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('employees-create',['as'=>'employees_create','middleware' => 'modules','uses'=>'EmployeesController@create']);
        Route::get('employees-edit/{id?}',['as'=>'employees_edit','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::put('employees-edit/{id}',['as'=>'employees_edit','middleware' => 'modules', 'uses'=>'EmployeesController@edit']);
        Route::delete('employees-delete/{id}',['as'=>'employees_delete','middleware' => 'modules', 'uses'=>'EmployeesController@delete']);
        //end gestion de usuarios

        //gestion de usuarios
        Route::group(['prefix' => 'enterprises', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'enterprises','uses'=>'HomeController@basePath']);
            Route::get('/form-create',['as'=>'enterprises_form_create','uses'=>'EnterprisesController@formCreate']);
            Route::get('/form-edit',['as'=>'enterprises_form_edit','uses'=>'EnterprisesController@formEdit']);
            Route::get('/all',['as'=>'enterprises_all','uses'=>'EnterprisesController@all']);
            Route::get('/find/{id}',['as'=>'enterprises_find','uses'=>'EnterprisesController@find']);
            Route::get('/file',['as'=>'enterprise_find','uses'=>'EnterprisesController@printFile']);
            Route::post('/upload-photo',['as'=>'enterprise_upload-photo','uses'=>'EnterprisesController@uploadPhoto']);
        });
        Route::get('enterprises-create',['as'=>'enterprises_create','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('enterprises-create',['as'=>'enterprises_create','middleware' => 'modules','uses'=>'EnterprisesController@create']);
        Route::get('enterprises-edit/{id?}',['as'=>'enterprises_edit','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::put('enterprises-edit/{id}',['as'=>'enterprises_edit','middleware' => 'modules', 'uses'=>'EnterprisesController@edit']);
        Route::delete('enterprises-delete/{id}',['as'=>'enterprises_delete','middleware' => 'modules', 'uses'=>'EnterprisesController@delete']);
        //end gestion de usuarios

        //gestion de usuarios
        Route::group(['prefix' => 'areas', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'areas','uses'=>'HomeController@basePath']);
            Route::get('/form-create',['as'=>'areas_form_create','uses'=>'AreasController@formCreate']);
            Route::get('/form-edit',['as'=>'areas_form_edit','uses'=>'AreasController@formEdit']);
            Route::get('/all',['as'=>'areas_all','uses'=>'AreasController@all']);
            Route::get('/find/{id}',['as'=>'areas_find','uses'=>'AreasController@find']);
        });
        Route::get('areas-create',['as'=>'areas_create','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('areas-create',['as'=>'areas_create','middleware' => 'modules','uses'=>'AreasController@create']);
        Route::get('areas-edit/{id?}',['as'=>'areas_edit','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::put('areas-edit/{id}',['as'=>'areas_edit','middleware' => 'modules', 'uses'=>'AreasController@edit']);
        Route::delete('areas-delete/{id}',['as'=>'areas_delete','middleware' => 'modules', 'uses'=>'AreasController@delete']);
        //end gestion de usuarios

        //gestion de usuarios
        Route::group(['prefix' => 'area-jobs', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'area_jobs','uses'=>'HomeController@basePath']);
            Route::get('/form-create',['as'=>'area_jobs_form_create','uses'=>'AreaJobsController@formCreate']);
            Route::get('/form-edit',['as'=>'area_jobs_form_edit','uses'=>'AreaJobsController@formEdit']);
            Route::get('/all',['as'=>'area_jobs_all','uses'=>'AreaJobsController@all']);
            Route::get('/find/{id}',['as'=>'area_jobs_find','uses'=>'AreaJobsController@find']);
        });
        Route::get('area-jobs-create',['as'=>'area_jobs_create','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('area-jobs-create',['as'=>'area_jobs_create','middleware' => 'modules','uses'=>'AreaJobsController@create']);
        Route::get('area-jobs-edit/{id?}',['as'=>'area_jobs_edit','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::put('area-jobs-edit/{id}',['as'=>'area_jobs_edit','middleware' => 'modules', 'uses'=>'AreaJobsController@edit']);
        Route::delete('area-jobs-delete/{id}',['as'=>'area_jobs_delete','middleware' => 'modules', 'uses'=>'AreaJobsController@delete']);
        //end gestion de usuarios


        //gestion de usuarios
        Route::group(['prefix' => 'customers', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'customers','uses'=>'HomeController@basePath']);
            Route::get('/form-create',['as'=>'customers_form_create','uses'=>'CustomersController@formCreate']);
            Route::get('/form-edit',['as'=>'customers_form_edit','uses'=>'CustomersController@formEdit']);
            Route::get('/all',['as'=>'customers_all','uses'=>'CustomersController@all']);
            Route::get('/find/{id}',['as'=>'customers_find','uses'=>'CustomersController@find']);
        });
        Route::get('black-list',['as'=>'customers_black-list','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('black-list/{id}',['as'=>'customers_black-list','middleware' => 'modules','uses'=>'CustomersController@setBlackList']);
        Route::post('black-list/d/{id}',['as'=>'customers_black-list','middleware' => 'modules','uses'=>'CustomersController@unSetBlackList']);
        Route::get('customers-create',['as'=>'customers_create','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('customers-create',['as'=>'customers_create','middleware' => 'modules','uses'=>'CustomersController@create']);
        Route::get('customers-edit/{id?}',['as'=>'customers_edit','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::put('customers-edit/{id}',['as'=>'customers_edit','middleware' => 'modules', 'uses'=>'CustomersController@edit']);
        Route::delete('customers-delete/{id}',['as'=>'customers_delete','middleware' => 'modules', 'uses'=>'CustomersController@delete']);
        //end gestion de usuarios

        //gestion de proveedores
        Route::group(['prefix' => 'suppliers', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'suppliers','uses'=>'HomeController@basePath']);
            Route::get('/form-create',['as'=>'suppliers_form_create','uses'=>'SuppliersController@formCreate']);
            Route::get('/form-edit',['as'=>'suppliers_form_edit','uses'=>'SuppliersController@formEdit']);
            Route::get('/all',['as'=>'suppliers_all','uses'=>'SuppliersController@all']);
            Route::get('/find/{id}',['as'=>'suppliers_find','uses'=>'SuppliersController@find']);
        });
        Route::get('suppliers-create',['as'=>'suppliers_create','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('suppliers-create',['as'=>'suppliers_create','middleware' => 'modules','uses'=>'SuppliersController@create']);
        Route::get('suppliers-edit/{id?}',['as'=>'suppliers_edit','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::put('suppliers-edit/{id}',['as'=>'suppliers_edit','middleware' => 'modules', 'uses'=>'SuppliersController@edit']);
        Route::delete('suppliers-delete/{id}',['as'=>'suppliers_delete','middleware' => 'modules', 'uses'=>'SuppliersController@delete']);
        //end gestion de proveedores

        //gestion de proveedores
        Route::group(['prefix' => 'suppliers', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'suppliers','uses'=>'HomeController@basePath']);
            Route::get('/form-create',['as'=>'suppliers_form_create','uses'=>'SuppliersController@formCreate']);
            Route::get('/form-edit',['as'=>'suppliers_form_edit','uses'=>'SuppliersController@formEdit']);
            Route::get('/all',['as'=>'suppliers_all','uses'=>'SuppliersController@all']);
            Route::get('/find/{id}',['as'=>'suppliers_find','uses'=>'SuppliersController@find']);
        });
        Route::get('suppliers-create',['as'=>'suppliers_create','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('suppliers-create',['as'=>'suppliers_create','middleware' => 'modules','uses'=>'SuppliersController@create']);
        Route::get('suppliers-edit/{id?}',['as'=>'suppliers_edit','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::put('suppliers-edit/{id}',['as'=>'suppliers_edit','middleware' => 'modules', 'uses'=>'SuppliersController@edit']);
        Route::delete('suppliers-delete/{id}',['as'=>'suppliers_delete','middleware' => 'modules', 'uses'=>'SuppliersController@delete']);
        //end gestion de proveedores

        //gestion de prestamos
        Route::group(['prefix' => 'loans', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'loans','uses'=>'HomeController@basePath']);
            Route::get('/index',['as'=>'loans_index','uses'=>'LoansController@index']);
            Route::get('/form-create',['as'=>'loans_form_create','uses'=>'LoansController@formCreate']);
            Route::get('/form-edit',['as'=>'loans_form_edit','uses'=>'LoansController@formEdit']);
            Route::get('/all',['as'=>'loans_all','uses'=>'LoansController@all']);
            Route::get('/find/{id}',['as'=>'loans_find','uses'=>'LoansController@find']);
            Route::get('/print-schedule',['as'=>'loans_print-schedule','uses'=>'LoansController@printSchedule']);
            Route::post('/annulled-loan',['as'=>'loans_annulled-loan','uses'=>'LoansController@annulled']);
            Route::post('/lose-loan',['as'=>'loans_lose-loan','uses'=>'LoansController@loseLoan']);
            Route::post('/warranty-loan',['as'=>'loans_warranty-loan','uses'=>'LoansController@warrantyLoan']);
            Route::post('/change-employee',['as'=>'loans_change-employee','uses'=>'LoansController@changeEmployee']);
            Route::post('/accept-loan',['as'=>'loans_accept-loan','uses'=>'LoansController@acceptLoan']);
        });
        Route::get('loans-create',['as'=>'loans_create','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('loans-create',['as'=>'loans_create','middleware' => 'modules','uses'=>'LoansController@create']);
        Route::get('loans-show/{id?}',['as'=>'loans_show','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::delete('loans-delete/{id}',['as'=>'loans_delete','middleware' => 'modules', 'uses'=>'LoansController@delete']);
        //end gestion de prestamos

        //caja
        Route::group(['prefix' => 'cash-desk-details', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'cash-desk-details','uses'=>'HomeController@basePath']);
            Route::get('/show',['as'=>'cash-desk-details-show','uses'=>'CashDeskDetailsController@show']);
            Route::get('/index',['as'=>'cash-desk-details-index','uses'=>'CashDeskDetailsController@index']);
            Route::post('/open',['as'=>'cash-desk-details-open','uses'=>'CashDeskDetailsController@open']);
            Route::get('/loan',['as'=>'cash-desk-details-loan','uses'=>'CashDeskDetailsController@loan']);
            Route::get('/get-pending-payments',['as'=>'cash-desk-details-get-pending-payments','uses'=>'CashDeskDetailsController@pendingPayments']);
            Route::get('/constancia',['as'=>'cash-desk-details-constancia','uses'=>'CashDeskDetailsController@constancia']);
            Route::get('/free-warranty',['as'=>'cash-desk-details-free-warranty','uses'=>'CashDeskDetailsController@freeWarranty']);
            Route::post('/disbursement',['as'=>'cash-desk-details-disbursement','uses'=>'CashDeskDetailsController@disbursement']);
            Route::post('/payF',['as'=>'cash-desk-details-payF','uses'=>'CashDeskDetailsController@payF']);
            Route::post('/payL',['as'=>'cash-desk-details-payL','uses'=>'CashDeskDetailsController@payL']);
            Route::post('/payE',['as'=>'cash-desk-details-payL','uses'=>'CashDeskDetailsController@payE']);
            Route::post('/payC',['as'=>'cash-desk-details-payC','uses'=>'CashDeskDetailsController@payC']);
            Route::get('/cancellation-total',['as'=>'cash-desk-details-cancellation-total','uses'=>'CashDeskDetailsController@reportTotalCancellation']);
            Route::get('/expenses',['as'=>'cash-desk-details-expenses','uses'=>'CashDeskDetailsController@expenses']);
            Route::get('/expenses-g',['as'=>'cash-desk-details-expenses','uses'=>'CashDeskDetailsController@expensesG']);
            Route::get('/incomes',['as'=>'cash-desk-details-incomes','uses'=>'CashDeskDetailsController@incomes']);
            Route::get('/deposits',['as'=>'cash-desk-details-deposits','uses'=>'CashDeskDetailsController@deposits']);
            Route::get('/disbursements',['as'=>'cash-desk-details-disbursements','uses'=>'CashDeskDetailsController@disbursements']);
            Route::get('/close',['as'=>'cash-desk-details-close','uses'=>'CashDeskDetailsController@close']);
            Route::get('/find',['as'=>'cash-desk-details-close','uses'=>'CashDeskDetailsController@find']);
            Route::post('/close',['as'=>'cash-desk-details-close','uses'=>'CashDeskDetailsController@closeCashDesk']);
            Route::post('/accept-consignment',['as'=>'cash-desk-details-close','uses'=>'CashDeskDetailsController@acceptConsignment']);

            Route::post('/expense-create',['as'=>'cash-desk-details-expenses','uses'=>'CashDeskDetailsController@expenseCreate']);
            Route::post('/income-create',['as'=>'cash-desk-details-incomes','uses'=>'CashDeskDetailsController@incomeCreate']);
            Route::put('/expense-edit',['as'=>'cash-desk-details-expenses','uses'=>'CashDeskDetailsController@expenseEdit']);
            Route::put('/income-edit',['as'=>'cash-desk-details-incomes','uses'=>'CashDeskDetailsController@incomeEdit']);
            Route::delete('/expense-delete/{id}',['as'=>'cash-desk-details-expenses','uses'=>'CashDeskDetailsController@expenseDelete']);
            Route::delete('/income-delete/{id}',['as'=>'cash-desk-details-incomes','uses'=>'CashDeskDetailsController@incomeDelete']);

            Route::post('/re-print-disbursement',['as'=>'cash-desk-details-re-print-disbursement','uses'=>'CashDeskDetailsController@rePrintDisbursement']);
            Route::post('/re-print-deposit',['as'=>'cash-desk-details-re-print-deposit','uses'=>'CashDeskDetailsController@rePrint']);
            Route::get('/re-print-deposit2',['as'=>'cash-desk-details-re-print-deposit','uses'=>'CashDeskDetailsController@rePrintX']);
            Route::post('/annulled-deposit',['as'=>'cash-desk-details-annulled-deposit','uses'=>'CashDeskDetailsController@annulledPayment']);
            Route::post('/create-loan-expense',['as'=>'create-loan-expense','uses'=>'CashDeskDetailsController@createLoanExpense']);

            Route::get('/expenses-view',['as'=>'cash-desk-details-expenses-view','uses'=>'CashDeskDetailsController@expensesView']);
            Route::get('/expenses-g-view',['as'=>'cash-desk-details-expenses-view','uses'=>'CashDeskDetailsController@expensesGView']);
            Route::get('/disbursements-view',['as'=>'cash-desk-details-disbursements-view','uses'=>'CashDeskDetailsController@disbursementsView']);
            Route::get('/incomes-view',['as'=>'cash-desk-details-incomes-view','uses'=>'CashDeskDetailsController@incomesView']);
            Route::get('/deposits-view',['as'=>'cash-desk-details-deposits-view','uses'=>'CashDeskDetailsController@depositsView']);
            Route::get('/close-view',['as'=>'cash-desk-details-close-view','uses'=>'CashDeskDetailsController@closeView']);

        });

        Route::get('cash-desk-details-expenses',['as'=>'cash-desk-details-expenses','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::get('cash-desk-details-expenses-g',['as'=>'cash-desk-details-expenses-g','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::get('cash-desk-details-incomes',['as'=>'cash-desk-details-incomes','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::get('cash-desk-details-disbursements',['as'=>'cash-desk-details-disbursement','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::get('cash-desk-details-deposits',['as'=>'cash-desk-details-deposits','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::get('cash-desk-details-close',['as'=>'cash-desk-details-close','middleware' => 'modules','uses'=>'HomeController@basePath']);
        //end caja

        //mensajes de texto
        Route::group(['prefix' => 'sent-messages', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'sent-messages','uses'=>'HomeController@basePath']);
            Route::get('/form-create',['as'=>'sent-messages_form_create','uses'=>'SentMessagesController@formCreate']);
            Route::get('/all',['as'=>'sent-messages_all','uses'=>'SentMessagesController@all']);
            Route::get('/find/{id}',['as'=>'sent-messages_find','uses'=>'SentMessagesController@find']);
        });

        Route::get('sent-messages-create',['as'=>'profiles_create','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('sent-messages-create',['as'=>'profiles_create','middleware' => 'modules','uses'=>'SentMessagesController@create']);
        //mensajes de texto

        //negocios
        Route::group(['prefix' => 'business', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'business','uses'=>'HomeController@basePath']);
            Route::get('/index',['as'=>'business_index','uses'=>'BusinessController@index']);
            Route::get('/loan/{id}',['as'=>'loans_find','uses'=>'LoansController@find']);
            Route::get('/print-schedule',['as'=>'loans_find','uses'=>'BusinessController@printSchedule']);
            Route::post('/print-simulated-schedule',['as'=>'loans_find','uses'=>'BusinessController@printSimulated']);
            Route::get('/print-simulated-schedule',['as'=>'loans_find','uses'=>'BusinessController@printSimulated']);
            Route::get('/enterprise-loans',['as'=>'enterprise-loans','uses'=>'BusinessController@loanEnterprise']);
            Route::get('/person-info',['as'=>'person-info','uses'=>'PeopleController@printFile']);
            Route::get('/billing',['as'=>'person-billing','uses'=>'BusinessController@billing']);
            Route::get('/print-customer',['as'=>'print-customer','uses'=>'BusinessController@printCustomers']);
            Route::get('/comissions',['as'=>'comissions','middleware' => 'modules','uses'=>'BusinessController@commission']);
        });

        Route::get('visits',['as'=>'comissions','uses'=>'BusinessController@payments']);
        Route::get('/print-historial',['as'=>'loans_find','uses'=>'BusinessController@printHistorial']);
        Route::get('/print-comments',['as'=>'loans_find','uses'=>'BusinessController@printComments']);

        //end negocios


        //reportes
        Route::group(['prefix' => 'reports', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'business','uses'=>'HomeController@basePath']);
            Route::get('/get-data',['as'=>'business_index','uses'=>'ReportsController@generalReport']);
            Route::get('/index',['as'=>'index','uses'=>'ReportsController@index']);
            Route::get('/print-desembolsos',['as'=>'reports-print-cancelados','uses'=>'ReportsController@printDesembolsos']);
            Route::get('/print-cancelados',['as'=>'reports-print-cancelados','uses'=>'ReportsController@printCancelados']);
            Route::get('/print-anulados',['as'=>'reports-print-anulados','uses'=>'ReportsController@printAnulados']);
            Route::get('/print-canceladas',['as'=>'reports-print-canceladas','uses'=>'ReportsController@printCanceladas']);
            Route::get('/print-canceladas-sunat',['as'=>'reports-print-canceladas-sunat','uses'=>'ReportsController@printCanceladasSunat']);
            Route::get('/print-expenses',['as'=>'reports-print-expenses','uses'=>'ReportsController@printExpenses']);
            Route::get('/print-expenses-g',['as'=>'reports-print-expenses','uses'=>'ReportsController@printExpensesG']);
            Route::get('/print-incomes',['as'=>'reports-print-incomes','uses'=>'ReportsController@printIncomes']);
            Route::get('/print-customers',['as'=>'reports-print-customers','uses'=>'ReportsController@printCustomers']);
            Route::get('/print-perdida',['as'=>'reports-print-perdida','uses'=>'ReportsController@printPerdida']);
            Route::get('/print-garantia-v',['as'=>'reports-print-garantia-v','uses'=>'ReportsController@printGarantiaV']);
            Route::get('/print-perdida-cancelado',['as'=>'reports-print-perdida-cancelado','uses'=>'ReportsController@printPerdidaCancelado']);
            Route::get('/print-garantia-ejecutada',['as'=>'reports-print-garantia-ejecutada','uses'=>'ReportsController@printGarantiaEjecutada']);
            Route::get('/print-grantia-ejecutada-cancelado',['as'=>'reports-print-grantia-ejecutada-cancelado','uses'=>'ReportsController@printGarantiaCancelado']);
            Route::get('/print-atrasadas',['as'=>'reports-print-atrasadas','uses'=>'ReportsController@printAtrasadas']);
            Route::get('/print-vigentes',['as'=>'reports-print-vigentes','uses'=>'ReportsController@printVigentes']);
        });
        //reportes

        //gestion de solicitud de prestamo
        Route::group(['prefix' => 'request-loans', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'suppliers','uses'=>'HomeController@basePath']);
            Route::get('/form-create',['as'=>'suppliers_form_create','uses'=>'RequestLoansController@formCreate']);
            Route::get('/form-edit',['as'=>'suppliers_form_edit','uses'=>'RequestLoansController@formEdit']);
            Route::get('/all',['as'=>'suppliers_all','uses'=>'RequestLoansController@all']);
            Route::get('/find/{id}',['as'=>'suppliers_find','uses'=>'RequestLoansController@find']);
            Route::get('/show/{id}',['as'=>'loans_find','uses'=>'RequestLoansController@show']);
        });
        Route::get('request-loans-create',['as'=>'suppliers_create','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('request-loans-create',['as'=>'suppliers_create','middleware' => 'modules','uses'=>'RequestLoansController@create']);
        Route::get('request-loans-edit/{id?}',['as'=>'suppliers_edit','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::put('request-loans-edit/{id}',['as'=>'suppliers_edit','middleware' => 'modules', 'uses'=>'RequestLoansController@edit']);
        Route::delete('request-loans-delete/{id}',['as'=>'suppliers_delete','middleware' => 'modules', 'uses'=>'RequestLoansController@delete']);
        //end gestion de solicitud de prestamo

        //planilla
        Route::group(['prefix' => 'payroll-salaries', 'middleware' => 'modules'], function()
        {
            Route::get('/',['as'=>'payroll-salaries','uses'=>'HomeController@basePath']);
            Route::get('/form-create',['as'=>'payroll-salaries_form_create','uses'=>'PayrollSalariesController@formCreate']);
            Route::get('/form-pay',['as'=>'payroll-salaries_form_pay','uses'=>'PayrollSalariesController@formPay']);
            Route::get('/form-discount',['as'=>'payroll-salaries_form_discount','uses'=>'PayrollSalariesController@formDiscount']);
            Route::get('/all',['as'=>'payroll-salaries_all','uses'=>'PayrollSalariesController@all']);
            Route::get('/discounts',['as'=>'payroll-salaries_discounts','uses'=>'PayrollSalariesController@getDiscounts']);
            Route::get('/employe-discounts',['as'=>'payroll-salaries_discounts','uses'=>'PayrollSalariesController@getEmployeeDiscount']);
            Route::get('/find/{id}',['as'=>'payroll-salaries_find','uses'=>'PayrollSalariesController@find']);
            Route::get('/show/{id}',['as'=>'payroll-salaries_show','uses'=>'PayrollSalariesController@show']);
            Route::post('/pay-employee',['as'=>'payroll-salaries_pay-employee','uses'=>'PayrollSalariesController@payEmployee']);
            Route::post('/close',['as'=>'payroll-salaries_close','uses'=>'PayrollSalariesController@closePayrollSalary']);
        });

        Route::get('payroll-salaries-create',['as'=>'payroll-salaries_create','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::get('payroll-salaries-pay/{id?}',['as'=>'payroll-salaries_pay','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::get('payroll-salaries-discount',['as'=>'payroll-salaries_discount','middleware' => 'modules','uses'=>'HomeController@basePath']);
        Route::post('payroll-salaries-discount',['as'=>'payroll-salaries_discount','middleware' => 'modules','uses'=>'PayrollSalariesController@createDiscount']);
        Route::post('payroll-salaries-create',['as'=>'payroll-salaries_create','middleware' => 'modules','uses'=>'PayrollSalariesController@create']);
        Route::get('payroll-salaries-edit/{id?}',['as'=>'payroll-salaries_edit','middleware' => 'modules','uses'=>'HomeController@basePath']);
        //end


        //api
        Route::group(['prefix' => 'api'], function()
        {
            Route::get('/departments',['as'=>'api_departments', 'uses'=>'ApiController@departments']);
            Route::get('/people',['as'=>'api_persons', 'uses'=>'ApiController@people']);
            Route::get('/users',['as'=>'api_users', 'uses'=>'ApiController@users']);
            Route::get('/profiles',['as'=>'api_profiles', 'uses'=>'ApiController@profiles']);
            Route::get('/enterprises',['as'=>'api_enterprises', 'uses'=>'ApiController@enterprises']);
            Route::get('/customers',['as'=>'api_customers', 'uses'=>'ApiController@customers']);
            Route::get('/areas',['as'=>'api_areas', 'uses'=>'ApiController@areas']);
            Route::get('/jobs',['as'=>'api_jobs', 'uses'=>'ApiController@jobs']);
            Route::get('/employees',['as'=>'api_employees', 'uses'=>'ApiController@employees']);
            Route::get('/companies',['as'=>'api_companies', 'uses'=>'ApiController@companies']);
            Route::get('/sites',['as'=>'api_sites', 'uses'=>'ApiController@sites']);
            Route::get('/suppliers',['as'=>'api_suppliers', 'uses'=>'ApiController@suppliers']);
            Route::get('/modules',['as'=>'api_modules', 'uses'=>'ApiController@modules']);
            Route::get('/loans',['as'=>'api_loans', 'uses'=>'ApiController@loans']);
            Route::get('/years',['as'=>'api_years', 'uses'=>'ApiController@years']);
            Route::get('/months',['as'=>'api_months', 'uses'=>'ApiController@months']);

            Route::get('/reniec',['as'=>'api_reniec', 'uses'=>'ApiController@reniec']);
            Route::get('/sunat',['as'=>'api_reniec', 'uses'=>'ApiController@sunat']);

            Route::post('/generate-schedule',['as'=>'api_departments', 'uses'=>'ApiController@generateSchedule']);
        });

        //end api

    });

});

Route::get('login', ['as' => 'login', 'uses' => 'HomeController@basePath']);
Route::post('login', ['as' => 'login', 'uses' => 'AuthController@login']);
Route::get('logout', ['as' => 'login', 'uses' => 'AuthController@logout']);

Route::get('/edit-payment',['as'=>'edit-payment','uses'=>'LoansController@editPayment']);

Route::get('/consolidate-report',['as'=>'consolidate-report','uses'=>'ReportsController@consolidatePaymentsReport']);
Route::get('/disbursements-report',['as'=>'disbursements-report','uses'=>'ReportsController@disbursements']);
Route::get('/cash-desk-detail-report',['as'=>'cash-desk-detail-report','uses'=>'ReportsController@cashDeskDetails']);
Route::get('/cash-desk-detail-report/{id}',['as'=>'cash-desk-detail-report-show','uses'=>'ReportsController@cashDeskDetailReport']);

Route::get('/loans-report',['as'=>'loans-report','uses'=>'LoansController@loanReport']);

Route::post('/loans-lose',['as'=>'loans-lose','uses'=>'LoansController@toLose']);
Route::post('/loans-ok',['as'=>'loans-ok','uses'=>'LoansController@toOkLoan']);
Route::post('/loans-lose-total',['as'=>'loans-lose-total','uses'=>'LoansController@toLoseTotal']);

Route::get('/wall/index',['as'=>'wall-index','uses'=>'WallController@index']);
Route::get('/wall-all',['as'=>'wall-all','uses'=>'WallController@all']);
Route::post('/expenses',['as'=>'expenses','uses'=>'WallController@storeExpense']);
Route::post('/incomes',['as'=>'incomes','uses'=>'WallController@storeIncome']);
Route::delete('/expenses/{id}',['as'=>'delete-expense','uses'=>'WallController@annulledExpense']);
Route::delete('/incomes/{id}',['as'=>'delete-income','uses'=>'WallController@annulledIncome']);


