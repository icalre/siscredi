<?php

/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;


use App\Account;
use App\AccountDetail;
use App\Core\Components\MainMenu;
use App\Core\Entities\CashDesk;
use App\Core\Entities\CashDeskDetail;
use App\Core\Entities\Loan;
use App\Core\Entities\LoanComment;
use App\Core\Entities\LoanExpense;
use App\Core\Repositories\CashDeskDetailRepo;
use App\Core\Managers\CashDeskDetailManager;
use App\Core\Repositories\CashDeskRepo;
use App\Core\Repositories\PaymentRepo;
use App\Core\Repositories\PaymentDocumentRepo;
use App\Core\Managers\PaymentDocumentManager;
use App\Core\Repositories\PaymentDocumentDetailRepo;
use App\Core\Managers\PaymentDocumentDetailManager;
use App\Core\Repositories\LoanRepo;
use App\Core\Managers\PaymentManager;
use App\Core\Repositories\CashDeskExpenseRepo;
use App\Core\Managers\CashDeskExpenseManager;
use App\Core\Managers\CashDeskIncomeManager;
use App\Core\Repositories\CashDeskIncomeRepo;
use App\Core\Managers\DisbursementManager;
use App\Core\Repositories\DisbursementRepo;
use App\Core\Repositories\AditionalPaymentRepo;
use App\Core\Managers\AditionalPaymentManager;
use App\Core\Repositories\CustomerRepo;

use App\Core\Repositories\PersonRepo;
use App\Core\Repositories\SalaryDiscountRepo;
use App\Core\Repositories\UserRepo;
use Carbon\Carbon;
use App\Core\Services\PrintService;
use App\Core\Managers\SalaryDiscountManager;
use Illuminate\Support\Facades\DB;

class CashDeskDetailsController extends Controller
{

    protected $cashDeskRepo;
    protected $cashDeskDetailRepo;
    protected $paymentRepo;
    protected $paymentDocumentRepo;
    protected $paymentDocumentDetailRepo;
    protected $loanRepo;
    protected $expenseRepo;
    protected $incomeRepo;
    protected $disbursementRepo;
    protected $customerRepo;
    protected $personRepo;
    protected $printService;

    public function __construct(
        CashDeskRepo $cashDeskRepo,
        CashDeskDetailRepo $cashDeskDetailRepo,
        PaymentRepo $paymentRepo,
        PaymentDocumentRepo $paymentDocumentRepo,
        PaymentDocumentDetailRepo $paymentDocumentDetailRepo,
        LoanRepo $loanRepo,
        CashDeskExpenseRepo $expenseRepo,
        CashDeskIncomeRepo $incomeRepo,
        DisbursementRepo $disbursementRepo,
        CustomerRepo $customerRepo,
        PersonRepo $personRepo,
        PrintService $printService
    ) {

        $this->cashDeskRepo = $cashDeskRepo;
        $this->cashDeskDetailRepo = $cashDeskDetailRepo;
        $this->paymentRepo = $paymentRepo;
        $this->paymentDocumentRepo = $paymentDocumentRepo;
        $this->paymentDocumentDetailRepo = $paymentDocumentDetailRepo;
        $this->loanRepo = $loanRepo;
        $this->expenseRepo = $expenseRepo;
        $this->incomeRepo = $incomeRepo;
        $this->disbursementRepo = $disbursementRepo;
        $this->customerRepo = $customerRepo;
        $this->personRepo = $personRepo;
        $this->printService = $printService;
    }

    public function index()
    {
        $cash_desk = CashDesk::find(1);
        $accounts = Account::all();

        return \View::make('cash-desk-details.index', compact('cash_desk', 'accounts'));
    }

    public function open()
    {
        $cash_desk = CashDesk::find(1);
        $cashDeskDetail = $this->cashDeskDetailRepo->getModel();
        $data = \Request::all();
        $data['cash_desk_id'] = $cash_desk->id;
        $data['user_id'] = \Auth::user()->id;
        $data['state'] = 1;
        $data['date_start'] = date('Y-m-d H: i:s');
        $manager = new CashDeskDetailManager($cashDeskDetail, $data);
        $manager->save();

        $cash_desk->state = 1;
        $cash_desk->save();

        $accounts = Account::all();

        foreach ($accounts as $account) {
            AccountDetail::create([
                'account_id' => $account->id,
                'cash_desk_detail_id' => $cashDeskDetail->id,
                'initial_amount' => $account->amount,
                'final_amount' => 0
            ]);
        }

        return \Response::json(['response' => true, 'data' => $cash_desk]);
    }

    public function loan()
    {
        $code = \Request::get('code');
        $date = Carbon::createFromFormat('Y-m-d', date('Y-m-d'));
        $loan = $this->loanRepo->find($code);
        $fee = ($loan->interest) / 30;
        $fee = $fee / 100;


        $capital = $loan->amount - round($this->paymentRepo->sumCapital($loan->id), 2);
        $loan->capital_amount = number_format($capital, 2, '.', '');
        $payments = array();

        if ($loan->type_payment == 'Libre' && ($loan->state == 'Vigente' || $loan->state == 'Atrasado')) {
            if (count($loan->payments) == 1) {
                $first_payment = true;
            } else {
                $first_payment = false;
            }


            foreach ($loan->payments as $payment) {
                if ($payment->state == 'Pendiente') {
                    if ($payment->expiration != date('d-m-Y')) {
                        if ($first_payment) {
                            $date_ini = Carbon::createFromFormat('d-m-Y', $loan->date_disbursement);
                        } else {
                            $date_ini = Carbon::createFromFormat('d-m-Y', $payment->expiration)->subMonth();
                        }
                        $days_dif = $date_ini->diffInDays();
                        $interest = $payment->capital * $fee * $days_dif;
                        $payment->interest = round($interest, 2);
                        $payment->amount = round($interest, 2) + $payment->capital;
                    }
                    $payments[] = $payment;
                }
            }
        } elseif ($loan->state == 'Perdida' or $loan->state == 'Garantía Ejecutada') {

            $payments = $loan->payments()->where('state', 'Pendiente')->get();

            $date_ini = Carbon::createFromFormat('d-m-Y', $payments[0]->expiration);

            $days_dif = $date_ini->diffInDays();
            $interest = $payments[0]->capital * $fee * $days_dif;
            $payments[0]->interest = $payments[0]->interest + round($interest, 2);
            $payments[0]->amount = $payments[0]->interest + $payments[0]->capital;
            $payments[0]->pre_interest = $payments[0]->interest;
            $payments[0]->pre_capital = $payments[0]->capital;
            $payments[0]->select = 1;
        }

        $loan->payments = $payments;

        return \Response::json(['response' => true, 'loan' => $loan, 'payments' => $payments, 'capital' => $capital]);
    }

    public function pendingPayments()
    {
        $data = \Request::all();

        $payments = $this->loanRepo->find($data['code']);

        foreach ($payments->payments as $payment) {
            if ($payments->period == 'Mensual') {
                $date_ini = Carbon::createFromFormat('d-m-Y', $payment->expiration)->subMonth();
                $days_dif = $date_ini->diffInDays(Carbon::now(), false);

                if ($days_dif > 1 && $payment->state == 'Pendiente') {
                    return \Response::json(['response' => false]);
                }
            }

            if ($payments->period == 'Semanal') {
                $date_ini = Carbon::createFromFormat('d-m-Y', $payment->expiration)->subDays(7);
                $days_dif = $date_ini->diffInDays(Carbon::now(), false);

                if ($days_dif > 1 && $payment->state == 'Pendiente') {
                    return \Response::json(['response' => false]);
                }
            }
        }

        return \Response::json(['response' => true, 'loan' => $payments]);
    }


    public function disbursement(MainMenu $mainMenu)
    {

        DB::beginTransaction();
        try{
            $disbursement = $this->disbursementRepo->getModel();
            $data = \Request::all();
            $data['account_id'] = $data['type_payment'];
            $loan = $this->loanRepo->find($data['loan_id']);


            if ($loan->state != 'Aprobado') {
                return \Response::json(['response' => false]);
            }

            $cash_desk_detail = $this->cashDeskDetailRepo->lastDetail();

            if ($cash_desk_detail->state > 0) {
                $data['cash_desk_detail_id'] = $cash_desk_detail->id;
            }


            $manager = new DisbursementManager($disbursement, $data);
            $manager->save();
            $loan->state = 'Vigente';
            $loan->save();

            $date = Carbon::now()->format('d/m/Y H:i:s');

            $this->updateAmountAccount($data['account_id'], $data['amount'] * -1);

            DB::commit();

            return \Response::json(['response' => true, 'data' => compact('date', 'disbursement', 'loan'), 'action' => 'desembolso']);
        }catch (\Exception $exception){
            DB::rollback();
            return \Response::json(['response' => false]);
        }
    }

    public function payF(MainMenu $mainMenu)
    {
        DB::beginTransaction();

        try{
            $data = \Request::all();
            $menu = $mainMenu->menu(\Auth::user()->profile_id);
            $sub_modules = $menu['sub_modules'];
            $host = explode(':', \Request::server("HTTP_HOST"));

            $cash_desk_detail = $this->cashDeskDetailRepo->lastDetail();

            if ($cash_desk_detail->state > 0) {
                $data['cash_desk_detail_id'] = $cash_desk_detail->id;
            }


            foreach ($data['paymentDocumentDetails'] as $item) {
                $payment = $this->paymentRepo->find($item['id']);
                if ($payment->state == 'Pagado') {
                    return \Response::json(['response' => false, 'message' => 'Cuota Pagada']);
                }
            }

            $paymentDocument = $this->paymentDocumentRepo->getModel();
            $paymentData = [
                'amount' => $data['amount'],
                'cash_desk_detail_id' => $data['cash_desk_detail_id'],
                'user_id' => \Auth::user()->id,
                'customer_id' => $data['customer_id'],
                'document_type_id' => 4,
                'account_id' => $data['type_payment']
            ];
            $manager = new PaymentDocumentManager($paymentDocument, $paymentData);
            $manager->save();

            $flag_expense = 0;
            $expenses_amount = 0;
            $payment_id = 0;
            foreach ($data['paymentDocumentDetails'] as $item) {
                $payment = $this->paymentRepo->find($item['id']);
                $loan_id = $item['loan_id'];
                $payment->state = 'Pagado';
                if ($item['pay_mora']) {
                    $payment->amount = $item['amount'];
                    $payment->mora = $item['mora'];
                }

                if ($data['expense_amount'] > 0 && $flag_expense == 0) {
                    $payment->amount = $payment->amount + $data['expense_amount'];
                    $payment->expenses = $data['expense_amount'];
                    $expenses_amount = $data['expense_amount'];
                    $payment_id = $payment->id;
                }

                $payment->date_pay = date('Y-m-d H:i:s');
                $payment->account_id = $data['type_payment'];
                $payment->save();
                $paymentDocumentDetail = $this->paymentDocumentDetailRepo->getModel();
                $item_data = [
                    'payment_document_id' => $paymentDocument->id,
                    'payment_id' => $item['id'],
                    'amount' => $payment->amount
                ];
                $item_manager = new PaymentDocumentDetailManager($paymentDocumentDetail, $item_data);
                $item_manager->save();

                $flag_expense++;
            }

            $loan = $this->loanRepo->find($loan_id);
            $capital = round($loan->amount, 0) - round($this->paymentRepo->sumCapital($loan->id), 0);

            if ($data['expense_amount'] > 0) {
                $expenses = $loan->expenses;

                foreach ($expenses as $expense) {
                    $expense->state = 'Pagado';
                    $expense->payment_id = $payment_id;
                    $expense->payment_document_id = $paymentDocument->id;
                    $expense->save();
                }
            }

            if ($capital <= 0) {
                $loan->state = 'Cancelado';
                $loan->date_end = date('Y-m-d');
                $loan->save();
            }

            $loan = $this->paymentRepo->getPendingPayments($loan->code);

            $paymentD = $this->paymentDocumentRepo->find($paymentDocument->id);
            $paymentD->capital = $capital;
            $paymentD->save();

            $this->updateAmountAccount($data['type_payment'], $data['amount']);

            $date = Carbon::now()->format('d/m/Y H:i:s');

            DB::commit();
            return \Response::json(['response' => true, 'data' => compact('date', 'paymentD', 'loan', 'expenses_amount'), 'action' => 'fija']);

        }catch (\Exception $exception){
            DB::rollback();
            return \Response::json(['response' => false]);
        }
    }


    public function payL(MainMenu $mainMenu)
    {
        DB::beginTransaction();
        try{
            $data = \Request::all();
            $cash_desk_detail = $this->cashDeskDetailRepo->lastDetail();

            if ($cash_desk_detail->state > 0) {
                $data['cash_desk_detail_id'] = $cash_desk_detail->id;
            }

            $payment = $this->paymentRepo->find($data['payment']['id']);


            if ($payment->state == 'Pagado') {
                return \Response::json(['response' => false]);
            }


            $paymentDocument = $this->paymentDocumentRepo->getModel();
            $paymentData = [
                'amount' => $data['amount'],
                'cash_desk_detail_id' => $data['cash_desk_detail_id'],
                'user_id' => \Auth::user()->id,
                'customer_id' => $data['customer_id'],
                'document_type_id' => 4,
                'account_id' => $data['type_payment']
            ];

            $manager = new PaymentDocumentManager($paymentDocument, $paymentData);

            $manager->save();

            $loan_id = $data['payment']['loan_id'];
            $payment->interest = $data['payment']['interest'];;
            $payment->capital = $data['payment']['capital'];
            $payment->amount = $data['payment']['amount'];
            $payment->date_pay = date('Y-m-d H:i:s');
            $payment->state = 'Pagado';
            $payment->account_id = $data['type_payment'];

            $expenses_amount = null;

            if ($data['expense_amount'] > 0) {
                $payment->amount = $payment->amount + $data['expense_amount'];
                $payment->expenses = $data['expense_amount'];
                $expenses_amount = $data['expense_amount'];
                $payment_id = $payment->id;
            }

            $payment->save();
            $paymentDocumentDetail = $this->paymentDocumentDetailRepo->getModel();
            $item_data = [
                'payment_document_id' => $paymentDocument->id,
                'payment_id' => $data['payment']['id'],
                'amount' => $payment->amount
            ];
            $item_manager = new PaymentDocumentDetailManager($paymentDocumentDetail, $item_data);
            $item_manager->save();

            $loan = $this->loanRepo->find($loan_id);
            $capital = round($loan->amount, 2) - round($this->paymentRepo->sumCapital($loan->id), 2);


            if ($data['expense_amount'] > 0) {
                $expenses = $loan->expenses;

                foreach ($expenses as $expense) {
                    $expense->state = 'Pagado';
                    $expense->payment_id = $payment_id;
                    $expense->payment_document_id = $paymentDocument->id;
                    $expense->save();
                }
            }


            $new_payment = null;

            if ($capital <= 0) {
                $loan->state = 'Cancelado';
                $loan->date_end = date('Y-m-d');
                $loan->save();
            } else {
                $date = Carbon::createFromFormat('Y-m-d H:i:s', $payment->date_pay);
                $fee = $capital * ($loan->interest) / 100;
                $new_payment = $this->paymentRepo->getModel();
                if ($loan->period == 'Mensual') {
                    $vcto = $date->addMonth(1)->format('d-m-Y');
                } elseif ($data->period == 'Semanal') {
                    $vcto = $date->addDays(7)->format('d-m-Y');
                }

                $payment_manager = new PaymentManager($new_payment, [
                    'loan_id' => $loan->id,
                    'amount' => $fee + $capital,
                    'interest' => $fee,
                    'capital' => $capital,
                    'expiration' => $vcto,
                    'state' => 'Pendiente'
                ]);
                $payment_manager->save();
            }

            $paymentD = $this->paymentDocumentRepo->find($paymentDocument->id);
            $paymentD->capital = $capital;
            $paymentD->save();
            $customer = $loan->customer->customer;

            $date = Carbon::now()->format('d/m/Y H:i:s');

            $this->updateAmountAccount($data['type_payment'], $data['amount']);

            DB::commit();

            return \Response::json(['response' => true, 'data' => compact('date', 'paymentD', 'new_payment', 'loan', 'capital', 'expenses_amount'), 'action' => 'libre']);
        }catch (\Exception $exception){
            DB::rollback();
            return \Response::json(['response' => false]);
        }
    }

    public function payE(MainMenu $mainMenu)
    {
        DB::beginTransaction();
        try{
            $data = \Request::all();

            $menu = $mainMenu->menu(\Auth::user()->profile_id);
            $sub_modules = $menu['sub_modules'];
            $host = explode(':', \Request::server("HTTP_HOST"));

            $cash_desk = $this->cashDeskDetailRepo->getDetail(1);

            $cash_desk_detail = $this->cashDeskDetailRepo->lastDetail();

            if ($cash_desk_detail->state > 0) {
                $data['cash_desk_detail_id'] = $cash_desk_detail->id;
            }

            foreach ($data['paymentDocumentDetails'] as $item) {
                $payment = $this->paymentRepo->find($item['id']);
                if ($payment->state == 'Pagado') {
                    return \Response::json(['response' => false]);
                }
            }

            $paymentDocument = $this->paymentDocumentRepo->getModel();
            $paymentData = [
                'amount' => $data['amount'],
                'cash_desk_detail_id' => $data['cash_desk_detail_id'],
                'user_id' => \Auth::user()->id,
                'customer_id' => $data['customer_id'],
                'document_type_id' => 4,
                'account_id' => $data['type_payment']
            ];
            $manager = new PaymentDocumentManager($paymentDocument, $paymentData);
            $manager->save();
            $i = 0;
            $payment = '';

            foreach ($data['paymentDocumentDetails'] as $item) {
                $payment = $this->paymentRepo->find($item['id']);
                $loan_id = $item['loan_id'];
                $payment->interest = 0;
                $payment->amount = $payment->capital;
                $payment->state = 'Pagado';
                $payment->date_pay = date('Y-m-d H:i:s');
                $payment->account_id = $data['type_payment'];
                $payment->save();
                $paymentDocumentDetail = $this->paymentDocumentDetailRepo->getModel();
                $date_ini = Carbon::createFromFormat('d-m-Y', $payment->expiration)->subMonth();
                $days_dif = $date_ini->diffInDays(Carbon::now(), false);
                if ($i == 0 && $days_dif > 1) {
                    $item_data = [
                        'payment_document_id' => $paymentDocument->id,
                        'payment_id' => $item['id'],
                        'amount' => $item['amount']
                    ];
                } else {
                    $item_data = [
                        'payment_document_id' => $paymentDocument->id,
                        'payment_id' => $item['id'],
                        'amount' => $item['capital']
                    ];
                }

                $item_manager = new PaymentDocumentDetailManager($paymentDocumentDetail, $item_data);
                $item_manager->save();
                $i++;
            }
            $loan = $this->loanRepo->find($loan_id);
            $capital = round($loan->amount, 0) - round($this->paymentRepo->sumCapital($loan->id), 0);

            if ($capital <= 0) {
                $loan->state = 'Cancelado';
                $loan->date_end = date('Y-m-d');
                $loan->save();
            } else {
                $loan = $this->paymentRepo->getPendingPayments($loan->code);
                $loan_day = Carbon::createFromFormat('d-m-Y', $loan->date_start);
                $date = date('Y-m-') . $loan_day->format('d');

                $date = Carbon::createFromFormat('Y-m-d', $date);
                $nro_quotas = count($data['paymentDocumentDetails']);

                foreach ($loan->payments as $item) {
                    if ($loan->period == 'Mensual') {
                        $day = $date->dayOfWeek;
                        $item->expiration = $date->addMonth(1)->format('d-m-Y');

                        if ($day == 0) {
                            $new_date = Carbon::createFromFormat('d-m-Y', $item->expiration);
                            $vcto = $new_date->addDays(1)->format('d-m-Y');
                            $item->expiration = $vcto;
                        }
                    } elseif ($loan->period == 'Semanal') {
                        $new_expiration = Carbon::createFromFormat('d-m-Y', $item->expiration);
                        $item->expiration = $new_expiration->subDays(7 * $nro_quotas)->format('d-m-Y');
                    }

                    $item->save();
                }
            }

            $loan = $this->paymentRepo->getPendingPayments($loan->code);
            $paymentD = $this->paymentDocumentRepo->find($paymentDocument->id);
            $paymentD->capital = $capital;
            $paymentD->save();


            $this->updateAmountAccount($data['type_payment'], $data['amount']);

            DB::commit();
            $date = Carbon::now()->format('d/m/Y H:i:s');
            return \Response::json(['response' => true, 'data' => compact('date', 'paymentD', 'loan'), 'action' => 'fija']);
        }catch (\Exception $exception){
            DB::rollback();
            eturn \Response::json(['response' => false]);
        }


    }

    public function constancia(MainMenu $mainMenu)
    {
        $customer_id = \Request::get('customer-id');
        $menu = $mainMenu->menu(\Auth::user()->profile_id);
        $sub_modules = $menu['sub_modules'];
        $host = explode(':', \Request::server("HTTP_HOST"));

        $cash_desk_detail = $this->cashDeskDetailRepo->lastDetail();

        if ($cash_desk_detail->state > 0) {
            $cask_desk_detail_id = $cash_desk_detail->id;
        }


        $paycons = \Request::get('pay');

        $person = $this->personRepo->find($customer_id);

        $customer = $person->customer()->with(['customer'])->first();

        $loans = $customer->loans()->where('state', 'Vigente')->get();

        $valid_customer = true;

        if (count($loans) > 0) {
            $valid_customer = false;
        }

        if (!$valid_customer) {
            return \Response::json(['response' => true]);
        }

        $months = array(
            ['id' => 1, 'name' => 'Enero'],
            ['id' => 2, 'name' => 'Febrero'],
            ['id' => 3, 'name' => 'Marzo'],
            ['id' => 4, 'name' => 'Abril'],
            ['id' => 5, 'name' => 'Mayo'],
            ['id' => 6, 'name' => 'Junio'],
            ['id' => 7, 'name' => 'Julio'],
            ['id' => 8, 'name' => 'Agosto'],
            ['id' => 9, 'name' => 'Septiembre'],
            ['id' => 10, 'name' => 'Octubre'],
            ['id' => 11, 'name' => 'Noviembre'],
            ['id' => 12, 'name' => 'Diciembre']
        );

        if ($paycons == 1) {
            $outputName = md5(microtime());
            $pdfPath = public_path('temp/') . $outputName . '.pdf';
            $date = Carbon::now()->format('d/m/Y');
            $h = 136;
            $pdf = \PDF::loadView('pdf.constanciaT', compact('customer'));

            $pdf->setOrientation('portrait');

            if (in_array('app/printer-optional', $sub_modules) || $host[0] == '52.42.139.31') {
                $pdf->setPaper('a4');
                $pdf->setOption('zoom', 0.90);
            } else {
                $pdf->setOption('page-width', '71mm');
                $pdf->setOption('page-height', $h . 'mm');
            }

            $pdf->setOption('margin-top', '0mm');
            $pdf->setOption('margin-left', '5mm');
            $pdf->setOption('margin-left', '5mm');
            $pdf->setOption('margin-bottom', '0mm');
            $pdf->setOption('margin-right', '0mm');
            $pdf->setOption('viewport-size', '1366x768');
            $pdf->save($pdfPath);

            if (in_array('app/printer-optional', $sub_modules) || $host[0] == '52.42.139.31') {
                $this->printService->printTicket($pdfPath);
            } else {
                $cmd = "lpr -PTSP700II ";
                $cmd .= $pdfPath;
                shell_exec($cmd);
            }

            $income = $this->incomeRepo->getModel();
            $data = [
                'amount' => 10,
                'description' => 'Constancia de no Adeudo',
                'cash_desk_detail_id' => $cask_desk_detail_id,
                'type' => 'Crediperu',
                'account_id' => \Request::get('account-id')
            ];
            $manager = new CashDeskIncomeManager($income, $data);
            $response = $manager->save();
            $this->updateAmountAccount($income->account_id, $income->amount);

            return \Response::json([$response]);
        }


        $index = date('n');

        $month = $months[$index - 1];

        $outputName = md5(microtime());

        $ext = ".pdf";

        return \PDF::loadView('pdf.constancia', compact('month', 'customer'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 5)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . $ext);
    }


    public function freeWarranty(MainMenu $mainMenu)
    {
        $code = \Request::get('code');
        $loan = $this->loanRepo->find($code);
        $menu = $mainMenu->menu(\Auth::user()->profile_id);
        $sub_modules = $menu['sub_modules'];
        $host = explode(':', \Request::server("HTTP_HOST"));

        $valid_loan = false;

        if ($loan->state == 'Garantía Ejecutada') {
            return \Response::json(['response' => false]);
        }

        if ($loan->state == 'Cancelado' || $loan->state == 'Garantía Ejecutada Cancelado') {
            $valid_loan = true;
        }

        if (!$valid_loan) {
            return \Response::json(['response' => false]);
        }

        if ($loan->warranty_option == 'Si') {
            $valid_warranty = true;
            foreach ($loan->warranties as $item) {
                if ($item->state != 'Custodia') {
                    $valid_warranty = false;
                }
            }

            if (!$valid_warranty) {
                return \Response::json(['response' => false]);
            }

            foreach ($loan->warranties as $item) {
                $item->state = 'Rescatado';
                $item->liberado_date = date('Y-m-d');
                $item->save();
            }

            $outputName = md5(microtime());
            $pdfPath = public_path('temp/') . $outputName . '.pdf';
            $date = Carbon::now()->format('d/m/Y');
            $h = 150 + count($loan->warranties) * 8;

            $pdf = \PDF::loadView('pdf.rescue', compact('date', 'loan'));
            $pdf->setOrientation('portrait');

            if (in_array('app/printer-optional', $sub_modules) || $host[0] == '52.42.139.31') {
                $pdf->setPaper('a4');
                $pdf->setOption('zoom', 0.90);
            } else {
                $pdf->setOption('page-width', '71mm');
                $pdf->setOption('page-height', $h . 'mm');
            }

            $pdf->setOption('margin-top', '0mm');
            $pdf->setOption('margin-left', '5mm');
            $pdf->setOption('margin-bottom', '0mm');
            $pdf->setOption('margin-right', '0mm');
            $pdf->setOption('viewport-size', '1366x768');
            $pdf->save($pdfPath);

            if (in_array('app/printer-optional', $sub_modules) || $host[0] == '52.42.139.31') {
                $this->printService->printTicket($pdfPath);
            } else {
                $cmd = "lpr -PTSP700II ";
                $cmd .= $pdfPath;
                shell_exec($cmd);
            }

            return \Response::json(['response' => true]);
        } else {
            return \Response::json(['response' => false]);
        }
    }


    public function expensesView()
    {
        return \View::make('cash-desk-details.expenses');
    }

    public function expensesGView()
    {
        return \View::make('cash-desk-details.expenses-g');
    }

    public function incomesView()
    {
        return \View::make('cash-desk-details.incomes');
    }

    public function depositsView()
    {
        return \View::make('cash-desk-details.deposits');
    }

    public function disbursementsView()
    {
        return \View::make('cash-desk-details.disbursements');
    }


    public function closeView()
    {
        return \View::make('cash-desk-details.close');
    }


    public function expenses()
    {
        $cash_desk_detail = $this->cashDeskDetailRepo->lastDetail();

        $expenses = $cash_desk_detail->expenses()->with(['discount', 'discount.employee', 'discount.employee.person'])->where('sub_type', 0)->get();

        return \Response::json($expenses);
    }

    public function expensesG()
    {
        $cash_desk_detail = $this->cashDeskDetailRepo->lastDetail();

        $expenses = $cash_desk_detail->expenses()->where('sub_type', 1)->get();

        return \Response::json($expenses);
    }

    public function expenseCreate(SalaryDiscountRepo $salaryDiscountRepo)
    {
        DB::beginTransaction();

        try {
            $expense = $this->expenseRepo->getModel();
            $data = \Request::all();

            $cash_desk_detail = $this->cashDeskDetailRepo->lastDetail();

            if ($data['type'] == 'Planilla') {
                $data['description'] = $data['description'] . ' (' . $data['discount']['employee']['person']['customer_name'] . ')';
            }


            if ($data['type'] == 'Previsiones' && !isset($data['loan_id']) || (isset($data['loan_id']) && empty($data['loan_id']))) {
                return \Response::json(['response' => false, 'message' => $data['loan_id']]);
            }


            if ($data['type'] == 'Previsiones') {
                $data['loan_id'] = intval(substr($data['loan_id'], 2));
            }


            $cash_desk_detail_id = '';

            if ($cash_desk_detail->state > 0) {
                $cash_desk_detail_id = $cash_desk_detail->id;
            }

            $data['cash_desk_detail_id'] = $cash_desk_detail_id;

            $manager = new CashDeskExpenseManager($expense, $data);
            $response = $manager->save();

            if ($data['type'] == 'Planilla') {
                $salary_discount = $salaryDiscountRepo->getModel();

                $salary_discount_manager = new SalaryDiscountManager(
                    $salary_discount,
                    [
                        'amount' => $expense->amount,
                        'employee_id' => $data['discount']['employee']['id'],
                        'state' => 1,
                        'type' => 'Descuento',
                        'description' => $expense->description,
                        'cash_desk_expense_id' => $expense->id
                    ]
                );

                $salary_discount_manager->save();
            }


            $this->updateAmountAccount($expense->account_id, $expense->amount * -1);

            if ($data['type'] == 'Previsiones') {

                $loan = Loan::find($data['loan_id']);
                $loan->effective_state = 0;
                $loan->save();
            }

            DB::commit();

            return \Response::json($response);
        } catch (\Exception $exception) {
            DB::rollback();
            return \Response::json(['success' => false, 'message' => $exception->getMessage()]);
        }
    }

    public function expenseEdit()
    {
        DB::beginTransaction();

        try {
            $expense = $this->expenseRepo->find(\Request::get('id'));
            $old_values = ['account_id' => $expense->account_id, 'amount' => $expense->amount];
            $data = \Request::all();

            if ($data['type'] == 'Remesas') {
                return \Response::json(['response' => false, 'errors' => ['No se puede editar una remesa.']]);
            }

            $manager = new CashDeskExpenseManager($expense, $data);
            $response = $manager->save();

            if ($data['type'] == 'Planilla') {
                $salary_discount = $expense->discount;
                $salary_discount->amount = $expense->amount;
                $salary_discount->description = $expense->description;
                $salary_discount->save();
            }

            $this->updateAmountAccount($old_values['account_id'], $old_values['amount']);
            $this->updateAmountAccount($expense->account_id, $expense->amount * -1);
            DB::commit();

            return \Response::json($response);
        } catch (\Exception $exception) {
            DB::rollback();
            return \Response::json(['success' => false, 'message' => $exception->getMessage()]);
        }
    }

    public function expenseDelete($id)
    {
        $expense = $this->expenseRepo->find($id);

        if ($expense->type == 'Remesas') {
            return \Response::json(['response' => false, 'errors' => ['No se puede eliminar una remesa.']]);
        }

        if ($expense->type == 'Planilla') {
            $salary_discount = $expense->discount;
            $salary_discount->delete();
        }

        $income = $expense->income;

        $expense->delete();

        if (isset($income->id)) {
            $cash_desk_detail = $expense->cashDeskDetail;
            $cash_desk_detail->cuenta_corriente = $cash_desk_detail->cuenta_corriente + $income->amount;
            $cash_desk_detail->save();
            $income->delete();
        }

        if ($expense->type == 'Previsiones') {
            $loan = Loan::find($expense->loan_id);
            $loan->effective_state = 1;
            $loan->save();
        }

        $this->updateAmountAccount($expense->account_id, $expense->amount);

        return \Response::json(['response' => true]);
    }


    public function incomes()
    {

        $cash_desk_detail = $this->cashDeskDetailRepo->lastDetail();

        return \Response::json($cash_desk_detail->incomes);
    }

    public function incomeCreate()
    {
        $income = $this->incomeRepo->getModel();
        $data = \Request::all();
        $cash_desk_detail = $this->cashDeskDetailRepo->lastDetail();

        $cash_desk_detail_id = '';

        if ($cash_desk_detail->state > 0) {
            $cash_desk_detail_id = $cash_desk_detail->id;
        }

        $data['cash_desk_detail_id'] = $cash_desk_detail_id;

        $manager = new CashDeskIncomeManager($income, $data);
        $response = $manager->save();

        $this->updateAmountAccount($income->account_id, $income->amount);

        return \Response::json($response);
    }

    public function incomeEdit()
    {
        $income = $this->incomeRepo->find(\Request::get('id'));

        $old_values = ['account_id' => $income->account_id, 'amount' => $income->amount];

        $data = \Request::all();
        $manager = new CashDeskIncomeManager($income, $data);
        $response = $manager->save();

        $this->updateAmountAccount($old_values['account_id'], $old_values['amount'] * -1);
        $this->updateAmountAccount($income->account_id, $income->amount);

        return \Response::json($response);
    }

    public function incomeDelete($id)
    {
        $income = $this->incomeRepo->find($id);

        if ($income->type == 'Remesas') {
            return \Response::json(['response' => false, 'errors' => ['No se puede eliminar una remesa.']]);
        }

        if ($income->corriente == 1) {
            $cash_desk_detail = $income->cashDeskDetail;
            $cash_desk_detail->cuenta_corriente = $cash_desk_detail->cuenta_corriente + $income->amount;
            $cash_desk_detail->save();

            $expense = $income->expense;
            if (isset($expense->id)) {
                $expense->delete();
            }
        }

        $income->delete();

        $this->updateAmountAccount($income->account_id, $income->amount * -1);

        return \Response::json(['response' => true]);
    }

    public function deposits()
    {
        $cash_desk_detail = $this->cashDeskDetailRepo->lastDetail();

        $deposits = $this->cashDeskDetailRepo->getPaymentDocuments($cash_desk_detail->id);

        return \Response::json($deposits);
    }

    public function disbursements()
    {
        $cash_desk_detail = $this->cashDeskDetailRepo->lastDetail();

        $disbursements = $this->cashDeskDetailRepo->getDisbursements($cash_desk_detail->id);

        return \Response::json($disbursements);
    }


    public function rePrint(MainMenu $mainMenu)
    {
        $paymentD = $this->paymentDocumentRepo->find(\Request::get('id'));
        $outputName = md5(microtime());
        $pdfPath = public_path('temp/') . $outputName . '.pdf';
        $date = Carbon::createFromFormat('d-m-Y H:i:s', $paymentD->created_at)->format('d/m/Y H:i:s');
        $copy = 1;
        $loan = $paymentD->details[0]->payment->loan;
        $menu = $mainMenu->menu(\Auth::user()->profile_id);
        $sub_modules = $menu['sub_modules'];
        $host = explode(':', \Request::server("HTTP_HOST"));

        $customer = $loan->customer->customer;


        if (true) {
            if ($loan->type_payment == 'Fija') {
                $loan2 = $this->paymentRepo->getPendingPayments($loan->code);
                $loan->payments = $loan2->payments;
                $action = 'fija';
                $data = compact('date', 'paymentD', 'loan', 'copy');
            } elseif ($loan->type_payment == 'Libre') {
                $capital = 0;
                $payments = $loan->payments;

                foreach ($payments as $payment) {
                    if ($payment->state = 'Pagado' && $payment->id <= $paymentD->details[0]->payment_id) {
                        $capital = $capital + $payment->capital;
                    }
                }

                $new_date = Carbon::createFromFormat('d-m-Y H:i:s', $paymentD->created_at);

                $capital = $loan->amount - $capital;

                $new_payment = array(
                    'expiration' => $new_date->addMonth()->format('d-m-Y'),
                    'interest' => $capital * ($loan->interest / 100)
                );

                $action = 'libre';
                $data = compact('date', 'paymentD', 'new_payment', 'loan', 'capital', 'copy');
            }

            return \Response::json(['response' => true, 'data' => $data, 'action' => $action]);
        } else {
            if ($loan->type_payment == 'Fija') {
                $h = count($paymentD->details) * 10 + 136;
                $pdf = \PDF::loadView('pdf.ticketF', compact('date', 'paymentD', 'loan', 'copy'));
                $pdf->setOrientation('portrait');

                if (in_array('app/printer-optional', $sub_modules) || $host[0] == '52.42.139.31') {
                    $pdf->setPaper('a4');
                    $pdf->setOption('zoom', 0.90);
                } else {
                    $pdf->setOption('page-width', '71mm');
                    $pdf->setOption('page-height', $h . 'mm');
                }

                $pdf->setOption('margin-top', '0mm');
                $pdf->setOption('margin-left', '5mm');
                $pdf->setOption('margin-bottom', '0mm');
                $pdf->setOption('margin-right', '0mm');
                $pdf->setOption('viewport-size', '1366x768');
                $pdf->save($pdfPath);
            } elseif ($loan->type_payment == 'Libre') {
                $capital = 0;
                $payments = $loan->payments;

                foreach ($payments as $payment) {
                    if ($payment->state = 'Pagado' && $payment->id <= $paymentD->details[0]->payment_id) {
                        $capital = $capital + $payment->capital;
                    }
                }

                $new_date = Carbon::createFromFormat('d-m-Y H:i:s', $paymentD->created_at);

                $capital = $loan->amount - $capital;

                $new_payment = array(
                    'expiration' => $new_date->addMonth()->format('d-m-Y'),
                    'interest' => $capital * ($loan->interest / 100)
                );


                $h = 155;


                $pdf = \PDF::loadView('pdf.ticketL', compact('date', 'paymentD', 'new_payment', 'loan', 'capital', 'copy'));

                $pdf->setOrientation('portrait');

                if (in_array('app/printer-optional', $sub_modules) || $host[0] == '52.42.139.31') {
                    $pdf->setPaper('a4');
                    $pdf->setOption('zoom', 0.90);
                } else {
                    $pdf->setOption('page-width', '71mm');
                    $pdf->setOption('page-height', $h . 'mm');
                }

                $pdf->setOption('margin-top', '0mm');
                $pdf->setOption('margin-left', '5mm');
                $pdf->setOption('margin-bottom', '0mm');
                $pdf->setOption('margin-right', '0mm');
                $pdf->setOption('viewport-size', '1366x768');
                $pdf->save($pdfPath);
            }
        }


        if (in_array('app/printer-optional', $sub_modules) || $host[0] == '52.42.139.31') {
            $this->printService->printTicket($pdfPath);
        } else {
            $cmd = "lpr -PTSP700II ";
            $cmd .= $pdfPath;
            shell_exec($cmd);
        }

        return \Response::json(['response' => true]);
    }

    public function rePrintDisbursement(MainMenu $mainMenu)
    {
        $disbursement = $this->disbursementRepo->find(\Request::get('id'));
        $loan = $this->loanRepo->find($disbursement->loan_id);


        $date = Carbon::now()->format('d/m/Y H:i:s');
        $copy = 1;

        return \Response::json(['response' => true, 'data' => compact('date', 'disbursement', 'loan', 'copy'), 'action' => 'desembolso']);
    }


    public function annulledPayment(MainMenu $mainMenu)
    {
        $paymentD = $this->paymentDocumentRepo->find(\Request::get('id'));

        if ($paymentD->state == 'Anulled') {
            return \Response::json(['response' => false]);
        }

        $date = Carbon::now()->format('d/m/Y H:i:s');
        $loan = $paymentD->details[0]->payment->loan;
        $capital = 0;
        $interest = 0;
        $extorno = 1;
        $action = '';
        $data = [];

        if (
            $loan->type_payment == 'Fija' && $loan->state != 'Perdida Cancelado'
            && $loan->state != 'Perdida' && $loan->state != 'Garantía Ejecutada'
            && $loan->state != 'Garantía Ejecutada Cancelado'
        ) {

            $customer = $loan->customer->customer;

            $action = 'fija';
            $data = compact('date', 'paymentD', 'loan', 'extorno');
        }

        if ($loan->state == 'Perdida Cancelado' || $loan->state == 'Perdida' || $loan->state == 'Garantía Ejecutada' || $loan->state == 'Garantía Ejecutada Cancelado') {
            $capital = round($loan->amount, 2) - round($this->paymentRepo->sumCapital($loan->id), 2);
            $pending_payments = $loan->payments;
            $new_payment = '';

            foreach ($pending_payments as $item) {
                if ($item->state == 'Pendiente') {
                    $new_payment = $item;
                }
            }

            $customer = $loan->customer->customer;

            $action = 'libre';
            $data = compact('date', 'paymentD', 'new_payment', 'loan', 'capital', 'extorno');


            $payments = $loan->payments;

            foreach ($payments as $item) {
                if ($item->state == 'Pendiente') {
                    $capital = $item->capital;
                    $item->delete();
                }
            }
            if ($loan->state == 'Perdida Cancelado') {
                $loan->state = 'Perdida';
            } else if ($loan->state == 'Garantía Ejecutada Cancelado') {
                $loan->state = 'Garantía Ejecutada';
            }
        } else {
            $loan->state = 'Vigente';
        }

        if ($loan->type_payment == 'Libre' && $loan->state != 'Perdida Cancelado' && $loan->state != 'Perdida') {
            $capital = round($loan->amount, 2) - round($this->paymentRepo->sumCapital($loan->id), 2);
            $pending_payments = $loan->payments;
            $new_payment = '';
            foreach ($pending_payments as $item) {
                if ($item->state == 'Pendiente') {
                    $new_payment = $item;
                }
            }

            $customer = $loan->customer->customer;
            $action = 'libre';
            $data = compact('date', 'paymentD', 'new_payment', 'loan', 'capital', 'extorno');


            $payments = $loan->payments;

            foreach ($payments as $item) {
                if ($item->state == 'Pendiente') {
                    $capital = $item->capital;
                    $interest = $item->interest;
                    $item->delete();
                }
            }
        }

        foreach ($paymentD->details as $item) {

            $item->state = 'Anulled';
            $item->payment->state = 'Pendiente';

            $amount_pay = $item->payment->capital + $item->payment->interest + $item->payment->mora;

            if ($item->payment->mora > 0) {
                $item->payment->amount = $item->payment->capital + $item->payment->interest;
                $item->payment->mora = 0;
            }

            if ($item->payment->expenses > 0) {
                $item->payment->amount = $item->payment->capital + $item->payment->interest;
                $item->payment->expenses = 0;
            }

            if ($loan->type_payment == 'Libre' && $loan->state != 'Perdida Cancelado' && $loan->state != 'Perdida') {
                $item->payment->capital = $item->payment->capital + $capital;
                $item->payment->interest = $item->payment->capital * ($loan->interest) / 100;
                $item->payment->amount = $item->payment->capital + $item->payment->interest;
            } elseif ($loan->state == 'Perdida Cancelado' || $loan->state == 'Perdida') {
                $item->payment->capital = $item->payment->capital + $capital;
                $item->payment->interest = $item->payment->interest + $interest;
                $item->payment->amount = $item->payment->capital + $item->payment->interest;
            }

            $item->payment->save();
            $item->save();
        }

        foreach ($paymentD->expenses as $expense) {
            $expense->state = 'Pendiente';
            $expense->save();
        }

        $paymentD->state = 'Anulled';
        $paymentD->save();

        $loan->save();

        $comments = $paymentD->comments();

        foreach ($comments as $comment) {
            $comment->delete();
        }

        $this->updateAmountAccount($paymentD->account_id, $paymentD->amount * -1);

        return \Response::json(['response' => true, 'data' => $data, 'action' => $action]);
    }


    public function find()
    {
        $cash_desk_detail = $this->cashDeskDetailRepo->lastDetail();

        $cash_desk_detail->disbursement_amount = $cash_desk_detail->disbursements()->sum('amount');
        $cash_desk_detail->income_amount = $cash_desk_detail->incomes()->sum('amount');
        $cash_desk_detail->expenses_amount = $cash_desk_detail->expenses()->where('sub_type', 0)->sum('amount');
        $cash_desk_detail->expenses_amount_g = $cash_desk_detail->expenses()->where('sub_type', 1)->sum('amount');
        $cash_desk_detail->deposit_amount = $cash_desk_detail->paymentDocuments()->where('state', 'Correct')->sum('amount');


        $cash_desk_detail->total_amount = $cash_desk_detail->initial_amount + $cash_desk_detail->income_amount
            + $cash_desk_detail->deposit_amount - $cash_desk_detail->expenses_amount - $cash_desk_detail->expenses_amount_g
            - $cash_desk_detail->disbursement_amount;


        $cash_desk_detail->cuenta_corriente = $cash_desk_detail->cuenta_corriente + $cash_desk_detail->paymentDocuments()->where('type_payment', 'Cuenta Corriente')->where('state', 'Correct')->sum('amount');
        $cash_desk_detail->efectivo = $cash_desk_detail->total_amount - $cash_desk_detail->cuenta_corriente;

        return \Response::json($cash_desk_detail);
    }

    public function acceptConsignment()
    {
        $data = \Request::all();
        $cash_desk_detail = $this->cashDeskDetailRepo->lastDetail();

        foreach ($cash_desk_detail->getConsignments as $consignment) {
            $consignment->pivot->state = 0;
            $consignment->pivot->save();

            $income = $this->incomeRepo->getModel();

            $income_manager = new CashDeskIncomeManager($income, [
                'amount' => $consignment->pivot->amount,
                'cash_desk_detail_id' => $cash_desk_detail->id,
                'description' => 'Remesa de caja 0' . $consignment->cash_desk_id,
                'type' => 'Remesas'
            ]);

            $income_manager->save();
        }

        return \Response::json(['response' => true]);
    }

    public function closeCashDesk()
    {
        DB::beginTransaction();
        try{
            $cash_desk_detail = $this->cashDeskDetailRepo->find(\Request::get('id'));
            $data = \Request::all();
            $data['date_start'] = $cash_desk_detail->created_at;
            $data['date_end'] = date('Y-m-d H:i:s');
            $data['state'] = 0;
            $cash_desk = $this->cashDeskRepo->find($data['cash_desk_id']);
            $cash_desk->state = 'Close';
            $cash_desk->save();

            $manager = new CashDeskDetailManager($cash_desk_detail, $data);
            $manager->save();

            $details = $cash_desk->cashDeskDetails()->where('state', 1)->get();

            foreach ($details as $detail){
                $detail->state = 0;
                $detail->save();
            }

            $person = $this->personRepo->find(\Auth::user()->person_id);

            $cash_desk_detail->disbursement_amount = $cash_desk_detail->disbursements()->sum('amount');
            $cash_desk_detail->income_amount = $cash_desk_detail->incomes()->sum('amount');
            $cash_desk_detail->expenses_amount = $cash_desk_detail->expenses()->where('sub_type', 0)->sum('amount');
            $cash_desk_detail->expenses_amount_g = $cash_desk_detail->expenses()->where('sub_type', 1)->sum('amount');
            $cash_desk_detail->deposit_amount = $cash_desk_detail->paymentDocuments()->where('state', 'Correct')->sum('amount');

            foreach ($cash_desk_detail->accountDetails as $accountDetail) {
                $accountDetail->final_amount = $accountDetail->account->amount;
                $accountDetail->save();
            }

            $outputName = md5(microtime());
            $pdfPath = public_path('temp/') . $outputName . '.pdf';

            $pdf = \PDF::loadView('pdf.cashDesk', compact('cash_desk_detail', 'person'))
                ->setPaper('a4')
                ->setOption('margin-top', 10)
                ->setOption('margin-left', 15)
                ->setOption('margin-bottom', 10)
                ->setOption('margin-right', 4)
                ->setOption('viewport-size', '1366x768')
                ->save($pdfPath);

            DB::commit();
            return \Response::json(['response' => true, 'data' => $cash_desk_detail->id, 'pdf' => 'temp/' . $outputName . '.pdf']);
        }catch (\Exception $exception){
            DB::rollback();
            return \Response::json(['response' => false]);
        }
    }

    public function payC()
    {
        $data = \Request::all();
        $payment = $this->paymentRepo->find($data['payment']['id']);
        $cash_desk_detail = $this->cashDeskDetailRepo->lastDetail();

        if ($cash_desk_detail->state > 0) {

            $data['cash_desk_detail_id'] = $cash_desk_detail->id;
        }

        if ($payment->state == 'Pagado') {
            return \Response::json(false);
        }

        $paymentDocument = $this->paymentDocumentRepo->getModel();
        $paymentData = [
            'amount' => $data['amount'],
            'cash_desk_detail_id' => $data['cash_desk_detail_id'],
            'user_id' => \Auth::user()->id,
            'customer_id' => $data['customer_id'],
            'document_type_id' => 4,
            'account_id' => $data['type_payment']
        ];
        $manager = new PaymentDocumentManager($paymentDocument, $paymentData);
        $manager->save();
        $loan_id = $data['payment']['loan_id'];
        $payment->interest = $data['payment']['interest'];;
        $payment->capital = $data['payment']['capital'];
        $payment->amount = $data['payment']['amount'];
        $payment->date_pay = date('Y-m-d H:i:s');
        $payment->state = 'Pagado';
        $payment->account_id = $data['type_payment'];

        if ($data['expense_amount'] > 0) {
            $payment->amount = $payment->amount + $data['expense_amount'];
            $payment->expenses = $data['expense_amount'];
            $expenses_amount = $data['expense_amount'];
            $payment_id = $payment->id;
        }

        $payment->save();
        $paymentDocumentDetail = $this->paymentDocumentDetailRepo->getModel();
        $item_data = [
            'payment_document_id' => $paymentDocument->id,
            'payment_id' => $data['payment']['id'],
            'amount' => $payment->amount
        ];
        $item_manager = new PaymentDocumentDetailManager($paymentDocumentDetail, $item_data);
        $item_manager->save();

        $loan = $this->loanRepo->find($loan_id);
        $capital = round($loan->amount, 2) - round($this->paymentRepo->sumCapital($loan->id), 2);

        $expenses = null;

        if ($data['expense_amount'] > 0) {
            $expenses = $loan->expenses;

            foreach ($expenses as $expense) {
                $expense->state = 'Pagado';
                $expense->payment_id = $payment_id;
                $expense->payment_document_id = $paymentDocument->id;
                $expense->save();
            }
        }

        $new_payment = null;

        if ($capital <= 0) {
            $loan->state = 'Perdida Cancelado';
            if ($loan->warranty_option == 'Si') {
                $loan->state = 'Garantía Ejecutada Cancelado';
            }
            $loan->date_end = date('Y-m-d');
            $loan->save();
        } else {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $payment->date_pay);
            $fee = $data['payment']['pre_interest'] - $data['payment']['interest'];
            $new_payment = $this->paymentRepo->getModel();
            if ($loan->period == 'Mensual') {
                $vcto = $date->addMonth(1)->format('Y-m-d');
            } elseif ($loan->period == 'Semanal') {
                $vcto = $date->addDays(7)->format('Y-m-d');
            }

            $payment_manager = new PaymentManager($new_payment, [
                'loan_id' => $loan->id,
                'amount' => $fee + $capital,
                'interest' => $fee,
                'capital' => $capital,
                'expiration' => date('d-m-Y'),
                'state' => 'Pendiente'
            ]);

            $payment_manager->save();
        }

        if (!empty($data['comment'])) {
            LoanComment::create(
                [
                    'loan_id' => $loan->id,
                    'comment' => $data['comment']
                ]
            );
        }

        $paymentD = $this->paymentDocumentRepo->find($paymentDocument->id);
        $paymentD->capital = $capital;
        $paymentD->save();

        $date = Carbon::now()->format('d/m/Y H:i:s');
        $customer = $loan->customer->customer;

        $this->updateAmountAccount($data['type_payment'], $data['amount']);

        return \Response::json(['response' => true, 'data' => compact('date', 'paymentD', 'new_payment', 'loan', 'capital', 'expenses'), 'action' => 'libre']);
    }

    public function reportTotalCancellation()
    {
        $loan_id = \Request::get('loan-id');

        $loan = $this->loanRepo->find($loan_id);

        $h1 = 0;
        $flag_pay = 0;
        foreach ($loan->payments as $item) {
            if ($item->state == 'Pendiente') {
                $item->pay_total = false;
                $h1 = $h1 + 1;
                if ($loan->period == 'Mensual') {
                    $date = Carbon::createFromFormat('d-m-Y', $item->expiration)->subMonth();
                } else if ($loan->period == 'Semanal') {
                    $date = Carbon::createFromFormat('d-m-Y', $item->expiration)->subWeek();
                }
                $days_dif = $date->diffInDays(Carbon::now(), false);
                if ($days_dif > 1 && $item->state == 'Pendiente') {
                    $item->pay_total = true;
                }
                $flag_pay++;
            }
        }

        return \Response::json(['response' => true, 'data' => $loan, 'action' => 'payTotal']);
    }


    public function createLoanExpense()
    {
        $data = \Request::all();
        $data['state'] = 'Pendiente';

        LoanExpense::create($data);

        LoanComment::create(
            [
                'loan_id' => $data['loan_id'],
                'comment' => 'Se agrego un gasto por concepto de: ' . $data['description']
            ]
        );

        return \Response::json(['response' => true]);
    }

    public function show(UserRepo $userRepo)
    {
        $cash_desk_detail_id = \Request::get('id');

        $cash_desk_detail = $this->cashDeskDetailRepo->find($cash_desk_detail_id);
        $user = $userRepo->find($cash_desk_detail->user_id);

        $outputName = md5(microtime());

        $ext = "pdf";

        return \PDF::loadView('pdf.cashDeskDetail', compact('cash_desk_detail', 'user'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function updateAmountAccount($id, $amount)
    {
        $account = Account::find($id);

        $account->amount = $account->amount + $amount;

        $account->save();
    }
}
