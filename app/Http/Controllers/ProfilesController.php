<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;
use App\Core\Managers\ProfileManager;
use App\Core\Repositories\ProfileRepo;
use Illuminate\Routing\Route;

class ProfilesController extends Controller {

    protected $profileRepo;
    protected $profile;

    public function __construct(ProfileRepo $profileRepo){
        $this->profileRepo = $profileRepo;
    }

    public function findProfile($id)
    {
        $this->profile = $this->profileRepo->find($id);
    }


    public function index()
    {
        return \View::make('profiles.index');
    }

    public function formCreate()
    {
        return \View::make('profiles.create');
    }

    public function formEdit()
    {
        return \View::make('profiles.edit');
    }

    public function all()
    {
        $profiles = $this->profileRepo->paginate(25);

        return \Response::json($profiles);
    }

    public function create()
    {
        $profile = $this->profileRepo->getModel();
        $data = \Request::all();
        $manager = new ProfileManager($profile,$data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function find($id)
    {
        $this->findProfile($id);
        return \Response::json($this->profile);
    }

    public function edit($id)
    {
        $this->findProfile($id);
        $data = \Request::all();
        $manager = new ProfileManager($this->profile, $data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function delete($id)
    {
        $this->findProfile($id);
        $this->profile->delete();

        return \Response::json(['response'=>true, 'data'=>$this->profile]);
    }

} 