<?php

namespace App\Http\Controllers;

use App\Expense;
use App\ExtraAccount;
use App\Income;
use Carbon\Carbon;

class WallController extends Controller
{

    public function index()
    {
        $extra_accounts = ExtraAccount::all();
        return \View::make('wall.index', compact('extra_accounts'));
    }


    public function storeExpense()
    {
        $data = \Request::all();
        Expense::create($data);

        $this->updateAmountAccount($data['extra_account_id'], $data['amount'] * -1);

        return \Response::json(['success' => true]);
    }

    public function all()
    {
        $date_start = \Request::get('date_start');
        $date_end = \Request::get('date_end');

        if (!empty($date_start) && !empty($date_end)) {
            $date_start = Carbon::createFromFormat('d-m-Y', $date_start)->format('Y-m-d');
            $date_end = Carbon::createFromFormat('d-m-Y', $date_end)->format('Y-m-d');
        } else {
            $date_start = date('Y-m-01');
            $date_end = Carbon::now()->format('Y-m-d');
        }

        $expenses = Expense::with(['extraAccount'])
            ->whereBetween('created_at', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->get();

        $incomes = Income::with(['extraAccount'])
            ->whereBetween('created_at', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->get();

        return \Response::json(['success'=>true, 'expenses'=>$expenses, 'incomes'=>$incomes]);

    }

    public function annulledExpense($id)
    {

        $expense = Expense::find($id);
        $expense->state = 0;
        $expense->save();

        $this->updateAmountAccount($expense->extra_account_id, $expense->amount);

        return \Response::json(['success' => true]);
    }

    public function storeIncome()
    {
        $data = \Request::all();
        Income::create($data);
        $this->updateAmountAccount($data['extra_account_id'], $data['amount']);

        return \Response::json(['success' => true]);
    }

    public function annulledIncome($id)
    {
        $income = Income::find($id);
        $income->state = 0;
        $income->save();

        $this->updateAmountAccount($income->extra_account_id, $income->amount * -1);


        return \Response::json(['success' => true]);
    }

    public function updateAmountAccount($id, $amount)
    {
        $account = ExtraAccount::find($id);

        $account->amount = $account->amount + $amount;

        $account->save();

    }

}