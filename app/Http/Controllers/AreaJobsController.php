<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;
use App\Core\Managers\AreaJobManager;
use App\Core\Repositories\AreaJobRepo;
use Illuminate\Routing\Route;

class AreaJobsController extends Controller {

    protected $areaJobRepo;
    protected $areaJob;

    public function __construct(AreaJobRepo $areaJobRepo){
        $this->areaJobRepo = $areaJobRepo;
    }

    public function findAreaJob($id)
    {
        $this->areaJob = $this->areaJobRepo->find($id);
    }


    public function index()
    {
        return \View::make('area-jobs.index');
    }

    public function formCreate()
    {
        return \View::make('area-jobs.create');
    }

    public function formEdit()
    {
        return \View::make('area-jobs.edit');
    }

    public function all()
    {
        $areaJobs = $this->areaJobRepo->paginateFilter(25,\Request::get('type'));

        return \Response::json($areaJobs);
    }

    public function create()
    {
        $areaJob = $this->areaJobRepo->getModel();
        $data = \Request::all();
        $data['area_id'] = $data['area']['id'];
        $manager = new AreaJobManager($areaJob,$data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function find($id)
    {
        $this->findAreaJob($id);
        return \Response::json($this->areaJob);
    }

    public function edit($id)
    {
        $this->findAreaJob($id);
        $data = \Request::all();
        $data['area_id'] = $data['area']['id'];
        $manager = new AreaJobManager($this->areaJob, $data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function delete($id)
    {
        $this->findAreaJob($id);
        $this->areaJob->delete();

        return \Response::json(['response'=>true, 'data'=>$this->areaJob]);
    }

} 