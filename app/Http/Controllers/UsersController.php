<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;
use App\Core\Managers\UserManager;
use App\Core\Repositories\UserRepo;
use Illuminate\Routing\Route;

class UsersController extends Controller {

    protected $userRepo;
    protected $user;

    public function __construct(UserRepo $userRepo){
        $this->userRepo = $userRepo;
    }

    public function findUser($id)
    {
        $this->user = $this->userRepo->find($id);
    }


    public function index()
    {
        return \View::make('users.index');
    }

    public function formCreate()
    {
        return \View::make('users.create');
    }

    public function formEdit()
    {
        return \View::make('users.edit');
    }

    public function all()
    {
        $users = $this->userRepo->paginate(25);

        return \Response::json($users);
    }

    public function create()
    {
        $user = $this->userRepo->getModel();
        $data = \Request::all();
        $data['profile_id'] = $data['profile']['id'];
        $data['person_id'] = $data['person']['originalObject']['id'];
        $data['state'] = $data['state']['value'];
        $data['administrative_option'] = $data['administrative_option']['value'];
        $data['email'] = $data['name'].'@'.'hotmail.com';
        $manager = new UserManager($user,$data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function find($id)
    {
        $this->findUser($id);
        return \Response::json($this->user);
    }

    public function edit($id)
    {
        $this->findUser($id);
        $data = \Request::all();
        $data['profile_id'] = $data['profile']['id'];
        $data['person_id'] = $data['person']['originalObject']['id'];
        $data['state'] = $data['state']['value'];
        $data['administrative_option'] = $data['administrative_option']['value'];

        if(empty($data['password']) || empty($data['password_confirmation']))
        {
            unset($data['password'],$data['password_confirmation']);
        }

        $manager = new UserManager($this->user, $data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function delete($id)
    {
        $this->findUser($id);
        $this->user->delete();

        return \Response::json(['response'=>true, 'data'=>$this->user->name]);
    }

    public function changePassword()
    {
        $data = \Request::all();
        $credentials = ['name' => \Auth::user()->name, 'password' => $data['old_password']];

        if (\Auth::attempt($credentials, true))
        {
            $this->user = $this->userRepo->find(\Auth::user()->id);

            $new_data = [
                'name'=>$this->user->name,
                'profile_id'=>$this->user->profile_id,
                'state'=>$this->user->state['value'],
                'person_id'=>$this->user->person_id,
                'password'=>$data['password'],
                'password_confirmation' => $data['password_confirmation'],
                'administrative_option'=>$this->user->administrative_option['value']
            ];

            $manager = new UserManager($this->user, $new_data);

            $response = $manager->save();

            return \Response::json($response);
        }else{
            return \Response::json(['response'=>false]);
        }
    }

    public function uploadPhoto()
    {
        $this->user = $this->userRepo->find(\Auth::user()->id);
        $file = \Request::file('file');
        $token = $this->user->name;
        $image = \Image::make($file);
        $image->save(base_path('public/user-img').'/'.$token.'.jpg');
       // $file->move(base_path('public/user-img'), $token.'.jpg');
        $this->user->avatar = 'user-img/'.$token.'.jpg';
        $this->user->save();

        return \Response::json(['response'=>true]);
    }

    public function changeColor()
    {
        $this->user = $this->userRepo->find(\Auth::user()->id);

        $data = \Request::all();

        $new_data = [
            'name'=>$this->user->name,
            'profile_id'=>$this->user->profile_id,
            'state'=>$this->user->state['value'],
            'person_id'=>$this->user->person_id,
            'administrative_option'=>$this->user->administrative_option['value'],
            'color'=>$data['color']
        ];

        $manager = new UserManager($this->user, $new_data);

       $response =  $manager->save();

        return \Response::json($response);
    }

} 