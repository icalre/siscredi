<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;

use App\Core\Components\MainMenu;
use App\Core\Entities\Loan;
use App\Core\Entities\Payment;
use App\Core\Managers\LoanManager;
use App\Core\Repositories\LoanRepo;
use App\Core\Repositories\PaymentRepo;
use App\Core\Managers\PaymentManager;
use App\Core\Managers\EndorsementManager;
use App\Core\Repositories\EndorsementRepo;
use App\Core\Managers\WarrantyManager;
use App\Core\Repositories\PersonRepo;
use App\Core\Repositories\WarrantyRepo;
use App\Core\Repositories\CustomerRepo;
use App\Core\Repositories\CompanyRepo;
use App\Core\Managers\CustomerManager;
use App\Core\Services\PrintService;
use Carbon\Carbon;

use Illuminate\Routing\Route;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class LoansController extends Controller
{

    protected $loanRepo;
    protected $paymentRepo;
    protected $endorsementRepo;
    protected $warrantyRepo;
    protected $customerRepo;
    protected $companyRepo;

    public function __construct(LoanRepo $loanRepo, PaymentRepo $paymentRepo, EndorsementRepo $endorsementRepo,
                                WarrantyRepo $warrantyRepo, CustomerRepo $customerRepo, CompanyRepo $companyRepo)
    {
        $this->loanRepo = $loanRepo;
        $this->paymentRepo = $paymentRepo;
        $this->warrantyRepo = $warrantyRepo;
        $this->endorsementRepo = $endorsementRepo;
        $this->customerRepo = $customerRepo;
        $this->companyRepo = $companyRepo;
    }

    public function changeEmployee()
    {
        $data = \Request::all();

        $loan = $this->loanRepo->find($data['id']);
        $loan->employee_id = $data['employee']['originalObject']['id'];
        $loan->save();

        return \Response::json(['response' => true]);

    }

    public function findLoan($id)
    {
        $this->loan = $this->loanRepo->find($id);
    }


    public function index(MainMenu $mainMenu)
    {
        $menu = $mainMenu->menu(\Auth::user()->profile_id);
        $sub_modules = $menu['sub_modules'];

        return \View::make('loans.index', compact('sub_modules'));
    }

    public function formCreate(MainMenu $mainMenu)
    {
        $menu = $mainMenu->menu(\Auth::user()->profile_id);
        $sub_modules = $menu['sub_modules'];

        return \View::make('loans.create', compact('sub_modules'));
    }

    public function formEdit(MainMenu $mainMenu)
    {
        $menu = $mainMenu->menu(\Auth::user()->profile_id);
        $sub_modules = $menu['sub_modules'];

        return \View::make('loans.edit', compact('sub_modules'));
    }

    public function all()
    {

        $loan_lose = Loan::select('loans.id', 'loans.state')
            ->join('payments', 'payments.loan_id', '=', 'loans.id')
            ->whereRaw('TIMESTAMPDIFF(MONTH, payments.expiration, now()) >= 3')
            ->where('payments.state', 'Pendiente')
            ->where('loans.state', 'Vigente')
            ->where('loans.warranty_option', 'No')
            ->groupBy('loans.id')
            ->get();

        foreach ($loan_lose as $loan) {
            $loan->state = 'Atrasado';
            $loan->save();
        }

        $loans = $this->loanRepo->paginateFilter(25, \Request::get('type'), \Request::get('warranty'),
            \Request::get('state'), \Request::get('aval'), \Request::get('interest'), \Request::get('modalidad'), \Request::get('customer_type'));

        return \Response::json($loans);
    }

    public function create(CustomerRepo $customerRepo, PersonRepo $personRepo, MainMenu $mainMenu)
    {

        $response = array();
        $loan = $this->loanRepo->getModel();
        $payments = \Request::get('payments');
        $data = \Request::all();

        $company = $this->companyRepo->find(1);
        $data['interest'] = $data['interest'];

        if (isset($data['enterprise']) && isset($data['enterprise']['originalObject']['id'])) {
            $data['enterprise_id'] = $data['enterprise']['originalObject']['id'];
        }


        if (isset($data['customer'])) {
            $customer_validation = $customerRepo->validateCustomer(
                'Person',
                $data['customer']['originalObject']['id']);

            if (!$customer_validation) {
                $customer = $customerRepo->getModel();
                $customer_data = array();
                $customer_data['customer_id'] = $data['customer']['originalObject']['id'];
                $customer_data['customer_type'] = substr('App\Core\Entities\u', 0, -1) . ucfirst('Person');
                $customer_manager = new CustomerManager($customer, $customer_data);
                $customer_manager->save();
                $customer->code = str_pad($customer->id, 4, "0", STR_PAD_LEFT);
                $customer->save();
                $data['customer_id'] = $customer->id;
            } else {
                $person = $personRepo->find($data['customer']['originalObject']['id']);
                $data['customer_id'] = $person->customer->id;
                $customer = $person->customer;
            }

            $loan_customers = $customer->loans()->first();

            if (isset($loan_customers->id)) {
                $data['customer_type'] = 'Renovación';
            }

        } else {
            return \Response::json(['response' => false, 'errors' => ['No has seleccionado ningun cliente']]);
        }

        if (!isset($data['employee'])) {
            return \Response::json(['response' => false, 'errors' => ['No has seleccionado ningun trabajador']]);
        }

        if (isset($data['person'])) {
            $data['person_id'] = $data['person']['originalObject']['id'];
        }


        if ($company->contract_option == 1) {
            $data['nro_contrato'] = $company->contract_number;
            $company->contract_number = $company->contract_number + 1;
            $company->save();
        }

        $valid_warranty = true;
        foreach ($data['joyasA'] as $item) {
            if (empty($item['Description'])) {
                $valid_warranty = false;
            }
        }

        foreach ($data['artefactosA'] as $item) {
            if (empty($item['Description'])) {
                $valid_warranty = false;
            }
        }

        foreach ($data['otrosA'] as $item) {
            if (empty($item['Description'])) {
                $valid_warranty = false;
            }
        }

        if (!$valid_warranty) {
            return \Response::json(['errors' => ['Se ha enviado garantias con descripcion en blanco.']]);
        }

        if ($customer->rate == 'Malo') {
            return \Response::json(['errors' => ['Cliente con calificación mala, no se le puede generar ningun prestamo.']]);
        }

        $menu = $mainMenu->menu(\Auth::user()->profile_id);
        $sub_modules = $menu['sub_modules'];

        if (in_array('app/a-loan', $sub_modules)) {
            $data['state'] = 'Aprobado';
        } else {
            $data['state'] = 'Pre-Aprobado';
        }

        $data['date_disbursement'] = date('Y-m-d');
        $data['employee_id'] = $data['employee']['originalObject']['id'];
        $manager = new LoanManager($loan, $data);
        $response = $manager->save();
        $loan->code = 'CP' . str_pad($loan->id, 4, "0", STR_PAD_LEFT);
        $loan->save();

        $fee_add = 0;
        $flag_fee_add = 0;
        $diff_days = Carbon::createFromFormat('d-m-Y', $data['date_start'])->diffInDays(Carbon::now(), false);

        if ($diff_days < 0 && $loan->feed_option == 'Si') {
            $fee_add = $diff_days * (-1) * (($data['interest'] / 28) / 100) * $data['amount'];
        }

        if ($loan->type_payment == 'Libre') {
            foreach ($payments as $item) {
                $payment = $this->paymentRepo->getModel();
                $payment_manager = new PaymentManager($payment, [
                    'loan_id' => $loan->id,
                    'amount' => $item['amount'] + $fee_add,
                    'interest' => $item['interest'] + $fee_add,
                    'capital' => $item['capital'],
                    'expiration' => $item['expiration'],
                    'state' => 'Pendiente'
                ]);
                $payment_manager->save();
                if ($flag_fee_add == 0) {
                    $fee_add = 0;
                }

                $flag_fee_add++;
            }

        } elseif ($loan->type_payment == 'Fija') {
            $i = 1;
            foreach ($payments as $item) {
                $payment = $this->paymentRepo->getModel();
                $payment_manager = new PaymentManager($payment, [
                    'loan_id' => $loan->id,
                    'amount' => $item['amount'] + $fee_add,
                    'interest' => $item['interest'] + $fee_add,
                    'capital' => $item['capital'],
                    'expiration' => $item['expiration'],
                    'number' => $i++ . '/' . $data['quotas'],
                    'state' => 'Pendiente'
                ]);
                $response = $payment_manager->save();

                if ($flag_fee_add == 0) {
                    $fee_add = 0;
                }

                $flag_fee_add++;
            }
        }

        if ($data['endorsement_option'] == 'Si') {
            $endorsement = $this->endorsementRepo->getModel();
            $endorsement_manager = new EndorsementManager($endorsement, ['loan_id' => $loan->id,
                'person_id' => $data['endorsement']['originalObject']['id']]);
            $response = $endorsement_manager->save();
        }

        if ($data['warranty_option'] == 'Si') {
            foreach ($data['artefactosA'] as $item) {
                $warranty = $this->warrantyRepo->getModel();
                $warranty_manager = new WarrantyManager($warranty, ['loan_id' => $loan->id,
                    'Description' => $item['Description'],
                    'state' => 'Custodia',
                    'reference_price' => $item['reference_price'],
                    'type' => 'Artefacto']);
                $response = $warranty_manager->save();
            }

            foreach ($data['otrosA'] as $item) {
                $warranty = $this->warrantyRepo->getModel();
                $warranty_manager = new WarrantyManager($warranty, ['loan_id' => $loan->id,
                    'Description' => $item['Description'],
                    'state' => 'Custodia',
                    'reference_price' => $item['reference_price'],
                    'type' => 'Otro']);
                $response = $warranty_manager->save();
            }

            foreach ($data['joyasA'] as $item) {
                $warranty = $this->warrantyRepo->getModel();
                $warranty_manager = new WarrantyManager($warranty, ['loan_id' => $loan->id,
                    'Description' => $item['Description'],
                    'state' => 'Custodia',
                    'reference_price' => $item['reference_price'],
                    'type' => 'Joya',
                    'pb' => $item['pb'],
                    'pn' => $item['pn']
                ]);
                $response = $warranty_manager->save();
            }
        }

        return \Response::json($response);
    }

    public function find($id)
    {
        $this->findLoan($id);
        return \Response::json($this->loan);
    }

    public function delete($id)
    {
        $this->findLoan($id);
        $this->loan->delete();

        return \Response::json(['response' => true, 'data' => $this->loan]);
    }

    public function printSchedule(PrintService $printService)
    {
        $data = \Request::all();
        $host = explode(':', \Request::server("HTTP_HOST"));

        $loan = $this->loanRepo->find($data['loan-id']);
        $artefactosA = array();
        $otrosA = array();
        $joyasA = array();

        if ($loan->warranty_option == 'Si') {

            foreach ($loan->warranties as $warranty) {
                if ($warranty->type == 'Artefacto') {
                    $artefactosA[] = $warranty;
                } elseif ($warranty->type == 'Otro') {
                    $otrosA[] = $warranty;
                } elseif ($warranty->type == 'Joya') {
                    $joyasA[] = $warranty;
                }
            }

            $loan->artefactosA = $artefactosA;
            $loan->otrosA = $otrosA;
            $loan->joyasA = $joyasA;
        }

        $outputName = md5(microtime());

        if ($host[0] == '52.42.139.31') {
            $pdfPath = public_path('temp/') . $outputName . '.pdf';
            \PDF::loadView('pdf.schedule', compact('loan'))->setPaper('a4')
                ->setOption('margin-top', 10)
                ->setOption('margin-left', 15)
                ->setOption('margin-bottom', 10)
                ->setOption('margin-right', 4)
                ->setOption('viewport-size', '1366x768')
                ->save($pdfPath);

            $printService->printTicket($pdfPath, 'EPSON L220 Series');
        }


        return \PDF::loadView('pdf.schedule', compact('loan'))->setPaper('a4')
            ->setOption('margin-top', 10)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName);
    }

    public function acceptLoan()
    {

        $data = \Request::all();
        $loan = $this->loanRepo->find($data['id']);
        $loan->date_start = date('d-m-Y');
        $loan->state = 'Aprobado';
        $loan->save();

        $date = Carbon::createFromFormat('d-m-Y', $loan->date_start);

        foreach ($loan->payments as $payment) {
            if ($loan->period == 'Mensual') {
                $vcto = $date->addMonth(1)->format('d-m-Y');
                $day = $date->dayOfWeek;
                if ($day == 0) {
                    $new_date = Carbon::createFromFormat('d-m-Y', $vcto);
                    $vcto = $new_date->addDays(1)->format('d-m-Y');
                }

                $payment->expiration = $vcto;
                $payment->save();

            } else if ($loan->period == 'Semanal') {
                $vcto = $date->addDays(7)->format('d-m-Y');
                $day = $date->dayOfWeek;

                if ($day == 0) {
                    $new_date = Carbon::createFromFormat('d-m-Y', $vcto);
                    $vcto = $new_date->addDays(1)->format('d-m-Y');
                }

                $payment->expiration = $vcto;
                $payment->save();
            }
        }

        return \Response::json(['response' => true]);

    }


    public function annulled(MainMenu $mainMenu)
    {
        $data = \Request::all();
        $loan = $this->loanRepo->find($data['id']);

        $payments = $loan->payments;

        $menu = $mainMenu->menu(\Auth::user()->profile_id);
        $sub_modules = $menu['sub_modules'];


        if (!in_array('app/a-loan', $sub_modules) && $loan->state != 'Pre-Aprobado') {
            return \Response::json(['response' => false]);
        }

        if ($loan->state == 'Aprobado' || $loan->state == 'Vigente' || $loan->state == 'Pre-Aprobado') {
            foreach ($payments as $payment) {
                $payment->state = 'Anulado';
                $payment->save();
            }

            $loan->date_annulment = date('Y-m-d H:i:s');
            $loan->state = 'Anulado';

            $loan->save();

            $disbursement = $loan->disbursement;

            if (isset($disbursement->id)) {
                $disbursement->delete();
            }
        }

        return \Response::json(['response' => true]);

    }

    public function loseLoan()
    {
        $data = \Request::all();
        $loan = $this->loanRepo->find($data['id']);
        $fee = $loan->interest / 30;
        $fee = $fee / 100;
        $capital = $loan->amount - round($this->paymentRepo->sumCapital($loan->id), 2);

        if ($loan->state != 'Vigente') {
            return \Response::json(['response' => false]);
        }

        $payments = $loan->payments()->where('state', 'Pendiente')->get();
        $payments_pay = $loan->payments()->where('state', 'Pagado')->get();
        $index_last = count($payments_pay) - 1;

        if ($index_last < 0) {
            $payments_pay = $payments;
            $index_last = 0;
        }

        foreach ($payments as $payment) {
            $payment->state = 'Perdida';
            $payment->save();
        }

        $loan->state = 'Perdida';
        $loan->date_annulment = date('Y-m-d H:i:s');
        $loan->date_lose = date('Y-m-d H:i:s');
        $loan->save();

        $days = Carbon::createFromFormat('d-m-Y', $payments_pay[$index_last]->expiration)->subMonth();
        $days = $days->diffInDays();
        $interest = $capital * $fee * $days;

        $payment = $this->paymentRepo->getModel();
        $payment_manager = new PaymentManager($payment, [
            'loan_id' => $loan->id,
            'amount' => $capital + $interest + $payments_pay[$index_last]->interest,
            'interest' => $interest + $payments_pay[$index_last]->interest,
            'capital' => $capital,
            'expiration' => date('d-m-Y'),
            'state' => 'Pendiente'
        ]);
        $payment_manager->save();

        return \Response::json(['response' => true]);
    }

    public function warrantyLoan()
    {
        $data = \Request::all();
        $loan = $this->loanRepo->find($data['id']);
        $fee = $loan->interest / 30;
        $fee = $fee / 100;
        $capital = $loan->amount - round($this->paymentRepo->sumCapital($loan->id), 2);


        if ($loan->state != 'Vigente' || $loan->warranty_option == 'No') {
            return \Response::json(['response' => false]);
        }

        $payments = $loan->payments()->where('state', 'Pendiente')->get();
        $payments_pay = $loan->payments()->where('state', 'Pagado')->get();
        $index_last = count($payments_pay) - 1;

        if ($index_last < 0) {
            $payments_pay = $payments;
            $index_last = 0;
        }

        foreach ($payments as $payment) {
            $payment->state = 'Garantia';
            $payment->save();
        }

        $loan->state = 'Garantía Ejecutada';
        $loan->date_annulment = date('Y-m-d H:i:s');
        $loan->save();

        $days = Carbon::createFromFormat('d-m-Y', $payments_pay[$index_last]->expiration)->subMonth();
        $days = $days->diffInDays();
        $interest = $capital * $fee * $days;

        $payment = $this->paymentRepo->getModel();
        $payment_manager = new PaymentManager($payment, [
            'loan_id' => $loan->id,
            'amount' => $capital + $interest,
            'interest' => $interest,
            'capital' => $capital,
            'expiration' => date('d-m-Y'),
            'state' => 'Pendiente'
        ]);
        $payment_manager->save();

        return \Response::json(['response' => true]);
    }

    public function editPayment()
    {
        $data = \Request::all();

        if ($data['p'] == 252711) {
            $payment = $this->paymentRepo->find($data['id']);

            $payment->amount = $data['amount'];
            $payment->interest = $data['amount'] - $payment->capital;

            $payment->save();

            $detail = $payment->detail;

            $diference = $detail->amount - $data['amount'];

            $detail->amount = $payment->amount;

            $detail->save();


            $payment_document = $detail->paymentDocument;


            $payment_document->amount = $payment_document->amount - $diference;

            $payment_document->save();


            return \Response::json([true]);

        }
    }

    public function loanReport()
    {
        ini_set('memory_limit', '-1');
        $results = Loan::selectRaw("
        concat(p.name, ' ', p.last_name) as nombre,
       p.dni,
       p.telephone,
       p.address,
       loans.code,
       loans.type_payment as loan_type,
       loans.amount as loan_amount,
       (select sum(payments.capital) as pagadas from payments where payments.loan_id = loans.id and payments.state = 'Pagado') as total_capital,
       (select sum(payments.interest) as pagadas from payments where payments.loan_id = loans.id and payments.state = 'Pagado') as total_interest,
       concat((select count(distinct payments.id) as pagadas from payments where payments.loan_id = loans.id and payments.state = 'Pagado') , '/', loans.quotas) as total_payments,
       (select sum(payments.capital) as pagadas from payments where payments.loan_id = loans.id and payments.state = 'Pendiente') as pending_capital,
       (select sum(payments.amount) as pagadas from payments where payments.loan_id = loans.id and payments.state = 'Pendiente') as total_pending,
       DATE_FORMAT(loans.date_disbursement, '%d/%m/%Y') as date_loan,
       loans.state as loan_state,
       concat(people.name, ' ', people.last_name) as employee,
       loans.period,
       e2.razon_social
        ")
            ->join('customers as c', 'c.id', '=', 'loans.customer_id')
            ->join('people as p', 'p.id', '=', 'c.customer_id')
            ->join('employees as e', 'e.id', '=', 'loans.employee_id')
            ->join('people', 'people.id', '=', 'e.person_id')
            ->leftJoin('enterprises as e2', 'loans.enterprise_id', '=', 'e2.id')
            ->where('loans.id','>',0)
            ->warranty(\Request::get('warranty'))
            ->aval(\Request::get('aval'))
            ->type(\Request::get('type'))
            ->filter(\Request::get('interest'))
            ->filterState(\Request::get('state'))
            ->filterModalidad(\Request::get('modalidad'))
            ->filterCustomerType(\Request::get('customer_type'))
            ->groupBy('loans.id')
            ->get();

        $name = 'prestamos-' . date('d-m-Y-H-i-s') . '.xlsx';

        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();
        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial')->setSize(12);
        $sheet->getStyle('1:1')->getFont()->setBold(true);

        $headers = [
            'NOMBRE',
            'DNI',
            'TELEFONO',
            'DIRECCIÓN',
            'EMPRESA',
            'CÓDIGO DE PRESTAMO',
            'TIPO DE PRESTAMO',
            'MONTO DE PRESTAMO',
            'CAPITAL PAGADO',
            'INTERES PAGADO',
            'CUOTAS PAGADAS',
            'CAPITAL PENDIENTE',
            'TOTAL PENDIENTE',
            'FECHA DE PRESTAMO',
            'ESTADO DEL PRESTAMO',
            'ASESOR',
            'PERIODO'
        ];


        foreach ($headers as $key => $header) {
            $sheet->setCellValueByColumnAndRow(1 + $key, 1, $header);
        }


        foreach ($results as $key => $item) {

            if ($item->loan_state == 'Perdida') {
                $item->loan_state = 'C. Pesada';
            }

            if ($item->loan_state == 'Perdida Cancelado') {
                $item->loan_state = 'C. Pesada Cancelado';
            }

            if ($item->loan_state == 'Atrasado') {
                $item->loan_state = 'Perdida';
            }

            $values = [
                $item->nombre,
                $item->dni,
                $item->telephone,
                $item->address,
                $item->razon_social,
                $item->code,
                $item->loan_type,
                $item->loan_amount,
                $item->total_capital,
                $item->total_interest,
                $item->total_payments,
                $item->pending_capital,
                $item->total_pending,
                $item->date_loan,
                $item->loan_state,
                $item->employee,
                $item->period
            ];

            foreach ($headers as $index => $header) {
                $sheet->setCellValueByColumnAndRow(1 + $index, $key + 2, $values[$index]);
            }
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $name . '"');
        header('Cache-Control: max-age=0');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;

    }


    public function toLose()
    {
        $id = \Request::get('id');

        $loan = Loan::find($id);

        if($loan->state != 'Vigente')
        {
            return  \Response::json(['response' => false]);
        }

        $loan->state = 'Atrasado';
        $loan->save();

        return  \Response::json(['response' => true]);


    }

    public function toOkLoan()
    {
        $id = \Request::get('id');
        $loan = Loan::find($id);

        if($loan->state != 'Atrasado')
        {
            return  \Response::json(['response' => false]);
        }

        $loan->state = 'Vigente';
        $loan->save();

        return  \Response::json(['response' => true]);
    }

    public function toLoseTotal(){
        $id = \Request::get('id');
        $loan = Loan::find($id);
        $loan->state = 'Perdida Total';
        $loan->save();

        return  \Response::json(['response' => true]);
    }

} 
