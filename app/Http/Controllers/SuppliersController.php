<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;
use App\Core\Managers\SupplierManager;
use App\Core\Repositories\SupplierRepo;
use Illuminate\Routing\Route;

class SuppliersController extends Controller {

    protected $supplierRepo;
    protected $supplier;

    public function __construct(SupplierRepo $supplierRepo){
        $this->supplierRepo = $supplierRepo;
    }

    public function findSupplier($id)
    {
        $this->supplier = $this->supplierRepo->find($id);
    }


    public function index()
    {
        return \View::make('suppliers.index');
    }

    public function formCreate()
    {
        return \View::make('suppliers.create');
    }

    public function formEdit()
    {
        return \View::make('suppliers.edit');
    }

    public function all()
    {
        $suppliers = $this->supplierRepo->paginate2(25,\Request::get('type'));

        return \Response::json($suppliers);
    }

    public function create()
    {
        $supplier = $this->supplierRepo->getModel();
        $data = \Request::all();

        if(isset($data['supplier']))
        {
            $data['supplier_id'] = $data['supplier']['originalObject']['id'];
            $supplier_validation = $this->supplierRepo->validateSupplier($data['type'],$data['supplier_id']);

            if($supplier_validation)
            {
                return \Response::json(['response'=>false,'errors'=>'El cliente ya existe']);
            }
        }

        $data['supplier_type'] = substr('App\Core\Entities\u',0,-1).ucfirst($data['type']);

        $manager = new SupplierManager($supplier,$data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function find($id)
    {
        $this->findSupplier($id);
        return \Response::json($this->supplier);
    }

    public function edit($id)
    {
        $this->findSupplier($id);
        $data = \Request::all();

        if(isset($data['supplier']))
        {
            $data['supplier_id'] = $data['supplier']['originalObject']['id'];
            $supplier_validation = $this->supplierRepo->validateSupplier($data['type'],$data['supplier_id'],
                                    $this->supplier->id);

            if($supplier_validation)
            {
                return \Response::json(['response'=>false,'errors'=>['El cliente ya existe']]);
            }
        }

        $data['supplier_type'] = substr('App\Core\Entities\u',0,-1).ucfirst($data['type']);

        $manager = new SupplierManager($this->supplier, $data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function delete($id)
    {
        $this->findSupplier($id);
        $this->supplier->delete();

        return \Response::json(['response'=>true, 'data'=>$this->supplier]);
    }

} 