<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;
use App\Core\Managers\EnterpriseManager;
use App\Core\Repositories\EnterpriseRepo;
use Illuminate\Routing\Route;

class EnterprisesController extends Controller {

    protected $enterpriseRepo;
    protected $enterprise;

    public function __construct(EnterpriseRepo $enterpriseRepo){
        $this->enterpriseRepo = $enterpriseRepo;
    }

    public function findEnterprise($id)
    {
        $this->enterprise = $this->enterpriseRepo->find($id);
    }


    public function index()
    {
        return \View::make('enterprises.index');
    }

    public function formCreate()
    {
        return \View::make('enterprises.create');
    }

    public function formEdit()
    {
        return \View::make('enterprises.edit');
    }

    public function all()
    {
        $enterprises = $this->enterpriseRepo->paginate(25);

        return \Response::json($enterprises);
    }

    public function create()
    {
        $enterprise = $this->enterpriseRepo->getModel();
        $data = \Request::all();

        $data['department_id']= $data['department']['id'];
        $data['province_id'] = $data['province']['id'];
        $data['district_id'] = $data['district']['id'];
        $manager = new EnterpriseManager($enterprise,$data);
        $response = $manager->save();

        if(count($data['people']) > 0)
        {
            foreach ($data['people'] as $person)
            {
                $enterprise->people()->attach($person['person']['originalObject']['id'], ['cargo'=>$person['cargo']]);
            }
        }

        \Session::set('Enterprise_'.\Auth::user()->id, $enterprise->id);

        return \Response::json($response);
    }

    public function find($id)
    {
        $this->findEnterprise($id);
        return \Response::json($this->enterprise);
    }

    public function edit($id)
    {
        $this->findEnterprise($id);
        $data = \Request::all();
        $data['department_id']= $data['department']['id'];
        $data['province_id'] = $data['province']['id'];
        $data['district_id'] = $data['district']['id'];
        $manager = new EnterpriseManager($this->enterprise, $data);
        $response = $manager->save();

        $this->enterprise->people()->detach();

        if(count($data['people']) > 0)
        {
            foreach ($data['people'] as $person)
            {
                $this->enterprise->people()->attach($person['person']['originalObject']['id'], ['cargo'=>$person['cargo']]);
            }
        }

        \Session::set('Enterprise_'.\Auth::user()->id, $this->enterprise->id);

        return \Response::json($response);
    }

    public function delete($id)
    {
        $this->findEnterprise($id);
        $this->enterprise->delete();

        return \Response::json(['response'=>true, 'data'=>$this->enterprise]);
    }

    public function printFile()
    {
        $id = \Request::get('enterprise-id');
        $this->findEnterprise($id);
        $enterprise = $this->enterprise;

        $outputName = md5(microtime());

        $ext = ".pdf";

        return \PDF::loadView('pdf.enterpriseInfo', compact('enterprise'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom',10)
            ->setOption('margin-right',4)
            ->setOption('viewport-size','1366x768')
            ->stream($outputName.$ext);
    }

    public function uploadPhoto()
    {

        $enterprise_id = \Session::get('Enterprise_'.\Auth::user()->id);

        \Session::forget('Enterprise_'.\Auth::user()->id);

        $enterprise = $this->enterpriseRepo->find($enterprise_id);

        $file = \Request::file('file');
        $token = $enterprise->ruc;
        $image = \Image::make($file);
        $image->resize(400, 400);
        $image->save(base_path('public/enterprise-img').'/'.$token.'.jpg');
        $enterprise->signature = 'enterprise-img/'.$token.'.jpg';
        $enterprise->save();

        return \Response::json(['response'=>true]);

    }

} 