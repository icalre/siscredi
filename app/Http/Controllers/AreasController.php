<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;
use App\Core\Managers\AreaManager;
use App\Core\Repositories\AreaRepo;
use Illuminate\Routing\Route;

class AreasController extends Controller {

    protected $areaRepo;
    protected $area;

    public function __construct(AreaRepo $areaRepo){
        $this->areaRepo = $areaRepo;
    }

    public function findArea($id)
    {
        $this->area = $this->areaRepo->find($id);
    }


    public function index()
    {
        return \View::make('areas.index');
    }

    public function formCreate()
    {
        return \View::make('areas.create');
    }

    public function formEdit()
    {
        return \View::make('areas.edit');
    }

    public function all()
    {
        $areas = $this->areaRepo->paginate(25);

        return \Response::json($areas);
    }

    public function create()
    {
        $area = $this->areaRepo->getModel();
        $data = \Request::all();
        $manager = new AreaManager($area,$data);
        $response = $manager->save();

        $sites = array();
        foreach($data['sites'] as $site)
        {
            $sites[] = $site['id'];
        }

        $area->sites()->sync($sites);

        return \Response::json($response);
    }

    public function find($id)
    {
        $this->findArea($id);
        return \Response::json($this->area);
    }

    public function edit($id)
    {
        $this->findArea($id);
        $data = \Request::all();
        $manager = new AreaManager($this->area, $data);
        $response = $manager->save();

        $sites = array();
        foreach($data['sites'] as $site)
        {
            $sites[] = $site['id'];
        }

        $this->area->sites()->sync($sites);

        return \Response::json($response);
    }

    public function delete($id)
    {
        $this->findArea($id);
        $this->area->delete();

        return \Response::json(['response'=>true, 'data'=>$this->area]);
    }

} 