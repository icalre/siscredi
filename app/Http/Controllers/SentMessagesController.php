<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;

use App\Core\Managers\SentMessageManager;
use App\Core\Repositories\SentMessageRepo;
use App\Core\Services\SmsService;

use Nexmo\Client\Credentials\Basic;
use Nexmo\Client;

class SentMessagesController extends Controller
{

    protected $sentMessageRepo;
    protected $sentMessage;

    public function __construct(SentMessageRepo $sentMessageRepo)
    {
        $this->sentMessageRepo = $sentMessageRepo;
    }

    public function findSentMessage($id)
    {
        $this->sentMessage = $this->sentMessageRepo->find($id);
    }


    public function index()
    {
        return \View::make('sent-messages.index');
    }

    public function formCreate()
    {
        return \View::make('sent-messages.create');
    }

    public function all()
    {

        /*$sms = $this->sentMessageRepo->all();

        foreach ($sms as $row)
        {
            $basic = new Basic('f0d728bb','de55a8dd527021fd');
            $client = new Client($basic);

            $message = $client->message()->send([
                'to' => '51970998118',
                'from' => 'Crediperu',
                'text' => 'acabo'
            ]);
        }

        dd($sms);*/


        $sentMessages = $this->sentMessageRepo->paginateFilter(25, \Request::get('message'));

        return \Response::json($sentMessages);
    }

    public function create(SmsService $smsService)
    {


        try {

            $data = \Request::all();

            $response = ['response' => false];

            foreach ($data['people'] as $person) {
                $response = $smsService->sendSms('+51' . $person['telephone'], $data['message'], $data['type']['value']);
                $sentMessage = $this->sentMessageRepo->getModel();

                $prefix = 'SMS: ';

                if($data['type']['value']== 2){
                    $prefix = 'WHATSAPP: ';
                }

                $manager = new SentMessageManager($sentMessage, [
                    'message' => $prefix.$data['message'],
                    'person_id' => $person['id']
                ]);

                $manager->save();
            }

            return \Response::json($response);


        } catch (\Exception $exception) {

            return \Response::json(['response' => false,
                'errors' => [$exception->getMessage()]]);

        }


    }

    public function find($id)
    {
        $this->findSentMessage($id);
        return \Response::json($this->sentMessage);
    }


}
