<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;
use App\Core\Managers\SiteManager;
use App\Core\Repositories\SiteRepo;
use Illuminate\Routing\Route;

class SitesController extends Controller {

    protected $siteRepo;
    protected $site;

    public function __construct(SiteRepo $siteRepo){
        $this->siteRepo = $siteRepo;
    }

    public function findSite($id)
    {
        $this->site = $this->siteRepo->find($id);
    }


    public function index()
    {
        return \View::make('sites.index');
    }

    public function formCreate()
    {
        return \View::make('sites.create');
    }

    public function formEdit()
    {
        return \View::make('sites.edit');
    }

    public function all()
    {
        $sites = $this->siteRepo->paginate(25);

        return \Response::json($sites);
    }

    public function create()
    {
        $site = $this->siteRepo->getModel();
        $data = \Request::all();
        $data['company_id'] = $data['company']['id'];
        $manager = new SiteManager($site,$data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function find($id)
    {
        $this->findSite($id);
        return \Response::json($this->site);
    }

    public function edit($id)
    {
        $this->findSite($id);
        $data = \Request::all();
        $data['company_id'] = $data['company']['id'];
        $manager = new SiteManager($this->site, $data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function delete($id)
    {
        $this->findSite($id);
        $this->site->delete();

        return \Response::json(['response'=>true, 'data'=>$this->site]);
    }

} 