<?php

namespace App\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;

use App\Core\Repositories\AreaRepo;
use App\Core\Repositories\CompanyRepo;
use App\Core\Repositories\CustomerRepo;
use App\Core\Repositories\DepartmentRepo;
use App\Core\Repositories\EmployeeRepo;
use App\Core\Repositories\EnterpriseRepo;
use App\Core\Repositories\AreaJobRepo;
use App\Core\Repositories\LoanRepo;
use App\Core\Repositories\ModuleRepo;
use App\Core\Repositories\PersonRepo;
use App\Core\Repositories\SiteRepo;
use App\Core\Repositories\SupplierRepo;
use App\Core\Repositories\UserRepo;
use App\Core\Repositories\ProfileRepo;
use Carbon\Carbon;
use Peru\Reniec\Dni;
use Peru\Sunat\Ruc;

class ApiController extends Controller
{

    public function reniec()
    {

        $dni = \Request::get('dni');

        $token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.MTAyNw.eOcWxQnDe_v6dVr5z4zagaPeXOqWQDn0b50XawJ5Qjs';
        $query = "
                    query {
                        persona(dni:\"$dni\") {
                            pri_nom,
                            seg_nom,
                            ap_pat,
                            ap_mat
                        }
                    }";

        $body = json_encode($query);
        $headers = [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($body),
            'Authorization: Bearer ' . $token,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://quertium.com/api/v1/reniec/dni");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $jsonString = curl_exec($ch);
        curl_close($ch);
        $out = json_decode($jsonString);

        if (!$out->data) {
            return \Response::json(['response' => false]);
        }

        $res = $out->data->persona;


        return \Response::json([
            'response' => true,
            'data' => [
                'nombres' => $res->seg_nom ? $res->pri_nom . ' ' . $res->seg_nom : $res->pri_nom,
                'apellidoPaterno' => $res->ap_pat,
                'apellidoMaterno' => $res->ap_mat
            ]
        ]);
    }

    public function sunat()
    {
        $ruc = new Ruc();

        $enterprise = $ruc->get(\Request::get('ruc'));

        if (!$enterprise) {
            return \Response::json(['response' => false]);
        }

        $enterprise->razonSocial = substr($enterprise->razonSocial, 2);


        return \Response::json(['response' => true, 'data' => $enterprise]);
    }


    public function departments(DepartmentRepo $departmentRepo)
    {
        $departments = $departmentRepo->all();
        return \Response::json($departments);
    }

    public function people(PersonRepo $personRepo)
    {
        $people = $personRepo->search(\Request::get('q'), 10);

        return \Response::json($people);
    }

    public function users(UserRepo $userRepo)
    {
        $users = $userRepo->search(\Request::get('q'), 10);

        return \Response::json($users);
    }

    public function profiles(ProfileRepo $profileRepo)
    {
        $profiles = $profileRepo->all();

        return \Response::json($profiles);
    }

    public function enterprises(EnterpriseRepo $enterpriseRepo)
    {
        $enterprises = $enterpriseRepo->search(\Request::get('q'), 10);

        return \Response::json($enterprises);
    }

    public function customers(CustomerRepo $customerRepo)
    {
        $customers = $customerRepo->search(\Request::get('type'), \Request::get('q'), \Request::get('rate'), 10);
        return \Response::json($customers);
    }

    public function areas(AreaRepo $areaRepo)
    {
        $q = \Request::get('q');
        if (isset($q)) {
            $areas = $areaRepo->search($q, 10);
        } else {
            $areas = $areaRepo->all();
        }

        return \Response::json($areas);
    }

    public function jobs(AreaJobRepo $jobRepo)
    {
        $jobs = $jobRepo->search(\Request::get('type'), \Request::get('q'));

        return \Response::json($jobs);
    }

    public function employees(EmployeeRepo $employeeRepo)
    {
        $q = \Request::get('q');

        if (!empty($q)) {
            $employees = $employeeRepo->search(\Request::get('q'), 10);
        } else {
            $employees = $employeeRepo->all();

            foreach ($employees as $employee) {
                $employee->person_name = $employee->person->customerName;
            }
        }

        return \Response::json($employees);
    }

    public function companies(CompanyRepo $companyRepo)
    {
        $companies = $companyRepo->all();

        return \Response::json($companies);
    }

    public function sites(SiteRepo $siteRepo)
    {
        $sites = $siteRepo->all();
        return \Response::json($sites);
    }

    public function suppliers(SupplierRepo $supplierRepo)
    {
        $suppliers = $supplierRepo->search(\Request::get('type'), \Request::get('q'), 10);
        return \Response::json($suppliers);
    }

    public function modules(ModuleRepo $moduleRepo)
    {
        $modules = $moduleRepo->all();

        $modules_print = array();
        $i = 0;

        foreach ($modules as $module) {
            $modules_print[$i] = [
                'name' => $module->name,
                'icon' => $module->icon,
            ];

            $modules_print[$i]['submodules'] = array();

            foreach ($module->submodules as $submodule) {
                $modules_print[$i]['submodules'][] = [
                    'name' => $submodule->name,
                    'uri' => $submodule->uri,
                    'link_menu' => $submodule->link_menu
                ];
            }

            $i++;
        }

        dd($modules_print);
    }


    public function generateSchedule()
    {
        $data = \Request::all();
        $quotas = array();
        $amount = $data['amount'];
        $period = 0;
        $data['interest'] = $data['interest'];
        $fee_ini = $data['interest'] / 100;
        $number_quotas = $data['quotas'];

        if ($data['period'] == 'Mensual') {
            $period = 1;
        } else if ($data['period'] == 'Semanal') {
            $period = 7;
            $data['quotas'] = $data['quotas'] / 4;
        } else if ($data['period'] == 'Quincenal') {
            $period = 15;
            $fee_ini = $fee_ini / 2;
        }

        if ($fee_ini == 0) {
            $quota = $amount / $data['quotas'];
        } else {
            $quota = $amount * ($fee_ini * pow(1 + $fee_ini, $data['quotas']) / (pow(1 + $fee_ini, $data['quotas']) - 1));
            $quota = round($quota, 2);
        }


        $date = Carbon::createFromFormat('d-m-Y', $data['date_start']);

        $fee_add = 0;
        $flag_fee_add = 0;
        $diff_days = 0;

        if (isset($data['date_disbursement'])) {
            $date_dis = Carbon::createFromFormat('d-m-Y', $data['date_disbursement']);
        } else {
            $date_dis = Carbon::now();
        }

        if (!isset($data['feed_option'])) {
            $diff_days = Carbon::createFromFormat('d-m-Y', $data['date_start'])->diffInDays($date_dis, false);
        }


        if ($diff_days < 0) {
            $fee_add = $diff_days * (-1) * (($data['interest'] / 28) / 100) * $data['amount'];
        }


        for ($i = 0; $i < $number_quotas; $i++) {
            $interest = $amount * $fee_ini;
            $capital = $quota - $interest;
            if ($data['period'] == 'Mensual') {
                $vcto = $date->addMonth($period)->format('d-m-Y');
                $day = $date->dayOfWeek;
                if ($day == 0) {
                    $new_date = Carbon::createFromFormat('d-m-Y', $vcto);
                    $vcto = $new_date->addDays(1)->format('d-m-Y');
                }

                $quotas[] = [
                    'expiration' => $vcto,
                    'amount' => $quota + $fee_add,
                    'capital' => round($capital, 2),
                    'interest' => round($interest, 2) + $fee_add,
                    'state' => 'Pendiente'
                ];
            } else if ($data['period'] == 'Semanal') {
                $rest_capital = ($i + 1) % 4;
                $vcto = $date->addDays($period)->format('d-m-Y');
                $day = $date->dayOfWeek;
                if ($day == 0) {
                    $new_date = Carbon::createFromFormat('d-m-Y', $vcto);
                    $vcto = $new_date->addDays(1)->format('d-m-Y');
                }
                $quotas[] = [
                    'expiration' => $vcto,
                    'amount' => $quota / 4 + $fee_add / 4,
                    'capital' => round($capital / 4, 2),
                    'interest' => round($interest / 4, 2) + $fee_add / 4,
                    'state' => 'Pendiente'
                ];
            } else if ($data['period'] == 'Quincenal') {
                $vcto = $date->addDays($period)->format('d-m-Y');
                $day = $date->dayOfWeek;
                if ($day == 0) {
                    $new_date = Carbon::createFromFormat('d-m-Y', $vcto);
                    $vcto = $new_date->addDays(1)->format('d-m-Y');
                }

                $quotas[] = [
                    'expiration' => $vcto,
                    'amount' => $quota + $fee_add,
                    'capital' => round($capital, 2),
                    'interest' => round($interest, 2) + $fee_add,
                    'state' => 'Pendiente'
                ];
            }

            if ($data['period'] == 'Semanal' && $rest_capital == 0) {
                $amount = $amount - $capital;
            } else if ($data['period'] == 'Quincenal') {
                $amount = $amount - $capital;
            } else if ($data['period'] == 'Mensual') {
                $amount = $amount - $capital;
            }

            if ($flag_fee_add == 0) {
                $fee_add = 0;
            }

            $flag_fee_add++;
        }

        return \Response::json(['response' => true, 'quotas' => $quotas]);
    }


    public function loans(LoanRepo $loanRepo)
    {
        $q = \Request::get('q');
        $loans = $loanRepo->search(
            $q,
            \Request::get('type'),
            \Request::get('warranty'),
            \Request::get('state'),
            \Request::get('aval'),
            \Request::get('interest')
        );

        return \Response::json($loans);
    }

    public function years()
    {
        $years = array();
        $year_ini = 2015;
        for ($i = 0; $i < 10; $i++) {
            $years[] = ['id' => $i, 'name' => $year_ini];
            $year_ini++;
        }

        return $years;
    }

    public function months()
    {
        $months = array(
            ['id' => 1, 'name' => 'Enero'],
            ['id' => 2, 'name' => 'Febrero'],
            ['id' => 3, 'name' => 'Marzo'],
            ['id' => 4, 'name' => 'Abril'],
            ['id' => 5, 'name' => 'Mayo'],
            ['id' => 6, 'name' => 'Junio'],
            ['id' => 7, 'name' => 'Julio'],
            ['id' => 8, 'name' => 'Agosto'],
            ['id' => 9, 'name' => 'Septiembre'],
            ['id' => 10, 'name' => 'Octubre'],
            ['id' => 11, 'name' => 'Noviembre'],
            ['id' => 12, 'name' => 'Diciembre']
        );

        return $months;
    }
}
