<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;
use App\Core\Components\MainMenu;
use App\Core\Managers\PersonManager;
use App\Core\Repositories\PersonRepo;
use Illuminate\Routing\Route;

class PeopleController extends Controller {

    protected $personRepo;
    protected $person;
    protected $mainMenu;

    public function __construct(PersonRepo $personRepo, MainMenu $mainMenu ){
        $this->personRepo = $personRepo;
        $this->mainMenu = $mainMenu;
    }

    public function findPerson($id)
    {
        $this->person = $this->personRepo->find($id);
    }


    public function index()
    {
        return \View::make('people.index');
    }

    public function formCreate()
    {
        return \View::make('people.create');
    }

    public function formEdit()
    {
        return \View::make('people.edit');
    }

    public function all()
    {
        $people = $this->personRepo->paginate(25);

        return \Response::json($people);
    }

    public function create()
    {
        $person = $this->personRepo->getModel();
        $data = \Request::all();
        $data['department_id']= $data['department']['id'];
        $data['province_id'] = $data['province']['id'];
        $data['district_id'] = $data['district']['id'];
        $data['gender'] = $data['gender']['value'];
        $manager = new PersonManager($person,$data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function find($id)
    {
        $this->findPerson($id);
        return \Response::json($this->person);
    }


    public function printFile()
    {
        $id = \Request::get('person-id');
        $this->findPerson($id);
        $person = $this->person;
        $person->file = 1;
        $person->save();

        $outputName = md5(microtime());

        $ext = ".pdf";

        return \PDF::loadView('pdf.personInfo', compact('person'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom',10)
            ->setOption('margin-right',4)
            ->setOption('viewport-size','1366x768')
            ->stream($outputName.$ext);
    }

    public function edit($id)
    {
        $menu = $this->mainMenu->menu(\Auth::user()->profile_id);

        $sub_modules = $menu['sub_modules'];

        if(!in_array('app/edit-person', $sub_modules))
        {
            return \Response::json(['response'=>false,'errors'=>['No estas autorizado para editar.']]);
        }

        $this->findPerson($id);
        $data = \Request::all();
        $data['department_id']= $data['department']['id'];
        $data['province_id'] = $data['province']['id'];
        $data['district_id'] = $data['district']['id'];
        $data['gender'] = $data['gender']['value'];
        $manager = new PersonManager($this->person, $data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function delete($id)
    {
        $this->findPerson($id);
        $this->person->delete();

        return \Response::json(['response'=>true, 'data'=>$this->person->full_name]);
    }

} 