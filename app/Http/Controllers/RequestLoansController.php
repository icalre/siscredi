<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;
use App\Core\Managers\RequestLoanManager;
use App\Core\Repositories\PersonRepo;
use App\Core\Repositories\RequestLoanRepo;
use App\Core\Components\MainMenu;
use Carbon\Carbon;

use Illuminate\Routing\Route;

class RequestLoansController extends Controller {

    protected $requestLoanRepo;
    protected $mainMenu;


    public function __construct(RequestLoanRepo $requestLoanRepo,MainMenu $mainMenu)
    {
        $this->requestLoanRepo = $requestLoanRepo;
        $this->mainMenu = $mainMenu;
    }
    

    public function findRequestLoan($id)
    {
        $this->requestLoan = $this->requestLoanRepo->find($id);
    }


    public function index()
    {
        return \View::make('request-loans.index');
    }

    public function formCreate()
    {
        return \View::make('request-loans.create');
    }

    public function formEdit()
    {
        return \View::make('request-loans.edit');
    }

    public function all(PersonRepo $personRepo)
    {
        $employee_id = NULL;
        $menu = $this->mainMenu->menu(\Auth::user()->profile_id);

        $sub_modules = $menu['sub_modules'];

        if(!in_array('app/all-request-loans', $sub_modules))
        {
            $person = $personRepo->find(\Auth::user()->person_id);
            $employee = $person->employee;
            $employee_id = $employee->id;
        }

        $requestLoans = $this->requestLoanRepo->paginateFilter(25,$employee_id);

        return \Response::json($requestLoans);
    }

    public function create(PersonRepo $personRepo)
    {

        $requestLoan = $this->requestLoanRepo->getModel();
        $data = \Request::all();

        $data['person_id'] = $data['person']['originalObject']['id'];
        $person = $personRepo->find(\Auth::user()->person_id);
        $employee = $person->employee;

        if(!isset($employee->id))
        {
            return \Response::json(['response'=>false,'errors'=>['No estas registrado como trabajador de la empresa.']]);
        }else{
            $data['employee_id'] = $employee->id;
        }

        $manager = new RequestLoanManager($requestLoan,$data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function find($id)
    {
        $this->findRequestLoan($id);
        return \Response::json($this->requestLoan);
    }

    public function delete($id)
    {
        $this->findRequestLoan($id);
        $this->requestLoan->delete();

        return \Response::json(['response'=>true, 'data'=>$this->requestLoan]);
    }

    public function show($id)
    {
        $this->findRequestLoan($id);
        $request_loan = $this->requestLoan;

       $outputName = md5(microtime());


        return \PDF::loadView('pdf.request-loan',compact('request_loan'))->setPaper('a4')
            ->setOption('margin-top', 10)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom',10)
            ->setOption('margin-right',4)
            ->setOption('viewport-size','1366x768')
            ->stream($outputName);
    }

} 