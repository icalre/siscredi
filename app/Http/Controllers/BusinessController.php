<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;

use App\Core\Repositories\CustomerRepo;
use App\Core\Repositories\EmployeeRepo;
use App\Core\Repositories\EnterpriseRepo;
use App\Core\Repositories\LoanRepo;
use App\Core\Components\MainMenu;
use App\Core\Repositories\PaymentRepo;
use App\Core\Repositories\PersonRepo;
use Carbon\Carbon;

class BusinessController extends Controller {

    protected $mainmenu;
    protected $customerRepo;
    protected $employeeRepo;
    protected $paymentRepo;
    protected $loanRepo;

    public function __construct(MainMenu $mainMenu, CustomerRepo $customerRepo, EmployeeRepo $employeeRepo,
        PaymentRepo $paymentRepo, LoanRepo $loanRepo)
    {
        $this->mainMenu =  $mainMenu;
        $this->customerRepo = $customerRepo;
        $this->employeeRepo = $employeeRepo;
        $this->paymentRepo = $paymentRepo;
        $this->loanRepo = $loanRepo;
    }

    public function index()
    {
        $menu = $this->mainMenu->menu(\Auth::user()->profile_id);
        $employees = $this->employeeRepo->all();

        foreach ($employees as $employee)
        {
            $employee->loan_counts = count($employee->loans()->whereIn('state',['Vigente', 'Atrasado'])->groupBy('customer_id')->get());
        }

        //dd($employees);

        $sub_modules = $menu['sub_modules'];

        return \View::make('business.index', compact('sub_modules', 'employees'));
    }

    public function printSchedule(LoanRepo $loanRepo)
    {
        $data = \Request::all();

        $loan = $loanRepo->find($data['loan-id']);
        $artefactosA = array();
        $otrosA = array();
        $joyasA = array();

        if ($loan->warranty_option == 'Si')
        {

            foreach ($loan->warranties as $warranty)
            {
                if($warranty->type == 'Artefacto')
                {
                    $artefactosA[] = $warranty;
                }elseif ($warranty->type == 'Otro')
                {
                    $otrosA[] = $warranty;
                }elseif ($warranty->type == 'Joya')
                {
                    $joyasA[] = $warranty;
                }
            }

            $loan->artefactosA = $artefactosA;
            $loan->otrosA = $otrosA;
            $loan->joyasA = $joyasA;
        }

        $outputName = md5(microtime());
        $ext = "pdf";


        return \PDF::loadView('pdf.schedule2',compact('loan'))->setPaper('a4')
            ->setOption('margin-top', 10)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom',10)
            ->setOption('margin-right',4)
            ->setOption('viewport-size','1366x768')
            ->stream($outputName.'.'.$ext);
    }

    public function printSimulated()
    {
        $data=\Request::all();


        if(isset($data['loan-id']))
        {

            $code = $data['loan-id'];
            $data = \Session::get($code);


            if($data['currency'] == 'Soles')
            {
                $data['currency_format'] = 'S/.';
            }else{
                $data['currency_format'] = '$/.';
            }
            return \PDF::loadView('pdf.scheduleS',compact('data'))->setPaper('a4')
                ->setOption('margin-top', 10)
                ->setOption('margin-left', 15)
                ->setOption('margin-bottom',10)
                ->setOption('margin-right',4)
                ->setOption('viewport-size','1366x768')
                ->stream($code);
        }else{
            $name = md5(microtime());
            \Session::set($name, $data);

            return \Response::json(['response'=>true, 'code'=>$name]);
        }
    }

    public function commission(PersonRepo $personRepo)
    {
            $data = \Request::all();
            $menu = $this->mainMenu->menu(\Auth::user()->profile_id);
            $sub_modules = $menu['sub_modules'];

            $data['date1']= Carbon::createFromFormat('d-m-Y', $data['date1'])->format('Y-m-d');
            $data['date2']  = Carbon::createFromFormat('d-m-Y', $data['date2'])->format('Y-m-d');

            if(!in_array('app/comissions', $sub_modules)){
                $person = $personRepo->find(\Auth::user()->person_id);
                $data['employee'] = $person->employee->id;
            }

            $customers = $this->customerRepo->commission($data['employee'],$data['date1'], $data['date2']);
            $employee = $this->employeeRepo->find($data['employee']);

            foreach($customers as $customer)
            {
                $customer->interest = 0;
                $customer->enterprise = '';
                foreach($customer->loans as $loan)
                {
                    foreach($loan->payments as $payment)
                    {
                        if($employee->id != 8)
                        {
                            $customer->interest =  $customer->interest + $payment->interest;
                        }else{
                            $customer->interest =  $customer->interest + $payment->amount;
                        }

                    }

                    if(isset($loan->enterprise->id))
                    {
                      $customer->enterprise = $loan->enterprise->razon_social;
                    }
                }
            }

            $outputName = md5(microtime());
            $ext = "pdf";

            $data['date1']= Carbon::createFromFormat('Y-m-d', $data['date1'])->format('d-m-Y');
            $data['date2']  = Carbon::createFromFormat('Y-m-d', $data['date2'])->format('d-m-Y');

            return \PDF::loadView('pdf.commission',compact('customers','data', 'employee'))->setPaper('a4')
                ->setOption('margin-top', 7)
                ->setOption('margin-left', 15)
                ->setOption('margin-bottom',10)
                ->setOption('margin-right',4)
                ->setOption('footer-center', 'Pagina [page]')
                ->setOption('viewport-size','1366x768')
                ->stream($outputName.'.'.$ext);
    }

    public function payments( PersonRepo $personRepo, EnterpriseRepo $enterpriseRepo)
    {
        $date1 = \Request::get('date1');
        $date2 = \Request::get('date2');
        $data = \Request::all();
        $id = '';
        $enterprise_id = $data['enterprise-id'];
        $enterprise = '';

        if(isset($enterprise_id))
        {
            $enterprise = $enterpriseRepo->find($enterprise_id);
        }


        $menu = $this->mainMenu->menu(\Auth::user()->profile_id);

        $sub_modules = $menu['sub_modules'];


        if(isset($data['employee']) && !empty($data['employee']) && in_array('app/comissions', $sub_modules))
        {
            $id  = $data['employee'];
        }else{
            if(!in_array('app/comissions', $sub_modules) && !in_array('app/recaudacion', $sub_modules))
            {
                $person = $personRepo->find(\Auth::user()->person_id);
                $employee = $person->employee;
                $id = $employee->id;
            }
        }

        $date_ini = Carbon::createFromFormat('d-m-Y',$date1)->format('Y-m-d');
        $date_end = Carbon::createFromFormat('d-m-Y',$date2)->format('Y-m-d');

        $payments = array();

        $loans = $this->paymentRepo->payments($date_ini,$date_end,$id, $enterprise_id);

        foreach($loans as $item){
            $fee = $item->interest/30;
            $fee = $fee/100;
            if(count($item->payments) > 0)
            {
                $payment_total = $this->paymentRepo->loanPayments($item->id);

                if(count($payment_total) == 1){
                    $first_payment = true;
                }else{
                    $first_payment = false;
                }

                foreach ($item->payments as $payment)
                {
                    if($item->type_payment == 'Libre'){
                        if($payment->expiration != date('d-m-Y'))
                        {
                            if($first_payment){
                                $date_ini = Carbon::createFromFormat('d-m-Y',$item->date_disbursement);
                            }else{
                                $date_ini = Carbon::createFromFormat('d-m-Y',$payment->expiration)->subMonth();
                            }
                            $days_dif = $date_ini->diffInDays();
                            $interest = $payment->capital * $fee * $days_dif;
                            $payment->interest = round($interest,2);
                            $payment->amount = round($interest,2) + $payment->capital;
                        }
                        $amount = $payment->amount;

                    }else{
                        $amount = $payment->amount;
                    }

                    $last_name_dealer = explode(' ',$item->employee->person->last_name);

                    $person = $personRepo->find($item->employee->person->id);
                    $user = $person->user;

                    $user_color = '';

                    if(isset($user->color))
                    {
                        $user_color = $user->color;
                    }

                    $payments[] = [
                        'code'=>$item->code,
                        'customer'=>$item->customer->customer->last_name.' '.$item->customer->customer->name,
                        'dni'=>$item->customer->customer->dni,
                        'phone'=>$item->customer->customer->telephone,
                        'dealer'=>$last_name_dealer[0].' '.$item->employee->person->name,
                        'amount'=>number_format($amount,2,'.',''),
                        'expiration'=>$payment->expiration,
                        'number'=>$payment->number,
                        'days'=>$payment->days,
                        'color'=> $user_color
                    ];
                }
            }

        }

        usort($payments, function($a, $b ){
            return strtotime($a['expiration']) - strtotime($b['expiration']);
        });

        $date = Carbon::now()->format('d-m-Y');
        $outputName = md5(microtime());

        $ext = "pdf";

        return \PDF::loadView('pdf.visits',compact('payments', 'date','date1','date2', 'enterprise'))->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom',10)
            ->setOption('margin-right',4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size','1366x768')
            ->stream($outputName.'.'.$ext);

    }

    public function printHistorial()
    {
        $loan_id = \Request::get('loan-id');

        $loan = $this->loanRepo->find($loan_id);

        $outputName = md5(microtime());

        $ext = "pdf";

        return \PDF::loadView('pdf.comments',compact('loan'))->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom',10)
            ->setOption('margin-right',4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size','1366x768')
            ->stream($outputName.'.'.$ext);
    }

    public function printComments()
    {
        $customer_id = \Request::get('customer-id');

        $customer = $this->customerRepo->find($customer_id);

        $outputName = md5(microtime());

        $ext = "pdf";

        return \PDF::loadView('pdf.customer-comments',compact('customer'))->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom',10)
            ->setOption('margin-right',4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size','1366x768')
            ->stream($outputName.'.'.$ext);
    }



    public function loanEnterprise(EnterpriseRepo $enterpriseRepo)
    {
        $id = \Request::get('id');

        $result = $enterpriseRepo->getLoans($id);


        return \Response::json($result);
    }

    public function billing()
    {
        $customer_id = \Request::get('customer-id');

        $customer = $this->customerRepo->findCustomerLoans($customer_id);

        $date_now = Carbon::now();

        $billing = array();
        $loans_ok = array();

        $first_payment = false;

        foreach ($customer->loans as $loan)
        {

            if(count($loan->payments) == 1){
                $first_payment = true;
            }else{
                $first_payment = false;
            }


                foreach ($loan->payments as $payment)
                {
                    if(!in_array($loan->code,$loans_ok)) {
                        $date_expiration = Carbon::createFromFormat('d-m-Y', $payment->expiration);
                        $diff_days = $date_expiration->diffInDays($date_now, false);
                        $payment->loan_code = $loan->code;
                        $payment->loan_amount = $loan->amount;
                        $payment->loan_employee = $loan->employee->person->customer_name;
                        $payment->type_payment = $loan->type_payment;
                        $payment->capital_pending = $loan->amount - $this->paymentRepo->sumCapital($loan->id);


                        if ($loan->type_payment == 'Libre') {

                            if ($diff_days <= 0) {
                                $payment->amount = $payment->interest;
                                $payment->capital = 0;
                                $billing[] = $payment;
                            } else {
                                $fee = ($loan->interest) / 30;
                                $fee = $fee / 100;
                                $date_ini = Carbon::createFromFormat('d-m-Y', $payment->expiration)->subMonth();
                                $days_dif = $date_ini->diffInDays();
                                $interest = $payment->capital * $fee * $days_dif;
                                $payment->interest = round($interest, 2);
                                $payment->capital = 0;
                                $payment->amount = round($interest, 2);
                                $billing[] = $payment;
                            }

                            $loans_ok[] = $loan->code;

                        } elseif ($loan->type_payment == 'Fija') {
                            if ($diff_days <= 0) {
                                $billing[] = $payment;
                                $loans_ok[] = $loan->code;
                            } else {
                                $billing[] = $payment;
                            }

                        }
                    }
                }
        }

        usort($billing, function($a, $b ){
            return strtotime($a->expiration) - strtotime($b->expiration);
        });

        $outputName = md5(microtime());

        $ext = "pdf";

        return \PDF::loadView('pdf.billing',compact('customer', 'billing'))->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom',10)
            ->setOption('margin-right',4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size','1366x768')
            ->stream($outputName.'.'.$ext);

    }

    public function printCustomers()
    {
        $employee_id = \Request::get('employee');

        $employee = $this->employeeRepo->find($employee_id);

        $outputName = md5(microtime());

        $ext = "pdf";


        return \PDF::loadView('pdf.customers',compact('employee'))->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom',10)
            ->setOption('margin-right',4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size','1366x768')
            ->stream($outputName.'.'.$ext);
    }
}
