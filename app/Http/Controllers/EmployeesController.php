<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;
use App\Core\Managers\EmployeeManager;
use App\Core\Repositories\EmployeeRepo;
use Illuminate\Routing\Route;

class EmployeesController extends Controller {

    protected $employeeRepo;
    protected $employee;

    public function __construct(EmployeeRepo $employeeRepo){
        $this->employeeRepo = $employeeRepo;
    }

    public function findEmployee($id)
    {
        $this->employee = $this->employeeRepo->find($id);
    }


    public function index()
    {
        return \View::make('employees.index');
    }

    public function formCreate()
    {
        return \View::make('employees.create');
    }

    public function formEdit()
    {
        return \View::make('employees.edit');
    }

    public function all()
    {
        $employees = $this->employeeRepo->paginateFilter(25,\Request::get('site'),\Request::get('area'), \Request::get('job'));

        return \Response::json($employees);
    }

    public function create()
    {
        $employee = $this->employeeRepo->getModel();
        $data = \Request::all();
        $data['area_job_id'] = $data['area_job']['id'];
        $data['area_id'] = $data['area']['id'];
        $data['site_id'] = $data['site']['id'];
        $data['person_id'] = $data['person_id'] = $data['person']['originalObject']['id'];
        $data['state'] = $data['state']['value'];
        $manager = new EmployeeManager($employee,$data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function find($id)
    {
        $this->findEmployee($id);
        return \Response::json($this->employee);
    }

    public function edit($id)
    {
        $this->findEmployee($id);
        $data = \Request::all();
        $data['area_job_id'] = $data['area_job']['id'];
        $data['area_id'] = $data['area']['id'];
        $data['site_id'] = $data['site']['id'];
        $data['person_id'] = $data['person_id'] = $data['person']['originalObject']['id'];
        $data['state'] = $data['state']['value'];
        $manager = new EmployeeManager($this->employee, $data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function delete($id)
    {
        $this->findEmployee($id);
        $this->employee->delete();

        return \Response::json(['response'=>true, 'data'=>$this->employee]);
    }

} 