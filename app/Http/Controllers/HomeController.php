<?php namespace App\Http\Controllers;

use App\Account;
use App\Core\Entities\HistoryCapital;
use App\Core\Entities\HistoryCustomer;
use App\Core\Entities\HistoryPayment;
use App\Core\Repositories\CashDeskDetailRepo;
use App\Core\Repositories\CashDeskExpenseRepo;
use App\Core\Repositories\CashDeskIncomeRepo;
use App\Core\Repositories\CashDeskRepo;
use App\Core\Repositories\CustomerRepo;
use App\Core\Repositories\DisbursementRepo;
use App\Core\Repositories\LoanRepo;
use App\Core\Repositories\PaymentDocumentRepo;
use App\Core\Repositories\PaymentRepo;
use App\Core\Repositories\UserRepo;
use App\Core\Components\MainMenu;
use App\User;
use Carbon\Carbon;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/


	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index(UserRepo $userRepo,CashDeskDetailRepo $cashDeskDetailRepo ,CashDeskRepo $cashDeskRepo, MainMenu $mainMenu,
                          PaymentRepo $paymentRepo, LoanRepo $loanRepo)
	{
	    $user = $userRepo->find(\Auth::user()->id);

        $cash_desks = $cashDeskRepo->all();

        $cash_flow = 0;
        $efectivo = 0;
        $cuenta_corriente = 0;

        foreach ($cash_desks as $cash_desk){
            $cash_desk_detail = $cashDeskDetailRepo->getDetailFlow($cash_desk->id);

            if(isset($cash_desk_detail->id))
            {
                $cash_desk_corriente = 0;

                if($cash_desk_detail->state == 1)
                {
                    $cash_desk_corriente = $cash_desk_detail->paymentDocuments()->where('type_payment', 'Cuenta Corriente')->where('state', 'Correct')->sum('amount');
                }

                $cash_flow = $cash_flow + $cash_desk_detail->total_amount;
                $cuenta_corriente = $cuenta_corriente + $cash_desk_detail->cuenta_corriente + $cash_desk_corriente;

                $efectivo = $cash_flow - $cuenta_corriente;

            }
        }

        $loan = $loanRepo->getModel();

        $capital_pending = 0;
        $loans_process_count = 0;
        $loans_process = $loan->where('state','Vigente')->get();

        foreach($loans_process as $loan)
        {
            $capital_pending = $capital_pending + $loan->amount -$paymentRepo->sumCapital($loan->id);
            $loans_process_count++;
        }

        \Session::set('cash_flow', $cash_flow);
        \Session::set('efectivo', $efectivo);
        \Session::set('cuenta_corriente', $cuenta_corriente);
        \Session::set('capital_pending', $capital_pending);

        $accounts = Account::all();

        $menu = $mainMenu->menu(\Auth::user()->profile_id);

        $sub_modules = $menu['sub_modules'];


		return \Response::View('home.index', compact('user', 'cash_flow', 'efectivo', 'cuenta_corriente', 'sub_modules', 'accounts'));
	}

	public function getMenu(MainMenu $mainMenu)
    {
        $response = $mainMenu->menu(\Auth::user()->profile_id);

        $modules = array();
        $empty_index = array();

        $i = 0;

        foreach ($response['modules'] as $item)
        {
            $modules[$i]['submenu'] = array();

            $valid_module = false;

            $modules[$i] = [
                "text"=> $item->name,
                "sref"=> "#",
                "icon"=> $item->icon
            ] ;

            foreach($item->submodules as $sub_module)
            {
                if($sub_module->link_menu == 1)
                {
                    $modules[$i]['submenu'][] = [
                        "text"=> $sub_module->name,
                        "sref"=> str_replace('/', ".", $sub_module->uri)
                    ];

                    $valid_module = true;
                }
            }

            if(!$valid_module)
            {
                $empty_index[] = $i;
            }

            $i++;
        }

        if(count($empty_index) > 0)
        {
            foreach ($empty_index as $item)
            {
                unset($modules[$item]);
            }
        }

        return \Response::json($modules);
    }

    public function basePath()
    {
        return view('layouts.master');
    }

    public function home(MainMenu $mainMenu)
    {
        $menu = $mainMenu->menu(\Auth::user()->profile_id);

        $sub_modules = $menu['sub_modules'];

        $accounts = Account::all();
        $total = 0;

        foreach ($accounts as $account)
        {
            $total = $total + $account->amount;
        }

        return view('home.inicio', compact('sub_modules', 'total'));
    }

    public function pages($id)
    {
        $v = \Request::get('v');
        if($id == 403)
        {
            $error = 403;
            if($v == 2)
            {
                return view('errors.master-errors', compact('error'));
            }
            return view('errors.403');
        }

    }

    public function profile(UserRepo $userRepo)
    {
        $user = $userRepo->find(\Auth::user()->id);

        return view('home.user-profile', compact('user'));
    }

    public function getReportTargets(PaymentDocumentRepo $paymentRepo, CustomerRepo $customerRepo,
                                     DisbursementRepo $disbursementRepo, CashDeskDetailRepo $cashDeskDetailRepo,
                                        CashDeskIncomeRepo $cashDeskIncomeRepo, CashDeskExpenseRepo $cashDeskExpenseRepo)
    {
        $date = date('Y-m-d');

        $cash_desk_details = $cashDeskDetailRepo->getModel();
        $cash_desk_details = $cash_desk_details->where('state', 1)->get();

        $cash_desk_detail_ids = array();

        foreach ($cash_desk_details as $row)
        {
            $cash_desk_detail_ids[] = $row->id;
        }

        if(empty($payment))
        {
            $payment = 0;
        }

        $customer = $customerRepo->getModel();
        $customer = $customer->whereBetween('created_at', [$date.' 00:00:00', $date.' 23:59:59'])->count();

        $disbursement = $disbursementRepo->getModel();
        $disbursement = $disbursement->whereIn('cash_desk_detail_id', $cash_desk_detail_ids)->sum('amount');

        $payment = $paymentRepo->getModel();
        $payment = $payment->where('state', 'Correct')->whereIn('cash_desk_detail_id', $cash_desk_detail_ids)->sum('amount');

        $cash_desk_incomes = $cashDeskIncomeRepo->getModel();
        $cash_desk_incomes = $cash_desk_incomes->whereIn('cash_desk_detail_id', $cash_desk_detail_ids)->sum('amount');

        $egresos = $cashDeskExpenseRepo->getModel();
        $egresos = $egresos->where('sub_type', 1)->whereIn('cash_desk_detail_id', $cash_desk_detail_ids)->sum('amount');
        $gastos = $cashDeskExpenseRepo->getModel();
        $gastos = $gastos->where('sub_type', 0)->whereIn('cash_desk_detail_id', $cash_desk_detail_ids)->sum('amount');


        if(empty($disbursement))
        {
            $disbursement = 0;
        }

        return \Response::json(compact('payment', 'customer', 'disbursement', 'cash_desk_incomes', 'egresos', 'gastos'));

    }


    public function getReportLinesNumbers (PaymentRepo $paymentRepo, CustomerRepo $customerRepo,
                                           DisbursementRepo $disbursementRepo, HistoryCustomer $historyCustomer,
                                            HistoryPayment $historyPayment)
    {

        $flag  = false;

        $month_current = date('m');
        $current_day = date('Y-m-d');

        $months = [
          ['name'=>'Ene', 'value'=>'01'],
            ['name'=>'Feb', 'value'=>'02'],
            ['name'=>'Mar','value'=>'03'],
            ['name'=>'Abr', 'value'=>'04'],
            ['name'=>'May','value'=>'05'],
            ['name'=>'Jun', 'value'=>'06'],
            ['name'=>'Jul','value'=>'07'],
            ['name'=>'Ago','value'=>'08'],
            ['name'=>'Sep','value'=>'09'],
            ['name'=>'Oct', 'value'=>'10'],
            ['name'=>'Nov', 'value'=>'11'],
            ['name'=>'Dic', 'value'=>'12']
        ];

        $year = date('Y');

        $cuotas_pagadas = [
            "label"=> "Cuotas Pagadas",
            "color"=> "#5ab1ef",
            "data"=> []
        ];
				$flag  = false;

        foreach ($months as $month)
        {

            if($flag)
            {
                $cuotas_pagadas['data'][] = [$month['name'], NULL];
            }else{
                $value = $paymentRepo->getModel()
                    ->whereBetween('date_pay', [$year.'-'.$month['value'].'-01'.' 00:00:00', $year.'-'.$month['value'].'-31'.' 23:59:59'])
                    ->where('state','Pagado')
                    ->count();
                $cuotas_pagadas['data'][] = [$month['name'], $value];
            }

            if($month_current == $month['value'])
            {
                $flag = true;
            }
        }


        $cuotas_vencidas = [
            "label"=> "Cuotas Atrasadas",
            "color"=> "#e80808",
            "data"=> []
        ];

				$flag  = false;

        foreach ($months as $month)
        {

            if($flag)
            {
                $cuotas_vencidas['data'][] = [$month['name'], 0];
            }else{
                $log = $historyPayment->where('year',$year)
                        ->where('month', $month['value'])
                        ->first();

                $current_day = Carbon::createFromFormat('Y-m-d', $current_day)->subDay(1)->format('Y-m-d');

                if($month_current == $month['value'])
                {
                    $value = $paymentRepo->getModel()
                        ->select('payments.loan_id', 'payments.amount', 'payments.interest', 'payments.capital',
                            'payments.expiration', 'payments.date_pay', 'payments.state', 'payments.number', 'payments.mora')
                        ->join('loans','loans.id','=','payments.loan_id')
                        ->where('loans.state','Vigente')
                        ->whereBetween('payments.expiration', ['2009-01-01'.' 00:00:00', Carbon::now()->subDay(1)->format('Y-m-d 00:00:00')])
                        ->where('payments.state','Pendiente')
                        ->count();

                    if(isset($log->id))
                    {
                        $log->count = $value;
                        $log->save();
                    }else{
                        $historyPayment::create(
                            [
                                'count'=>$value,
                                'list'=>'-',
                                'month'=>$month['value'],
                                'name'=>$year.'-'.$month['name'],
                                'year'=>$year,
                                'amount'=>0
                            ]
                        );
                    }
                    $flag = true;
                }else{
                    $value = $log->count;
                }

                $cuotas_vencidas['data'][] = [$month['name'], $value];
            }
        }

        $clientes_nuevos = [
            "label"=> "Clientes Nuevos",
            "color"=> "#f0da14",
            "data"=> [ ]
        ];

				$flag  = false;

        foreach ($months as $month)
        {

            if($flag)
            {
                $clientes_nuevos['data'][] = [$month['name'], NULL];
            }else{
                $value = $customerRepo->getModel()
                        ->whereBetween('created_at', [$year.'-'.$month['value'].'-01'.' 00:00:00', $year.'-'.$month['value'].'-31'.' 23:59:59'])
                        ->count();
                $clientes_nuevos['data'][] = [$month['name'], $value];
            }

            if($month_current == $month['value'])
            {
                $flag = true;
            }
        }

        $clientes= [
            "label"=> "Clientes",
            "color"=> "#1a730b",
            "data"=> [ ]
        ];

        $flag = false;

        foreach ($months as $month)
        {

            if($flag)
            {
                $clientes['data'][] = [$month['name'], NULL];
            }else{
                $log = $historyCustomer->where('year',$year)
                    ->where('month', $month['value'])
                    ->first();

                $current_day = Carbon::createFromFormat('Y-m-d', $current_day)->subDay(1)->format('Y-m-d');

                if($month_current == $month['value'])
                {
                    $value = $customerRepo->getModel()
                        ->select('customers.id','loans.customer_id','customers.customer_type','customers.code','customers.rate')
                        ->join('loans','customers.id','=','loans.customer_id')
                        ->whereBetween('customers.created_at', ['2009-01-01'.' 00:00:00', $current_day.' 23:59:59'])
                        ->whereBetween('loans.created_at', ['2009-01-01'.' 00:00:00', $current_day.' 23:59:59'])
                        ->where('loans.state', 'Vigente')
                        ->distinct('loans.customer_id')
                        ->count('loans.customer_id');

                    if(isset($log->id))
                    {
                        $log->count = $value;
                        $log->save();
                    }else{
                        $historyCustomer::create(
                            [
                                'count'=>$value,
                                'list'=>'-',
                                'month'=>$month['value'],
                                'name'=>$year.'-'.$month['name'],
                                'year'=>$year
                            ]
                        );
                    }
                    $flag = true;
                }else{
                    $value = $log->count;
                }

                $clientes['data'][] = [$month['name'], $value];
            }
        }

        $desembolsos =
            [
                "label"=> "Desembolsos",
                "color"=> "#ed860d",
                "data"=> []
            ];

						$flag  = false;

        foreach ($months as $month)
        {

						if($flag)
						{
							$desembolsos['data'][]  = [$month['name'], NULL];
						}else{
							$value = $disbursementRepo->getModel()
	                ->whereBetween('created_at', [$year.'-'.$month['value'].'-01'.' 00:00:00', $year.'-'.$month['value'].'-31'.' 23:59:59'])
	                ->count();
							$desembolsos['data'][]  = [$month['name'], $value];
						}

						if($month_current == $month['value'])
						{
							$flag = true;
						}
        }


        $data = [
            $cuotas_pagadas
            , $cuotas_vencidas
            ,$clientes_nuevos
            ,$desembolsos, $clientes

        ];

        return \Response::json($data);
    }

    public function getReportLinesAmounts(PaymentRepo $paymentRepo, DisbursementRepo $disbursementRepo, HistoryPayment $historyPayment)
    {

        $flag  = false;
        $month_current = date('m');
        $current_day = date('Y-m-d');

        $months = [
            ['name'=>'Ene', 'value'=>'01'],
            ['name'=>'Feb', 'value'=>'02'],
            ['name'=>'Mar','value'=>'03'],
            ['name'=>'Abr', 'value'=>'04'],
            ['name'=>'May','value'=>'05'],
            ['name'=>'Jun', 'value'=>'06'],
            ['name'=>'Jul','value'=>'07'],
            ['name'=>'Ago','value'=>'08'],
            ['name'=>'Sep','value'=>'09'],
            ['name'=>'Oct', 'value'=>'10'],
            ['name'=>'Nov', 'value'=>'11'],
            ['name'=>'Dic', 'value'=>'12']
        ];

        $year = date('Y');

        $cuotas_pagadas = [
            "label"=> "Cuotas Pagadas",
            "color"=> "#5ab1ef",
            "data"=> []
        ];

        foreach ($months as $month)
        {

            $value = $paymentRepo->getModel()
                ->whereBetween('date_pay', [$year.'-'.$month['value'].'-01'.' 00:00:00', $year.'-'.$month['value'].'-31'.' 23:59:59'])
                ->sum('amount');

            $cuotas_pagadas['data'][] = [$month['name'], $value];
        }


        $cuotas_vencidas = [
            "label"=> "Cuotas Atrasadas",
            "color"=> "#e80808",
            "data"=> []
        ];

        foreach ($months as $month)
        {

            if($flag)
            {
                $cuotas_vencidas['data'][] = [$month['name'], 0];
            }else{
                $log = $historyPayment->where('year',$year)
                    ->where('month', $month['value'])
                    ->first();

                $current_day = Carbon::createFromFormat('Y-m-d', $current_day)->subDay(1)->format('Y-m-d');

                if($month_current == $month['value'])
                {
                    $value = $paymentRepo->getModel()
                        ->select('payments.loan_id', 'payments.amount', 'payments.interest', 'payments.capital',
                            'payments.expiration', 'payments.date_pay', 'payments.state', 'payments.number', 'payments.mora')
                        ->join('loans','loans.id','=','payments.loan_id')
                        ->where('loans.state','Vigente')
                        ->whereBetween('payments.expiration', ['2009-01-01'.' 00:00:00', Carbon::now()->subDay(1)->format('Y-m-d 00:00:00')])
                        ->where('payments.state','Pendiente')
                        ->sum('payments.amount');

                    if(isset($log->id))
                    {
                        $log->amount = $value;
                        $log->save();
                    }

                    $flag = true;
                }else{
                    $value = $log->amount;
                }

                $cuotas_vencidas['data'][] = [$month['name'], $value];
            }
        }

        $interes_ganado = [
            "label"=> "Interes Ganado",
            "color"=> "#07c21a",
            "data"=> []
        ];

        foreach ($months as $month)
        {

            $value = $paymentRepo->getModel()
                ->whereBetween('date_pay', [$year.'-'.$month['value'].'-01'.' 00:00:00', $year.'-'.$month['value'].'-31'.' 23:59:59'])
                ->where('state','Pagado')
                ->sum('interest');

            $interes_ganado['data'][] = [$month['name'], $value];
        }


        $desembolsos =
            [
                "label"=> "Desembolsos",
                "color"=> "#ed860d",
                "data"=> []
            ];

        foreach ($months as $month)
        {

            $value = $disbursementRepo->getModel()
                ->whereBetween('created_at', [$year.'-'.$month['value'].'-01'.' 00:00:00', $year.'-'.$month['value'].'-31'.' 23:59:59'])
                ->sum('amount');

            $desembolsos['data'][] = [$month['name'], $value];
        }


        $data = [
            $cuotas_pagadas
            , $cuotas_vencidas,
            $interes_ganado
            ,$desembolsos

        ];

        return \Response::json($data);
    }

    public function capital(CashDeskDetailRepo $cashDeskDetailRepo, CashDeskRepo $cashDeskRepo,
                            HistoryCapital $historyCapital, LoanRepo $loanRepo, PaymentRepo $paymentRepo)
    {
        $flag  = false;
        $month_current = date('m');
        $current_day = date('Y-m-d');

        $months = [
            ['name'=>'Ene', 'value'=>'01'],
            ['name'=>'Feb', 'value'=>'02'],
            ['name'=>'Mar','value'=>'03'],
            ['name'=>'Abr', 'value'=>'04'],
            ['name'=>'May','value'=>'05'],
            ['name'=>'Jun', 'value'=>'06'],
            ['name'=>'Jul','value'=>'07'],
            ['name'=>'Ago','value'=>'08'],
            ['name'=>'Sep','value'=>'09'],
            ['name'=>'Oct', 'value'=>'10'],
            ['name'=>'Nov', 'value'=>'11'],
            ['name'=>'Dic', 'value'=>'12']
        ];

        $year = date('Y');


        $capital = [
            "label"=> "Capital",
            "color"=> "#2212de",
            "data"=> []
        ];

        foreach ($months as $month)
        {

            if($flag)
            {
                $capital['data'][] = [$month['name'], 0];
            }else{
                $log = $historyCapital->where('year',$year)
                    ->where('month', $month['value'])
                    ->first();

                $current_day = Carbon::createFromFormat('Y-m-d', $current_day)->subDay(1)->format('Y-m-d');



                if($month_current == $month['value'])
                {
                    $cash_desks = $cashDeskRepo->all();

                    $cash_flow = 0;

                    foreach ($cash_desks as $cash_desk){
                        $cash_desk_detail = $cashDeskDetailRepo->getDetailFlow($cash_desk->id);

                        if(isset($cash_desk_detail->id))
                        {
                            $cash_flow = $cash_flow + $cash_desk_detail->total_amount;
                        }
                    }

                    $capital_pending = 0;
                    $loans_process = $loanRepo->getModel()
                        ->where('state','Vigente')->get();

                    foreach($loans_process as $loan)
                    {
                        $capital_pending = $capital_pending + $loan->amount -$paymentRepo->sumCapital($loan->id);
                    }

                    $value = $capital_pending + $cash_flow;

                    if(isset($log->id))
                    {
                        $log->amount = $value;
                        $log->amount_capital = $capital_pending;
                        $log->save();
                    }else{
                        $historyCapital::create(
                            [
                                'month'=>$month['value'],
                                'name'=>$year.'-'.$month['name'],
                                'year'=>$year,
                                'amount'=>$value,
                                'amount_capital'=>$capital_pending
                            ]
                        );
                    }

                    $flag = true;

                }else{
                    $value = $log->amount;
                }

                $capital['data'][] = [$month['name'], $value];
            }
        }

        return \Response::json([$capital]);

    }

    public function printPayed(PaymentDocumentRepo $paymentDocumentRepo, CashDeskDetailRepo $cashDeskDetailRepo){
        $cash_desk_details = $cashDeskDetailRepo->getModel();
        $cash_desk_details = $cash_desk_details->where('state', 1)->get();

        $cash_desk_detail_ids = array();

        foreach ($cash_desk_details as $row)
        {
            $cash_desk_detail_ids[] = $row->id;
        }

        $payments = $paymentDocumentRepo->getModel();
        $payments = $payments->where('state', 'Correct')->whereIn('cash_desk_detail_id', $cash_desk_detail_ids)->get();

        $accounts = Account::all();

        $payments_final = array();

        foreach ($payments as $payment)
        {
            foreach ($payment->details as $detail)
            {
                $payments_final[] = $detail->payment;
            }
        }

        $payments = $payments_final;

        $outputName = md5(microtime());

        $ext = "pdf";


        return \PDF::loadView('pdf.home.cuotas-canceladas',compact('payments', 'accounts'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom',10)
            ->setOption('margin-right',4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size','1366x768')
            ->stream($outputName.'.'.$ext);



    }

    public function printIncomes(CashDeskIncomeRepo $cashDeskIncomeRepo, CashDeskDetailRepo $cashDeskDetailRepo)
    {
        $cash_desk_details = $cashDeskDetailRepo->getModel();
        $cash_desk_details = $cash_desk_details->where('state', 1)->get();

        $cash_desk_detail_ids = array();

        foreach ($cash_desk_details as $row)
        {
            $cash_desk_detail_ids[] = $row->id;
        }

        $incomes = $cashDeskIncomeRepo->getModel();
        $incomes = $incomes->whereIn('cash_desk_detail_id', $cash_desk_detail_ids)->get();

        $outputName = md5(microtime());

        $ext = "pdf";


        return \PDF::loadView('pdf.home.incomes',compact('incomes'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom',10)
            ->setOption('margin-right',4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size','1366x768')
            ->stream($outputName.'.'.$ext);
    }

    public function printDesembolsos(DisbursementRepo $disbursementRepo, CashDeskDetailRepo $cashDeskDetailRepo){
        $cash_desk_details = $cashDeskDetailRepo->getModel();
        $cash_desk_details = $cash_desk_details->where('state', 1)->get();

        $cash_desk_detail_ids = array();

        foreach ($cash_desk_details as $row)
        {
            $cash_desk_detail_ids[] = $row->id;
        }

        $disbursement = $disbursementRepo->getModel();
        $disbursement = $disbursement->whereIn('cash_desk_detail_id', $cash_desk_detail_ids)->get();

        $desembolsos = $disbursement;

        $outputName = md5(microtime());

        $ext = "pdf";


        return \PDF::loadView('pdf.home.desembolsos',compact('desembolsos'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom',10)
            ->setOption('margin-right',4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size','1366x768')
            ->stream($outputName.'.'.$ext);
    }

    public function printEgresos(CashDeskExpenseRepo $cashDeskExpenseRepo, CashDeskDetailRepo $cashDeskDetailRepo){
        $cash_desk_details = $cashDeskDetailRepo->getModel();
        $cash_desk_details = $cash_desk_details->where('state', 1)->get();

        $cash_desk_detail_ids = array();

        foreach ($cash_desk_details as $row)
        {
            $cash_desk_detail_ids[] = $row->id;
        }

        $egresos = $cashDeskExpenseRepo->getModel();
        $egresos = $egresos->where('sub_type', 1)->whereIn('cash_desk_detail_id', $cash_desk_detail_ids)->get();

        $expenses = $egresos;

        $outputName = md5(microtime());

        $ext = "pdf";


        return \PDF::loadView('pdf.home.expenses',compact('expenses'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom',10)
            ->setOption('margin-right',4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size','1366x768')
            ->stream($outputName.'.'.$ext);

    }

    public function printGastos(CashDeskExpenseRepo $cashDeskExpenseRepo, CashDeskDetailRepo $cashDeskDetailRepo){
        $cash_desk_details = $cashDeskDetailRepo->getModel();
        $cash_desk_details = $cash_desk_details->where('state', 1)->get();

        $cash_desk_detail_ids = array();

        foreach ($cash_desk_details as $row)
        {
            $cash_desk_detail_ids[] = $row->id;
        }

        $gastos = $cashDeskExpenseRepo->getModel();
        $gastos = $gastos->where('sub_type', 0)->whereIn('cash_desk_detail_id', $cash_desk_detail_ids)->get();

        $expenses = $gastos;

        $outputName = md5(microtime());

        $ext = "pdf";


        return \PDF::loadView('pdf.home.expenses-g',compact('expenses'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom',10)
            ->setOption('margin-right',4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size','1366x768')
            ->stream($outputName.'.'.$ext);


    }


}
