<?php namespace App\Http\Controllers;

use App\Core\Components\MainMenu;
use App\Core\Repositories\UserRepo;
use App\Core\Managers\UserManager;
use App\Core\Repositories\ProfileRepo;
use Illuminate\Support\Facades\Redis;

class AuthController extends Controller {

    protected $userRepo;

    public function __construct(UserRepo $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function index()
    {
        if(!\Auth::check())
        {
            return \View::make('auth.login');
        }else{
            return \Redirect::to('/');
        }
    }

    public function testPassword()
    {
        $user = $this->userRepo->find(1);
    }

    public function login(ProfileRepo $profileRepo, MainMenu $mainMenu)
    {
        $data = \Request::only('name', 'password', 'remember');
        $credentials = ['name' => $data['name'], 'password' => $data['password'], 'state'=>1];

        if (\Auth::attempt($credentials, $data['remember']))
        {
            return \Response::json(['response'=>true]);
        }

        return \Response::json(['response'=>false, 'message' => 'Usuario o Contraseña incorrecta']);
    }

    public function logout()
    {
        \Auth::logout();
        return \Redirect::route('home');
    }

    public function  changePassword()
    {
        return \View::make('change_password');
    }

    public function  updatePassword()
    {
        $data = \Request::all();
        $credentials = ['user' => \Auth::user()->user, 'password' => $data['oldpassword']];
        if (\Auth::attempt($credentials))
        {
            $user = $this->userRepo->find(Auth::user()->id);
            $data['user'] = $user->user;
            $data['profile_id'] = $user->profile_id;
            $data['state'] = $user->state;
            $data['person_id'] = $user->person_id;
            $manager = new UserManager($user, $data);
            $manager->save();
            return \Redirect::to('/');
        }else{
            return \Redirect::back();
        }
    }


}
