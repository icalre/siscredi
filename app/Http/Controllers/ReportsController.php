<?php

/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;

use App\Account;
use App\Core\Entities\CashDesk;
use App\Core\Entities\CashDeskDetail;
use App\Core\Entities\Disbursement;
use App\Core\Entities\Payment;
use App\Core\Repositories\CashDeskDetailRepo;
use App\Core\Repositories\CashDeskRepo;
use App\Core\Repositories\DisbursementRepo;
use App\Core\Repositories\CashDeskIncomeRepo;
use App\Core\Repositories\CashDeskExpenseRepo;
use App\Core\Repositories\LoanRepo;
use App\Core\Repositories\PaymentRepo;
use Carbon\Carbon;
use App\Core\Entities\Loan;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;


class ReportsController extends Controller
{


    protected $cashDetailRepo;
    protected $disbursementRepo;
    protected $incomeRepo;
    protected $expenseRepo;
    protected $loanRepo;
    protected $paymentRepo;

    public function __construct(
        CashDeskDetailRepo $cashDeskDetailRepo,
        DisbursementRepo $disbursementRepo,
        CashDeskIncomeRepo $incomeRepo,
        CashDeskExpenseRepo $expenseRepo,
        PaymentRepo $paymentRepo,
        LoanRepo $loanRepo
    ) {
        $this->cashDetailRepo = $cashDeskDetailRepo;
        $this->disbursementRepo = $disbursementRepo;
        $this->expenseRepo = $expenseRepo;
        $this->incomeRepo = $incomeRepo;
        $this->paymentRepo = $paymentRepo;
        $this->loanRepo = $loanRepo;
    }

    public function index()
    {
        return \View::make('reports.index');
    }

    public function generalReport(CashDeskRepo $cashDeskRepo)
    {
        $date_start = \Request::get('date-start');
        $date_end = \Request::get('date-end');
        $date_start = Carbon::createFromFormat('d-m-Y', $date_start)->format('Y-m-d');
        $date_end = Carbon::createFromFormat('d-m-Y', $date_end)->format('Y-m-d');
        $loan = $this->loanRepo->getModel();
        $payment = $this->paymentRepo->getModel();
        $disbursement = $this->disbursementRepo->getModel();

        $cash_desk_detail = array();
        $cash_desks = $cashDeskRepo->all();

        foreach ($cash_desks as $cash_desk) {
            $item = $this->cashDetailRepo->getDetailFlow($cash_desk->id);

            if (isset($item->id)) {
                if (!isset($cash_desk_detail->id)) {
                    $cash_desk_detail = $item;
                } else {
                    $cash_desk_detail->total_pay = $cash_desk_detail->total_pay + $item->total_pay;
                    $cash_desk_detail->income_amount = $cash_desk_detail->income_amount + $item->income_amount;
                    $cash_desk_detail->desembolsos = $cash_desk_detail->desembolsos + $item->desembolsos;
                    $cash_desk_detail->expenses_amount = $cash_desk_detail->expenses_amount + $item->expenses_amount;
                    $cash_desk_detail->expenses_amount_g = $cash_desk_detail->expenses_amount_g + $item->expenses_amount_g;
                    $cash_desk_detail->total_amount = $cash_desk_detail->total_amount + $item->total_amount;
                }
            }
        }


        $expense = $this->expenseRepo->getModel();
        $expenses = $expense->whereBetween('created_at', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])->where('sub_type', 0)->get();

        $expenses_g = $expense->whereBetween('created_at', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])->where('sub_type', 1)->get();

        $income = $this->incomeRepo->getModel();
        $incomes = $income->whereBetween('created_at', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])->get();

        $income_amount = 0;

        $income_amount_crediperu = 0;

        $expenses_amount = 0;

        $expenses_amount_g = 0;

        foreach ($expenses as $item) {
            $expenses_amount = $expenses_amount + $item->amount;
        }

        foreach ($expenses_g as $item) {
            $expenses_amount_g = $expenses_amount_g + $item->amount;
        }

        foreach ($incomes as $item) {
            $income_amount = $income_amount + $item->amount;

            if ($item->type == 'Crediperu') {
                $income_amount_crediperu = $income_amount_crediperu + $item->amount;
            }
        }


        $desembolsos = $disbursement->whereBetween('created_at', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->sum('amount');

        $final_amount = $cash_desk_detail->initial_amount + $income_amount - $expenses_amount - $desembolsos;


        $loans_cancelled = $loan->whereIn('state', ['Cancelado', 'Perdida Cancelado', 'Garantía Ejecutada Cancelado'])
            ->whereBetween('date_end', [$date_start, $date_end])
            ->orderBy('date_end', 'ASC')
            ->count();

        $loans_lose_cancelled = $loan->where('state', 'Perdida Cancelado')
            ->count();

        $loans_execute_warranty = $loan->with(['employee', 'customer', 'employee.person', 'customer.customer'])
            ->where('state', 'Garantía Ejecutada')
            ->count();

        $loans_execute_warranty_cancelled = $loan->where('state', 'Garantía Ejecutada Cancelado')
            ->count();

        $loans_annulment = $loan->where('state', 'Anulado')->whereBetween('date_annulment', [$date_start, $date_end])
            ->count();

        $loans_lose = $loan->where('state', 'Perdida')
            ->count();

        $loans_customers = $loan->where('state', 'Vigente')
            ->groupBy('customer_id')
            ->orderBy('customer_id', 'ASC')
            ->count();

        $capital_pending = 0;
        $loans_process_count = 0;
        $loans_process = $loan->where('state', 'Vigente')->get();

        foreach ($loans_process as $loan) {
            $capital_pending = $capital_pending + $loan->amount - $this->paymentRepo->sumCapital($loan->id);
            $loans_process_count++;
        }

        $loans_warranty = $loan->where('warranty_option', 'Si')
            ->where('state', 'Vigente')->count();

        $payments_expired = $payment->select(
            'payments.loan_id',
            'payments.amount',
            'payments.interest',
            'payments.capital',
            'payments.expiration',
            'payments.date_pay',
            'payments.state',
            'payments.number',
            'payments.mora'
        )
            ->where('payments.state', 'Pendiente')
            ->whereBetween('expiration', ['2009-01-01' . ' 00:00:00', Carbon::now()->subDay(1)->format('Y-m-d 00:00:00')])
            //->where('expiration', '<',  Carbon::now()->format('Y-m-d'))
            ->join('loans', 'loans.id', '=', 'payments.loan_id')
            ->where('loans.state', 'Vigente')
            ->orderBy('expiration', 'ASC')
            ->count();


        $payments_payed = $payment->where('state', 'Pagado')
            ->whereBetween('date_pay', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->orderBy('date_pay', 'ASC')
            ->count();

        $capital = $payment->where('state', 'Pagado')
            ->whereBetween('date_pay', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->orderBy('date_pay', 'ASC')
            ->sum('capital');
        $interest = $payment->where('state', 'Pagado')
            ->whereBetween('date_pay', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->orderBy('date_pay', 'ASC')
            ->sum('interest');

        $total_pay = $payment->where('state', 'Pagado')
            ->whereBetween('date_pay', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->orderBy('date_pay', 'ASC')
            ->sum('amount');


        $payments_payed_sunat = $payment->where('payments.state', 'Pagado')
            ->join('loans', 'loans.id', '=', 'payments.loan_id')
            ->where('loans.nro_contrato', '>', 0)
            ->whereBetween('date_pay', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->orderBy('date_pay', 'ASC')
            ->count();

        $loans_lose_period = $loan->where('state', 'Perdida')->whereBetween('date_annulment', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])->get();

        $loan_lose_amount = 0;

        foreach ($loans_lose_period as $loan) {
            $loan->lose_capital = 0;

            foreach ($loan->payments as $payment) {
                if ($payment->state == 'Perdida') {
                    $loan->lose_capital = $loan->lose_capital + $payment->capital;
                }
            }

            $loan_lose_amount = $loan_lose_amount + $loan->lose_capital;
        }


        return \Response::json([
            'desembolsos' => $desembolsos,
            'response' => true,
            'expenses_amount' => $expenses_amount,
            'expenses_amount_g' => $expenses_amount_g,
            'income_amount' => $income_amount,
            'cash_desk_detail' => $cash_desk_detail,
            'final_amount' => $final_amount,
            'income_amount_crediperu' => $income_amount_crediperu,
            'loans_cancelled_count' => $loans_cancelled,
            'loans_lose_cancelled_count' => $loans_lose_cancelled,
            'loans_execute_warranty_cancelled_count' => $loans_execute_warranty_cancelled,
            'loans_annulment_count' => $loans_annulment,
            'loans_lose_count' => $loans_lose,
            'loan_customers_count' => $loans_customers,
            'loans_process_count' => $loans_process_count,
            'loans_warranty_count' => $loans_warranty,
            'payments_expired_count' => $payments_expired,
            'loans_execute_warranty_count' => $loans_execute_warranty,
            'payments_payed_count' => $payments_payed,
            'payments_payed_sunat_count' => $payments_payed_sunat,
            'capital_pending' => $capital_pending,
            'total_pay' => $total_pay,
            'capital' => $capital,
            'interest' => $interest,
            'loans_lose_period' => $loan_lose_amount
        ]);
    }

    public function printCancelados()
    {
        $date_start = \Request::get('date-start');
        $date_end = \Request::get('date-end');
        $json = \Request::get('json');
        $date_start2 = Carbon::createFromFormat('d-m-Y', $date_start)->format('Y-m-d');
        $date_end2 = Carbon::createFromFormat('d-m-Y', $date_end)->format('Y-m-d');
        $loan = $this->loanRepo->getModel();

        $loans = $loan->with(['employee', 'customer', 'employee.person', 'customer.customer', 'payments'])
            ->whereIn('state', ['Cancelado', 'Perdida Cancelado', 'Garantía Ejecutada Cancelado'])
            ->whereBetween('date_end', [$date_start2, $date_end2])
            ->orderBy('date_end', 'ASC')
            ->get();

        foreach ($loans as $loan) {
            $loan->lose_capital = 0;
            $loan->amount_interest = 0;
            $loan->mora_amount = 0;
            $loan->gastos_amount = 0;


            foreach ($loan->payments as $payment) {
                if ($payment->state == 'Pagado') {
                    $loan->amount_interest = $loan->amount_interest + $payment->interest;
                    $loan->mora_amount = $loan->mora_amount + $payment->mora;
                    $loan->gastos_amount = $loan->gastos_amount + $payment->expenses;
                }
            }

            $loan->lose_capital = $loan->lose_capital;
        }

        if ($json == 1) {
            return \Response::json($loans);
        }

        $outputName = md5(microtime());

        $ext = "pdf";


        return \PDF::loadView('pdf.prestamos-cancelados', compact('loans', 'date_start', 'date_end'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 10)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function printAnulados()
    {
        $date_start = \Request::get('date-start');
        $date_end = \Request::get('date-end');
        $date_start2 = Carbon::createFromFormat('d-m-Y', $date_start)->format('Y-m-d');
        $date_end2 = Carbon::createFromFormat('d-m-Y', $date_end)->format('Y-m-d');
        $loan = $this->loanRepo->getModel();

        $loans = $loan->with(['employee', 'customer', 'employee.person', 'customer.customer'])
            ->where('state', 'Anulado')->whereBetween('date_annulment', [$date_start2, $date_end2])->get();

        $json = \Request::get('json');
        if ($json == 1) {
            return \Response::json($loans);
        }

        $outputName = md5(microtime());

        $ext = "pdf";

        return \PDF::loadView('pdf.prestamos-anulados', compact('loans', 'date_start', 'date_end'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function printCanceladas()
    {
        $date_start = \Request::get('date-start');
        $date_end = \Request::get('date-end');
        $date_start2 = Carbon::createFromFormat('d-m-Y', $date_start)->format('Y-m-d');
        $date_end2 = Carbon::createFromFormat('d-m-Y', $date_end)->format('Y-m-d');
        $payment = $this->paymentRepo->getModel();

        $payments = $payment->selectRaw("
            payments.id, 
            payments.loan_id,  
            payments.expiration,
            payments.state,
            payments.mora,
            payments.capital,
            payments.amount,
            payments.number,
            payments.interest,
            payments.date_pay,
            payments.expenses,
            payments.type_payment,
            payments.account_id,
            concat((select count(distinct p.id) as pagadas from payments as p where p.loan_id = payments.loan_id and p.state = 'Pagado' and p.id < payments.id) + 1 , '/', (select count(distinct p2.id) as pagadas from payments as p2 where p2.loan_id = payments.loan_id and p2.state != 'Anulado')) as total_payments
            ")
            ->with(['loan', 'account', 'loan.employee', 'loan.customer', 'loan.employee.person', 'loan.customer.customer'])
            ->where('state', 'Pagado')
            ->whereBetween('date_pay', [$date_start2 . ' 00:00:00', $date_end2 . ' 23:59:59'])
            ->orderBy('date_pay', 'ASC')
            ->get();

        $json = \Request::get('json');

        if ($json == 1) {
            return \Response::json($payments);
        }


        $outputName = md5(microtime());

        $totals = [];

        $ext = "pdf";

        return \PDF::loadView('pdf.cuotas-canceladas', compact('payments', 'date_start', 'date_end', 'totals'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 10)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 10)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function printCanceladasSunat()
    {
        $date_start = \Request::get('date-start');
        $date_end = \Request::get('date-end');
        $date_start2 = Carbon::createFromFormat('d-m-Y', $date_start)->format('Y-m-d');
        $date_end2 = Carbon::createFromFormat('d-m-Y', $date_end)->format('Y-m-d');
        $payment = $this->paymentRepo->getModel();

        $payments = $payment->with(['loan', 'loan.employee', 'loan.customer', 'loan.employee.person', 'loan.customer.customer'])
            ->where('state', 'Pagado')
            ->whereBetween('date_pay', [$date_start2 . ' 00:00:00', $date_end2 . ' 23:59:59'])
            ->orderBy('date_pay', 'ASC')
            ->get();

        $json = \Request::get('json');
        if ($json == 1) {
            return \Response::json($payments);
        }


        $outputName = md5(microtime());

        $ext = "pdf";

        return \PDF::loadView('pdf.cuotas-canceladas-sunat', compact('payments', 'date_start', 'date_end'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function printExpenses()
    {
        $date_start = \Request::get('date-start');
        $date_end = \Request::get('date-end');
        $date_start2 = Carbon::createFromFormat('d-m-Y', $date_start)->format('Y-m-d');
        $date_end2 = Carbon::createFromFormat('d-m-Y', $date_end)->format('Y-m-d');
        $expense = $this->expenseRepo->getModel();
        $expenses = $expense->where('sub_type', 0)
            ->whereBetween('created_at', [$date_start2 . ' 00:00:00', $date_end2 . ' 23:59:59'])->get();

        $outputName = md5(microtime());

        $ext = "pdf";

        return \PDF::loadView('pdf.expenses', compact('expenses', 'date_start', 'date_end'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function printExpensesG()
    {
        $date_start = \Request::get('date-start');
        $date_end = \Request::get('date-end');
        $date_start2 = Carbon::createFromFormat('d-m-Y', $date_start)->format('Y-m-d');
        $date_end2 = Carbon::createFromFormat('d-m-Y', $date_end)->format('Y-m-d');
        $expense = $this->expenseRepo->getModel();
        $expenses = $expense->where('sub_type', 1)
            ->whereBetween('created_at', [$date_start2 . ' 00:00:00', $date_end2 . ' 23:59:59'])->get();

        $outputName = md5(microtime());

        $ext = "pdf";

        return \PDF::loadView('pdf.expenses-g', compact('expenses', 'date_start', 'date_end'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function printIncomes()
    {
        $date_start = \Request::get('date-start');
        $date_end = \Request::get('date-end');
        $date_start2 = Carbon::createFromFormat('d-m-Y', $date_start)->format('Y-m-d');
        $date_end2 = Carbon::createFromFormat('d-m-Y', $date_end)->format('Y-m-d');
        $income = $this->incomeRepo->getModel();
        $incomes = $income->whereBetween('created_at', [$date_start2 . ' 00:00:00', $date_end2 . ' 23:59:59'])->get();

        $outputName = md5(microtime());

        $ext = "pdf";

        return \PDF::loadView('pdf.incomes', compact('incomes', 'date_start', 'date_end'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function printCustomers()
    {

        $loan = $this->loanRepo->getModel();

        $loans = $loan->with(['employee', 'customer', 'employee.person', 'customer.customer'])
            ->where('state', 'Vigente')
            ->groupBy('customer_id')
            ->orderBy('customer_id', 'ASC')
            ->get();

        $json = \Request::get('json');
        if ($json == 1) {
            return \Response::json($loans);
        }

        $outputName = md5(microtime());

        $ext = "pdf";

        return \PDF::loadView('pdf.r-customers', compact('loans'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function printDesembolsos()
    {
        $date_start = \Request::get('date-start');
        $date_end = \Request::get('date-end');
        $date_start2 = Carbon::createFromFormat('d-m-Y', $date_start)->format('Y-m-d');
        $date_end2 = Carbon::createFromFormat('d-m-Y', $date_end)->format('Y-m-d');

        $disbursement = $this->disbursementRepo->getModel();
        $desembolsos = $disbursement->whereBetween('created_at', [$date_start2 . ' 00:00:00', $date_end2 . ' 23:59:59'])
            ->get();

        $json = \Request::get('json');
        if ($json == 1) {
            return \Response::json($desembolsos);
        }

        $outputName = md5(microtime());

        $ext = "pdf";

        return \PDF::loadView('pdf.desembolsos', compact('desembolsos', 'date_start', 'date_end'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function printPerdida()
    {

        $loan = $this->loanRepo->getModel();

        $loans = $loan->with(['employee', 'customer', 'employee.person', 'customer.customer'])
            ->where('state', 'Perdida')
            ->get();

        foreach ($loans as $loan) {
            $loan->lose_capital = 0;

            foreach ($loan->payments as $payment) {
                if ($payment->state == 'Perdida') {
                    $loan->lose_capital = $loan->lose_capital + $payment->capital;
                }
            }

            $loan->lose_capital = $loan->lose_capital;
            $loan->capital_pending = $loan->amount - $this->paymentRepo->sumCapital($loan->id);
        }

        $json = \Request::get('json');
        if ($json == 1) {
            return \Response::json($loans);
        }

        $outputName = md5(microtime());

        $ext = "pdf";


        return \PDF::loadView('pdf.prestamos-perdida', compact('loans'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function printGarantiaV()
    {
        $loan = $this->loanRepo->getModel();
        $loans_warranty = $loan->with(['employee', 'customer', 'employee.person', 'customer.customer', 'warranties'])
            ->where('warranty_option', 'Si')
            ->where('state', 'Vigente')->get();


        foreach ($loans_warranty as $loan) {
            $artefactosA = array();
            $otrosA = array();
            $joyasA = array();

            foreach ($loan->warranties as $warranty) {

                if ($warranty->type == 'Artefacto') {
                    $artefactosA[] = $warranty;
                } elseif ($warranty->type == 'Otro') {
                    $otrosA[] = $warranty;
                } elseif ($warranty->type == 'Joya') {
                    $joyasA[] = $warranty;
                }
            }

            $loan->joyasA = $joyasA;
            $loan->otrosA = $otrosA;
            $loan->artefactosA = $artefactosA;
        }

        $json = \Request::get('json');
        if ($json == 1) {
            return \Response::json($loans_warranty);
        }

        $outputName = md5(microtime());

        $ext = "pdf";


        return \PDF::loadView('pdf.prestamos-v-garantia', compact('loans_warranty'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function printPerdidaCancelado()
    {
        $loan = $this->loanRepo->getModel();
        $loans = $loan->with(['employee', 'customer', 'employee.person', 'customer.customer', 'payments'])
            ->where('state', 'Perdida Cancelado')
            ->get();

        foreach ($loans as $loan) {
            $loan->lose_capital = 0;
            $loan->amount_interest = 0;
            $loan->mora_amount = 0;

            foreach ($loan->payments as $payment) {
                if ($payment->state == 'Perdida') {
                    $loan->lose_capital = $loan->lose_capital + $payment->capital;
                }

                if ($payment->state == 'Pagado') {
                    $loan->amount_interest = $loan->amount_interest + $payment->interest;
                    $loan->mora_amount = $loan->mora_amount + $payment->mora;
                }
            }

            $loan->lose_capital = $loan->lose_capital;
        }

        $json = \Request::get('json');
        if ($json == 1) {
            return \Response::json($loans);
        }

        $outputName = md5(microtime());

        $ext = "pdf";


        return \PDF::loadView('pdf.prestamos-perdida-cancelados', compact('loans'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function printGarantiaEjecutada()
    {

        $loan = $this->loanRepo->getModel();

        $loans = $loan->with(['employee', 'customer', 'employee.person', 'customer.customer'])
            ->where('state', 'Garantía Ejecutada')
            ->get();

        foreach ($loans as $loan) {
            $loan->lose_capital = 0;

            foreach ($loan->payments as $payment) {
                if ($payment->state == 'Garantia') {
                    $loan->lose_capital = $loan->lose_capital + $payment->capital;
                }
            }

            $loan->lose_capital = $loan->lose_capital;
            $loan->capital_pending = $loan->amount - $this->paymentRepo->sumCapital($loan->id);
        }

        $json = \Request::get('json');
        if ($json == 1) {
            return \Response::json($loans);
        }

        $outputName = md5(microtime());

        $ext = "pdf";


        return \PDF::loadView('pdf.prestamos-garantia-ejecutada', compact('loans'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function printGarantiaCancelado()
    {
        $loan = $this->loanRepo->getModel();
        $loans = $loan->with(['employee', 'customer', 'employee.person', 'customer.customer', 'payments'])
            ->where('state', 'Garantía Ejecutada Cancelado')
            ->get();

        foreach ($loans as $loan) {
            $loan->lose_capital = 0;
            $loan->amount_interest = 0;
            $loan->mora_amount = 0;

            foreach ($loan->payments as $payment) {
                if ($payment->state == 'Garantia') {
                    $loan->lose_capital = $loan->lose_capital + $payment->capital;
                }

                if ($payment->state == 'Pagado') {
                    $loan->amount_interest = $loan->amount_interest + $payment->interest;
                    $loan->mora_amount = $loan->mora_amount + $payment->mora;
                }
            }

            $loan->lose_capital = $loan->lose_capital;
        }

        $json = \Request::get('json');
        if ($json == 1) {
            return \Response::json($loans);
        }

        $outputName = md5(microtime());

        $ext = "pdf";


        return \PDF::loadView('pdf.prestamos-garantia-cancelados', compact('loans'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function printAtrasadas()
    {

        $payment = $this->paymentRepo->getModel();

        $payments = $payment->with(['loan', 'loan.employee', 'loan.customer', 'loan.employee.person', 'loan.customer.customer'])
            ->where('payments.state', 'Pendiente')
            ->where('expiration', '<', Carbon::now()->format('Y-m-d'))
            ->join('loans', 'loans.id', '=', 'payments.loan_id')
            ->where('loans.state', 'Vigente')
            ->orderBy('expiration', 'ASC')
            ->get();

        $json = \Request::get('json');
        if ($json == 1) {
            return \Response::json($payments);
        }


        $outputName = md5(microtime());

        $ext = "pdf";

        return \PDF::loadView('pdf.cuotas-atrasadas', compact('payments'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function printVigentes()
    {
        $loan = $this->loanRepo->getModel();

        $loans = $loan->with(['employee', 'customer', 'employee.person', 'customer.customer'])->where('state', 'Vigente')->get();

        $json = \Request::get('json');
        if ($json == 1) {
            return \Response::json($loans);
        }

        $outputName = md5(microtime());

        $ext = "pdf";

        return \PDF::loadView('pdf.prestamos-vigentes', compact('loans', 'date_start', 'date_end'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function printLoseLoans()
    {

        $date_start = \Request::get('date-start');
        $date_end = \Request::get('date-end');
        $date_start = Carbon::createFromFormat('d-m-Y', $date_start)->format('Y-m-d');
        $date_end = Carbon::createFromFormat('d-m-Y', $date_end)->format('Y-m-d');

        $loans = Loan::with(['employee', 'customer', 'employee.person', 'customer.customer'])
            ->where('state', 'Perdida')
            ->whereBetween('date_annulment', [$date_start . ' 00:00:00', $date_end . ' 23:59:59'])
            ->get();

        foreach ($loans as $loan) {
            $loan->lose_capital = 0;

            foreach ($loan->payments as $payment) {
                if ($payment->state == 'Perdida') {
                    $loan->lose_capital = $loan->lose_capital + $payment->capital;
                }
            }

            $loan->capital_pending = $loan->amount - $this->paymentRepo->sumCapital($loan->id);
        }

        $outputName = md5(microtime());

        $ext = "pdf";

        return \PDF::loadView('pdf.prestamos-perdida', compact('loans'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function consolidatePaymentsReport()
    {
        $date_start = \Request::get('date-start');
        $date_end = \Request::get('date-end');

        if (!empty($date_start) && !empty($date_end)) {
            $date_start = Carbon::createFromFormat('d-m-Y', $date_start)->format('Y-m-d 00:00:00');
            $date_end = Carbon::createFromFormat('d-m-Y', $date_end)->format('Y-m-d 23:59:59');
        }

        $results = Payment::selectRaw("
        concat(p.last_name, ' ', p.name) as nombre,
       p.dni,
       p.telephone,
       p.address,
       l.code,
       payments.id  as payment_id,
       l.type_payment as loan_type,
       l.amount as loan_amount,
       payments.capital as total_capital,
       payments.interest as total_interest,
       concat((select count(distinct payments.id) as pagadas from payments where payments.loan_id = l.id and payments.state = 'Pagado' and payments.id < payment_id) + 1 , '/', (select count(distinct payments.id) as pagadas from payments where payments.loan_id = l.id)) as total_payments,
       (select sum(payments.capital) as pagadas from payments where payments.loan_id = l.id and payments.state = 'Pendiente') as pending_capital,
       (select sum(payments.amount) as pagadas from payments where payments.loan_id = l.id and payments.state = 'Pendiente') as total_pending,
       DATE_FORMAT(l.date_disbursement, '%d/%m/%Y') as date_loan,
       l.state as loan_state,
       concat(people.last_name, ' ',people.name) as employee,
       l.period,
       payments.expiration,
       payments.date_pay,
       payments.amount,
       payments.state,
       accounts.name as type_payment,
       e2.razon_social
        ")
            ->join('loans as l', 'l.id', '=', 'payments.loan_id')
            ->join('customers as c', 'c.id', '=', 'l.customer_id')
            ->join('people as p', 'p.id', '=', 'c.customer_id')
            ->join('employees as e', 'e.id', '=', 'l.employee_id')
            ->join('people', 'people.id', '=', 'e.person_id')
            ->leftJoin('enterprises as e2', 'l.enterprise_id', '=', 'e2.id')
            ->leftJoin('accounts', 'accounts.id', '=', 'payments.account_id')
            ->whereBetween('payments.expiration', [$date_start, $date_end])
            ->whereNotIn('payments.state', ['Perdida', 'Anulado', 'Garantia'])
            ->groupBy('payments.id')
            ->get();

        $name = 'solicitudes-' . date('d-m-Y-H-i-s') . '.xlsx';

        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();
        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial')->setSize(12);
        $sheet->getStyle('1:1')->getFont()->setBold(true);

        $headers = [
            'NOMBRE',
            'DNI',
            'TELEFONO',
            'DIRECCIÓN',
            'EMPRESA',
            'CÓDIGO DE PRESTAMO',
            'TIPO DE PRESTAMO',
            'MONTO DE PRESTAMO',
            'CAPITAL PAGADO',
            'INTERES PAGADO',
            'CUOTAS PAGADAS',
            'CAPITAL PENDIENTE',
            'TOTAL PENDIENTE',
            'FECHA DE PRESTAMO',
            'ESTADO DEL PRESTAMO',
            'ASESOR',
            'PERIODO',
            'VENCIMIENTO',
            'FECHA DE PAGO',
            'MONTO DE CUOTA',
            'ESTADO DEL PAGO',
            'METODO DE PAGO'
        ];

        foreach ($headers as $key => $header) {
            $sheet->setCellValueByColumnAndRow(1 + $key, 1, $header);
        }


        foreach ($results as $key => $item) {

            $date_pay = '';

            if ($item->state == 'Pagado') {
                $date_pay = Carbon::createFromFormat('Y-m-d H:i:s', $item->date_pay)->format('d/m/Y');
            }

            $values = [
                $item->nombre,
                $item->dni,
                $item->telephone,
                $item->address,
                $item->razon_social,
                $item->code,
                $item->loan_type,
                $item->loan_amount,
                $item->total_capital,
                $item->total_interest,
                $item->total_payments,
                $item->pending_capital,
                $item->total_pending,
                $item->date_loan,
                $item->loan_state,
                $item->employee,
                $item->period,
                Carbon::createFromFormat('d-m-Y', $item->expiration)->format('d/m/Y'),
                $date_pay,
                $item->amount,
                $item->state,
                $item->type_payment
            ];

            foreach ($headers as $index => $header) {
                $sheet->setCellValueByColumnAndRow(1 + $index, $key + 2, $values[$index]);
            }
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $name . '"');
        header('Cache-Control: max-age=0');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

    public function disbursements()
    {
        $date_start = \Request::get('date-start');
        $date_end = \Request::get('date-end');

        if (!empty($date_start) && !empty($date_end)) {
            $date_start = Carbon::createFromFormat('d-m-Y', $date_start)->format('Y-m-d 00:00:00');
            $date_end = Carbon::createFromFormat('d-m-Y', $date_end)->format('Y-m-d 23:59:59');
        }

        $disbursements = Disbursement::with([
            'loan', 'loan.customer', 'loan.enterprise', 'loan.customer.customer', 'loan.employee', 'loan.employee.person',
            'loan.payments' => function ($query) {
                $query->selectRaw('sum(payments.interest) as interest, payments.loan_id');
                $query->groupBy('payments.loan_id');
            }
        ])
            ->whereBetween('disbursements.created_at', [$date_start, $date_end])
            ->get();

        $name = 'desembolsos-' . date('d-m-Y-H-i-s') . '.xlsx';

        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();
        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial')->setSize(12);
        $sheet->getStyle('1:1')->getFont()->setBold(true);

        $headers = [
            'FECHA',
            'ASESOR',
            'EMPRESA',
            'Nº DE CONTRATO',
            'Nº DE PRESTAMO',
            'COD. CLIENTE',
            'CLIENTE',
            'DNI',
            'TELEFONO',
            'DIRECCIÓN',
            'TIPO DE PRESTAMO',
            'MONTO DE PRESTAMO',
            'TASA DE INTERES',
            'UTILIDAD',
            'CUOTAS',
            'PLAZO',
            'ESTADO'
        ];

        foreach ($headers as $key => $header) {
            $sheet->setCellValueByColumnAndRow(1 + $key, 1, $header);
        }

        foreach ($disbursements as $key => $disbursement) {
            $values = [
                $disbursement->created_at,
                $disbursement->loan->employee->person->last_name . ' ' . $disbursement->loan->employee->person->name,
                $disbursement->loan->enterprise ? $disbursement->loan->enterprise->razon_social : '',
                $disbursement->loan->nro_contrato,
                $disbursement->loan->code,
                $disbursement->loan->customer->code,
                $disbursement->loan->customer->customer->last_name . ' ' . $disbursement->loan->customer->customer->name,
                $disbursement->loan->employee->person->dni,
                $disbursement->loan->employee->person->telephone,
                $disbursement->loan->employee->person->address,
                $disbursement->loan->type_payment,
                $disbursement->amount,
                $disbursement->loan->interest,
                $disbursement->loan->payments ? $disbursement->loan->payments[0]->interest : '',
                $disbursement->loan->quotas,
                $disbursement->loan->period,
                $disbursement->loan->state
            ];

            $color = '000000';

            if ($disbursement->loan->state == 'Anulado') {
                $color = 'FC0404';
            }

            $styleArray = array(
                'font'  => array(
                    'color' => array('rgb' => $color)
                )
            );

            foreach ($headers as $index => $header) {
                $sheet->getStyle((2 + $key) . ':' . (2 + $key))->applyFromArray($styleArray);
                $sheet->setCellValueByColumnAndRow(1 + $index, $key + 2, $values[$index]);
            }
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $name . '"');
        header('Cache-Control: max-age=0');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

    public function cashDeskDetails()
    {
        $date_start = \Request::get('date-start');
        $date_end = \Request::get('date-end');

        if (!empty($date_start) && !empty($date_end)) {
            $date_start = Carbon::createFromFormat('d-m-Y', $date_start)->format('Y-m-d 00:00:00');
            $date_end = Carbon::createFromFormat('d-m-Y', $date_end)->format('Y-m-d 23:59:59');
        }

        $cash_desk_details = CashDeskDetail::with(['accountDetails', 'accountDetails.account'])
            ->whereBetween('cash_desk_details.created_at', [$date_start, $date_end])
            ->get();

        $outputName = md5(microtime());

        $ext = "pdf";

        return \PDF::loadView('pdf.cash-desks-list', compact('cash_desk_details', 'date_start', 'date_end'))
            ->setPaper('a4')
            ->setOption('margin-top', 7)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('footer-center', 'Pagina [page]')
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.' . $ext);
    }

    public function cashDeskDetailReport($id)
    {

        $accounts = Account::all();
        $cash_desk_detail = CashDeskDetail::with([
            'user',
            'user.person',
            'accountDetails',
            'accountDetails.account',
            'paymentDocuments',
            'paymentDocuments.account',
            'incomes',
            'incomes.account',
            'expenses',
            'expenses.account',
            'disbursements',
            'disbursements.account',
            'disbursements.loan',
            'disbursements.loan.customer',
            'disbursements.loan.customer.customer'
        ])->find($id);

        $cash_desk_detail->disbursement_amount = $cash_desk_detail->disbursements()->where('state', 'Correcto')->sum('amount');
        $cash_desk_detail->income_amount = $cash_desk_detail->incomes()->sum('amount');
        $cash_desk_detail->expenses_amount = $cash_desk_detail->expenses()->where('sub_type', 0)->sum('amount');
        $cash_desk_detail->expenses_amount_g = $cash_desk_detail->expenses()->where('sub_type', 1)->sum('amount');
        $cash_desk_detail->deposit_amount = $cash_desk_detail->paymentDocuments()->where('state', 'Correct')->sum('amount');

        $outputName = md5(microtime());

        $base_totals = [];
        foreach ($accounts as $account) {
            $base_totals[$account->name] = 0;
        }

        $totals = [
            'payments' => $base_totals,
            'incomes' => $base_totals,
            'expenses_1' => $base_totals,
            'expenses_2' => $base_totals,
            'disbursements' => $base_totals
        ];

        return \PDF::loadView('pdf.cash-desk-detail-report', compact('cash_desk_detail', 'accounts', 'totals'))
            ->setPaper('a4')
            ->setOption('margin-top', 10)
            ->setOption('margin-left', 15)
            ->setOption('margin-bottom', 10)
            ->setOption('margin-right', 4)
            ->setOption('viewport-size', '1366x768')
            ->stream($outputName . '.pdf');
    }
}
