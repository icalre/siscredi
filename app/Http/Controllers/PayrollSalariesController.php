<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;

use App\Core\Repositories\PayrollSalaryRepo;
use App\Core\Repositories\SalaryDiscountRepo;
use App\Core\Managers\PayrollSalaryManager;
use App\Core\Managers\SalaryDiscountManager;
use App\Core\Managers\PayrollSalaryDetailManager;
use App\Core\Repositories\PayrollSalaryDetailRepo;

class PayrollSalariesController extends Controller {

    protected $salaryDiscountRepo;
    protected $payrollSalaryRepo;
    protected $payrollSalary;

    public function __construct(SalaryDiscountRepo $salaryDiscountRepo, PayrollSalaryRepo $payrollSalaryRepo){
        $this->salaryDiscountRepo= $salaryDiscountRepo;
        $this->payrollSalaryRepo = $payrollSalaryRepo;
    }


    public function index()
    {
        return \View::make('payroll-salaries.index');
    }

    public function all()
    {
        $salaryDiscounts = $this->payrollSalaryRepo->paginateFilter(25,\Request::get('year'),\Request::get('month'));

        return \Response::json($salaryDiscounts);
    }

    public function formCreate()
    {
        return \View::make('payroll-salaries.create');
    }

    public function formEdit()
    {
        return \View::make('payroll-salaries.edit');
    }

    public function create()
    {
        $payrollSalary = $this->payrollSalaryRepo->getModel();
        $data = \Request::all();
        $data['name']= $data['month']['name'].'-'.$data['year']['name'];
        $data['year'] = $data['year']['name'];
        $data['month']= $data['month']['id'];
        $data['state'] = 1;
        $valid_payroll = $this->payrollSalaryRepo->validPayrollSalary();

        if(isset($valid_payroll->id))
        {
            return \Response::json(['Aun no se ha cerrado la panilla '.$valid_payroll->name]);
        }



        $manager = new PayrollSalaryManager($payrollSalary,$data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function find($id)
    {
        $this->payrollSalary = $this->payrollSalaryRepo->find($id);
        $this->payrollSalary->amount_salaries = $this->payrollSalary->details()->sum('amount');
        $this->payrollSalary->amount_discounts = $this->payrollSalary->details()->sum('discounts');
        $this->payrollSalary->amount_payed = $this->payrollSalary->details()->sum('amount_pay');
        return \Response::json($this->payrollSalary);
    }

    public function edit($id)
    {
        if($this->payrollSalary->state == 'Cerrada')
        {
            return \Response::json(['response'=>false, 'data'=>'succes']);
        }
        $data = \Request::all();
        $manager = new PayrollSalaryManager($this->payrollSalary, $data);
        $manager->save();

        return \Response::json(['response'=>true, 'data'=>'succes']);
    }

    public function getEmployeeDiscount()
    {
        $data = \Request::all();
        $discounts = $this->salaryDiscountRepo->getEmployeeDiscounts($data['employee']);

        return \Response::json($discounts);
    }

    public function getPayrollSalaryActive()
    {
        $payroll_salary = $this->payrollSalaryRepo->validPayrollSalary();

        return \Response::json($payroll_salary);
    }

    public function payEmployee(PayrollSalaryDetailRepo $payrollSalaryDetailRepo)
    {
        $data = \Request::all();
        $data['employee_id'] = $data['employee']['id'];
        $data['payroll_salary_id']= $data['payrollSalary']['id'];

        $payrollSalary = $this->payrollSalaryRepo->find($data['payrollSalary']['id']);

        foreach($payrollSalary->details as $detail)
        {
            if($detail->employee->id == $data['employee']['id'])
            {
                return \Response::json(['response'=>false, 'errors'=>['Este pago ya se encuentra registrador.']]);
            }
        }

        if($data['amount_pay'] <= 0)
        {
            return \Response::json(['response'=>false, 'errors'=>['El monto a pagar no debe ser menor o igual al monto de descuentos.']]);
        }

        $payrollSalaryDetail = $payrollSalaryDetailRepo->getModel();

        $payrollSalaryDetailManager = new PayrollSalaryDetailManager($payrollSalaryDetail,$data);
        $response = $payrollSalaryDetailManager->save();

        foreach($data['pendingDiscounts'] as $discount)
        {
            if($discount['state'] == 'Descontado')
            {
                $salary_discount = $this->salaryDiscountRepo->find($discount['id']);
                $salary_discount->payroll_salary_detail_id = $payrollSalaryDetail->id;
                $salary_discount->state = 0;
                $salary_discount->save();
            }
        }

        return \Response::json($response);
    }


    public function formPay()
    {
        return \View::make('payroll-salaries.pay');
    }

    public function formDiscount()
    {
        return \View::make('payroll-salaries.discount');
    }

    public function  createDiscount()
    {

        $salaryDiscount = $this->salaryDiscountRepo->getModel();
        $data = \Request::all();

        if($data['amount'] <= 0)
        {
            return \Response::json(['response'=>false, 'erros'=>['Se esta registando con monto 0.00']]);
        }

        $manager = new SalaryDiscountManager($salaryDiscount,$data);
        $response = $manager->save();

        return \Response::json($response);

    }

    public function getDiscounts()
    {
        $salaryDiscounts = $this->salaryDiscountRepo->paginateFilter(25,\Request::get('type'),\Request::get('employee'));

        return \Response::json($salaryDiscounts);
    }

    public function closePayrollSalary()
    {
        $data = \Request::all();

        $this->payrollSalary = $this->payrollSalaryRepo->find($data['id']);
        $this->payrollSalary->state = 0;
        $this->payrollSalary->save();

        return \Response::json(['response'=>true]);
    }


} 