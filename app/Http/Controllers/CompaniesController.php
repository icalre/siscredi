<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;
use App\Core\Managers\CompanyManager;
use App\Core\Repositories\CompanyRepo;
use Illuminate\Routing\Route;

class CompaniesController extends Controller {

    protected $companyRepo;
    protected $company;

    public function __construct(CompanyRepo $companyRepo){
        $this->companyRepo = $companyRepo;
    }

    public function findCompany($id)
    {
        $this->company = $this->companyRepo->find($id);
    }


    public function index()
    {
        return \View::make('companies.index');
    }

    public function formCreate()
    {
        return \View::make('companies.create');
    }

    public function formEdit()
    {
        return \View::make('companies.edit');
    }

    public function all()
    {
        $companies = $this->companyRepo->paginate(25);

        return \Response::json($companies);
    }

    public function create()
    {
        $company = $this->companyRepo->getModel();
        $data = \Request::all();
        $data['department_id']= $data['department']['id'];
        $data['province_id'] = $data['province']['id'];
        $data['district_id'] = $data['district']['id'];
        $manager = new CompanyManager($company,$data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function find($id)
    {
        $this->findCompany($id);
        return \Response::json($this->company);
    }

    public function edit($id)
    {
        $this->findCompany($id);
        $data = \Request::all();
        $data['department_id']= $data['department']['id'];
        $data['province_id'] = $data['province']['id'];
        $data['district_id'] = $data['district']['id'];
        $manager = new CompanyManager($this->company, $data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function delete($id)
    {
        $this->findCompany($id);
        $this->company->delete();

        return \Response::json(['response'=>true, 'data'=>$this->company]);
    }

} 