<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;
use App\Core\Managers\CustomerManager;
use App\Core\Repositories\CustomerRepo;
use Illuminate\Routing\Route;
use App\Core\Entities\CustomerComment;

class CustomersController extends Controller {

    protected $customerRepo;
    protected $customer;

    public function __construct(CustomerRepo $customerRepo){
        $this->customerRepo = $customerRepo;
    }

    public function findCustomer($id)
    {
        $this->customer = $this->customerRepo->find($id);
    }


    public function index()
    {
        return \View::make('customers.index');
    }

    public function formCreate()
    {
        return \View::make('customers.create');
    }

    public function formEdit()
    {
        return \View::make('customers.edit');
    }

    public function all()
    {
        $customers = $this->customerRepo->paginate2(25,\Request::get('type'),\Request::get('rate'));

        return \Response::json($customers);
    }

    public function create()
    {
        $customer = $this->customerRepo->getModel();
        $data = \Request::all();

        if(isset($data['customer']))
        {
            $data['customer_id'] = $data['customer']['originalObject']['id'];
            $customer_validation = $this->customerRepo->validateCustomer($data['type'],$data['customer_id']);

            if($customer_validation)
            {
                return \Response::json(['response'=>false,'errors'=>['El cliente ya existe']]);
            }
        }

        $data['customer_type'] = substr('App\Core\Entities\u',0,-1).ucfirst($data['type']);

        $manager = new CustomerManager($customer,$data);
        $response = $manager->save();
        $customer->code = str_pad($customer->id, 4, "0", STR_PAD_LEFT);
        $customer->save();

        return \Response::json($response);
    }

    public function find($id)
    {
        $this->findCustomer($id);
        return \Response::json($this->customer);
    }

    public function edit($id)
    {
        $this->findCustomer($id);
        $data = \Request::all();

        if(isset($data['customer']))
        {
            $data['customer_id'] = $data['customer']['originalObject']['id'];
            $customer_validation = $this->customerRepo->validateCustomer($data['type'],$data['customer_id'],
                                    $this->customer->id);

            if($customer_validation)
            {
                return \Response::json(['response'=>false,'errors'=>['El cliente ya existe']]);
            }
        }

        $data['customer_type'] = substr('App\Core\Entities\u',0,-1).ucfirst($data['type']);

        $manager = new CustomerManager($this->customer, $data);
        $response = $manager->save();

        return \Response::json($response);
    }

    public function delete($id)
    {
        $this->findCustomer($id);
        $this->customer->delete();

        return \Response::json(['response'=>true, 'data'=>$this->customer]);
    }

    public function setBlackList($id)
    {
        $data = \Request::all();
        $this->findCustomer($id);
        $this->customer->rate = 'Malo';
        $this->customer->save();

        $customer_comment = CustomerComment::create(
            [
                'customer_id'=>$this->customer->id,
                'comment'=>$data['note']
            ]
        );

        return \Response::json(['response'=>true, 'data'=>$this->customer]);
    }


    public function unSetBlackList($id)
    {
        $data = \Request::all();
        $this->findCustomer($id);
        $this->customer->rate = 'Bueno';
        $this->customer->save();

        $customer_comment = CustomerComment::create(
            [
                'customer_id'=>$this->customer->id,
                'comment'=>'Cliente pasa a tener calificacion positiva.'
            ]
        );

        return \Response::json(['response'=>true, 'data'=>$this->customer]);
    }

} 