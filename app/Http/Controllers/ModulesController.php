<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 02/03/15
 * Time: 11:56 PM
 */

namespace App\Http\Controllers;
use App\Core\Components\MainMenu;
use App\Core\Managers\ModuleManager;
use App\Core\Repositories\ModuleRepo;
use App\Core\Repositories\ProfileRepo;
use App\Core\Repositories\SubModuleRepo;
use App\Core\Managers\SubModuleManager;
use Illuminate\Routing\Route;

class ModulesController extends Controller {

    protected $moduleRepo;
    protected $subModuleRepo;
    protected $profileRepo;
    protected $module;

    public function __construct(ModuleRepo $moduleRepo, SubModuleRepo $subModuleRepo, ProfileRepo $profileRepo){
        $this->moduleRepo= $moduleRepo;
        $this->subModuleRepo = $subModuleRepo;
        $this->profileRepo = $profileRepo;
    }

    public function findModule($id)
    {
        $this->module= $this->moduleRepo->find($id);
    }

    public function index()
    {
        return \View::make('modules.index');
    }

    public function formCreate()
    {
        return \View::make('modules.create');
    }

    public function formEdit()
    {
        return \View::make('modules.edit');
    }

    public function all()
    {
        $modules = $this->moduleRepo->paginate(25);

        return \Response::json($modules);
    }

    public function create(MainMenu $mainMenu)
    {
        $module = $this->moduleRepo->getModel();
        $data = \Request::all();
        $manager = new ModuleManager($module,$data);
        $manager->save();

        foreach($data['sub_modules'] as $item)
        {
            $data = $item;
            $data['module_id'] = $module->id;
            if(isset($data['link_menu']))
            {
                $data['link_menu'] = $data['link_menu']['value'];
            }
            $sub_module = $this->subModuleRepo->getModel();
            $manager = new SubModuleManager($sub_module, $data);
            $manager->save();
            $profiles = array();
            foreach($item['profiles'] as $profile)
            {
                $profiles[] = $profile['id'];
            }
            $sub_module->profiles()->attach($profiles);
        }

        $profiles = $this->profileRepo->all();

        foreach($profiles as $profile){
            $response = $mainMenu->setMenu($profile->id);
        }

        return \Response::json(['response'=>true, 'data'=>$module->name]);
    }

    public function find($id)
    {
        $this->findModule($id);
        return \Response::json($this->module);
    }

    public function edit($id, MainMenu $mainMenu)
    {
        $this->findModule($id);

        $data = \Request::all();

        $manager = new ModuleManager($this->module, $data);
        $manager->save();

        foreach($this->module->submodules as $item)
        {
            $item->profiles()->detach();
            $item->delete();
        }

        foreach($data['sub_modules'] as $item)
        {
            $data = $item;
            $data['module_id'] = $this->module->id;

            if(isset($data['link_menu']))
            {
                $data['link_menu'] = $data['link_menu']['value'];
            }

            $sub_module = $this->subModuleRepo->getModel();
            $manager = new SubModuleManager($sub_module, $data);
            $manager->save();
            $profiles = array();
            foreach($item['profiles'] as $profile)
            {
                $profiles[] = $profile['id'];
            }
            $sub_module->profiles()->attach($profiles);
        }

        $profiles = $this->profileRepo->all();

        foreach($profiles as $profile){
            $mainMenu->setMenu($profile->id);
        }

        return \Response::json(['response'=>true, 'data'=>$this->module->name]);
    }

    public function delete($id, MainMenu $mainMenu)
    {
        $this->findModule($id);

        foreach($this->module->submodules as $item)
        {
            $item->profiles()->detach();
            $item->delete();
        }

        $this->module->delete();

        $profiles = $this->profileRepo->all();

        foreach($profiles as $profile){
            $mainMenu->setMenu($profile->id);
        }

        return \Response::json(['response'=>true]);
    }

} 