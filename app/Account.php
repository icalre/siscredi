<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'name',
        'amount'
    ];

    protected $appends = [
        'total'
    ];

    public function getTotalAttribute()
    {
        return 0;
    }
}
