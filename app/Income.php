<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{

    protected $fillable = [
        'description',
        'amount',
        'extra_account_id',
        'state'
    ];

    public function extraAccount()
    {
        return $this->belongsTo('App\ExtraAccount');
    }
}
