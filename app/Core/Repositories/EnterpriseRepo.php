<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\Enterprise;

class EnterpriseRepo extends BaseRepo{

    public function getModel()
    {
        return new Enterprise;
    }

    public function find($id)
    {
        return Enterprise::where('id','=',$id)
            ->with(['people','department','department.provinces','province', 'province.districts','district'])
            ->first();
    }

    public function search($q,$take)
    {
        return Enterprise::where('razon_social','like',$q.'%')
            ->orWhere('ruc','like',$q.'%')
            ->take($take)
            ->get();
    }

    public function getLoans($id)
    {
        return Enterprise::with(['loans'=>function($query)
        {
            $query->whereIn('state', ['Vigente', 'Atrasado']);
        }, 'loans.customer','loans.customer.customer'])
            ->where('id', $id)
            ->first();
    }
} 