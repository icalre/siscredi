<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\RequestLoan;

class RequestLoanRepo extends BaseRepo{
    public function getModel()
    {
        return new RequestLoan;
    }


    public function paginateFilter($per_page, $employee_id = null)
    {
        return RequestLoan::with(['employee','employee.person', 'person'])
            ->where('id','>',0)
            ->filterEmployee($employee_id)
            ->orderBy('id','DESC')
            ->paginate($per_page);
    }

    public function find($id)
    {
        return RequestLoan::where('id','=',$id)
            ->with(['employee','employee.person', 'person'])
            ->first();
    }


} 
