<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\Site;

class SiteRepo extends BaseRepo{

    public function getModel()
    {
        return new Site;
    }

    public function all()
    {
        return Site::with(['areas', 'areas.areaJobs'])
            ->where('id','>',0)
            ->get();
    }

    public function find($id)
    {
        return Site::with(['company'])->where('id',$id)
                ->first();
    }

    public function paginate($per_page)
    {
        return Site::with(['company'])
            ->paginate($per_page);
    }
}