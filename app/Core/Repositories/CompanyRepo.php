<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\Company;

class CompanyRepo extends BaseRepo{

    public function getModel()
    {
        return new Company;
    }

    public function find($id)
    {
        return Company::where('id','=',$id)
            ->with(['department','department.provinces','province', 'province.districts','district','sites'])
            ->first();
    }
} 