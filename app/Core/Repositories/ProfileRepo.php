<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\Profile;
use App\Core\Entities\Module;

class ProfileRepo extends BaseRepo{
    public function getModel()
    {
        return new Profile;
    }

    public function getList()
    {
        return Profile::lists('name', 'id');
    }

    public function find($id)
    {
        return Profile::where('id','=',$id)
            ->with(['sub_modules'])
            ->first();
    }

    public function modules($id)
    {
        return Module::with([
            'submodules' => function($query) use ($id){
                $query->join('permissions','permissions.sub_module_id', '=', 'sub_modules.id');
                $query->where('permissions.profile_id', '=',$id);
                $query->orderBy('name','ASC');
                $query->get();
            }
        ])->get();
    }
} 