<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\SentMessage;

class SentMessageRepo extends BaseRepo{
    public function getModel()
    {
        return new SentMessage;
    }

    public function paginateFilter($per_page, $message = NULL)
    {
        return SentMessage::with(['person'])
            ->where('id','>',0)
            ->filterMessage($message)
            ->orderBy('created_at', 'DESC')
            ->paginate($per_page);
    }


    public function find($id)
    {
        return SentMessage::where('id','=',$id)
            ->with(['person'])
            ->first();
    }

} 