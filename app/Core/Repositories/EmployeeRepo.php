<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;

use App\Core\Entities\Employee;

class EmployeeRepo extends BaseRepo
{

    public function getModel()
    {
        return new Employee;
    }

    public function all()
    {
        return Employee::with(['areaJob', 'area', 'person', 'site'])
            ->where('state', 1)
            ->get();
    }

    public function totalCustomers()
    {
        return Employee::with(['areaJob', 'area', 'person', 'site', 'loans' => function ($query) {
            $query->selectRaw('DISTINCT loans.id,loans.customer_id, loans.employee_id');
            $query->where('state', 'Vigente');
            $query->groupBy('customer_id');
        }])
            ->where('state', 1)
            ->get();
    }


    public function find($id)
    {
        return Employee::where('id', '=', $id)
            ->with(['areaJob', 'area', 'person', 'site', 'site.areas', 'site.areas.areaJobs',
                'loans' => function ($query) {
                    $query->select('loans.customer_id',
                        'loans.amount',
                        'loans.date_start',
                        'loans.state',
                        'loans.date_end',
                        'loans.code',
                        'loans.employee_id',
                        'loans.interest',
                        'loans.currency',
                        'loans.period',
                        'loans.type_payment',
                        'loans.quotas',
                        'loans.warranty_option',
                        'loans.endorsement_option',
                        'loans.observations',
                        'loans.date_disbursement',
                        'loans.nro_contrato',
                        'loans.person_id',
                        'loans.enterprise_id',
                        'loans.modalidad',
                        'loans.feed_option',
                        'people.last_name');
                    $query->join('customers', 'customers.id', '=', 'loans.customer_id');
                    $query->join('people', 'customers.customer_id', '=', 'people.id');
                    $query->whereIn('loans.state', ['Vigente', 'Atrasado']);
                    $query->groupBy('loans.customer_id');
                    $query->orderBy('people.last_name', 'ASC');
                }, 'loans.customer', 'loans.customer.customer'
            ])
            ->first();
    }

    public function paginateFilter($per_page, $site_id, $area_id, $job_id)
    {
        return Employee::where('id', '>', 0)->filterSite($site_id)
            ->filterArea($area_id)
            ->filterJob($job_id)
            ->with(['areaJob', 'area', 'person', 'site'])
            ->paginate($per_page);
    }

    public function paginate($per_page)
    {
        return Employee::with(['areaJob', 'area', 'person', 'site'])
            ->where('id', '>', 0)
            ->paginate($per_page);
    }


    public function search($q, $take)
    {
        return Employee::select("employees.id",
            "employees.code",
            "employees.state",
            "employees.area_job_id",
            "employees.site_id",
            "employees.area_id",
            "employees.person_id")
            ->with(['areaJob', 'area', 'person', 'site'])
            ->join('people', 'people.id', '=', 'employees.person_id')
            ->whereRaw('CONCAT(people.name," ", people.last_name) like "%' . $q . '%"')
            ->orWhere('people.dni', 'like', '%' . $q . '%')
            ->take($take)
            ->get();
    }
} 