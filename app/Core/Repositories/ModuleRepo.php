<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\Module;

class ModuleRepo extends BaseRepo{
    public function getModel()
    {
        return new Module;
    }

    public function all()
    {
        return Module::with(['submodules'])
            ->where('id','>',0)
            ->get();
    }

    public function paginate($per_page)
    {
        return Module::with(['submodules'])
            ->where('id','>',0)
            ->paginate($per_page);
    }

    public function find($id)
    {
        return Module::where('id','=',$id)
            ->with(['submodules','submodules.profiles'])
            ->first();
    }
}