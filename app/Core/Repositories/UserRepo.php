<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;

use App\Core\Entities\User;

class UserRepo extends BaseRepo
{

    public function getModel()
    {
        return new User;
    }


    public function all()
    {
        return User::with(['profile',
            'person'])
            ->where('id', '>', 0)
            ->get();
    }

    public function paginateFilter($per_page, $profile_id)
    {
        return User::filterProfile($profile_id)
            ->with(['profile',
                'person'])
            ->where('master', 0)
            ->paginate($per_page);
    }

    public function paginate($per_page)
    {
        return User::with(['profile',
            'person'])
            ->where('master', 0)
            ->paginate($per_page);
    }

    public function find($id)
    {
        return User::where('id', '=', $id)->with(['profile',
            'person', 'person.department', 'person.district', 'person.province'])->first();
    }

    public function search($q, $take)
    {
        return User::select('users.name', 'users.profile_id', 'users.person_id')
            ->with(['profile',
                'person'])
            ->join('people', 'people.id', '=', 'users.person_id')
            ->whereRaw('CONCAT(people.name," ", people.last_name) like "%' . $q . '%"')
            ->orWhere('users.name', 'like', $q . '%')
            ->take($take)
            ->get();
    }

    public function validateCode($code)
    {
        $data = ['response' => false];

        $code = User::where('code', $code)
            ->first();

        if (!empty($code)) {
            $data['response'] = true;
            $data['user'] = $code;
            return $data;
        }

        return $data;
    }


}