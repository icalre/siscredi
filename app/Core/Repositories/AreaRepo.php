<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\Area;

class AreaRepo extends BaseRepo{

    public function getModel()
    {
        return new Area;
    }

    public function all()
    {
        return Area::with(['areaJobs'])->get();
    }
    public function getList()
    {
        return Area::lists('name', 'id');
    }

    public function find($id)
    {
        return Area::with(['sites'])
            ->where('id','=',$id)
            ->first();
    }

    public function search($q,$take)
    {
        return Area::where('name', 'like', '%'.$q.'%')
            ->take($take)
            ->get();
    }
} 