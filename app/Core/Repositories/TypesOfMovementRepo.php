<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\TypesOfMovement;

class TypesOfMovementRepo extends BaseRepo{

    public function getModel()
    {
        return new TypesOfMovement;
    }

} 