<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\Payment;
use App\Core\Entities\Loan;

class PaymentRepo extends BaseRepo{

    public function getModel()
    {
        return new Payment;
    }

    public function find($id)
    {
        return Payment::with(['detail'])
            ->where('id', $id)
            ->first();
    }

    public function payments($date1,$date2,$id=NULL, $enterprise_id = NULL)
    {
        return Loan::select('loans.id','loans.customer_id' ,
            'loans.amount',
            'loans.date_start' ,
            'loans.state' ,
            'loans.date_end' ,
            'loans.code',
            'loans.employee_id',
            'loans.interest' ,
            'loans.currency',
            'loans.period',
            'loans.type_payment',
            'loans.quotas',
            'loans.warranty_option',
            'loans.endorsement_option',
            'loans.observations',
            'loans.date_disbursement',
            'loans.nro_contrato',
            'payments.expiration')
                ->with(['customer','customer.customer', 'employee','employee.person',
                    'payments'=>function($q) use ($date1,$date2) {
                            $q->whereBetween('expiration',[$date1,$date2]);
                            $q->where('state','=','Pendiente');
                            $q->orderBy('expiration','ASC');
                        }])
                ->join('payments','loans.id', '=', 'payments.loan_id')
                ->whereBetween('payments.expiration',[$date1,$date2])
                ->where('payments.state','=','Pendiente')
                ->whereIn('loans.state',['Vigente', 'Atrasado'])
                ->where('employee_id','like',$id.'%')
                ->filterEnterprise($enterprise_id)
                ->orderBy('payments.expiration','ASC')
                ->groupBy('loans.id')
                ->get();
    }

    public function findPayment($date,$code)
    {
        return Loan::with(['employee','employee.person','payments'=>function($q) use ($date) {
            $q->whereBetween('expiration',['0000-00-00',$date]);
            $q->where('state','!=','Pagado');
            $q->where('state','!=','Anulado');
        }])
            ->Where('code','=',$code)
            ->first();
    }

    public function sumCapital($loan_id)
    {
        return Payment::where('loan_id','=',$loan_id)
                ->where('state','=','Pagado')
                ->sum('capital');
    }

    public function getPendingPayments($code)
    {
        return Loan::with(['employee','employee.person','payments'=>function($q){
                        $q->where('state','=','Pendiente');
                    }])
                    ->where('code','=',$code)
                    ->first();
    }

    public function loanPayments($loan_id)
    {
        return Payment::where('loan_id', $loan_id)
                ->get();
    }
} 