<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;

use App\Core\Entities\SalaryDiscount;

class SalaryDiscountRepo extends BaseRepo
{

    public function getModel()
    {
        return new SalaryDiscount;
    }

    public function paginateFilter($per_page, $type, $employee)
    {
        return SalaryDiscount::where('id', '>', 0)
            ->filterType($type)
            ->filterEmployee($employee)
            ->with(['employee', 'employee.person'])
            ->orderBy('created_at', 'DESC')
            ->paginate($per_page);
    }

    public function find($id)
    {
        return SalaryDiscount::with(['employee', 'employee.person'])
            ->where('id', $id)
            ->first();
    }

    public function getEmployeeDiscounts($employee)
    {
        return SalaryDiscount::filterEmployee($employee)
            ->where('state', 1)
            ->get();
    }

    public function salaryDiscountRange($date_star, $date_end)
    {
        return SalaryDiscount::whereBetween('created_at', [$date_star . ' 00:00:00', $date_end . ' 23:59:59'])
            ->get();
    }

} 