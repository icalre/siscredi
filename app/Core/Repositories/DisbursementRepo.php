<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\Disbursement;
use App\Core\Entities\CashDeskDetail;

class DisbursementRepo extends BaseRepo{
    public function getModel()
    {
        return new Disbursement;
    }

    public function getDisbursements($id)
    {
        return CashDeskDetail::where('id','=',$id)->with(['disbursements','disbursements.loan','disbursements.loan.customer',
            'disbursements.loan.customer.person'])->first();
    }
} 