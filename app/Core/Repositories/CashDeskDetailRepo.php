<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\CashDesk;
use App\Core\Entities\CashDeskDetail;
use App\Core\Entities\Disbursement;
use App\Core\Entities\PaymentDocument;

class CashDeskDetailRepo extends BaseRepo{

    public function getModel()
    {
        return new CashDeskDetail;
    }


    public function find($id)
    {
        return CashDeskDetail::where('id','=',$id)
            ->with(['cashDesk','paymentDocuments','paymentDocuments.customer','paymentDocuments.customer.customer',
                'paymentDocuments.details','paymentDocuments.details.payment','paymentDocuments.details.payment.loan'
                ,'incomes',
                'expenses',
                'disbursements',
                'disbursements.loan',
                'disbursements.loan.customer',
                'disbursements.loan.customer.customer'
            ])
            ->first();
    }

    public function getDetail($cash_desk_id)
    {
        return CashDesk::with(['cashDeskDetails'=>function($query){
            $query->where('state',1);
            $query->first();
        }])->where('id',$cash_desk_id)->first();
    }

    public function getDetailFlow($cash_desk_id)
    {
        $cash_desk_details = CashDeskDetail::where('state',1)
            ->where('cash_desk_id', $cash_desk_id)
            ->get();

        if(count($cash_desk_details)> 0)
        {
            $cash_desk_detail = $cash_desk_details[0];
            $cash_desk_detail->total_pay = $cash_desk_detail->paymentDocuments()->where('state', 'Correct')->sum('amount');
            $cash_desk_detail->income_amount = $cash_desk_detail->incomes()->sum('amount');
            $cash_desk_detail->desembolsos = $cash_desk_detail->disbursements()->sum('amount');
            $cash_desk_detail->expenses_amount = $cash_desk_detail->expenses()->where('sub_type',0)->sum('amount');
            $cash_desk_detail->expenses_amount_g = $cash_desk_detail->expenses()->where('sub_type',1)->sum('amount');
            $cash_desk_detail->total_amount = $cash_desk_detail->income_amount - $cash_desk_detail->desembolsos
                - $cash_desk_detail->expenses_amount - $cash_desk_detail->expenses_amount_g
                + $cash_desk_detail->total_pay + $cash_desk_detail->initial_amount;
        }else{
            $cash_desk_detail = CashDeskDetail::where('cash_desk_id',$cash_desk_id)
                ->orderBy('created_at', 'desc')->first();

            if(isset($cash_desk_detail->id))
            {
                $cash_desk_detail->total_pay = $cash_desk_detail->paymentDocuments()->where('state', 'Correct')->sum('amount');
                $cash_desk_detail->income_amount = $cash_desk_detail->incomes()->sum('amount');
                $cash_desk_detail->desembolsos = $cash_desk_detail->disbursements()->sum('amount');
                $cash_desk_detail->expenses_amount = $cash_desk_detail->expenses()->where('sub_type',0)->sum('amount');
                $cash_desk_detail->expenses_amount_g = $cash_desk_detail->expenses()->where('sub_type',1)->sum('amount');
            }
        }

        return $cash_desk_detail;
    }

    public function getPaymentDocuments($cash_desk_detail_id)
    {
        return PaymentDocument::with(['details', 'details.payment', 'details.payment.loan',
            'details.payment.loan.customer','details.payment.loan.customer.customer'])
            ->where('cash_desk_detail_id', $cash_desk_detail_id)
            ->get();

    }

    public function getDisbursements($cash_desk_detail_id)
    {
        return Disbursement::with(['loan', 'loan.customer','loan.customer.customer'])
            ->where('cash_desk_detail_id', $cash_desk_detail_id)
            ->get();
    }

    public function lastDetail()
    {
        if(auth()->user()->master == 1 || auth()->user()->profile_id == 1){
            return CashDeskDetail::with(['cashDesk','getConsignments'=>function($query){
                    $query->where('consignments.state',1);
                }])
                ->orderBy('created_at', 'desc')
                ->first();
        }

        return CashDeskDetail::where('user_id', \Auth::user()->id)
            ->with(['cashDesk','getConsignments'=>function($query){
                $query->where('consignments.state',1);
            }])
        ->orderBy('created_at', 'desc')
            ->first();
    }

    public function getSendCashDeskDetail($cash_desk_detail_id, $cash_desk_id)
    {
        return CashDeskDetail::where('state','1')
            ->where('id','!=',$cash_desk_detail_id)
            ->where('cash_desk_id', '!=', $cash_desk_id)
            ->first();
    }

    
} 
