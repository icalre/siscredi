<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\Department;

class DepartmentRepo extends BaseRepo{

    public function getModel()
    {
        return new Department;
    }

    public function all()
    {
        return Department::with(['provinces','provinces.districts'])->get();
    }
} 