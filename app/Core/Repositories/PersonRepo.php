<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\Person;

class PersonRepo extends BaseRepo{

    public function getModel()
    {
        return new Person;
    }

    public function find($id)
    {
        return Person::where('id','=',$id)
            ->with(['department','department.provinces','province', 'province.districts','district'])
            ->first();
    }

    public function search($q,$take)
    {
        return Person::with(['customer', 'customer.loans'=>function($q)
        {
            $q->orderBy('id', 'DESC');
        }, 'customer.comments'])
                ->whereRaw('CONCAT(name," ", last_name) like "%'.$q.'%"')
                ->orWhere('dni','like',$q.'%')
                ->take($take)
                ->get();
    }
} 