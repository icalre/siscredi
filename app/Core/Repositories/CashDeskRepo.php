<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\CashDesk;

class CashDeskRepo extends BaseRepo{

    public function getModel()
    {
        return new CashDesk;
    }


    public function find($id)
    {
        return CashDesk::where('id','=',$id)
            ->with(['site'])
            ->first();
    }

    public function cashDeskAvalible()
    {
        return CashDesk::where('state',0)
            ->with(['site'])
            ->first();
    }


    public function paginateFilter($per_page,$site_id)
    {
        return CashDesk::with(['site'])
            ->where('id', '>', 0)
            ->filterSite($site_id)
            ->paginate($per_page);
    }


    public function search($q,$site_id,$take)
    {
        return CashDesk::with(['site'])
            ->filterSite($site_id)
            ->where('name','like','%'.$q.'%')
            ->take($take)
            ->get();
    }
} 