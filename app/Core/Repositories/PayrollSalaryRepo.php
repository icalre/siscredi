<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\PayrollSalary;

class PayrollSalaryRepo extends BaseRepo{

    public function getModel()
    {
        return new PayrollSalary;
    }

    public function paginateFilter($per_page,$year,$month)
    {
        return PayrollSalary::where('id','>',0)
            ->filterYear($year)
                ->filterMonth($month)
                ->paginate($per_page);
    }

    public function find($id)
    {
        return PayrollSalary::with(['details','details.employee','details.employee.person', 'details.salaryDiscounts'])
            ->where('id',$id)
            ->first();
    }

    public function validPayrollSalary()
    {
        return PayrollSalary::where('state',1)
            ->first();
    }

} 