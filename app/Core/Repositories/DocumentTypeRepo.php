<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\DocumentType;

class DocumentTypeRepo extends BaseRepo{

    public function getModel()
    {
        return new DocumentType;
    }

} 