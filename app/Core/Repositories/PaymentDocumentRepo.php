<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\PaymentDocument;
use App\Core\Entities\CashDeskDetail;

class PaymentDocumentRepo extends BaseRepo{

    public function getModel()
    {
        return new PaymentDocument;
    }

    public function find($id)
    {
        return PaymentDocument::where('id','=',$id)
            ->with(['user','cashDeskDetail',
                'customer','details',
                'details.payment',
                'details.payment.loan', 'expenses'])
            ->first();
    }

    public function getDeposits($id)
    {
        return CashDeskDetail::where('id','=',$id)->with(['paymentDocuments','paymentDocuments.details','paymentDocuments.customer',
            'paymentDocuments.customer.person','paymentDocuments.user','paymentDocuments.details.payment',
            'paymentDocuments.details.payment.loan'])
            ->first();
    }
} 