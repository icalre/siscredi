<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;

use App\Core\Entities\Customer;
use App\Core\Entities\Enterprise;
use App\Core\Entities\Person;

class CustomerRepo extends BaseRepo
{
    public function getModel()
    {
        return new Customer;
    }

    public function find($id)
    {
        return Customer::where('id', $id)
            ->with(['customer', 'comments'])
            ->first();
    }

    public function findCustomerLoans($id)
    {
        return Customer::where('id', $id)
            ->with(['customer', 'loans' => function ($query) {
                $query->where('state', 'Vigente');
            }, 'loans.payments' => function ($query) {
                $query->where('state', 'Pendiente');
                $query->groupBy('loan_id');
            }, 'loans.employee', 'loans.employee.person'])
            ->first();
    }

    public function paginate2($per_page, $type, $rate)
    {
        return Customer::where('id', '>', 0)->type($type)
            ->rate($rate)
            ->with(['customer'])
            ->paginate($per_page);
    }

    public function validateCustomer($type, $id, $customer_id = null)
    {
        if ($type == 'Person') {
            $person = Person::find($id);
            $customer = $person->customer()->where('id', '>', 0)
                ->filterCustomer($customer_id)->first();

        } elseif ($type == 'Enterprise') {
            $enterprise = Enterprise::find($id);
            $customer = $enterprise->customer()->where('id', '>', 0)
                ->filterCustomer($customer_id)->first();
        }

        if (isset($customer->id)) {
            return true;
        } else {
            return false;
        }
    }

    public function searchByDni($dni)
    {

        return Customer::select('customers.id', 'customers.customer_id', 'customers.customer_type')
            ->where('id', '>', 0)
            ->type('person')
            ->with(['customer'])
            ->join('people', 'people.id', '=', 'customers.customer_id')
            ->where('people.dni', $dni)
            ->first();
    }

    public function search($type, $q, $rate, $take)
    {
        if ($type == 'Person') {
            return Customer::select('customers.id', 'customers.customer_id', 'customers.customer_type', 'customers.code', 'customers.rate')
                ->type($type)
                ->rate($rate)
                ->with(['customer'])
                ->join('people', 'people.id', '=', 'customers.customer_id')
                ->whereRaw('CONCAT(people.last_name," ",people.name) like "%' . $q . '%"')
                ->take($take)
                ->get();
        } elseif ($type == 'Enterprise') {
            return Customer::select('customers.customer_id', 'customers.customer_type', 'customers.code', 'customers.rate')
                ->type($type)
                ->rate($rate)
                ->with(['customer'])
                ->join('enterprises', 'enterprises.id', '=', 'customers.customer_id')
                ->where('razon_social', 'like', '%' . $q . '%')
                ->take($take)
                ->get();
        }
    }

    public function commission($employee_id, $date1, $date2)
    {
        $customers = Customer::with(['loans' => function ($query) use ($employee_id) {
            $query->where('employee_id', $employee_id);
        }, 'loans.payments' => function ($query) use ($date1, $date2) {
            $query->whereBetween('date_pay', [$date1 . ' 00:00:00', $date2 . ' 23:59:59']);
            $query->where('state', 'Pagado');
        }, 'loans.enterprise'])
            ->select('customers.id', 'customers.customer_id', 'customers.code', 'people.name', 'people.last_name', 'people.dni')
            ->join('people', 'people.id', '=', 'customers.customer_id')
            ->join('loans', 'customers.id', '=', 'loans.customer_id')
            ->where('loans.employee_id', $employee_id)
            ->groupBy('customers.id')
            ->get();

        return $customers;
    }

}
