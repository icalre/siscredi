<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\Loan;

class LoanRepo extends BaseRepo{
    public function getModel()
    {
        return new Loan;
    }

    public function all2($interest)
    {
        return Loan::with(['payments','customer','customer.person','employee','employee.person'])
        ->filter($interest)
        ->orderBy('id','DESC')
        ->get();
    }

    public function paginateFilter($per_page, $type = null, $warranty= null, $state= null, $aval= null, $interest = null,
                $modalidad = null, $customer_type = null)
    {
        return Loan::with(['customer', 'customer.customer','employee','employee.person'])
            ->where('id','>',0)
            ->warranty($warranty)
            ->aval($aval)
            ->type($type)
            ->filter($interest)
            ->filterState($state)
            ->filterModalidad($modalidad)
            ->filterCustomerType($customer_type)
            ->orderBy('id','DESC')
            ->paginate($per_page);
    }

    public function find($id)
    {
        return Loan::where('id','=',$id)
            ->orWhere('code','=',$id)
            ->with(['payments','customer','person','customer.customer','employee','employee.person',
                    'warranties', 'endorsement','comments','enterprise',
                    'endorsement.person', 'expenses'=>function($query)
                {
                    $query->where('state', 'Pendiente');
                }])
            ->first();
    }

    public function findPayments($id)
    {
        return Loan::where('id','=',$id)
            ->with(['payments'])
            ->first();
    }



    public function findBusiness($id,$employee_id)
    {
        return Loan::where('id','=',$id)
            ->orWhere('code','=',$id)
            ->where('employee_id',$employee_id)
            ->with(['payments','customer','customer.person','employee','employee.person', 'warranties', 'endorsement',
                'endorsement.person'])
            ->first();
    }

    public function search($q,$type = null, $warranty= null, $state= null, $aval= null, $interest = null)
    {
        return Loan::select('loans.id','loans.code','loans.state','loans.currency','loans.customer_id','loans.employee_id',
            'loans.amount','loans.warranty_option','loans.endorsement_option','loans.date_disbursement','loans.type_payment')
            ->with(['payments','customer','customer.customer','employee','employee.person'])
            ->where('loans.id','>',0)
            ->warranty($warranty)
            ->aval($aval)
            ->type($type)
            ->filter($interest)
            ->filterState($state)
            ->join('customers','customers.id','=','loans.customer_id')
            ->join('people','people.id','=','customers.customer_id')
            ->where('loans.code','like','%'.$q.'%')
            ->orWhereRaw('CONCAT(people.name," ", people.last_name) like "%'.$q.'%"')
            ->orderBy('loans.id','ASC')
            ->get();
    }
} 
