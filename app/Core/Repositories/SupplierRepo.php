<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;

use App\Core\Entities\Person;
use App\Core\Entities\Enterprise;
use App\Core\Entities\Supplier;

class SupplierRepo extends BaseRepo
{

    public function getModel()
    {
        return new Supplier;
    }

    public function find($id)
    {
        return Supplier::where('id', $id)
            ->with(['supplier'])
            ->first();
    }

    public function paginate2($per_page, $type)
    {
        return Supplier::where('id', '>', 0)
            ->type($type)
            ->with(['supplier'])
            ->paginate($per_page);
    }

    public function validateSupplier($type, $id)
    {
        if ($type == 'Person') {
            $person = Person::find($id);
            $supplier = $person->supplier()->where('id', '>', 0)
                ->filterSupplier($id)->first();
        } elseif ($type == 'Enterprise') {
            $enterprise = Enterprise::find($id);
            $supplier = $enterprise->supplier()->where('id', '>', 0)
                ->filterSupplier($id)->first();
        }

        if (isset($supplier->id)) {
            return true;
        } else {
            return false;
        }
    }

    public function search($type, $q, $take)
    {
        if ($type == 'Person') {
            return Supplier::type($type)
                ->with(['supplier'])
                ->join('people', 'people.id', '=', 'suppliers.supplier_id')
                ->whereRaw('CONCAT(people.name," ", people.last_name) like "%' . $q . '%"')
                ->take($take)
                ->get();
        } elseif ($type == 'Enterprise') {
            return Supplier::type($type)
                ->with(['supplier'])
                ->join('enterprises', 'enterprises.id', '=', 'suppliers.supplier_id')
                ->where('razon_social', 'like', '%' . $q . '%')
                ->take($take)
                ->get();
        }
    }

} 