<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;
use App\Core\Entities\CashDeskExpense;

class CashDeskExpenseRepo extends BaseRepo{

    public function getModel()
    {
        return new CashDeskExpense;
    }


    public function find($id)
    {
        return CashDeskExpense::with(['discount','discount.employee','discount.employee.person'])
            ->where('id',$id)
            ->first();
    }
    
} 