<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 03:25 PM
 */

namespace App\Core\Repositories;

use App\Core\Entities\AreaJob;

class AreaJobRepo extends BaseRepo
{

    public function getModel()
    {
        return new AreaJob;
    }

    public function getList()
    {
        return AreaJob::lists('name', 'id');
    }

    public function paginateFilter($per_page, $area_id)
    {
        return AreaJob::where('id', '>', 0)
            ->filter($area_id)
            ->paginate($per_page);
    }

    public function find($id)
    {
        return AreaJob::where('id', '=', $id)
            ->with(['area'])
            ->first();
    }

    public function search($type, $q)
    {
        return AreaJob::where('name', 'like', $q . '%')
            ->where('area_id', 'like', $type . '%')
            ->take(10)
            ->get();
    }
} 