<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Person extends Model {
    protected $fillable = ['name','last_name','address','email','dni','department_id','province_id',
        'district_id','date_of_birth','telephone','gender','activity', 'file', 'phone_reference',
        'business_address'];

    protected $appends = ['full_name','customer_name','customer_document','type'];

    protected $dates = ['deleted_at'];

    public function department()
    {
        return $this->belongsTo('App\Core\Entities\Department');
    }

    public function employee()
    {
        return $this->hasOne('App\Core\Entities\Employee');
    }

    public function province()
    {
        return $this->belongsTo('App\Core\Entities\Province');
    }

    public function district()
    {
        return $this->belongsTo('App\Core\Entities\District');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtoupper($value);
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = strtoupper($value);
    }

    public function setAddressAttribute($value)
    {
        $this->attributes['address'] = strtoupper($value);
    }


    public function getFullNameAttribute()
    {
        return $this->attributes['last_name'].' '.$this->attributes['name'];
    }

    public function customer()
    {
        return $this->morphOne('App\Core\Entities\Customer','customer');
    }

    public function supplier()
    {
        return $this->morphOne('App\Core\Entities\Supplier','supplier');
    }

    public function getCustomerNameAttribute()
    {
        return $this->attributes['last_name'].' '.$this->attributes['name'];
    }

    public function getCustomerDocumentAttribute()
    {
        return $this->attributes['dni'];
    }

    public function setDateOfBirthAttribute($value)
    {
        if(!empty($value))
        {
            $this->attributes['date_of_birth'] = Carbon::createFromFormat('d-m-Y',$value)->format('Y-m-d');
        }
    }

    public function getDateOfBirthAttribute()
    {
        if($this->attributes['date_of_birth'] == '0000-00-00')
        {
            return '00-00-0000';
        }

         return Carbon::createFromFormat('Y-m-d', $this->attributes['date_of_birth'])->format('d-m-Y');
    }

    public function getTypeAttribute()
    {
        return 'person';
    }

    public function user()
    {
        return $this->hasOne('App\Core\Entities\User');
    }
}