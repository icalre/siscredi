<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class SentMessage extends Model {
    protected $fillable = ['message', 'person_id'];


    public function person()
    {
        return $this->belongsTo('App\Core\Entities\Person');
    }

    public function scopeFilterMessage($query,$value)
    {
        if(!empty($value))
        {
            return $query->where('message','like','%'.$value.'%');
        }
    }

}