<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CashDeskDetail extends Model {

    protected $fillable = ['arching', 'cash_desk_id', 'date_end', 'date_start', 'difference',
                            'initial_amount', 'state', 'total_amount','user_id',
        'efectivo',
        'cuenta_corriente'];

    public function cashDesk()
    {
        return $this->belongsTo('App\Core\Entities\CashDesk');
    }

    public function paymentDocuments()
    {
        return $this->hasMany('App\Core\Entities\PaymentDocument');
    }

    public function expenses()
    {
        return $this->hasMany('App\Core\Entities\CashDeskExpense');
    }

    public function incomes()
    {
        return $this->hasMany('App\Core\Entities\CashDeskIncome');
    }

    public function disbursements()
    {
        return $this->hasMany('App\Core\Entities\Disbursement');
    }

    public function getDateStartAttribute()
    {
        $date_start = Carbon::createFromFormat('Y-m-d H:i:s',$this->attributes['date_start'])->format('d-m-Y H:i:s');

        if($this->attributes['date_start'] == '0000-00-00 00:00:00')
        {
            $date_start = '-';
        }

        return $date_start;
    }

    public function getDateEndAttribute()
    {
        $date_end = Carbon::createFromFormat('Y-m-d H:i:s',$this->attributes['date_end'])->format('d-m-Y H:i:s');

        if($this->attributes['date_end'] == '0000-00-00 00:00:00')
        {
            $date_end = '-';
        }

        return $date_end;
    }

    public function getConsignments()
    {
        return $this->belongsToMany('App\Core\Entities\CashDeskDetail','consignments','to_cash_desk_detail_id','cash_desk_detail_id')
            ->withPivot(['amount', 'state','date']);
    }

    public function sendConsignments()
    {
        return $this->belongsToMany('App\Core\Entities\CashDeskDetail','consignments','cash_desk_detail_id','to_cash_desk_detail_id')
            ->withPivot(['amount','state','date']);
    }

    public function accountDetails()
    {
        return $this->hasMany('App\AccountDetail');
    }

    public function user()
    {
        return $this->belongsTo('App\Core\Entities\User');
    }
}