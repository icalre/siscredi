<?php namespace
App\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class DepartmentStore extends Model {

	protected $fillable = ['name','description','code','area_id','site_id','printer_option','printer_name','printer_code',
                            'print_number','screen_option'];


    public function area()
    {
        return $this->belongsTo('App\Entities\Area');
    }

    public function site()
    {
        return $this->belongsTo('App\Entities\Site');
    }

    public function scopeFilterSite($query,$value)
    {
        if(!empty($value))
        {
            return $query->where('site_id',$value);
        }
    }

    public function scopeFilterArea($query,$value)
    {
        if(!empty($value))
        {
            return $query->where('area_id',$value);
        }
    }

    public function stocks()
    {
        return $this->hasMany('App\Entities\Stock');
    }

}
