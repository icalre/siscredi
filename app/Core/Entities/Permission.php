<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model {
    protected $fillable = ['profile_id','sub_module_id'];

    public function subModule()
    {
        return $this->belongsTo('App\Core\Entities\SubModule');
    }

    public function profile()
    {
        return $this->belongsTo('App\Core\Entities\Profile');
    }
}