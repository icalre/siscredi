<?php
namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class Endorsement extends Model {
    protected $fillable = ['loan_id',
	                        'person_id'];

    public function person()
    {
        return $this->belongsTo('App\Core\Entities\Person');
    }
}