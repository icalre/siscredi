<?php
namespace App\Core\Entities;

class PaymentDocumentDetail extends \Eloquent {
    protected $fillable = [
        'payment_document_id',
	    'payment_id',
	    'amount',
        'state'
    ];

    public function payment()
    {
        return $this->belongsTo('App\Core\Entities\Payment');
    }

    public function paymentDocument()
    {
        return $this->belongsTo('App\Core\Entities\PaymentDocument');
    }

}