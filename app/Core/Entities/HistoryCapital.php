<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class HistoryCapital extends Model {
    protected $fillable = ['month', 'name', 'year','amount','amount_capital'];
}