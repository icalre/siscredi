<?php
namespace App\Core\Entities;
use Carbon\Carbon;

class PaymentDocument extends \Eloquent {
    protected $fillable = [
        'amount' ,
        'cash_desk_detail_id',
        'user_id',
        'customer_id',
        'document_type_id',
        'payment_id', 'state', 'type_payment',
        'account_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Core\Entities\User');
    }

    public function customer()
    {
        return $this->belongsTo('App\Core\Entities\Customer');
    }

    public function cashDeskDetail()
    {
        return $this->belongsTo('App\Core\Entities\CashDeskDetail');
    }

    public function comments()
    {
        return $this->hasMany('App\Core\Entities\LoanComment');
    }

    public function details()
    {
        return $this->hasMany('App\Core\Entities\PaymentDocumentDetail');
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s',$this->attributes['created_at'])->format('d-m-Y H:i:s');
    }

    public function expenses()
    {
        return $this->hasMany('App\Core\Entities\LoanExpense');
    }

    public function account()
    {
        return $this->belongsTo('App\Account');
    }

}