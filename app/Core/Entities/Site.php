<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class Site extends Model {
    protected $fillable = ['name','company_id','code','address'];


    public function company()
    {
        return $this->belongsTo('App\Core\Entities\Company');
    }

    public function areas()
    {
        return $this->belongsToMany('App\Core\Entities\Area','area_sites','site_id','area_id');
    }

    public function areasDepartmentStore()
    {
        return $this->belongsToMany('App\Core\Entities\Area','department_stores','site_id','area_id');
    }

    public function departmentStores()
    {
        return $this->hasMany('App\Core\Entities\DepartmentStore');
    }

    public function cashDesks()
    {
        return $this->hasMany('App\Core\Entities\CashDesk');
    }

}