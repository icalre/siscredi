<?php
namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class Warranty extends Model {
    protected $fillable = ['loan_id',
	                        'Description',
                            'state',
                            'remate_date',
                            'liberado_date',
                            'reference_price',
                            'pb',
                            'pn',
                            'type'
                            ];
}