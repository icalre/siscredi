<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class SalaryDiscount extends Model {
    protected $fillable = ['amount',
        'employee_id',
        'state','type',
        'description',
        'payroll_salary_detail_id',
        'cash_desk_expense_id'];

    public function expense()
    {
        return $this->belongsTo('App\Core\Entities\CashDeskExpense');
    }

    public function employee()
    {
        return $this->belongsTo('App\Core\Entities\Employee');
    }

    public function scopeFilterType($query,$type)
    {
        if(!empty($type))
        {
            return $query->where('type',$type);
        }
    }

    public function scopeFilterEmployee($query,$employee)
    {
        if(!empty($employee))
        {
            return $query->where('employee_id',$employee);
        }
    }

    public function getStateAttribute()
    {
        if($this->attributes['state'] == 1)
        {
            return 'Pendiente';
        }else{
            return  'Descontado';
        }
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s',$this->attributes['created_at'])->format('d-m-Y');
    }

}