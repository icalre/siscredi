<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class Area extends Model {
    protected $fillable = ['name','description'];

    public function areaJobs()
    {
        return $this->hasMany('App\Core\Entities\AreaJob');

    }

    public function sites()
    {
        return $this->belongsToMany('App\Core\Entities\Site','area_sites','area_id','site_id');
    }
}