<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CashDeskExpense extends Model
{
    protected $table = 'cash_desk_expenses';
    protected $fillable = ['amount',
        'description',
        'cash_desk_detail_id',
        'type', 'sub_type', 'cash_desk_income_id', 'account_id', 'loan_id'];

    public function getCreatedAtAttribute()
    {
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d-m-Y');
        return $date;
    }

    public function discount()
    {
        return $this->hasOne('App\Core\Entities\SalaryDiscount');
    }

    public function income()
    {
        return $this->belongsTo('App\Core\Entities\CashDeskIncome', 'cash_desk_income_id');
    }

    public function cashDeskDetail()
    {
        return $this->belongsTo('App\Core\Entities\CashDeskDetail');
    }

    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    public function loan()
    {
        return $this->belongsTo('App\Core\Entities\Loan');
    }

}