<?php
namespace App\Core\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class LoanExpense extends Model {

    protected $fillable = [
        'description', 'loan_id', 'payment_document_id', 'payment_id', 'state','amount'
    ];

    public function loan()
    {
        return $this->belongsTo('App\Core\Entities\Loan');
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s',$this->attributes['created_at'])->format('d-m-Y H:i:s');
    }

}
