<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class HistoryCustomer extends Model {
    protected $fillable = ['count', 'list', 'month', 'name', 'year'];
}