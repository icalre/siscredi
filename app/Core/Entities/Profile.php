<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model {
    protected $fillable = ['name','description'];


    public function sub_modules()
    {
        return $this->belongsToMany('App\Core\Entities\SubModule','permissions');
    }
}