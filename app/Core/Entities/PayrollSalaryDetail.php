<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class PayrollSalaryDetail extends Model {
    protected $fillable = ['amount', 'amount_pay', 'discounts', 'employee_id', 'payroll_salary_id'];

    public function employee()
    {
        return $this->belongsTo('App\Core\Entities\Employee');
    }

    public function salaryDiscounts()
    {
        return $this->hasMany('App\Core\Entities\SalaryDiscount');
    }

}