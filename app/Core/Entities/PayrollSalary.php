<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class PayrollSalary extends Model {
    protected $fillable = ['name', 'state','month','year'];

    public function getStateAttribute()
    {
        if($this->attributes['state'] == 1)
        {
            return 'Pendiente';
        }else{
            return  'Cerrada';
        }
    }

    public function details()
    {
        return $this->hasMany('App\Core\Entities\PayrollSalaryDetail');
    }

    public function scopeFilterYear($query,$year)
    {
        if(!empty($year))
        {
            $query->where('year',$year);
        }
    }

    public function scopeFilterMonth($query,$month)
    {
        if(!empty($month))
        {
            $query->where('month',$month);
        }
    }

}