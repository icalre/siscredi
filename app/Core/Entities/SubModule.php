<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class SubModule extends Model {
    protected $fillable = ['name','uri','module_id', 'link_menu'];

    public function module()
    {
        return $this->belongsTo('App\Core\Entities\Module');
    }

    public function profiles()
    {
        return $this->belongsToMany('App\Core\Entities\Profile','permissions');
    }
}