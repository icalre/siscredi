<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model {
    protected $fillable = ['supplier_id','supplier_type','code'];
    protected $appends = ['type', 'type_esp'];


    public function getTypeAttribute ()
    {
        return substr($this->attributes['supplier_type'],18,20);
    }

    public function getTypeEspAttribute ()
    {
        $type = substr($this->attributes['supplier_type'],18,20);

        if($type == 'Person')
        {
            return 'Persona';
        }else{
            return 'Empresa';
        }
    }

    public function supplier()
    {
        return $this->morphTo();
    }

    public function scopeFilterSupplier($query,$id)
    {
        if(!empty($id))
        {
            return  $query->where('id', '!=',$id);
        }
    }

    public function scopeType($query,$type)
    {
        return  $query->where('supplier_type', 'like','%'.$type.'%');
    }

    public function purchases()
    {
        return $this->hasMany('App\Entities\Purchase');
    }
}