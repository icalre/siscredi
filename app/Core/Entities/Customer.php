<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model {
    protected $fillable = ['customer_id','customer_type','code','rate'];
    protected $appends = ['type', 'type_esp'];

    public function customer()
    {
        return $this->morphTo();
    }

    public function comments()
    {
        return $this->hasMany('App\Core\Entities\CustomerComment');
    }

    public function getTypeAttribute ()
    {
        return substr($this->attributes['customer_type'],18,20);
    }

    public function getTypeEspAttribute ()
    {
        $type = substr($this->attributes['customer_type'],18,20);

        if($type == 'Person')
        {
            return 'Persona';
        }else{
            return 'Empresa';
        }
    }

    public function scopeType($query,$type)
    {
        return  $query->where('customer_type', 'like','%'.$type.'%');
    }

    public function scopeRate($query,$value)
    {
        if(!empty($value))
        {
            return  $query->where('rate',$value);
        }
    }

    public function scopeFilterCustomer($query,$id)
    {
        if(!empty($id))
        {
            return  $query->where('id', '!=',$id);
        }
    }


    public function loans()
    {
        return $this->hasMany('App\Core\Entities\Loan');
    }
}