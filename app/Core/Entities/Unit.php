<?php
namespace App\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model {

    protected $fillable = ['alias','name','description'];

}
