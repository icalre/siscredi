<?php
namespace App\Core\Entities;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','person_id','profile_id' ,'password', 'state', 'avatar','online','administrative_option',
                            'code','email', 'color'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];


    public function person()
    {
        return $this->belongsTo('App\Core\Entities\Person');
    }

    public function profile()
    {
        return $this->belongsTo('App\Core\Entities\Profile');
    }

    public function setUserAttribute($value)
    {
        $this->attributes['user'] = strtolower($value);
    }

    public function setPasswordAttribute($value)
    {
        if ( ! empty ($value))
        {
            $this->attributes['password'] = \Hash::make($value);
        }
    }

    public function getStateAttribute()
    {
        if($this->attributes['state'] == 1)
        {
            return ['id'=>2, 'value'=>1, 'name'=>'Activo'];
        }else{
            return ['id'=>1, 'value'=>0, 'name'=>'Inactivo'];
        }
    }

    public function getAdministrativeOptionAttribute()
    {
        if($this->attributes['administrative_option'] == 1)
        {
            return ['id'=>2, 'value'=>1, 'name'=>'Si'];
        }else{
            return ['id'=>1, 'value'=>0, 'name'=>'No'];
        }
    }

    public function scopeFilterProfile($query,$profile_id)
    {
        if($profile_id != '')
        {
            return $query->where('profile_id',$profile_id);
        }
    }

}
