<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class Enterprise extends Model {
    protected $fillable = [
                                'razon_social', 'ruc', 'address', 'department_id', 'province_id',
                                'district_id', 'telephone','email', 'website', 'phone_reference', 'activity',
                                'signature'
                            ];

    protected $appends = ['customer_name','customer_document','type'];


    public function department()
    {
        return $this->belongsTo('App\Core\Entities\Department');
    }

    public function province()
    {
        return $this->belongsTo('App\Core\Entities\Province');
    }

    public function district()
    {
        return $this->belongsTo('App\Core\Entities\District');
    }

    public function customer()
    {
        return $this->morphOne('App\Core\Entities\Customer','customer');
    }

    public function supplier()
    {
        return $this->morphOne('App\Core\Entities\Supplier','supplier');
    }

    public function getCustomerNameAttribute()
    {
        return $this->attributes['razon_social'];
    }

    public function getCustomerDocumentAttribute()
    {
        return $this->attributes['ruc'];
    }

    public function getTypeAttribute()
    {
        return 'Enterprise';
    }

    public function loans()
    {
        return $this->hasMany('App\Core\Entities\Loan');
    }

    public function people()
    {
        return $this->belongsToMany('App\Core\Entities\Person', 'enterperise_person')->withPivot(['cargo']);
    }
}