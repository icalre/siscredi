<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class Department extends Model {
    protected $fillable = ['departamento'];


    public function provinces()
    {
        return $this->hasMany('App\Core\Entities\Province');
    }

}