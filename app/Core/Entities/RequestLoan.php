<?php
namespace App\Core\Entities;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RequestLoan extends Model {
    protected $fillable = [
        'amount', 'employee_id', 'observation', 'period', 'person_id', 'quotas', 'type_payment', 'interest'
    ];

    public function getCreatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s',$this->attributes['created_at'])->format('d-m-Y');
    }

    public function employee()
    {
        return $this->belongsTo('App\Core\Entities\Employee', 'employee_id');
    }

    public function person()
    {
        return $this->belongsTo('App\Core\Entities\Person', 'person_id');
    }


    public function scopeFilterEmployee($query,$value)
    {
        if(!empty($value))
        {
            return $query->where('employee_id',$value);
        }
    }

}
