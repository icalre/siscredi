<?php

namespace App\Core\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'loan_id',
        'amount',
        'interest',
        'capital',
        'expiration',
        'date_pay',
        'state',
        'number',
        'mora',
        'type_payment',
        'account_id'
    ];

    protected $appends = ['date_p', 'days', 'score', 'pay_mora'];

    public function loan()
    {
        return $this->belongsTo('App\Core\Entities\Loan');
    }

    public function setExpirationAttribute($value)
    {
        $this->attributes['expiration'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
    }


    public function getExpirationAttribute()
    {
        $date = Carbon::createFromFormat('Y-m-d', $this->attributes['expiration'])->format('d-m-Y');;
        return $date;
    }

    public function getDatePAttribute()
    {
        if (isset($this->attributes['date_pay'])) {
            if ($this->attributes['date_pay'] == '0000-00-00 00:00:00') {
                $date = '0000-00-00';
            } else {
                $date = Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['date_pay'])->format('d-m-Y');
            }
            return $date;
        }

        return '0000-00-00';

    }

    public function getScoreAttribute()
    {

        if (isset($this->attributes['date_pay'])) {
            if ($this->attributes['date_pay'] == '0000-00-00 00:00:00') {
                return '-';
            } else {
                $date1 = Carbon::createFromFormat('Y-m-d', $this->attributes['expiration']);
                $date2 = Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['date_pay']);
                $score = $date1->diffInDays($date2, false);

                if ($score < 0) {
                    $score = 0;
                }

                return $score;
            }
        }

        return '-';
    }

    public function getDaysAttribute()
    {
        $date = Carbon::createFromFormat('Y-m-d', $this->attributes['expiration']);

        return $date->diffInDays(Carbon::now(), false);
    }

    public function getTypePaymentAttribute()
    {
        if ($this->attributes['state'] == 'Pendiente')
        {
            return  '';
        }

        return $this->attributes['type_payment'];
    }

    public function getMoraAttribute()
    {

        if ($this->attributes['state'] == 'Pagado') {
            $mora = $this->attributes['mora'];
        } else {
            $date = Carbon::createFromFormat('Y-m-d', $this->attributes['expiration']);
            $mora = 3 * $date->diffInDays(Carbon::now(), false);
        }

        if ($mora < 0) {
            $mora = 0;
        }


        return $mora;
    }

    public function getPayMoraAttribute()
    {

        return false;
    }

    public function detail()
    {
        return $this->hasOne('App\Core\Entities\PaymentDocumentDetail');
    }

    public function account()
    {
        return $this->belongsTo('App\Account');
    }


}
