<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class Code extends Model {
    protected $fillable = ['user_id','code'];


    public function user()
    {
        return $this->belongsTo('App\Entities\User');
    }

}