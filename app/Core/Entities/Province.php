<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class Province extends Model {
    protected $fillable = ['department_id', 'provincia'];

    public function districts()
    {
        return $this->hasMany('App\Core\Entities\District');
    }
}