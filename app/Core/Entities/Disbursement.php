<?php
namespace App\Core\Entities;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Disbursement extends Model {
    protected $fillable = [
                            'amount',
                            'cash_desk_detail_id',
                            'loan_id',
                            'account_id'
                            ];

    protected  $appends = ['date_c'];

    public function loan()
    {
        return $this->belongsTo('App\Core\Entities\Loan');
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s',$this->attributes['created_at'])->format('d-m-Y H:i:s');
    }

    public function getDateCAttribute()
    {
        if(isset($this->attributes['created_at']))
        {
            return Carbon::createFromFormat('Y-m-d H:i:s',$this->attributes['created_at'])->format('d-m-Y');
        }

        return null;
    }

    public function account()
    {
        return $this->belongsTo('App\Account');
    }
}
