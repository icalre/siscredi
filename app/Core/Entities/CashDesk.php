<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class CashDesk extends Model {

    protected $fillable = ['name','site_id','state'];

    public function site()
    {
        return $this->belongsTo('App\Core\Entities\Site');
    }

    public function cashDeskDetails()
    {
        return $this->hasMany('App\Core\Entities\CashDeskDetail');
    }

    public function scopeFilterSite($query,$site_id)
    {
        if(!empty($site_id))
        {
            return $query->where('site_id',$site_id);
        }
    }
}