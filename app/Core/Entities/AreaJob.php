<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class AreaJob extends Model {
    protected $fillable = ['name','description','area_id'];

    public function area()
    {
        return $this->belongsTo('App\Core\Entities\Area');
    }

    public function scopeFilter($query,$area_id)
    {
        if($area_id != '')
        {
            return $query->where('area_id',$area_id);
        }
    }
}