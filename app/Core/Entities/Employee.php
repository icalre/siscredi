<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model {
    protected $fillable = ['person_id','area_job_id','code','state','site_id',
        'salary_amount','hours_day',
        'salary_day','area_id',
        'salary_hour'];

    public function schedules()
    {
        return $this->hasMany('App\Core\Entities\EmployeeSchedule');
    }

    public function loans()
    {
        return $this->hasMany('App\Core\Entities\Loan');
    }

    public function area()
    {
        return $this->belongsTo('App\Core\Entities\Area');
    }

    public function areaJob()
    {
        return $this->belongsTo('App\Core\Entities\AreaJob','area_job_id');
    }

    public function person()
    {
        return $this->belongsTo('App\Core\Entities\Person');
    }

    public function setCodeAttribute($value)
    {
        if ( !empty ($value))
        {
            $this->attributes['code'] = str_pad($value, 4, "0", STR_PAD_LEFT);
        }
    }

    public function scopeFilterArea($query,$area_id)
    {
        if($area_id != '')
        {
            return $query->where('area_id',$area_id);
        }
    }

    public function scopeFilterJob($query,$job_id)
    {
        if($job_id != '')
        {
            return $query->where('area_job_id',$job_id);
        }
    }

    public function scopeFilterSite($query,$site_id)
    {
        if($site_id != '')
        {
            return $query->where('site_id',$site_id);
        }
    }


    public function site()
    {
        return $this->belongsTo('App\Core\Entities\Site');
    }
}