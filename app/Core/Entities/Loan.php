<?php

namespace App\Core\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $fillable = [
        'customer_id',
        'amount',
        'date_start',
        'state',
        'date_end',
        'code',
        'employee_id',
        'interest',
        'currency',
        'period',
        'type_payment',
        'quotas',
        'warranty_option',
        'endorsement_option',
        'observations',
        'date_disbursement',
        'user_id',
        'nro_contrato',
        'person_id',
        'enterprise_id',
        'modalidad',
        'feed_option',
        'date_lose',
        'customer_type',
        'effective_warranty',
        'effective_state'
    ];

    protected $appends = ['currency_format', 'desembolso', 'state_string'];


    public function getStateStringAttribute()
    {
        if (isset($this->attributes['state'])) {

            if ($this->attributes['state'] == 'Perdida') {
                return 'C. Pesada';
            }

            if ($this->attributes['state'] == 'Perdida Cancelado') {
                return 'C. Pesada Cancelado';
            }

            if ($this->attributes['state'] == 'Atrasado') {
                return 'Perdida';
            }

            return $this->attributes['state'];
        }

        return '';
    }

    public function setDateStartAttribute($value)
    {
        $this->attributes['date_start'] = Carbon::createFromFormat('d-m-Y', $value)->format('Y-m-d');
    }

    public function getDateStartAttribute()
    {
        return Carbon::createFromFormat('Y-m-d', $this->attributes['date_start'])->format('d-m-Y');
    }

    public function getDateEndAttribute()
    {
        if(!isset($this->attributes['date_end']) && !empty($this->attributes['date_end']))
        {
            return Carbon::createFromFormat('Y-m-d', $this->attributes['date_end'])->format('d-m-Y');
        }
        
        return null;
    }

    public function getDateDisbursementAttribute()
    {
        return Carbon::createFromFormat('Y-m-d', $this->attributes['date_disbursement'])->format('d-m-Y');
    }

    public function getDesembolsoAttribute()
    {
        return Carbon::createFromFormat('Y-m-d', $this->attributes['date_disbursement'])->format('d-m-Y');
    }

    public function getDateAnnulmentAttribute()
    {
        if(!isset($this->attributes['date_end']) && !empty($this->attributes['date_end']))
        {
            return Carbon::createFromFormat('Y-m-d', $this->attributes['date_annulment'])->format('d-m-Y');
        }
        
        return null;
    }

    public function getDateLoseAttribute()
    {
        if (isset($this->attributes['date_lose']) && !empty($this->attributes['date_lose'])) {
            return Carbon::createFromFormat('Y-m-d', $this->attributes['date_lose'])->format('d-m-Y');
        } else {
            return '00-00-0000';
        }

    }

    public function getCurrencyFormatAttribute()
    {
        if ($this->attributes['currency'] == 'Soles') {
            $currency = 'S/.';
        } elseif ($this->attributes['currency'] == 'Dolares') {
            $currency = '$';
        }

        return $currency;
    }

    public function customer()
    {
        return $this->belongsTo('App\Core\Entities\Customer');
    }

    public function employee()
    {
        return $this->belongsTo('App\Core\Entities\Employee', 'employee_id');
    }

    public function person()
    {
        return $this->belongsTo('App\Core\Entities\Person', 'person_id');
    }

    public function payments()
    {
        return $this->hasMany('App\Core\Entities\Payment');
    }

    public function endorsement()
    {
        return $this->hasOne('App\Core\Entities\Endorsement');
    }

    public function warranties()
    {
        return $this->hasMany('App\Core\Entities\Warranty');
    }

    public function scopeWarranty($query, $value)
    {
        if ($value != '') {
            return $query->where('loans.warranty_option', $value);
        }
    }

    public function scopeAval($query, $value)
    {
        if ($value != '') {
            return $query->where('loans.endorsement_option', $value);
        }
    }

    public function scopeType($query, $value)
    {
        if ($value != '') {
            return $query->where('loans.type_payment', $value);
        }
    }

    public function aditionalPayments()
    {
        return $this->hasMany('App\Core\Entities\AditionalPayment');
    }

    public function scopeFilter($query, $interest)
    {
        if (!empty($interest)) {
            return $query->where('loans.interest', $interest);
        }
    }

    public function scopeFilterState($query, $value)
    {
        if (!empty($value)) {
            return $query->where('loans.state', $value);
        }
    }

    public function scopeFilterEnterprise($query, $value)
    {
        if (!empty($value)) {
            return $query->where('loans.enterprise_id', $value);
        }
    }

    public function scopeFilterModalidad($query, $value)
    {
        if (!empty($value)) {
            return $query->where('loans.modalidad', $value);
        }
    }

    public function scopeFilterCustomerType($query, $value)
    {
        if (!empty($value)) {
            return $query->where('loans.customer_type', $value);
        }
    }

    public function comments()
    {
        return $this->hasMany('App\Core\Entities\LoanComment');
    }

    public function expenses()
    {
        return $this->hasMany('App\Core\Entities\LoanExpense');
    }

    public function disbursement()
    {
        return $this->hasOne('App\Core\Entities\Disbursement');
    }

    public function enterprise()
    {
        return $this->belongsTo('App\Core\Entities\Enterprise');
    }


}
