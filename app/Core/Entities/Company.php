<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model {
    protected $fillable = [
                            'razon_social', 'ruc','nombre_comercial', 'address', 'department_id', 'province_id',
                            'district_id', 'telephone','email', 'website','commercial_permission','health_license',
                            'operating_license','permit_fumigation','vale_number','internal_number',
                            'contract_option', 'contract_number'
                            ];

    use SoftDeletes;

    public function department()
    {
        return $this->belongsTo('App\Core\Entities\Department');
    }

    public function province()
    {
        return $this->belongsTo('App\Core\Entities\Province');
    }

    public function district()
    {
        return $this->belongsTo('App\Core\Entities\District');
    }

    public function sites()
    {
        return $this->hasMany('App\Core\Entities\Site');
    }

}