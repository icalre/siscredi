<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;

class Module extends Model {
    protected $fillable = ['name', 'icon'];


    public function subModules()
    {
        return $this->hasMany('App\Core\Entities\SubModule');
    }
}