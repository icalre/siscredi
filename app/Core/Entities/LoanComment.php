<?php
namespace App\Core\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class LoanComment extends Model {

    protected $fillable = [
        'loan_id',
	    'comment'
    ];

    protected $appends = ['date'];

    public function loan()
    {
        return $this->belongsTo('App\Core\Entities\Loan');
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s',$this->attributes['created_at'])->format('d-m-Y H:i:s');
    }

    public function getDateAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s',$this->attributes['created_at'])->format('d-m-Y');
    }

}
