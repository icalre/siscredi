<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 03/03/15
 * Time: 12:38 AM
 */

namespace App\Core\Entities;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class CashDeskIncome extends Model {
    
    protected $fillable = ['amount', 'cash_desk_detail_id', 'description', 'type', 'corriente', 'account_id'];

    protected $casts = ['corriente'=>'string'];

    public function getCreatedAtAttribute()
    {
        $date = Carbon::createFromFormat('Y-m-d H:i:s',$this->attributes['created_at'])->format('d-m-Y');
        return $date;
    }

    public function cashDeskDetail()
    {
        return $this->belongsTo('App\Core\Entities\CashDeskDetail');
    }

    public function expense()
    {
        return $this->hasOne('App\Core\Entities\CashDeskExpense', 'cash_desk_income_id');
    }

    public function account()
    {
        return $this->belongsTo('App\Account');
    }

}