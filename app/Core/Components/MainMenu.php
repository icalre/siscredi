<?php namespace App\Core\Components;

use Illuminate\Support\Facades\Redis;
use App\Core\Repositories\ProfileRepo;

class MainMenu{

    protected $profileRepo;

    public function __construct(ProfileRepo $profileRepo)
    {
        $this->profileRepo = $profileRepo;
    }

    public function menu($profile_id)
    {
        $modules = json_decode(Redis::get('siscredi_modules_'.$profile_id));
        $sub_modules = json_decode(Redis::get('siscredi_sub_modules_'.$profile_id));

        if(empty($modules) || empty($sub_modules))
        {
            $this->setMenu($profile_id);

            $modules = json_decode(Redis::get('siscredi_modules_'.$profile_id));
            $sub_modules = json_decode(Redis::get('siscredi_sub_modules_'.$profile_id));
        }

        return ['modules'=>$modules, 'sub_modules'=>$sub_modules];
    }

    public function setMenu($profile_id)
    {

            $profile = $this->profileRepo->find($profile_id);
            $modules = $this->profileRepo->modules($profile_id);
            $sub_modules_data = $profile->sub_modules()->orderBy('name','ASC')->get();
            $sub_modules = array();

            foreach($sub_modules_data as $sub_module)
            {
                $sub_modules[] = $sub_module->uri;
            }

            Redis::set('siscredi_modules_'.$profile_id,$modules);
            Redis::set('siscredi_sub_modules_'.$profile_id,json_encode($sub_modules));

    }
}
