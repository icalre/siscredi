<?php

namespace App\Core\Services;
/**
 * Created by PhpStorm.
 * User: Ivan Calvay
 * Date: 27/10/2016
 * Time: 03:15 PM
 */

use Twilio\Rest\Client;

class SmsService
{
    protected $sid;
    protected $token;
    protected $number;

    public function __construct()
    {
        $this->sid = env('SID');
        $this->token = env('TOKEN');
        $this->number = env('NUMBER');
    }

    public function sendSms($number, $message, $type)
    {
        $client = new Client($this->sid, $this->token);

        if ($type == 2) {
            $number = 'whatsapp:' . $number;
            $this->number = 'whatsapp:+14155238886';
        }

        $client->messages->create($number,
            [
                'from' => $this->number,
                'body' => $message
            ]
        );

        return ['response' => true];
    }

}
