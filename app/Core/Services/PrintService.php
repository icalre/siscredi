<?php
namespace App\Core\Services;
/**
 * Created by PhpStorm.
 * User: Ivan Calvay
 * Date: 27/10/2016
 * Time: 03:15 PM
 */

use PrintNode\Credentials;
use PrintNode\Request;
use PrintNode\PrintJob;

class PrintService
{


    public function printTicket($path, $name = 'BSC10')
    {
        $credentials = new Credentials();
        $credentials->setApiKey('203ba25bb30f99d34c3538d246590d7976b31bec');
        $request = new Request($credentials);
        $printers = $request->getPrinters();
        $index = 0;
        $i = 0;
        foreach ($printers as $item)
        {
            if($item->name == $name)
            {
                $index = $i;
            }
            $i++;
        }

        $printJob = new PrintJob();
        $printJob->printer = $printers[$index];
        $printJob->contentType = 'pdf_base64';
        $printJob->content = base64_encode(file_get_contents($path));
        $printJob->source = 'My App/1.0';
        $printJob->title = 'Test PrintJob from My App/1.0';
        $response = $request->post($printJob);

        return $response;
    }

}