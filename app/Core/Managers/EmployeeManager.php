<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class EmployeeManager extends BaseManager {

    public function getRules()
    {
        $rules = [
                    'area_id'=>'required|exists:areas,id',
                    'area_job_id'=>'required|exists:area_jobs,id',
                    'person_id'=>'required|exists:people,id|unique:employees,person_id,'.$this->entity->id,
                    'state'=>'required',
                    'code'=>'|unique:employees,code,'.$this->entity->id,
                    'site_id'=>'required',
                    'salary_amount'=>'',
                    'hours_day'=>'',
                    'salary_day'=>'',
                    'salary_hour'=>''
                  ];
        return $rules;
    }
} 