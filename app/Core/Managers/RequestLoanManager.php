<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class RequestLoanManager extends BaseManager {

    public function getRules()
    {
        $rules =[
            'amount'=>'',
            'employee_id'=>'',
            'observation'=>'',
            'period'=>'',
            'person_id'=>'',
            'quotas'=>'',
            'type_payment'=>'',
            'interest'=>''
                ];
        return $rules;
    }
} 