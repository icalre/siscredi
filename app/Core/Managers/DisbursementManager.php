<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class DisbursementManager extends BaseManager {

    public function getRules()
    {
        $rules = [
            'amount'=>'required',
            'cash_desk_detail_id'=>'required',
            'loan_id'=>'required|unique:disbursements,loan_id',
            'account_id'=>''
        ];
        return $rules;
    }
} 