<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class CompanyManager extends BaseManager {

    public function getRules()
    {
        $rules = [
                    'razon_social'=>'required',
                    'nombre_comercial'=>'required',
                    'ruc'=>'required|digits:11|numeric',
                    'address'=>'required',
                    'department_id'=>'required|exists:departments,id',
                    'province_id'=>'required|exists:provinces,id',
                    'district_id'=>'required|exists:districts,id',
                    'telephone'=>'',
                    'email'=>'email',
                    'website'=>'',
                    'commercial_permission'=>'',
                    'health_license'=>'',
                    'permit_fumigation'=>'',
                    'operating_license'=>'',
                    'contract_option'=>'',
                    'contract_number'=>''
                  ];
        return $rules;
    }
} 