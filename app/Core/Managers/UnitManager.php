<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class UnitManager extends BaseManager {

    public function getRules()
    {
        $rules = [
                    'name'=>'required',
                    'description'=>'required',
                    'alias'=>'required'
                  ];
        return $rules;
    }
} 