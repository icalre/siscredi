<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class CashDeskDetailManager extends BaseManager {

    public function getRules()
    {
        $rules = [
                    'arching'=>'',
                    'cash_desk_id'=>'required|exists:cash_desks,id',
                    'date_end'=>'',
                    'date_start'=>'',
                    'difference'=>'',
                    'initial_amount'=>'',
                    'state'=>'',
                    'total_amount'=>'',
                    'user_id'=>'required|exists:users,id',
                    'efectivo'=>'',
                    'cuenta_corriente'=>''
                  ];
        return $rules;
    }
} 