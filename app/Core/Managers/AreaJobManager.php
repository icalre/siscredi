<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class AreaJobManager extends BaseManager {

    public function getRules()
    {
        $rules = [  'name'=>'required|unique:area_jobs,name,'.$this->entity->id,
                    'description'=>'required',
                    'area_id'=>'required'
                  ];
        return $rules;
    }
} 