<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class WarrantyManager extends BaseManager {

    public function getRules()
    {
        $rules = [
                    'loan_id'=>'',
                    'Description'=>'',
                    'state'=>'',
                    'remate_date'=>'',
                    'liberado_date'=>'',
                    'reference_price'=>'',
                    'pb'=>'',
                    'pn'=>'',
                    'type' =>''
                  ];
        return $rules;
    }
} 