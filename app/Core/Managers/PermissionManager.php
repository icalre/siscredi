<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class PermissionManager extends BaseManager {

    public function getRules()
    {
        $rules = [  'profile_id'=>'required',
                    'sub_module_id'=>'required'
                  ];
        return $rules;
    }
} 