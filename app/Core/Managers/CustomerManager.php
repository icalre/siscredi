<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class CustomerManager extends BaseManager {

    public function getRules()
    {
        $rules = [  'code'=>'',
                    'customer_id'=>'required',
                    'customer_type'=>'required',
                    'rate'=>''
                  ];
        return $rules;
    }
} 