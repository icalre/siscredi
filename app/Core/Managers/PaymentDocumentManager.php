<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class PaymentDocumentManager extends BaseManager {

    public function getRules()
    {
        $rules =[
            'amount'=>'' ,
            'cash_desk_detail_id'=>'' ,
            'user_id'=>'' ,
            'customer_id'=>'' ,
            'document_type_id'=>'' ,
            'payment_id'=>'',
            'state'=>'',
            'type_payment'=>'',
            'account_id'=>''
        ];
        return $rules;
    }
} 