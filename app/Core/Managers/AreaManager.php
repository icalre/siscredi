<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class AreaManager extends BaseManager {

    public function getRules()
    {
        $rules = [  'name'=>'required|unique:areas,name,'.$this->entity->id,
                    'description'=>'required'
                  ];
        return $rules;
    }
} 