<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class SiteManager extends BaseManager {

    public function getRules()
    {
        $rules = [  'name'=>'required|unique:sites,name,'.$this->entity->id,
                    'company_id'=>'required',
                    'code'=>'',
                    'address'=>'required'
                  ];
        return $rules;
    }
} 