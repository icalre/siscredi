<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class UserManager extends BaseManager {

    public function getRules()
    {
        $rules = [
                    'name'=>'required|unique:users,name,'.$this->entity->id,
                    'profile_id'=>'required|exists:profiles,id',
                    'state'=>'required',
                    'person_id'=>'required|exists:people,id|unique:users,person_id,'.$this->entity->id,
                    'password'=>'confirmed',
                    'password_confirmation' => '',
                    'online'=>'',
                    'administrative_option'=>'required',
                    'code'=>'',
                    'email'=>'',
                    'color'=>'unique:users,color,'.$this->entity->id
                  ];
        return $rules;
    }
} 