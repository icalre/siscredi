<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class PaymentDocumentDetailManager extends BaseManager {

    public function getRules()
    {
        $rules =[
            'payment_document_id'=>'',
            'payment_id'=>'',
            'amount'=>'',
            'state'=>''
        ];
        return $rules;
    }
} 