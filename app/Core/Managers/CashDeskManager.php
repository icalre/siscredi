<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class CashDeskManager extends BaseManager {

    public function getRules()
    {
        $rules = [  'name'=>'required',
                    'site_id'=>'required|exists:sites,id',
                    'state'=>''
                  ];
        return $rules;
    }
} 