<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class PayrollSalaryDetailManager extends BaseManager {

    public function getRules()
    {
        $rules = [  'amount'=>'required',
                    'amount_pay'=>'required',
                    'discounts'=>'required',
                    'employee_id'=>'required',
                    'payroll_salary_id'=>'required'
                  ];
        return $rules;
    }
} 