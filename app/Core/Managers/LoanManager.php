<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class LoanManager extends BaseManager
{

    public function getRules()
    {
        $rules = [
            'customer_id' => 'required|exists:customers,id',
            'employee_id' => 'required|exists:employees,id',
            'amount' => 'required',
            'date_start' => 'required',
            'state' => '',
            'date_end' => '',
            'code' => '',
            'interest' => 'required',
            'currency' => 'required',
            'period' => 'required',
            'type_payment' => 'required',
            'quotas' => '',
            'warranty_option' => '',
            'endorsement_option' => '',
            'observations' => '',
            'date_disbursement' => '',
            'refer' => '',
            'nro_contrato' => '',
            'person_id' => '',
            'enterprise_id' => '',
            'modalidad' => '',
            'feed_option' => '',
            'date_lose' => '',
            'customer_type' => '',
            'effective_warranty' => '',
            'effective_state' => ''
        ];
        return $rules;
    }
} 
