<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class SubModuleManager extends BaseManager {

    public function getRules()
    {
        $rules = [  'name'=>'required',
                    'uri'=>'required',
                    'module_id'=>'required',
                    'link_menu'=>'required'
                  ];
        return $rules;
    }
} 