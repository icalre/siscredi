<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class PersonManager extends BaseManager {

    public function getRules()
    {
        $rules = [  'name'=>'required',
                    'last_name'=>'required',
                    'email'=>'email|unique:people,email,'.$this->entity->id,
                    'dni'=>'required|unique:people,dni,'.$this->entity->id,
                    'address'=>'max:255',
                    'department_id'=>'exists:departments,id',
                    'province_id'=>'exists:provinces,id',
                    'district_id'=>'exists:districts,id',
                    'date_of_birth'=>'',
                    'telephone'=>'',
                    'gender'=>'',
                    'activity'=>'',
                    'file'=>'',
                    'phone_reference'=>'',
                    'business_address'=>''
                  ];
        return $rules;
    }
} 