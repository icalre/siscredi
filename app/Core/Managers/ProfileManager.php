<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class ProfileManager extends BaseManager {

    public function getRules()
    {
        $rules = [  'name'=>'required|unique:profiles,name,'.$this->entity->id,
                    'description'=>'required'
                  ];
        return $rules;
    }
} 