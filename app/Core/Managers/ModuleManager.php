<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class ModuleManager extends BaseManager {

    public function getRules()
    {
        $rules = [  'name'=>'required',
                    'icon'=>''];
        return $rules;
    }
} 