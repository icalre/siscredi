<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class PayrollSalaryManager extends BaseManager {

    public function getRules()
    {
        $rules = [  'name'=>'required|unique:payroll_salaries,name,'.$this->entity->id,
                    'state'=>'required',
                    'month'=>'required',
                    'year'=>'required'
                  ];
        return $rules;
    }
} 