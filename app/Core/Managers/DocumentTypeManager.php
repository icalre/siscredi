<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class DocumentTypeManager extends BaseManager {

    public function getRules()
    {
        $rules = [  'name'=>'required|unique:document_types,name,'.$this->entity->id,
                    'alias'=>'required|unique:document_types,alias,'.$this->entity->id
                  ];

        return $rules;
    }
} 