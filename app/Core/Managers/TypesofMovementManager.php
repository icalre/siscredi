<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class TypesOfMovementManager extends BaseManager {

    public function getRules()
    {
        $rules = [  'name'=>'required|unique:types_of_movements,name,'.$this->entity->id,
                    'code'=>'required|unique:types_of_movements,code,'.$this->entity->id,
                    'type'=>''
                  ];

        return $rules;
    }
} 