<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class CodeManager extends BaseManager {

    public function getRules()
    {
        $rules = [  'code'=>'required|unique:codes,code,'.$this->entity->id,
                    'user_id'=>'required'
                  ];
        return $rules;
    }
} 