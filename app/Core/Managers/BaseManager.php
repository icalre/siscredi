<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 05:57 PM
 */

namespace App\Core\Managers;

use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\JsonResponse;

abstract class BaseManager {

    protected $entity;
    protected $data;

    public function __construct($entity, $data)
    {
        $this->entity = $entity;
        $this->data   = array_only($data, array_keys($this->getRules()));
    }

    abstract public function getRules();

    public function isValid()
    {
        $rules = $this->getRules();
        $validation = \Validator::make($this->data, $rules);


        return $validation;
    }

    public function prepareData($data)
    {
        return $data;
    }

    public function save()
    {
        $response = array();
        $validation = $this->isValid();

        if(!$validation->fails())
        {
            $this->entity->fill($this->prepareData($this->data));

            $this->entity->save();

            $response['response'] = true;
            $response['message'] = 'succes';
        }else{
            $response['response'] = false;
            $response['message'] = 'failed';
            $response['errors'] = $validation->errors()->all();
        }

        return $response;

    }
}