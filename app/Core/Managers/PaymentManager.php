<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class PaymentManager extends BaseManager {

    public function getRules()
    {
        $rules = [
            'loan_id'=>'required|exists:loans,id',
            'amount'=>'required',
            'interest'=>'required',
            'capital'=>'required',
            'expiration'=>'required',
            'date_pay'=>'',
            'state'=>'',
            'number'=>'',
            'account_id'=>'',
	    'mora'=>'',
            'type_payment'=>''
        ];
        return $rules;
    }
} 
