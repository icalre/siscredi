<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class SupplierManager extends BaseManager {

    public function getRules()
    {
        $rules = [  'code'=>'',
                    'supplier_id'=>'required',
                    'supplier_type'=>'required'
                  ];
        return $rules;
    }
} 