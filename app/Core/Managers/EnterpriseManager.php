<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class EnterpriseManager extends BaseManager {

    public function getRules()
    {
        $rules = [
                    'razon_social'=>'required',
                    'ruc'=>'required|digits:11|numeric|unique:enterprises,ruc,'.$this->entity->id,
                    'address'=>'required',
                    'department_id'=>'required|exists:departments,id',
                    'province_id'=>'required|exists:provinces,id',
                    'district_id'=>'required|exists:districts,id',
                    'telephone'=>'',
                    'email'=>'email',
                    'website'=>'',
                    'phone_reference'=>'',
                    'activity'=>'',
                    'signature'=>''
                  ];
        return $rules;
    }
} 