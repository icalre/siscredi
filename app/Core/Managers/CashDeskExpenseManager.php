<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class CashDeskExpenseManager extends BaseManager
{

    public function getRules()
    {
        $rules = [
            'amount' => 'required',
            'description' => 'required',
            'cash_desk_detail_id' => 'required',
            'type' => 'required',
            'sub_type' => '',
            'cash_desk_income_id' => '',
            'account_id' => '',
            'loan_id' => ''
        ];
        return $rules;
    }
} 