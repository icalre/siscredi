<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class CashDeskIncomeManager extends BaseManager
{

    public function getRules()
    {
        $rules = ['amount' => 'required',
            'cash_desk_detail_id' => 'required',
            'description' => 'required',
            'type' => '',
            'corriente' => '',
            'account_id'=>''
        ];
        return $rules;
    }
} 