<?php
/**
 * Created by PhpStorm.
 * User: icalvay
 * Date: 18/11/14
 * Time: 06:02 PM
 */

namespace App\Core\Managers;


class SalaryDiscountManager extends BaseManager {

    public function getRules()
    {
        $rules = [
                    'amount'=>'required',
                    'employee_id'=>'required',
                    'state'=>'required',
                    'type'=>'required',
                    'description'=>'',
                    'payroll_salary_detail_id'=>'',
                    'cash_desk_expense_id'=>''
                  ];
        return $rules;
    }
} 