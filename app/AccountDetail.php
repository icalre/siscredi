<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountDetail extends Model
{
    protected $fillable = [
        'account_id',
        'cash_desk_detail_id',
        'initial_amount',
        'final_amount'
    ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
