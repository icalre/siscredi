<?php

return array(


    'pdf' => array(
        'enabled' => true,
        'binary' => env('WKHTML_PDF_BIN'),
        'timeout' => false,
        'options' => array(),
    ),
    'image' => array(
        'enabled' => true,
        'binary' => base_path(env('WKHTML_IMAGE_BIN')),
        'timeout' => false,
        'options' => array(),
    ),


);
