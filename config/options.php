<?php

return array(
    'customer_types'=>[
        'Person'=>'Persona',
        'Enterprise'=>'Empresa'
    ],
    'supplier_types'=>[
        'Person'=>'Persona',
        'Enterprise'=>'Empresa'
    ],
    'person_types'=>[
        'customer'=>'Cliente',
        'employee'=>'Empleado'
    ],
    'enterprise_types'=>[
        'provider'=>'Provedor',
        'customer'=>'Cliente'
    ],
    'rate'=>[
        'Bueno'=>'Bueno',
        'Malo'=>'Malo'
    ],
    'states'=>[
        0=>'Inactivo',
        1=>'Activo'
    ],
    'option_de'=>[
        'No'=>'No',
        'Si'=>'Si'
    ],
    'type_payments'=>[
        'Fija'=>'Fija',
        'Libre'=>'Libre'
    ]
    ,
    'currencys'=>[
      'Soles'=>'Soles',
        'Dolares'=>'Dolares'
    ],
    'periodos'=>[
        'Semanal'=>'Semanal',
        'Quincenal'=>'Quincenal',
        'Mensual'=>'Mensual'
    ],
    'conditional'=>[
        1=>'Sí',
        0=>'No'
    ],
    'entities_namespace'=>substr('App\Core\Entities\u',0,-1),
    'payment_state'=>[
        'pending'=>'Pendiente',
        'paid'=>'Pagado'
    ],
    'purchase_types'=>[
        'existences'=>'Existencias',
        'services'=>'Servicios'
    ],
    'crediperu_options'=>[
        'Crediperu'=>'Crediperu',
        'Remesas'=>'Remesas',
        'Otros'=>'Otros'
    ],
    'modalidades'=>[
        'Convencional' => 'Convencional',
        'Descuento por Planilla'=> 'Descuento por Planilla',
        'Independientes'=>'Independientes',
        'Garantía de Joyas'=>'Garantía de Joyas',
        'Garantía de Electrodomesticos'=>'Garantía de Electrodomesticos',
        'Grupales'=>'Grupales',
        'Compara de Saldos'=>'Compara de Saldos'
    ]
);

