<div class="abs-center wd-xl">
    <!-- START panel-->
    <div class="text-center mb-xl">
        <div class="mb-lg">
            <em class="fa fa-wrench fa-5x text-muted"></em>
        </div>
        <div class="text-lg mb-lg">403</div>
        <p class="lead m0">Oh! No estas autorizado para visitar esta página. :(</p>
        <p>
            No se ha podido mostrar la página debido a que no cuentas con los permisos necesarios para poder
            acceder a ella, ponte en contacto con un administrador.
        </p>
    </div>
    <ul class="list-inline text-center text-sm mb-xl">
        <li>
            @if(isset($error))
                <a href="/app/home" class="text-muted">Regresar</a>
                @else
            <a href="" ui-sref="app.home" class="text-muted">Regresar</a>
                @endif
        </li>
    </ul>
    <div class="p-lg text-center">
        <span>&copy;</span>
        <span ng-bind="app.year">2016</span>
        <span>-</span>
        <span ng-bind="app.name">CIVIPOS</span>
        <br/>
        <span ng-bind="app.description">Sistema de Gestion de Ventas y Control</span>
    </div>
</div>