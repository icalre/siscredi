<!DOCTYPE html>
<html lang="en" data-ng-app="angle">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="@{{app.description}}">
    <meta name="keywords" content="app, responsive, angular, bootstrap, dashboard, admin">
    <title data-ng-bind="::pageTitle()">Angle - Angular Bootstrap Admin Template</title>
    <!-- Bootstrap styles-->
    {!! Html::style("/app-js/css/bootstrap.css", ['data-ng-if'=>"!app.layout.isRTL"]) !!}
    {!! Html::style("/app-js/css/bootstrap-rtl.css", ['data-ng-if'=>"app.layout.isRTL"]) !!}
    <!-- Application styles-->
    {!! Html::style("/app-js/css/app.css", ['data-ng-if'=>"!app.layout.isRTL"]) !!}
    {!! Html::style("/app-js/css/app-rtl.css", ['data-ng-if'=>"app.layout.isRTL"]) !!}
{!! Html::style("/vendor/fontawesome/css/font-awesome.min.css") !!}
    <!-- Themes-->
    {!!Html::style("/app-js/css/".env('TEMPLATE').".css") !!}

</head>

<body class="layout-fixed">

<div data-autoscroll="false" class="wrapper">
        @include('errors.'.$error)
</div>


</body>

</html>

