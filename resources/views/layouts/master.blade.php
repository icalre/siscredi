<!DOCTYPE html>
<html lang="es_LA" data-ng-app="angle">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="@{{app.description}}">
    <meta name="keywords" content="app, responsive, angular, bootstrap, dashboard, admin">
    <title data-ng-bind="::pageTitle()">Crediperu</title>
    <!-- Bootstrap styles-->
    {!! Html::style("/app-js/css/bootstrap.css", ['data-ng-if'=>"!app.layout.isRTL"]) !!}
    {!! Html::style("/app-js/css/bootstrap-rtl.css", ['data-ng-if'=>"app.layout.isRTL"]) !!}
    <!-- Application styles-->
    {!! Html::style("/app-js/css/app.css", ['data-ng-if'=>"!app.layout.isRTL"]) !!}
    {!! Html::style("/app-js/css/app-rtl.css", ['data-ng-if'=>"app.layout.isRTL"]) !!}
    <!-- Themes-->
    {!!Html::style("/app-js/css/".env('TEMPLATE').".css") !!}

    <!--bower components -->
    {!!Html::style("/bower_components/angucomplete-alt/angucomplete-alt.css") !!}
    {!!Html::style("/bower_components/pikaday/css/pikaday.css") !!}

    <base href="{{URL::to('/')}}/"/>

    <style>
        label{
            display: block;
        }
    </style>

</head>

<body data-ng-class="{ 'layout-fixed' : app.layout.isFixed, 'aside-collapsed' : app.layout.isCollapsed, 'layout-boxed' : app.layout.isBoxed, 'layout-fs': app.useFullLayout, 'hidden-footer': app.hiddenFooter, 'layout-h': app.layout.horizontal, 'aside-float': app.layout.isFloat, 'offsidebar-open': app.offsidebarOpen, 'aside-toggled': app.asideToggled, 'aside-collapsed-text': app.layout.isCollapsedText}">
<div data-preloader></div>
<div data-ui-view="" data-autoscroll="false" class="wrapper"></div>
{!! Html::script("/app-js/js/base.js") !!}
{!! Html::script("/app-js/js/app.js?1.0.3") !!}
{!! Html::script("/bower_components/moment/moment.js") !!}
{!! Html::script("/bower_components/angucomplete-alt/dist/angucomplete-alt.min.js") !!}
{!! Html::script("/bower_components/pikaday/pikaday.js") !!}
{!! Html::script("/bower_components/pikaday/plugins/pikaday.jquery.js") !!}
{!! Html::script("/app-js/js/templates.js") !!}
{!! Html::script("/app-js/js/directives.js") !!}
{!! Html::script("/app-js/js/routes.js?1.0.6") !!}
{!! Html::script("/app-js/js/services.js") !!}
{!! Html::script("/app-js/js/controller.js?v=1.0.1") !!}
{!! Html::script("/app-js/login/controller.js") !!}
{!! Html::script("/app-js/modules/controller.js") !!}
{!! Html::script("/app-js/people/controller.js?v=1.0.0") !!}
{!! Html::script("/app-js/profiles/controller.js") !!}
{!! Html::script("/app-js/users/controller.js") !!}
{!! Html::script("/app-js/companies/controller.js") !!}
{!! Html::script("/app-js/sites/controller.js") !!}
{!! Html::script("/app-js/areas/controller.js") !!}
{!! Html::script("/app-js/area-jobs/controller.js") !!}
{!! Html::script("/app-js/employees/controller.js") !!}
{!! Html::script("/app-js/enterprises/controller.js") !!}
{!! Html::script("/app-js/customers/controller.js") !!}
{!! Html::script("/app-js/suppliers/controller.js") !!}
{!! Html::script("/app-js/loans/controller.js?1.0.3") !!}
{!! Html::script("/app-js/cash-desk-details/controller.js?1.2.0") !!}
{!! Html::script("/app-js/business/controller.js?1.0.2") !!}
{!! Html::script("/app-js/reports/controller.js?1.0.4") !!}
{!! Html::script("/app-js/sent-messages/controller.js?v=1.0.0") !!}
{!! Html::script("/app-js/request-loans/controller.js") !!}
{!! Html::script("/app-js/black-list/controller.js?1.0.0") !!}
{!! Html::script("/app-js/payroll-salaries/controller.js") !!}
{!! Html::script("/app-js/wall/controller.js") !!}
</body>

</html>

