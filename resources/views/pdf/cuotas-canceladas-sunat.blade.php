@extends('pdf.layout')

@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            <strong style="font-size: 18px">CUOTAS CANCELADAS</strong> <br>
            <strong style="font-size: 18px">del {{$date_start}} al {{$date_end}}</strong>
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:d')}}
        </div>
    </div>
    <br>
<div class="grupo">
    <div class="caja base-100">
        <table style="margin: 0 auto; width: 100%">
            <thead>
            <th class="text-center" style="width: 25px">#</th>
            <th style="width: 50px">Contrato</th>
            <th style="width: 260px">Cliente</th>
            <th>Asesor</th>
            <th style="width: 80px">Monto</th>
            <th style="width: 80px">Intereses</th>
            <th style="width: 80px">Mora</th>
            <th style="width: 80px">F. Pago</th>
            <th style="width: 65px">Fecha</th>
            </thead>
            <tbody>
            <?php $i = 0;
                $interest = 0;
                    $mora = 0;
            $efectivo = 0;
            $cuenta_corriente = 0;
            ?>
            @foreach($payments as $payment)
                @if($payment->loan->nro_contrato > 0)
                    <tr>
                        <td>{{$i + 1}}</td>
                        <td>{{$payment->loan->nro_contrato}}</td>
                        <td>
                            {{$payment->loan->customer->customer->last_name}} {{$payment->loan->customer->customer->name}}
                        </td>
                        <td>
                            {{$payment->loan->employee->person->last_name}} {{$payment->loan->employee->person->name}}
                        </td>
                        <td class="text-right">
                            {{$payment->amount}}
                        </td>
                        <td class="text-right">
                            {{number_format($payment->interest,2,'.',',')}}
                        </td>
                        <td class="text-right">
                            {{number_format($payment->mora,2,'.',',')}}
                        </td>
                        <td class="text-right">
                            {{$payment->type_payment}}
                        </td>
                        <td class="text-center">
                            {{$payment->date_p}}
                        </td>
                    </tr>
                @endif
                <?php
                        if($payment->loan->nro_contrato > 0)
                            {
                                $i++ ;
                                $interest = $interest + $payment->interest;
                                $mora = $mora + $payment->mora;
                                if ($payment->type_payment == 'Efectivo') {
                                    $efectivo = $efectivo + $payment->amount;

                                } else {
                                    $cuenta_corriente = $cuenta_corriente + $payment->amount;

                                }
                            }
                ?>
            @endforeach
            <tr>
                <th colspan="5">
                    Total
                </th>
                <th class="text-right">
                    {{$interest}}
                </th>
                <th class="text-right">
                     {{number_format($mora,2,'.',',')}}
                </th>
            </tr>
            </tbody>
        </table>
        <br>
        <table>
            <tr>
                <th colspan="3">
                    EFECTIVO
                </th>
                <th class="text-right">
                    {{number_format($efectivo,2,'.','')}}
                </th>
            </tr>
            <tr>
                <th colspan="3">
                    CUENTA CORRIENTE
                </th>
                <th class="text-right">
                    {{number_format($cuenta_corriente,2,'.','')}}
                </th>
            </tr>
        </table>
    </div>
</div>
@stop
