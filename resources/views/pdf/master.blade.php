<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>CrediPeru - @section('module')Home @show</title>
    <!-- Bootstrap styles-->
    {!! Html::style("/app-js/css/bootstrap.css", ['data-ng-if'=>"!app.layout.isRTL"]) !!}
    {!! Html::style("/app-js/css/bootstrap-rtl.css", ['data-ng-if'=>"app.layout.isRTL"]) !!}
    <!-- Application styles-->
    {!! Html::style("/app-js/css/app.css", ['data-ng-if'=>"!app.layout.isRTL"]) !!}
    {!! Html::style("/app-js/css/app-rtl.css", ['data-ng-if'=>"app.layout.isRTL"]) !!}
    <!-- Themes-->
    {!!Html::style("/app-js/css/".env('TEMPLATE').".css") !!}

    <!--bower components -->
        {!!Html::style("/bower_components/angucomplete-alt/angucomplete-alt.css") !!}
        {!!Html::style("/bower_components/pikaday/css/pikaday.css") !!}

    <style>
        html,body{
            background: none;
            color: #000000;
        }
        
    </style>
</head>
<body>
@yield('content')
</body>
</html>