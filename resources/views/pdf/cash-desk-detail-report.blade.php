@extends('pdf.layout')

@section('content')
    <!-- title grupo -->
    <div class="grupo">
        <div class="caja base-100">
            <h1 class="page-header">
                {{Html::image('images/logo_membretado.jpg')}} Cierre de Caja
            </h1>
        </div><!-- /.col -->
    </div>
    <br>
    <br>
    <!-- info grupo -->
    <div class="grupo" style="font-size: 15px">
        <div class="caja base-25">
            <strong>Encargado</strong> <br/>
            <strong>Apertura</strong><br>
            <strong>Cierre</strong><br>
        </div>
        <div class="caja base-75">
            : {{$cash_desk_detail->user->person->name}} {{$cash_desk_detail->user->person->last_name}}<br/>
            : {{$cash_desk_detail->date_start}}<br>
            : {{$cash_desk_detail->date_end}}<br>
        </div>
    </div>
    <br>
    <br>
    <div class="grupo" style="font-size: 15px">
        <div class="caja base-100">
            <table class="table table-bordered" style="width: 100%">
                <tr>
                    <th class="text-left">Pago de Cuotas <span class="pull-right">(+)</span></th>
                    <th style="width: 200px"
                        class="text-right">{{number_format($cash_desk_detail->deposit_amount,2,'.',',')}}</th>
                </tr>
                <tr>
                    <th class="text-left">Ingresos <span class="pull-right">(+)</span></th>
                    <th style="width: 200px"
                        class="text-right">{{number_format($cash_desk_detail->income_amount,2,'.',',')}}</th>
                </tr>
                <tr>
                    <th class="text-left">Desembolsos<span class="pull-right">(-)</span></th>
                    <th style="width: 200px"
                        class="text-right">{{number_format($cash_desk_detail->disbursement_amount,2,'.',',')}}</th>
                </tr>
                <tr style="border-bottom: 2px #000000 solid">
                    <th class="text-left">Gastos <span class="pull-right">(-)</span></th>
                    <th style="width: 200px"
                        class="text-right">{{number_format($cash_desk_detail->expenses_amount,2,'.',',')}}</th>
                </tr>
                <tr style="border-bottom: 2px #000000 solid">
                    <th class="text-left">Egresos <span class="pull-right">(-)</span></th>
                    <th style="width: 200px"
                        class="text-right">{{number_format($cash_desk_detail->expenses_amount_g,2,'.',',')}}</th>
                </tr>
            </table>
            <br>
            <table>
                <tr>
                    <th>
                        CUENTA
                    </th>
                    <th>
                        INICIAL
                    </th>
                    <th>
                        FINAL
                    </th>
                </tr>
                @foreach($cash_desk_detail->accountDetails as $accountDetail)
                    <tr>
                        <th class="text-left">{{$accountDetail->account->name}}</th>
                        <th class="text-right">{{$accountDetail->initial_amount}}</th>
                        <th class="text-right">{{$accountDetail->final_amount}}</th>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <br>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="width: 100%">
                <thead>
                <tr style="background: deepskyblue">
                    <th colspan="6">
                        Pago de Cuotas
                    </th>
                </tr>
                <tr style="background: deepskyblue">
                    <th style="width: 25px">#</th>
                    <th style="width: 100px">C. Crédito</th>
                    <th>Cliente</th>
                    <th style="width: 100px">Monto</th>
                    <th>Cuenta</th>
                    <th style="width: 150px">Fecha</th>
                </tr>
                </thead>
                <tbody>
                <?php $total = 0; ?>
                @foreach($cash_desk_detail->paymentDocuments as $key=>$item)
                    <tr class="{{($item->state == 'Anulled') ? 'text-red' : ''}}">
                        <td>{{$key+ 1}}</td>
                        <td>{{count($item->details) > 0 ? $item->details[0]->payment->loan->code : '-'}}</td>
                        <td>{{$item->customer->customer->customer_name}}</td>
                        <td class="text-right">{{$item->amount}}</td>
                        <td class="text-right">{{$item->account ? $item->account->name:''}}</td>
                        <td>{{$item->created_at}}</td>
                    </tr>
                    @if ($item->state == 'Correct')
                        <?php $total = $total + $item->amount; ?>
                        @if(!empty($item->account_id) && isset($accounts[$item->account_id - 1]))
                            <?php $totals['payments'][$item->account->name]  = $totals['payments'][$item->account->name] + $item->amount; ?>
                        @endif
                    @endif
                @endforeach
                @foreach($accounts as $account)
                    <tr>
                        <th colspan="3">
                            {{$account->name}}
                        </th>
                        <td class="text-right">
                            {{$totals['payments'][$account->name]}}
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <th colspan="3">
                        TOTAL
                    </th>
                    <td class="text-right">
                        {{number_format($total, 2,'.', '')}}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="width: 100%">
                <tr style="background: deepskyblue">
                    <th colspan="6">
                        Ingresos
                    </th>
                </tr>
                <tr style="background: deepskyblue">
                    <th style="width: 25px">#</th>
                    <th style="width: 100px">Tipo</th>
                    <th style="width: 150px">Fecha</th>
                    <th style="width: 100px">Monto</th>
                    <th >Cuenta</th>
                    <th>Descripción</th>
                </tr>
                <?php $i = 0;
                $total_payments = 0;
                ?>
                @foreach($cash_desk_detail->incomes as $item)
                    <tr>
                        <td>{{$i+ 1}}</td>
                        <td>{{$item->type}}</td>
                        <td>{{$item->created_at}}</td>
                        <td class="text-right">{{$item->amount}}</td>
                        <td class="text-right">{{$item->account ? $item->account->name : ''}}</td>
                        <td>{{$item->description}}</td>
                    </tr>
                    <?php $i++;
                    $total_payments = $total_payments + $item->amount;
                    ?>
                    @if(!empty($item->account_id) && isset($accounts[$item->account_id - 1]))
                        <?php $totals['incomes'][$item->account->name]  = $totals['incomes'][$item->account->name] + $item->amount; ?>
                    @endif
                @endforeach
                @foreach($accounts as $account)
                    <tr>
                        <th colspan="3">
                            {{$account->name}}
                        </th>
                        <td class="text-right">
                            {{$totals['incomes'][$account->name]}}
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <th colspan="3">
                        TOTAL
                    </th>
                    <th class="text-right">
                        {{number_format($total_payments,2,'.','')}}
                    </th>
                </tr>
            </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="width: 100%">
                <tr style="background: orange">
                    <th colspan="6">
                        Gastos
                    </th>
                </tr>
                <tr style="background: orange">
                    <th style="width: 25px">#</th>
                    <th style="width: 100px">Tipo</th>
                    <th style="width: 150px">Fecha</th>
                    <th style="width: 100px">Monto</th>
                    <th>Cuenta</th>
                    <th>Descripción</th>
                </tr>
                <?php $i = 0;
                $total_payments = 0;
                ?>
                @foreach($cash_desk_detail->expenses as $item)
                    @if($item->sub_type == 0)
                        <tr>
                            <td>{{$i+ 1}}</td>
                            <td>{{$item->type}}</td>
                            <td>{{$item->created_at}}</td>
                            <td class="text-right">{{$item->amount}}</td>
                            <td class="text-right">{{$item->account ? $item->account->name : ''}}</td>
                            <td>{{$item->description}}</td>
                        </tr>
                        <?php $i++;
                        $total_payments = $total_payments + $item->amount;
                        ?>
                        @if(!empty($item->account_id) && isset($accounts[$item->account_id - 1]))
                            <?php $totals['expenses_1'][$item->account->name]  = $totals['expenses_1'][$item->account->name] + $item->amount; ?>
                        @endif
                    @endif
                @endforeach
                @foreach($accounts as $account)
                    <tr>
                        <th colspan="3">
                            {{$account->name}}
                        </th>
                        <td class="text-right">
                            {{$totals['expenses_1'][$account->name]}}
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <th colspan="3">
                        TOTAL
                    </th>
                    <th class="text-right">
                        {{number_format($total_payments,2,'.','')}}
                    </th>
                </tr>
            </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="width: 100%">
                <tr style="background: orange">
                    <th colspan="6">
                        Egresos
                    </th>
                </tr>
                <tr style="background: orange">
                    <th style="width: 25px">#</th>
                    <th style="width: 100px">Tipo</th>
                    <th style="width: 150px">Fecha</th>
                    <th style="width: 100px">Monto</th>
                    <th style="width: 100px">Cuenta</th>
                    <th>Descripción</th>
                </tr>
                <?php $i = 0;
                $total_payments = 0;
                ?>
                @foreach($cash_desk_detail->expenses as $item)
                    @if($item->sub_type == 1)
                        <tr>
                            <td>{{$i+ 1}}</td>
                            <td>{{$item->type}}</td>
                            <td>{{$item->created_at}}</td>
                            <td class="text-right">{{$item->amount}}</td>
                            <td>{{$item->account ? $item->account->name : ''}}</td>
                            <td>{{$item->description}}</td>
                        </tr>
                        <?php $i++;
                        $total_payments = $total_payments + $item->amount;
                        ?>
                        @if(!empty($item->account_id) && isset($accounts[$item->account_id - 1]))
                            <?php $totals['expenses_2'][$item->account->name]  = $totals['expenses_2'][$item->account->name] + $item->amount; ?>
                        @endif
                    @endif
                @endforeach
                @foreach($accounts as $account)
                    <tr>
                        <th colspan="3">
                            {{$account->name}}
                        </th>
                        <td class="text-right">
                            {{$totals['expenses_2'][$account->name]}}
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <th colspan="3">
                        TOTAL
                    </th>
                    <th class="text-right">
                        {{number_format($total_payments,2,'.','')}}
                    </th>
                </tr>
            </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="width: 100%">
                <tr style="background: orange">
                    <th colspan="6">
                        Desembolsos
                    </th>
                </tr>
                <tr style="background: orange">
                    <th style="width: 25px">#</th>
                    <th style="width: 100px">C. Crédito</th>
                    <th>Cliente</th>
                    <th style="width: 100px">Monto</th>
                    <th >Cuenta</th>
                    <th style="width: 150px">Fecha</th>
                </tr>
                <?php $i = 0;
                $total_payments = 0;
                ?>
                @foreach($cash_desk_detail->disbursements as $item)
                    <tr>
                        <td>{{$i+ 1}}</td>
                        <td>{{$item->loan->code}}</td>
                        <td>{{$item->loan->customer->customer->customer_name}}</td>
                        <td class="text-right">{{$item->amount}}</td>
                        <td class="text-right">{{$item->account ? $item->account->name : ''}}</td>
                        <td>{{$item->created_at}}</td>
                    </tr>
                    <?php $i++;
                    $total_payments = $total_payments + $item->amount;
                    ?>
                    @if(!empty($item->account_id) && isset($accounts[$item->account_id - 1]))
                        <?php $totals['disbursements'][$item->account->name]  = $totals['disbursements'][$item->account->name] + $item->amount; ?>
                    @endif
                @endforeach
                @foreach($accounts as $account)
                    <tr>
                        <th colspan="3">
                            {{$account->name}}
                        </th>
                        <td class="text-right">
                            {{$totals['disbursements'][$account->name]}}
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <th colspan="3">
                        TOTAL
                    </th>
                    <th class="text-right">
                        {{number_format($total_payments,2,'.','')}}
                    </th>
                </tr>
            </table>
        </div>
    </div>
@stop
