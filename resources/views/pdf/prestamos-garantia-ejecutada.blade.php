@extends('pdf.layout')

@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            <strong style="font-size: 18px">PRESTAMOS GARANTIA EJECUTADA</strong> <br>
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:d')}}
        </div>
    </div>
    <br>
<div class="grupo">
    <div class="caja base-100">
        <table style="margin: 0 auto; width: 100%">
            <thead>
            <th class="text-center" style="width: 25px">#</th>
            <th style="width: 50px">Código</th>
            <th style="width: 260px">Cliente</th>
            <th>Asesor</th>
            <th style="width: 80px">Prestamo</th>
            <th style="width: 80px">Capital Perdida</th>
            <th style="width: 80px">Capital Pendiente</th>
            <th style="width: 65px">Fecha</th>
            </thead>
            <tbody>
            <?php $i = 0;
                    $total_capital_pending = 0;
                    $total_capital_lose = 0;
            $total_loan_amount = 0;
            ?>
            @foreach($loans as $loan)
                <tr>
                    <td>{{$i + 1}}</td>
                    <td>{{$loan->code}}</td>
                    <td>
                        {{$loan->customer->customer->last_name}} {{$loan->customer->customer->name}}
                    </td>
                    <td>
                        {{$loan->employee->person->last_name}} {{$loan->employee->person->name}}
                    </td>
                    <td class="text-right">
                        {{ number_format($loan->amount,2,'.','')}}
                    </td>
                    <td class="text-right">
                        {{ number_format($loan->lose_capital,2,'.','')}}
                    </td>
                    <td class="text-right">
                        {{ number_format($loan->capital_pending,2,'.','')}}
                    </td>
                    <td class="text-center">
                        {{$loan->date_annulment}}
                    </td>
                </tr>
                <?php $i++ ;
                $total_capital_pending = $total_capital_pending + $loan->capital_pending;
                $total_capital_lose = $total_capital_lose + $loan->lose_capital;
                $total_loan_amount = $total_loan_amount + $loan->amount;
                ?>
            @endforeach
            <tr>
                <th colspan="4">
                    Total
                </th>
                <th class="text-right">
                    {{$total_loan_amount}}
                </th>
                <th class="text-right">
                    {{number_format($total_capital_lose,2,'.','')}}
                </th>
                <th class="text-right">
                    {{number_format($total_capital_pending,2,'.','')}}
                </th>
            </tr>
            </tbody>
        </table>
    </div>
</div>
@stop
