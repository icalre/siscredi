@extends('pdf.layout')

@section('content')
    <!-- title grupo -->
    <div class="grupo">
        <div class="caja base-100">
            <h1 class="page-header">
                {{Html::image('images/logo_membretado.jpg')}} Cierre de Caja
                <small class="pull-right">Fecha: {{date('d-m-Y H:i:s')}}</small>
            </h1>
        </div><!-- /.col -->
    </div>
    <br>
    <br>
    <!-- info grupo -->
    <div class="grupo">
        <div class="caja base-25">
            <strong>Encargado</strong> <br/>
            <strong>Apertura</strong><br>
            <strong>Cierre</strong><br>
        </div>
        <div class="caja base-75">
            : {{$person->name}} {{$person->last_name}}<br/>
            : {{$cash_desk_detail->date_start}}<br>
            : {{$cash_desk_detail->date_end}}<br>
        </div>
    </div>
    <br>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="width: 100%">
                <tr>
                    <th class="text-left">Pago de Cuotas <span class="pull-right">(+)</span></th>
                    <th style="width: 200px"
                        class="text-right">{{number_format($cash_desk_detail->deposit_amount,2,'.',',')}}</th>
                </tr>
                <tr>
                    <th class="text-left">Ingresos <span class="pull-right">(+)</span></th>
                    <th style="width: 200px"
                        class="text-right">{{number_format($cash_desk_detail->income_amount,2,'.',',')}}</th>
                </tr>
                <tr>
                    <th class="text-left">Desembolsos<span class="pull-right">(-)</span></th>
                    <th style="width: 200px"
                        class="text-right">{{number_format($cash_desk_detail->disbursement_amount,2,'.',',')}}</th>
                </tr>
                <tr style="border-bottom: 2px #000000 solid">
                    <th class="text-left">Gastos <span class="pull-right">(-)</span></th>
                    <th style="width: 200px"
                        class="text-right">{{number_format($cash_desk_detail->expenses_amount,2,'.',',')}}</th>
                </tr>
                <tr style="border-bottom: 2px #000000 solid">
                    <th class="text-left">Egresos <span class="pull-right">(-)</span></th>
                    <th style="width: 200px"
                        class="text-right">{{number_format($cash_desk_detail->expenses_amount_g,2,'.',',')}}</th>
                </tr>
            </table>
            <br>
            <table>
                <tr>
                    <th>
                        CUENTA
                    </th>
                    <th>
                        INICIAL
                    </th>
                    <th>
                        FINAL
                    </th>
                </tr>
                @foreach($cash_desk_detail->accountDetails as $accountDetail)
                    <tr>
                        <th class="text-left">{{$accountDetail->account->name}}</th>
                        <th class="text-left">{{$accountDetail->initial_amount}}</th>
                        <th class="text-left">{{$accountDetail->final_amount}}</th>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@stop
