@extends('pdf.layout')

@section('content')
    <div class="grupo">
        <div class="caja base-20">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-60 text-center">
            <strong style="font-size: 18px">Desembolsos</strong>
        </div>
        <div class="caja base-20 text-right">
            Fecha: {{date('d-m-Y H:i:d')}}
        </div>
    </div>
    <br>

    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="font-size: 11px; width: 100%;">
                <thead>
                <tr>
                    <th style="width: 15px" class="text-center">#</th>
                    <th class="text-center" style="width: 100px">Fecha</th>
                    <th class="text-center" style="width: 100px">Codigo</th>
                    <th class="text-center">Cliente</th>
                    <th class="text-center" style="width: 100px">Monto</th>
                </thead>
                <tbody>
                <?php $i = 1;
                $desembolsos_monto = 0;
                ?>
                @foreach($desembolsos as $item)
                    <tr>
                        <td>{{$i++}}</td>
                        <td class="text-center" >{{$item->date_c}}</td>
                        <td>{{$item->loan->code}}</td>
                        <td>{{$item->loan->customer->customer->customer_name}}</td>
                        <td class="text-right">{{$item->amount}}</td>
                    </tr>
                    <?php $desembolsos_monto = $desembolsos_monto + $item->amount; ?>
                @endforeach
                <tr>
                    <th colspan="4">TOTAL</th>
                    <td class="text-right">S/. {{number_format($desembolsos_monto,2,'.','') }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@stop
