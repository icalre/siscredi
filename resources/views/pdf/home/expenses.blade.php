@extends('pdf.layout')
@section('module')
Cronograma
@stop
@section('content')
    <div class="grupo">
        <div class="caja base-20">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-70 text-center">
            <strong style="font-size: 18px">Gastos activos e inversión</strong>
        </div>
        <div class="caja base-20 text-right">
            Fecha: {{date('d-m-Y H:i:d')}}
        </div>
    </div>
    <br>

    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="font-size: 11px; width: 100%;">
                <thead>
                <tr>
                    <th style=" background: deepskyblue; " class="text-center" colspan="4">
                        CREDIPERU
                    </th>
                </tr>
                <tr>
                    <th style="width: 15px" class="text-center">#</th>
                    <th class="text-center" style="width: 100px">Fecha</th>
                    <th class="text-center">Descripcion</th>
                    <th class="text-center" style="width: 100px">Monto</th>

                </thead>
                <tbody>
                <?php $i = 1;
                   $amount_crediperu = 0;
                ?>
                @foreach($expenses as $item)
                    @if($item->type == 'Crediperu')
                    <tr>
                        <td>{{$i++}}</td>
                        <td class="text-center" >{{$item->created_at}}</td>
                        <td>{{$item->description}}</td>
                        <td class="text-right">{{$item->amount}}</td>
                    </tr>
                    <?php $amount_crediperu = $amount_crediperu + $item->amount; ?>
                    @endif
                @endforeach
                <tr style=" background: deepskyblue; ">
                    <th colspan="3">TOTAL</th>
                    <td class="text-right">S/. {{number_format($amount_crediperu,2,'.','') }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br>
@stop
