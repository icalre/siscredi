@extends('pdf.layout')

@section('style-sheet')
    <style>
        body, html {
            width: 279.72px;
            font-size: 15px;
        }
    </style>
@stop
@section('content')
        <!-- title grupo -->
	<div class="grupo">
         <div class="caja base-100 text-center">
        @if(isset($copy) && $copy == 1)
            <center>--COPIA--</center>
        @endif
        </div>
        </div>

        <div class="grupo">
            <div class="caja base-100 text-center">
                        {{Html::image('images/logo_membretado.jpg')}}
                        <br/>
                        <strong>R.U.C. : 20539239504</strong>
                        <br/>
                <strong>Calle Torrez Paz #303 - Chiclayo</strong>
                <br>
                        <strong>
                            @if(isset($copy) && $copy == 1)
                            Fecha:{{$date}}
                            @else
                            Fecha:{{date('d/m/Y H:i:s')}}
                            @endif
                        </strong><br/>
                        <strong>No. {{str_pad($paymentD->id, 4, "0", STR_PAD_LEFT)}}</strong>
                        <br/>
                        <strong>
                            @if(isset($extorno) && $extorno == 1)
                                <small>Extorno -</small>
                                @endif
                            Pago de Crédito - {{$loan->code}}</strong>
            </div><!-- /.col -->
        </div>
        <br>
        <div class="grupo">
            <div class="caja base-40">
                Cajero
            </div>
            <div class="caja base-60">
            : {{\Auth::user()->name}}
            </div>
        </div>
        <div class="grupo">
            <div class="caja base-40">
                Co. Cliente
            </div>
            <div class="caja base-60">
            :  {{$paymentD->customer->code}}
            </div>
        </div>
        <div class="grupo">
            <div class="caja base-40">
                Cliente
            </div>
            <div class="caja base-60">
            : <small>{{$paymentD->customer->customer->customer_name}}</small>
            </div>
        </div>
        <br/>
        <?php $total = 0; ?>
        <?php $currency= '';?>
	<?php $mora = 0; ?>
        @foreach($paymentD->details as $item)
        <div class="grupo">
            <div class="caja base-40">
               Cuota <small>{{$item->payment->number}}</small>
            </div>
            <div class="caja base-60 text-right">
                <?php $currency = $item->payment->loan->currency_format; ?>
                {{$item->payment->loan->currency_format}} {{number_format($item->amount - $item->payment->mora - $item->payment->expenses,2,'.',',')}}
                <?php $total = $total+ $item->amount;?>
                <?php if($item->payment->mora > 0){
			$mora = $mora  + $item->payment->mora;
		}?>
            </div>
        </div>
        @endforeach

	@if($mora > 0)
	<div class="grupo">
            <div class="caja base-40">
                Mora
            </div>
            <div class="caja base-60 text-right">
               {{$currency}} {{number_format($mora,2,'.',',')}}
            </div>
        </div>
	@endif
        @if(count($paymentD->expenses) > 0)
            <?php
                    $expenses = 0;

                    foreach ($paymentD->expenses as $expense)
                        {
                            $expenses = $expense->amount + $expenses;
                        }
            ?>
            <div class="grupo">
                <div class="caja base-40">
                    Gastos
                </div>
                <div class="caja base-60 text-right">
                    {{$currency}} {{number_format($expenses,2,'.',',')}}
                </div>
            </div>

        @endif

        <div class="grupo">
            <div class="caja base-40">
                Total
            </div>
            <div class="caja base-60 text-right">
               {{$currency}} {{number_format($paymentD->amount,2,'.',',')}}
            </div>
        </div>
        <br/>
        <p>
            @if(isset($loan->payments[0]))
                    Proximo Vcto: {{$loan->payments[0]->expiration}}<br/>
            @endif
        </p>
        <p class="text-center">
            <small>ESTE DOCUMENTO NO ES UN TICKET BAJO REGLAMENTO DE COMPROBANTES DE PAGO</small>
        </p>
        <br>
        <br>
        <div class="grupo">
            <div class="caja base-100 text-center">
                --
            </div>
        </div>
@stop
