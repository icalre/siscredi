@extends('pdf.layout')

@section('content')
    <br/>
    <br/>
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            <strong style="font-size: 30px">PROPUESTA DE CRÉDITO</strong>
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:s')}}
        </div>
    </div>
    <br>
    <br/>
    <div class="grupo">
        <div class="caja base-100">
            <table  style ="font-size: 12px; width: 100%;">
                <tr>
                    <th class="text-left" style="width: 25%">Cliente</th>
                    <td>
                        : {{$request_loan->person->customer_name}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Asesor de Negocios</th>
                    <td>
                        : {{$request_loan->employee->person->customer_name}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Monto de Credito</th>
                    <td>
                        : S/. {{number_format($request_loan->amount,2,'.',',')}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Tipo</th>
                    <td>
                        : Cuota {{$request_loan->type_payment}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Costo Efectivo</th>
                    <td>
                        : {{$request_loan->interest}}% (Mensual)
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Plazo ({{$request_loan->period}})</th>
                    <td>
                        : {{$request_loan->quotas}}
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">
                        SUGERENCIAS
                    </th>
                </tr>
                <tr>
                    <td colspan="2">
                        {{$request_loan->observation}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <div class="grupo">
        <div class="caja base-50 text-center">
            ______________________________ <br/>
            CREDIPERU <br/>
            ORBEGOZO GAMARRA RAPHAEL
        </div>
        <div class="caja base-50 text-center">
            ______________________________ <br/>
            ASESOR <br/>
            {{$request_loan->employee->person->customer_name}}
            <br>

        </div>
    </div>
@stop
