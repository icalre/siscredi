@extends('pdf.layout')

<style>
    body{
        font-size: 14px;
    }
    th{
        text-align: left;
    }
</style>

@section('content')
        <!-- title grupo -->
        <br/>
        <br/>
        <div class="grupo">
            <div class="caja base-20">
                {{Html::image('images/logo_membretado.jpg')}}
            </div>
            <div class="caja base-60 text-center">
                <br>
                <br>
                <br>
            </div>
            <div class="caja base-20 text-right">
                Fecha: {{date('d-m-Y H:i:d')}}
            </div>
        </div>
        <!-- info grupo -->
        <div class="grupo">
            <div class="caja base-20">
            </div>
            <div class="caja base-60 text-center">
                <br>
                <strong style="font-size: 30px">Datos Personales</strong>
            </div>
            <div class="caja base-20 text-right">
            </div>
        </div>
        <br>
        <br/>
        <br/>
        <div class="grupo">
            <table class="table table-hover" style="width: 100%">
                <tr>
                    <th class="base caja-40">NOMBRE</th>
                    <td class="base caja-60">: {{$person->name}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">APELLIDOS</th>
                    <td class="base caja-60">: {{$person->last_name}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">DNI</th>
                    <td class="base caja-60">: {{$person->dni}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">FECHA DE NACIMIENTO</th>
                    <td class="base caja-60">: {{$person->dateofbirth}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">DIRECCIÓN</th>
                    <td class="base caja-60">: {{$person->address}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">TELEFONO</th>
                    <td class="base caja-60">: {{$person->telephone}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">TELEFONO DE REFERENCIA</th>
                    <td class="base caja-60">: {{$person->phone_reference}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">DEPARTAMENTO</th>
                    <td class="base caja-60">: {{$person->department->departamento}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">PROVINCIA</th>
                    <td class="base caja-60">: {{$person->province->provincia}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">DISTRITO</th>
                    <td class="base caja-60">: {{$person->district->distrito}}</td>
                </tr>

                <tr>
                    <th class="base caja-40">CORREO ELECTRONICO</th>
                    <td class="base caja-60">: {{$person->email}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">ACTIVIDAD ECONOMICA</th>
                    <td class="base caja-60">: {{$person->activity}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">DIRECCIÓN LABORAL</th>
                    <td class="base caja-60">: {{$person->business_address}}</td>
                </tr>
            </table>
        </div>
@stop