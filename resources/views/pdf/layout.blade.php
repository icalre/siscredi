<!DOCTYPE html>
<html lang="es" id="content-body">
<head>
    <meta charset="UTF-8">
    <title>---</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    {{Html::style("/css-print/print-style.css")}}
    @section('style-sheet')
    @show
</head>

<body>
@yield('content')
</body>
</html>
