@extends('pdf.layout')
@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            <strong style="font-size: 18px">Comisiones Asesor de Negocios
                <br> del {{$data['date1']}} al {{$data['date2']}}</strong>
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:d')}}
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <strong style="font-size: 14px">{{$employee->person->lastname}} {{$employee->person->name}} </strong>
        </div>

    </div>

    <br>

    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="width: 100%">
                <thead>
                <tr>
                    <th style="width: 25px" class="text-center">#</th>
                    <th class="text-center" style="width: 85px;">Código</th>
                    <th class="text-center">Cliente</th>
                    <th class="text-center">Empresa</th>
                    <th class="text-center" style="width: 100px">DNI</th>
                    <th style="width:120px" class="text-center">Interes Pagado</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1;
                $total = 0;
                ?>
                @foreach($customers as $item)
                    @if($item->interest > 0)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$item->code}}</td>
                            <td>{{$item->last_name}} {{$item->name}}</td>
                            <td>{{$item->enterprise}}</td>
                            <td>{{$item->dni}}</td>
                            <td class="text-right">{{number_format($item->interest,2,'.',',')}}</td>
                        </tr>
                        <?php $i++;
                        $total = $total + $item->interest;
                        ?>
                    @endif
                @endforeach
                <tr>
                    <th colspan="5">TOTAL</th>
                    <th class="text-right">S/. {{number_format($total,2,'.',',')}}</th>
                </tr>
                <tr>
                    <th colspan="5">COMISIÓN</th>
                    @if($employee->id == 14)
                        <th class="text-right">S/. {{number_format($total*0.12,2,'.',',')}}</th>
                    @else
                        <th class="text-right">S/. {{number_format($total*0.10,2,'.',',')}}</th>
                    @endif
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop
