@extends('pdf.layout')
@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            <strong style="font-size: 30px">Condiciones de Crédito</strong>
        </div>
        <div class="caja base-30 text-right">
            <strong style="color: #000000">ANEXO 01</strong><br/>
            Fecha: {{date('d-m-Y H:i:s')}}
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table style="font-size: 12px; width: 100%;">
                @if(isset($loan->nro_contrato) && $loan->nro_contrato > 0)
                    <tr>
                        <th class="col-xs-4 text-left" style="width: 25%"> No. Contrato</th>
                        <td class="col-xs-8">
                            : {{$loan->nro_contrato}}
                        </td>
                    </tr>
                @endif
                <tr>
                    <th class="col-xs-4 text-left" style="width: 25%"> No. Crédito</th>
                    <td class="col-xs-8">
                        : {{$loan->code}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Modalidad</th>
                    <td>
                        : {{$loan->modalidad}}
                    </td>
                </tr>
                @if($loan->modalidad == 'Descuento por Planilla')
                    <tr>
                        <th class="text-left">Empresa</th>
                        <td>
                            : {{$loan->enterprise->razon_social}}
                        </td>
                    </tr>
                @endif
                <tr>
                    <th class="text-left">Tipo</th>
                    <td>
                        : Cuota {{$loan->type_payment}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Co. Cliente</th>
                    <td>
                        : {{$loan->customer->code}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Cliente</th>
                    <td>
                        : {{$loan->customer->customer->customer_name}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Telefono</th>
                    <td>
                        : {{$loan->customer->customer->telephone}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Actividad Economica</th>
                    <td>
                        : {{$loan->customer->customer->activity}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Asesor de Negocios</th>
                    <td>
                        : {{$loan->employee->person->customer_name}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Monto de Credito</th>
                    <td>
                        : {{$loan->currency_format}} {{number_format($loan->amount,2,'.',',')}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Fecha de Desembolso</th>
                    <td>
                        : {{$loan->desembolso}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Costo Efectivo</th>
                    <td>
                        : {{$loan->interest}}% (Mensual)
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Plazo ({{$loan->period}})</th>
                    <td>
                        : {{$loan->quotas}}
                    </td>
                </tr>
                @if($loan->endorsement_option == 'Si')
                    <tr>
                        <th class="text-left">Aval</th>
                        <td>: {{$loan->endorsement->person->customer_name}}</td>
                    </tr>
                @endif
                @if($loan->warranty_option == 'Si')
                    <tr>
                        <th class="text-left">Garantia</th>
                        <td style="padding: 8px">
                            @if(count($loan->artefactosA) > 0)
                                <table class="table table-bordered" style="width: 100%">
                                    <tr style="background-color: lightgrey;">
                                        <th class="text-center" colspan="3">
                                            Artefactos
                                        </th>
                                    </tr>
                                    <tr style="background: green; color: white">
                                        <th class="text-center">#</th>
                                        <th class="text-center">Descripción</th>
                                        <th class="text-center" style="width: 130px">Tasación</th>
                                    </tr>
                                    <?php $y = 0;
                                    $reference_total = 0;
                                    ?>
                                    @foreach($loan->artefactosA as $warranty)
                                        <tr>
                                            <td>{{$y + 1}}</td>
                                            <td>
                                                {{$warranty->Description}}
                                            </td>
                                            <td class="text-right">
                                                S/. {{$warranty->reference_price}}
                                            </td>
                                        </tr>
                                        <?php $y = $y + 1;
                                        $reference_total = $reference_total + $warranty->reference_price;
                                        ?>
                                    @endforeach
                                    <tr style="background-color: lightgrey;">
                                        <th colspan="2">TOTAL</th>
                                        <th class="text-right">S/. {{number_format($reference_total,2,'.','')}}</th>
                                    </tr>
                                </table>
                                <br>
                            @endif
                            @if(count($loan->otrosA) > 0)
                                <table class="table table-bordered" style="width: 100%">
                                    <tr style="background-color: lightgrey;">
                                        <th class="text-center" colspan="3">
                                            Otros
                                        </th>
                                    </tr>
                                    <tr style="background: green; color: white">
                                        <th class="text-center">#</th>
                                        <th class="text-center">Descripción</th>
                                        <th class="text-center" style="width: 130px">Tasación</th>
                                    </tr>
                                    <?php $y = 0;
                                    $reference_total = 0;
                                    ?>
                                    @foreach($loan->otrosA as $warranty)
                                        <tr>
                                            <td>{{$y + 1}}</td>
                                            <td>
                                                {{$warranty->Description}}
                                            </td>
                                            <td class="text-right">
                                                S/. {{$warranty->reference_price}}
                                            </td>
                                        </tr>
                                        <?php $y = $y + 1;
                                        $reference_total = $reference_total + $warranty->reference_price;
                                        ?>
                                    @endforeach
                                    <tr style="background-color: lightgrey;">
                                        <th colspan="2">TOTAL</th>
                                        <th class="text-right">S/. {{number_format($reference_total,2,'.','')}}</th>
                                    </tr>
                                </table>
                                <br>
                            @endif
                            @if(count($loan->joyasA) > 0)
                                <table class="table table-bordered" style="width: 100%">
                                    <tr>
                                        <th class="text-center" colspan="5" style="background-color: lightgrey;">
                                            Joyas
                                        </th>
                                    </tr>
                                    <tr style="background: green; color: white">
                                        <th class="text-center">#</th>
                                        <th class="text-center">Descripción</th>
                                        <th class="text-center" style="width: 120px">Peso Bruto (gr)</th>
                                        <th class="text-center" style="width: 120px">Peso Neto (gr)</th>
                                        <th class="text-center" style="width: 110px">Tasación</th>
                                    </tr>
                                    <?php $y = 0;
                                    $reference_total = 0;
                                    $pb = 0;
                                    $pn = 0;
                                    ?>
                                    @foreach($loan->joyasA as $warranty)
                                        <tr>
                                            <td>{{$y + 1}}</td>
                                            <td>
                                                {{$warranty->Description}}
                                            </td>
                                            <td class="text-right">
                                                {{$warranty->pb}}
                                            </td>
                                            <td class="text-right">
                                                {{$warranty->pn}}
                                            </td>
                                            <td class="text-right">
                                                S/. {{$warranty->reference_price}}
                                            </td>
                                        </tr>
                                        <?php $y = $y + 1;
                                        $reference_total = $reference_total + $warranty->reference_price;
                                        $pb = $pb + $warranty->pb;
                                        $pn = $pn + $warranty->pn;
                                        ?>
                                    @endforeach
                                    <tr style="background-color: lightgrey;">
                                        <th colspan="2">TOTAL</th>
                                        <th class="text-right"> {{number_format($pb,2,'.','')}}</th>
                                        <th class="text-right"> {{number_format($pn,2,'.','')}}</th>
                                        <th class="text-right">S/. {{number_format($reference_total,2,'.','')}}</th>
                                    </tr>
                                </table>
                            @endif
                        </td>
                    </tr>
                @endif
                @if(isset($loan->person))
                    <tr>
                        <th class="text-left">Referido por</th>
                        <td>: {{$loan->person->customer_name}}</td>
                    </tr>
                @endif
                @if(isset($loan->observations))
                    <tr>
                        <th class="text-left">Observaciones</th>
                        <td>: {{$loan->observations}}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
    <br>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table" style="font-size: 12px; width: 100%">
                <thead>
                <tr>
                    <th colspan="7">
                        CRONOGRAMA DE PAGOS
                    </th>
                </tr>
                <tr>
                    <th class="text-center" style="width: 30px">#</th>
                    <th class="text-center">Vcto</th>
                    <th class="col-sm-2 text-center">Situación</th>
                    <th class="col-sm-2 text-center">Cuota</th>
                    <th class="col-sm-2 text-center">Capital</th>
                    <th class="col-sm-1 text-center">Interes</th>
                    <th class="col-sm-1 text-center">Penalidad</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $capital = 0;
                $interest = 0;
                $total = 0;
                $i = 1;
                ?>
                @foreach($loan->payments as $payment)
                    <tr
                            @if($i%2 != 0)
                            style="background-color: lightgrey"
                            @endif
                    >
                        <td class="text-center">{{$i++}}</td>
                        <td>{{$payment->expiration}}</td>
                        <td class="text-center">
                            @if(isset($payment->state))
                                {{$payment->state}}
                            @endif
                        </td>
                        <td class="text-right">{{$loan->currency_format}} {{number_format($payment->amount,2,'.',',')}}</td>
                        <td class="text-right">{{$loan->currency_format}} {{number_format($payment->capital,2,'.',',')}}</td>
                        <td class="text-right">{{$loan->currency_format}} {{number_format($payment->interest,2,'.',',')}}</td>
                        <td class="text-right"></td>
                    </tr>
                    <?php
                    $capital = $capital + $payment->capital;
                    $interest = $interest + $payment->interest;
                    $total = $total + $payment->amount;
                    ?>
                @endforeach
                <tr>
                    <td class="text-right"></td>
                    <td></td>
                    <td></td>
                    <td class="text-right">{{$loan->currency_format}} {{number_format($total,2,'.',',')}}</td>
                    <td class="text-right">{{$loan->currency_format}} {{number_format( $capital,2,'.',',')}}</td>
                    <td class="text-right">{{$loan->currency_format}} {{number_format($interest,2,'.',',')}}</td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            @if($loan->type_payment == 'Libre' && $capital <= $loan->amount)
                <?php $index = count($loan->payments) - 1; ?>
                <p>
                    <strong>Saldo Capital
                        : {{$loan->currency_format}} {{number_format($loan->payments[$index]->capital,2,'.',',')}} </strong><br/>
                    <strong>Pago
                        Minimo: {{$loan->currency_format}} {{number_format($loan->payments[$index]->interest,2,'.',',')}} </strong>
                </p>
            @endif
        </div>
    </div>
    <br>
    @if(isset($loan->warranty_option))
        @if($loan->warranty_option == 'Si')
            <div class="grupo">
                <div class="caja base-100">
                    <table class="table table-hover" style="font-size: 11px; width: 100%">
                        <tr>
                            <th>
                                COMPROMISO DE PAGO Y DECLARACIÓN JURADA
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    - Declaro saber que vencido el plazo para el pago de mi obligación producto del
                                    préstamo
                                    que recibo, faculto a mi acreedor a ejecutar la garantía que otorgo a través del
                                    presente
                                    documento.
                                </p>
                                <p>
                                    - Finalmente, declaro bajo juramento haber leído, tener pleno conocimiento y
                                    entendimiento,
                                    y aceptar las condiciones establecidas en el contrato del cual forma parte
                                    integrante el
                                    presente anexo.
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        @endif
    @endif
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <div class="grupo">
        <div class="caja base-50 text-center">
            ______________________________ <br/>
            CREDIPERU <br/>
        </div>
        <div class="caja base-50 text-center">
            ______________________________ <br/>
            CLIENTE <br/>
        </div>
    </div>
    <div class="page-break">

    </div>

    <br/>
    <br/>
    <br/>
    <br/>
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            <strong style="font-size: 30px">HOJA RESUMEN</strong> <br>
            <strong style="font-size: 18px">Nº CREDITO: {{$loan->code}}</strong> <br>
            @if(isset($loan->nro_contrato) && $loan->nro_contrato > 0)
                <strong style="font-size: 18px">Nº CONTRATO: {{$loan->nro_contrato}}</strong>
            @endif
        </div>
        <div class="caja base-30 text-right">
            <strong style="color: #000000">ANEXO 02</strong><br/>
            Fecha: {{date('d-m-Y H:i:s')}}
        </div>
    </div>
    <br>
    <br/>
    <br/>
    <br/>
    <div class="grupo">
        <div class="caja base-100" style="font-size: 17px">
            - Tasa de costo efectivo mensual {{$loan->interest}}% (*) <br>
            (Tasa interes, comisiones,gastos administrativos, portes por envio notificaciones domiciliarias.) <br>
            - Penalidad pago tardío (Créditos cuota fija) S/. 3.00 de mora x día <br>
            - Cobro por constancia de no adeudo S/. 10.00  (Segundo requerimiento es gratuito) <br>
            - Duplicado de condiciones de crédito, contrato, cronogramas S/. 10.00 <br>
            - Cobro por custodia de bienes S/. 0.00 <br>
            - Seguro desgravamen (No Aplica)<br>
            - Gastos por envío de carta notarial S/. 30.00<br>
            - Gastos por protesto de letra S/. 50.00 <br>
            - Gastos por reporte Camara de Comercio S/. 50.00<br>
            - Gastos judiciales. (Se cargara al crédito del cliente)<br>
            (*) Sujeto a evaluación.
        </div>
    </div>
    <div style="border-bottom: 3px solid black; margin-bottom: 40px; width: 100%; margin-top: 40px"></div>
    <div class="grupo">
        <div class="caja base-100" style="font-size: 17px">
            <strong>DATOS DE LA EMPRESA</strong>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-80" style="font-size: 15px; margin: 5px">
            <div class="grupo">
                <div class="caja base-40">
                    RAZON SOCIAL
                </div>
                <div class="caja base-60">
                    : INVERSIONES CREDIPERU SAC
                </div>
            </div>
        </div>
    </div>
    <div class="grupo">
        <div class="caja base-80" style="font-size: 15px; margin: 5px">
            <div class="grupo">
                <div class="caja base-40">
                    RUC
                </div>
                <div class="caja base-60">
                    : 20539239504
                </div>
            </div>
        </div>
    </div>
    <div class="grupo">
        <div class="caja base-80" style="font-size: 15px; margin: 5px">
            <div class="grupo">
                <div class="caja base-40">
                    DIRECCIÓN
                </div>
                <div class="caja base-60">
                    : Calle Torres Paz N° 303 - Chiclayo
                </div>
            </div>
        </div>
    </div>
    <div class="grupo">
        <div class="caja base-80" style="font-size: 15px; margin: 5px">
            <div class="grupo">
                <div class="caja base-40">
                    TELEFONO OFICINA CHICLAYO
                </div>
                <div class="caja base-60">
                    : 074 - 223524
                </div>
            </div>
        </div>
    </div>
    <div class="grupo">
        <div class="caja base-80" style="font-size: 15px; margin: 5px">
            <div class="grupo">
                <div class="caja base-40">
                    E-MAIL
                </div>
                <div class="caja base-60">
                    : informes@crediperu.pe
                </div>
            </div>
        </div>
    </div>
    <div class="grupo">
        <div class="caja base-80" style="font-size: 15px; margin: 5px">
            <div class="grupo">
                <div class="caja base-40">
                    PAGINA WEB
                </div>
                <div class="caja base-60">
                    : www.crediperu.pe
                </div>
            </div>
        </div>
    </div>
    <div class="grupo">
        <div class="caja base-80" style="font-size: 15px; margin: 5px">
            <div class="grupo">
                <div class="caja base-40">
                    FACEBOOK
                </div>
                <div class="caja base-60">
                    : Inversiones CrediPErú SAC
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100" style="font-size: 15px; margin: 5px">
            <div class="grupo">
                <div class="caja base-40">
                    <strong>CUENTAS CORRIENTES:</strong>
                </div>
                <div class="caja base-30">
                    <strong>DEPOSITOS</strong>
                </div>
                <div class="caja base-30">
                    <strong>TRANSFERENCIAS (CCI)</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="grupo">
        <div class="caja base-100" style="font-size: 12px; margin: 5px">
            <div class="grupo">
                <div class="caja base-40">
                    CTA. CTE. BBVA S/.
                </div>
                <div class="caja base-30">
                    N° 0011-0285-01-00154294
                </div>
                <div class="caja base-30">
                    CCI: 011-285-000100154294-41
                </div>
            </div>
        </div>
    </div>
    <div class="grupo">
        <div class="caja base-100" style="font-size: 12px; margin: 5px">
            <div class="grupo">
                <div class="caja base-40">
                    CTA. CTE. BCP S/.
                </div>
                <div class="caja base-30">
                    N° 305-9828024-0-76
                </div>
                <div class="caja base-30">
                    CCI: 00230500982802407617
                </div>
            </div>
        </div>
    </div>
    <div class="grupo">
        <div class="caja base-100" style="font-size: 12px; margin: 5px">
            <div class="grupo">
                <div class="caja base-40">
                    CTA. CTE BCP US$
                </div>
                <div class="caja base-30">
                    N° : 305-9398479-1-46
                </div>
                <div class="caja base-30">
                    CCI: 00230500939847914612
                </div>
            </div>
        </div>
    </div>
    <div class="grupo">
        <div class="caja base-100" style="font-size: 12px; margin: 5px">
            <div class="grupo">
                <div class="caja base-40">
                    YAPE
                </div>
                <div class="caja base-30">
                    Cel. 940164015
                </div>
                <div class="caja base-30">
                </div>
            </div>
        </div>
    </div>
    <div class="grupo">
        <div class="caja base-100" style="font-size: 12px; margin: 5px">
            <div class="grupo">
                <div class="caja base-40">
                    PAGO EFECTIVO
                </div>
                <div class="caja base-60">
                    EN OFICINAS DE ICP EN CALLE TORRES PAZ N° 303 - CHICLAYO
                </div>
            </div>
        </div>
    </div>
    <div class="grupo">
        <div class="caja base-100" style="font-size: 12px; margin: 5px">
            <strong>INSTRUCCIONES:</strong> REMITIR VOUCHER DE CANCELACION A SU ASESOR PARA INFORMAR Y VALIDAR SU PAGO.
            <br>
            <strong>NOTA:</strong> LOS PAGOS SE REALIZAN UNICAMENTE EN LAS CUENTAS MENCIONADAS A NOMBRE
            DE INVERSIONES CREDIPERU SAC, O DE MANERA PRESENCIAL EN NUESTRAS OFICINAS.
            ICP NO SE HACE RESPONSABLE DE CUALQUIER OTRO MEDIO QUEEMPLEE EL CLIENTE.
        </div>
    </div>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>

    <div class="grupo">
        <div class="caja base-50 text-center">
            ______________________________ <br/>
            CREDIPERU <br/>
        </div>
        <div class="caja base-50 text-center">
            ______________________________ <br/>
            CLIENTE <br/>
        </div>
    </div>
@stop
