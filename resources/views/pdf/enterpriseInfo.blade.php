@extends('pdf.layout')

<style>
    body{
        font-size: 14px;
    }
    th{
        text-align: left;
    }
</style>

@section('content')
        <!-- title grupo -->
        <br/>
        <br/>
        <div class="grupo">
            <div class="caja base-20">
                {{Html::image('images/logo_membretado.jpg')}}
            </div>
            <div class="caja base-60 text-center">
                <br>
                <br>
                <br>
            </div>
            <div class="caja base-20 text-right">
                Fecha: {{date('d-m-Y H:i:d')}}
            </div>
        </div>
        <!-- info grupo -->
        <div class="grupo">
            <div class="caja base-20">
            </div>
            <div class="caja base-60 text-center">
                <br>
                <strong style="font-size: 30px">Datos Empresa</strong>
            </div>
            <div class="caja base-20 text-right">
            </div>
        </div>
        <br>
        <br/>
        <br/>
        <div class="grupo">
            <table class="table table-hover" style="width: 100%">
                <tr>
                    <th class="base caja-40">RAZÓN SOCIAL</th>
                    <td class="base caja-60">: {{$enterprise->razon_social}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">RUC</th>
                    <td class="base caja-60">: {{$enterprise->ruc}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">DIRECCIÓN</th>
                    <td class="base caja-60">: {{$enterprise->address}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">TELÉFONO</th>
                    <td class="base caja-60">: {{$enterprise->telephone}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">TELÉFONO DE REFERENCIA</th>
                    <td class="base caja-60">: {{$enterprise->phone_reference}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">DEPARTAMENTO</th>
                    <td class="base caja-60">: {{$enterprise->department->departamento}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">PROVINCIA</th>
                    <td class="base caja-60">: {{$enterprise->province->provincia}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">DISTRITO</th>
                    <td class="base caja-60">: {{$enterprise->district->distrito}}</td>
                </tr>

                <tr>
                    <th class="base caja-40">CORREO ELECTRÓNICO</th>
                    <td class="base caja-60">: {{$enterprise->email}}</td>
                </tr>
                <tr>
                    <th class="base caja-40">ACTIVIDAD ECONÓMICA</th>
                    <td class="base caja-60">: {{$enterprise->activity}}</td>
                </tr>
            </table>
        </div>
        <br>
        <div class="grupo">
            <div class="caja base-10">
            </div>
            <div class="caja base-80 text-center">
                <br>
                <table class="table table-hover" style="width: 100%">
                    <tr>
                        <th class="text-center" colspan="2" style="background-color: green; color: white">
                            REPRESENTANTE LEGAL
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center" style="width: 75%">
                            Nombre
                        </th>
                        <th class="text-center">
                            Cargo
                        </th>
                    </tr>
                    @foreach($enterprise->people as $person)
                        <tr>
                            <td>
                                {{$person->customer_name}}
                            </td>
                            <td>
                                {{$person->pivot->cargo}}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="caja base-10 text-right">
            </div>
        </div>
        <div class="grupo">
            <div class="caja base-10">
            </div>
            <div class="caja base-80 text-center">
                <br>
                {{Html::image($enterprise->signature,'Contact',['class'=>'img-circle'])}}
            </div>
            <div class="caja base-10 text-right">
            </div>
        </div>


@stop