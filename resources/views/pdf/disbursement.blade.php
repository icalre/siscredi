@extends('pdf.layout')

@section('style-sheet')
    <style>
        body, html {
            width: 279.72px;
            font-size: 14px;
        }
    </style>
@stop

@section('content')
        <!-- title row -->
        <div class="grupo">
            <div class="caja base-100 text-center">
                        {{Html::image('images/logo_membretado.jpg')}}
                        <br/>
                        <strong>Fecha:{{date('d/m/Y H:i:s')}}</strong><br/>
                        <strong>No. {{str_pad($disbursement->id, 4, "0", STR_PAD_LEFT)}}</strong>
                        <br/>
                        <strong>Desembolso</strong>
            </div><!-- /.col -->
        </div>
        <div class="grupo">
            <div class="caja base-40">
                Cajero
            </div>
            <div class="caja base-70">
            : {{\Auth::user()->name}}
            </div>
        </div>
        <div class="grupo">
            <div class="caja base-40">
                Co. Cliente
            </div>
            <div class="caja base-70">
            : {{$loan->customer->code}}
            </div>
        </div>
        <div class="grupo">
            <div class="caja base-40">
                Co. Crédito
            </div>
            <div class="caja base-70">
            : {{$loan->code}}
            </div>
        </div>
        <br/>
                <div class="grupo">
                    <div class="caja base-40">
                        Monto
                    </div>
                    <div class="caja base-40 text-right">
                         {{$loan->currency_format}} {{number_format($loan->amount,2,'.',',')}}
                    </div>
                </div>
                <br/>
                <br/>
        <br/>
        <br/>
                <div class="grupo">
                    <div class="caja base-100 text-center">
                        _______________________ <br/>
                        {{$loan->customer->customer->customer_name}}<br/>
                            CLIENTE <br/>
                    </div>
                </div>
        <br>
        <br>
        <div class="grupo">
            <div class="caja base-100 text-center">
                --
            </div>
        </div>
@stop