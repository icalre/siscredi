@extends('pdf.layout')

@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            <strong style="font-size: 18px">Ingresos del {{$date_start}} al {{$date_end}}</strong>
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:d')}}
        </div>
    </div>
    <br>

    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="font-size: 11px; width: 100%;">
                <thead>
                <tr>
                    <th style=" background: deepskyblue; " class="text-center" colspan="4">
                        CREDIPERU
                    </th>
                </tr>
                <tr>
                    <th style="width: 15px" class="text-center">#</th>
                    <th class="text-center" style="width: 100px">Fecha</th>
                    <th class="text-center">Descripcion</th>
                    <th class="text-center" style="width: 100px">Monto</th>

                </thead>
                <tbody>
                <?php $i = 1;
                   $amount_crediperu = 0;
                ?>
                @foreach($incomes as $item)
                    @if($item->type == 'Crediperu')
                    <tr>
                        <td>{{$i++}}</td>
                        <td class="text-center" >{{$item->created_at}}</td>
                        <td>{{$item->description}}</td>
                        <td class="text-right">{{$item->amount}}</td>
                    </tr>
                    <?php $amount_crediperu = $amount_crediperu + $item->amount; ?>
                    @endif
                @endforeach
                <tr>
                    <th colspan="3">TOTAL</th>
                    <td class="text-right">S/. {{number_format($amount_crediperu,2,'.','') }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="font-size: 11px; width: 100%;">
                <thead>
                <tr>
                    <th style=" background: lightsalmon; " class="text-center" colspan="4">
                        REMESAS
                    </th>
                </tr>
                <tr>
                    <th style="width: 15px" class="text-center">#</th>
                    <th class="text-center" style="width: 100px">Fecha</th>
                    <th class="text-center">Descripcion</th>
                    <th class="text-center" style="width: 100px">Monto</th>

                </thead>
                <tbody>
                <?php $i = 1;
                $amount_remesas = 0;
                ?>
                @foreach($incomes as $item)
                    @if($item->type == 'Remesas')
                        <tr>
                            <td>{{$i++}}</td>
                            <td class="text-center" >{{$item->created_at}}</td>
                            <td>{{$item->description}}</td>
                            <td class="text-right">{{$item->amount}}</td>
                        </tr>
                        <?php $amount_remesas = $amount_remesas + $item->amount; ?>
                    @endif
                @endforeach
                <tr>
                    <th colspan="3">TOTAL</th>
                    <td class="text-right">S/. {{number_format($amount_crediperu,2,'.','') }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="font-size: 11px; width: 100%;">
                <thead>
                <tr>
                    <th style=" background: yellow; " class="text-center" colspan="4">
                        OTROS
                    </th>
                </tr>
                <tr>
                    <th style="width: 15px" class="text-center">#</th>
                    <th class="text-center" style="width: 100px">Fecha</th>
                    <th class="text-center">Descripcion</th>
                    <th class="text-center" style="width: 100px">Monto</th>

                </thead>
                <tbody>
                <?php $i = 1;
                $amount_otros = 0;
                ?>
                @foreach($incomes as $item)
                    @if($item->type == 'Otros')
                        <tr>
                            <td>{{$i++}}</td>
                            <td class="text-center" >{{$item->created_at}}</td>
                            <td>{{$item->description}}</td>
                            <td class="text-right">{{$item->amount}}</td>
                        </tr>
                        <?php $amount_otros = $amount_otros + $item->amount; ?>
                    @endif
                @endforeach
                <tr>
                    <th colspan="3">TOTAL</th>
                    <td class="text-right">S/. {{number_format($amount_otros,2,'.','') }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-30">
            <table class="table table-bordered" style="font-size: 11px; width: 100%;">
                <thead>
                <tr>
                    <th class="text-center">
                        RESUMEN DE INGRESOS
                    </th>
                </tr>
                <tbody>
                <tr>
                    <th style=" background: deepskyblue; " >CREDIPERU</th>
                    <td style=" background: deepskyblue; " class="text-right">S/. {{number_format($amount_crediperu,2,'.','') }}</td>
                </tr>
                <tr>
                    <th style=" background: lightsalmon; " >REMESAS</th>
                    <td style=" background: lightsalmon; " class="text-right">
                        S/. {{number_format($amount_remesas,2,'.','')}}
                    </td>
                </tr>
                <tr>
                    <th style=" background: yellow; ">OTROS</th>
                    <td style=" background: yellow; " class="text-right">S/. {{number_format($amount_otros,2,'.','') }}</td>
                </tr>
                <tr>
                    <th >TOTAL</th>
                    <td class="text-right">S/. {{number_format($amount_otros+ $amount_remesas +$amount_crediperu ,2,'.','') }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@stop
