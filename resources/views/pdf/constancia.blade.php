@extends('pdf.layout')

@section('content')
    <br>
    <br>
   <br>
   <br>
<br>
    <br>
   <br>
   <br>
<br>
    <br>
   <br>
   <br>

    <div class="grupo">
        <div class="caja base-10">
        </div>
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="grupo">
        <div class="caja base-10">
        </div>
        <div class="caja base-80 text-center" style="font-style: italic; font-weight: 900;
        font-size: 30px;text-decoration: underline">
            CONSTANCIA DE NO ADEUDO
        </div>
    </div>
    <div class="grupo">
        <div class="caja base-10">
        </div>
        <div class="caja base-80" style="font-style: italic;
        font-size: 18px;">
            <br>
            <br>
            <p style="text-indent: 50px;">
                Conste por el presente documento que el <strong>Sr(a) {{$customer->customer->customer_name}}</strong> ,
                identificado con <strong>D.N.I. N° {{$customer->customer->dni}}</strong>,
                a la fecha no mantiene deuda pendiente con mi representada.
            </p>
            <br>
            <p style="text-indent: 50px;">
                Se expide el presente a solicitud del interesado para los fines que estime pertinentes.
            </p>
        </div>
    </div>
    <br>
    <br>
    <div class="grupo">
        <div class="caja base-10">
        </div>
        <div class="caja base-80 text-right" style="font-style: italic;
        font-size: 14px;">
            Chiclayo, {{date('d')}} de {{$month['name']}} del {{date('Y')}}
        </div>
    </div>
@stop
