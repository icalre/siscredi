@extends('pdf.layout')

@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            <strong style="font-size: 18px">CUOTAS ATRASADAS</strong> <br>
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:d')}}
        </div>
    </div>
    <br>
<div class="grupo">
    <div class="caja base-100">
        <table style="margin: 0 auto; width: 100%">
            <thead>
            <th class="text-center" style="width: 25px">#</th>
            <th style="width: 50px">Código</th>
            <th style="width: 260px">Cliente</th>
            <th>Asesor</th>
            <th style="width: 80px">Monto</th>
            <th class="text-center" style="width: 100px" >F. Vcto</th>
            <th class="text-center" style="width: 60px" >Días</th>
            </thead>
            <tbody>
            <?php $i = 0;
                $interest = 0;
                    $mora = 0;
            ?>
            @foreach($payments as $payment)
                <tr>
                    <td>{{$i + 1}}</td>
                    <td>{{$payment->loan->code}}</td>
                    <td>
                        {{$payment->loan->customer->customer->last_name}} {{$payment->loan->customer->customer->name}}
                    </td>
                    <td>
                        {{$payment->loan->employee->person->last_name}} {{$payment->loan->employee->person->name}}
                    </td>
                    <td class="text-right">
                        {{$payment->amount}}
                    </td>
                    <td class="text-center">
                        {{$payment->expiration}}
                    </td>
                    <td class="text-center" style="color: red">
                        {{$payment->days}}
                    </td>
                </tr>
                <?php $i++ ;
                $interest = $interest + $payment->interest;
                $mora = $mora + $payment->mora;
                ?>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop
