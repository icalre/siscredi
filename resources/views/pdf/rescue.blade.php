@extends('pdf.layout')

@section('style-sheet')
    <style>
        body, html {
            width: 279.72px;
            font-size: 13px;
        }
    </style>
@stop
@section('content')
        <!-- title grupo -->
        <div class="grupo">
            <div class="caja base-100 text-center">
                        {{Html::image('images/logo_membretado.jpg')}}
                        <br/>
                        <strong>Fecha:{{date('d/m/Y H:i:s')}}</strong><br/>
                        <br/>
                        <strong>Rescate de Garantía</strong>
            </div><!-- /.col -->
        </div>
        <div class="grupo">
            <div class="caja base-30">
                Cajero
            </div>
            <div class="caja base-70">
            : {{\Auth::user()->name}}
            </div>
        </div>
        <div class="grupo">
            <div class="caja base-30">
                Co. Cliente
            </div>
            <div class="caja base-70">
            : {{$loan->customer->code}}
            </div>
        </div>
        <div class="grupo">
            <div class="caja base-30">
                Co. Crédito
            </div>
            <div class="caja base-70">
            : {{$loan->code}}
            </div>
        </div>
        <br/>
                <div class="grupo">
                    <div class="caja base-100">
                        <table class="table table table-bordered">
                            <tr>
                                <th class="text-center" style="width: 20px">#</th>
                                <th class="text-center">Descripción</th>
                            </tr>
                            <?php $y= 0; ?>
                             @foreach($loan->warranties as $warranty)
                             <tr>
                                 <td>{{$y + 1}}</td>
                                 <td>
                                     {{$warranty->Description}}
                                 </td>
                             </tr>
                             <?php $y= $y + 1; ?>
                             @endforeach
                        </table>
                    </div>
                </div>
                <br/>
                <br/>
                <div class="grupo">
                    <div class="caja base-100 text-center">
                        _______________________ <br/>
                        RECIBI CONFORME
                        <br/>
                        {{$loan->customer->customer->customer_name}}<br/>
                            CLIENTE <br/>
                    </div>
                </div>
        <br>
        <br>
        <div class="grupo">
            <div class="caja base-100 text-center">
                --
            </div>
        </div>
@stop