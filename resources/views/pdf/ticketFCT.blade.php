@extends('pdf.layout')
@section('style-sheet')
    <style>
        body, html {
            width: 279.72px;
            font-size: 15px;
        }
    </style>
@stop
@section('content')
        <!-- title grupo -->
	<div class="grupo">
         <div class="caja base-100 text-center">
        @if(isset($copy) && $copy == 1)
            <center>--COPIA--</center>
        @endif
        </div>
        </div>

        <div class="grupo">
            <div class="caja base-100 text-center">
                        {{Html::image('images/logo_membretado.jpg')}}
                        <br/>
                        <strong>R.U.C. : 20539239504</strong>
                        <br/>
                <strong>Calle Torrez Paz #303 - Chiclayo</strong>
                <br>
                        <strong>
                            @if(isset($copy) && $copy == 1)
                            Fecha:{{$date}}
                            @else
                            Fecha:{{date('d/m/Y H:i:s')}}
                            @endif
                        </strong><br/>
                        <strong>
                            @if(isset($extorno) && $extorno == 1)
                                <small>Extornar -</small>
                                @endif
                            Crédito - {{$loan->code}}</strong>
			<br/>
			<strong>Simulador Cancelacion de Crédito</strong>
            </div><!-- /.col -->
        </div>
        <br>
        <div class="grupo">
            <div class="caja base-40">
                Cajero
            </div>
            <div class="caja base-60">
            : {{\Auth::user()->name}}
            </div>
        </div>
        <div class="grupo">
            <div class="caja base-40">
                Co. Cliente
            </div>
            <div class="caja base-60">
            :  {{$loan->customer->code}}
            </div>
        </div>
        <div class="grupo">
            <div class="caja base-40">
                Cliente
            </div>
            <div class="caja base-60">
            : <small>{{$loan->customer->customer->customer_name}}</small>
            </div>
        </div>
        <br/>
        <?php $total = 0; ?>
        <?php $currency= '';?>
        <?php $flag = 0;?>
        @foreach($loan->payments as $item)
            <?php $currency = $loan->currency_format; ?>
                @if($item->state == 'Pendiente')
                    <div class="grupo">
                        <div class="caja base-40">
                            Cuota <small>{{$item->number}}</small>
                        </div>
                        <div class="caja base-60 text-right">
                            @if($item->pay_total)
                            	{{$loan->currency_format}} {{number_format($item->amount,2,'.',',')}}
				<?php  $total = $total + $item->amount; ?>
                            @else
                                {{$loan->currency_format}} {{number_format($item->capital,2,'.',',')}}
                                <?php  $total = $total + $item->capital; ?>
                            @endif
                        </div>
                    </div>
                @endif
        @endforeach
        <div class="grupo">
            <div class="caja base-40">
                Total
            </div>
            <div class="caja base-60 text-right">
               {{$currency}} {{number_format($total,2,'.',',')}}
            </div>
        </div>
         <br/>
	<br/>
        <p class="text-center">
            <small>ESTE DOCUMENTO NO ES UN TICKET BAJO REGLAMENTO DE COMPROBANTES DE PAGO</small>
        </p>
        <br>
        <br>
        <div class="grupo">
            <div class="caja base-100 text-center">
                --
            </div>
        </div>
@stop
