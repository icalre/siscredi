@extends('pdf.layout')

@section('content')
    <!-- title grupo -->
    <div class="grupo">
        <div class="caja base-100">
            <h1 class="page-header">
                {{Html::image('images/logo_membretado.jpg')}} Cierre de Caja
                <small class="pull-right">Fecha: {{date('d-m-Y')}}</small>
            </h1>
        </div><!-- /.col -->
    </div>
    <br>
    <br>
    <!-- info grupo -->
    <div class="grupo">
        <div class="caja base-25">
            <strong>Encargado</strong> <br/>
            <strong>Apertura</strong><br>
            <strong>Cierre</strong><br>
        </div>
        <div class="caja base-75">
            : {{$user->person->name}} {{$user->person->last_name}}<br/>
            : {{$cash_desk_detail->date_start}}<br>
            : {{$cash_desk_detail->date_end}}<br>
        </div>
    </div>
    <br>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="width: 100%">
                <tr style="background: deepskyblue">
                    <th colspan="5">
                        Pago de Cuotas
                    </th>
                </tr>
                <tr style="background: deepskyblue">
                    <th style="width: 25px">#</th>
                    <th style="width: 100px">C. Crédito</th>
                    <th>Cliente</th>
                    <th style="width: 100px">Monto</th>
                    <th style="width: 150px">Fecha</th>
                </tr>
                @foreach($cash_desk_detail->paymentDocuments as $item)
                    <tr>
                        <td>{{$i+ 1}}</td>
                        <td>{{$item->details[0]->payment->loan->code}}</td>
                        <td>{{$item->customer->customer->customer_name}}</td>
                        <td class="text-right">{{$item->amount}}</td>
                        <td>{{$item->created_at}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="width: 100%">
                <tr style="background: deepskyblue">
                    <th colspan="5">
                        Ingresos
                    </th>
                </tr>
                <tr style="background: deepskyblue">
                    <th style="width: 25px">#</th>
                    <th style="width: 100px">Tipo</th>
                    <th style="width: 150px">Fecha</th>
                    <th style="width: 100px">Monto</th>
                    <th>Descripción</th>
                </tr>
                <?php $i = 0;
                $total_payments = 0;
                ?>
                @foreach($cash_desk_detail->incomes as $item)
                    <tr>
                        <td>{{$i+ 1}}</td>
                        <td>{{$item->type}}</td>
                        <td>{{$item->created_at}}</td>
                        <td class="text-right">{{$item->amount}}</td>
                        <td>{{$item->description}}</td>
                    </tr>
                    <?php $i++;
                    $total_payments = $total_payments + $item->amount;
                    ?>
                @endforeach
                <tr>
                    <th colspan="3">
                        TOTAL
                    </th>
                    <th class="text-right">
                        {{number_format($total_payments,2,'.','')}}
                    </th>
                </tr>
            </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="width: 100%">
                <tr style="background: orange">
                    <th colspan="5">
                        Gastos
                    </th>
                </tr>
                <tr style="background: orange">
                    <th style="width: 25px">#</th>
                    <th style="width: 100px">Tipo</th>
                    <th style="width: 150px">Fecha</th>
                    <th style="width: 100px">Monto</th>
                    <th>Descripción</th>
                </tr>
                <?php $i = 0;
                $total_payments = 0;
                ?>
                @foreach($cash_desk_detail->expenses as $item)
                    @if($item->sub_type == 0)
                        <tr>
                            <td>{{$i+ 1}}</td>
                            <td>{{$item->type}}</td>
                            <td>{{$item->created_at}}</td>
                            <td class="text-right">{{$item->amount}}</td>
                            <td>{{$item->description}}</td>
                        </tr>
                        <?php $i++;
                        $total_payments = $total_payments + $item->amount;
                        ?>
                    @endif
                @endforeach
                <tr>
                    <th colspan="3">
                        TOTAL
                    </th>
                    <th class="text-right">
                        {{number_format($total_payments,2,'.','')}}
                    </th>
                </tr>
            </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="width: 100%">
                <tr style="background: orange">
                    <th colspan="5">
                        Egresos
                    </th>
                </tr>
                <tr style="background: orange">
                    <th style="width: 25px">#</th>
                    <th style="width: 100px">Tipo</th>
                    <th style="width: 150px">Fecha</th>
                    <th style="width: 100px">Monto</th>
                    <th>Descripción</th>
                </tr>
                <?php $i = 0;
                $total_payments = 0;
                ?>
                @foreach($cash_desk_detail->expenses as $item)
                    @if($item->sub_type == 1)
                        <tr>
                            <td>{{$i+ 1}}</td>
                            <td>{{$item->type}}</td>
                            <td>{{$item->created_at}}</td>
                            <td class="text-right">{{$item->amount}}</td>
                            <td>{{$item->description}}</td>
                        </tr>
                        <?php $i++;
                        $total_payments = $total_payments + $item->amount;
                        ?>
                    @endif
                @endforeach
                <tr>
                    <th colspan="3">
                        TOTAL
                    </th>
                    <th class="text-right">
                        {{number_format($total_payments,2,'.','')}}
                    </th>
                </tr>
            </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="width: 100%">
                <tr style="background: orange">
                    <th colspan="5">
                        Desembolsos
                    </th>
                </tr>
                <tr style="background: orange">
                    <th style="width: 25px">#</th>
                    <th style="width: 100px">C. Crédito</th>
                    <th>Cliente</th>
                    <th style="width: 100px">Monto</th>
                    <th style="width: 150px">Fecha</th>
                </tr>
                <?php $i = 0;
                $total_payments = 0;
                ?>
                @foreach($cash_desk_detail->disbursements as $item)
                    <tr>
                        <td>{{$i+ 1}}</td>
                        <td>{{$item->loan->code}}</td>
                        <td>{{$item->loan->customer->customer->customer_name}}</td>
                        <td class="text-right">{{$item->amount}}</td>
                        <td>{{$item->created_at}}</td>
                    </tr>
                    <?php $i++;
                    $total_payments = $total_payments + $item->amount;
                    ?>
                @endforeach
                <tr>
                    <th colspan="3">
                        TOTAL
                    </th>
                    <th class="text-right">
                        {{number_format($total_payments,2,'.','')}}
                    </th>
                </tr>
            </table>
        </div>
    </div>
@stop
