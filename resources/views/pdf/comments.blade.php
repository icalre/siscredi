@extends('pdf.layout')
@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:s')}}
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
                <table  style ="font-size: 12px; width: 100%;">
                    @if(isset($loan->nro_contrato) && $loan->nro_contrato > 0)
                        <tr>
                            <th class="col-xs-4 text-left" style="width: 25%"> No. Contrato</th>
                            <td class="col-xs-8">
                                : {{$loan->nro_contrato}}
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <th class="col-xs-4 text-left"  style="width: 25%"> No. Crédito</th>
                        <td class="col-xs-8">
                            : {{$loan->code}}
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">Tipo</th>
                        <td>
                            : {{$loan->type_payment}}
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">Situación</th>
                        <td>
                            : {{$loan->state}}
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">Co. Cliente</th>
                        <td>
                            : {{$loan->customer->code}}
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">Cliente</th>
                        <td>
                            : {{$loan->customer->customer->customer_name}}
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">Telefono</th>
                        <td>
                            : {{$loan->customer->customer->telephone}}
                        </td>
                    </tr>
                        <tr>
                            <th class="text-left">Actividad Economica</th>
                            <td>
                                : {{$loan->customer->customer->activity}}
                            </td>
                        </tr>
                    <tr>
                        <th class="text-left">Asesor de Negocios</th>
                        <td>
                            : {{$loan->employee->person->customer_name}}
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">Monto de Credito</th>
                        <td>
                            : {{$loan->currency_format}} {{number_format($loan->amount,2,'.',',')}}
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">Fecha de Desembolso</th>
                        <td>
                            : {{$loan->desembolso}}
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">Costo Efectivo</th>
                        <td>
                            : {{$loan->interest}}% (Mensual)
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">Plazo ({{$loan->period}})</th>
                        <td>
                            : {{$loan->quotas}}
                        </td>
                    </tr>
                </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="base caja-100">
            <table class="table table bordered" style="width: 100%">
                <tr style="background: deepskyblue;">
                    <th style="width: 25%">Fecha</th>
                    <th class="text-center" >
                        HISTORIAL
                    </th>
                </tr>
                @foreach($loan->comments as $comment)
                <tr>
                    <td>
                        {{$comment->date}}
                    </td>
                    <td>
                        {{$comment->comment}}
                    </td>
                </tr>
                    @endforeach
            </table>
        </div>
    </div>

