@extends('pdf.layout')

@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:s')}}
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
                <table  style ="font-size: 12px; width: 100%;">
                    <tr>
                        <th class="col-xs-4 text-left"  style="width: 25%"> Co. Cliente</th>
                        <td class="col-xs-8">
                            : {{$customer->code}}
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">Cliente</th>
                        <td>
                            : {{$customer->customer->customer_name}}
                        </td>
                    </tr>
                </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="base caja-100">
            <table class="table table bordered" style="width: 100%">
                <tr style="background: deepskyblue;">
                    <th style="width: 25%">Fecha</th>
                    <th class="text-center" >
                        Comentario
                    </th>
                </tr>
                @foreach($customer->comments as $comment)
                <tr>
                    <td>
                        {{$comment->created_at}}
                    </td>
                    <td>
                        {{$comment->comment}}
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>

