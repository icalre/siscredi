@extends('pdf.layout')
@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            <strong style="font-size: 18px">Cuotas Pendientes de Pago </strong>
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:d')}}
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table style ="font-size: 12px; width: 100%;">
                <tr>
                    <th class="text-left">Co. Cliente</th>
                    <td>
                        : {{$customer->code}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Cliente</th>
                    <td>
                        : {{$customer->customer->customer_name}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Telefono</th>
                    <td>
                        : {{$customer->customer->telephone}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style ="font-size: 11px; width: 100%">
                <thead>
                <tr style="background: deepskyblue">
                    <th style="width: 25px" class="text-center">#</th>
                    <th class="text-center" style="width: 78px">Co. Crédito</th>
                    <th class="text-center" style="width:65px" class="text-right">Cuota </th>
                    <th class="col-sm-2 text-center">Capital</th>
                    <th class="col-sm-1 text-center">Interes</th>
                    <th class="col-sm-1 text-center">Mora</th>
                    <th class="text-center" style="width: 90px">Vcto</th>
                    <th class="text-center" style="width: 70px">Tipo</th>
                    <th class="text-center" style="width: 90px">Prestamo</th>
                    <th class="text-center" style="width: 90px">Capital Pendiente</th>
                    <th class="text-center">Asesor</th>
                    <th class="text-center" style="width: 25px"></th>
                </thead>
                <tbody>
                <?php $i = 1; ?>
                @foreach($billing as $item)
                    <tr @if($i%2 != 0)
                        style="background-color: lightgrey"
                            @endif>
                        <td class="text-center">{{$i++}}</td>
                        <td class="text-center">{{$item->loan_code}}</td>
                        <td class="text-right">
                            @if($item->type_payment != 'Libre')
                            {{ number_format($item->amount +$item->mora,2,'.', '') }}
                            @else
                                {{ number_format($item->amount,2,'.', '') }}
                            @endif
                        </td>
                        <td class="text-right">{{number_format($item->capital,2,'.','')}}</td>
                        <td class="text-right">{{number_format($item->interest,2,'.','')}}</td>
                        <td class="text-right">
                            @if($item->type_payment != 'Libre')
                                {{number_format($item->mora,2, '.', '')}}
                            @else
                                {{number_format(0,2, '.', '')}}
                            @endif
                        </td>
                        <td class="text-center">{{$item->expiration}}</td>
                        <td class="text-center">{{$item->type_payment}}</td>
                        <td class="text-right">{{$item->loan_amount}}</td>
                        <td class="text-right">{{number_format($item->capital_pending,2,'.','')}}</td>
                        <td>{{$item->loan_employee}}</td>
                        <td class="text-center" style="color: red">
                            @if($item->days > 0)
                            {{$item->days}}
                            @else
                                0
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
