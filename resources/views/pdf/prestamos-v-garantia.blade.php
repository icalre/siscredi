@extends('pdf.layout')
@section('style-sheet')
    <style>
        table{
            width: 100%;
        }
    </style>
@stop
@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            <strong style="font-size: 18px">PRESTAMOS VIGENTES CON GARANTÍA</strong> <br>
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:d')}}
        </div>
    </div>
    <br>
<div class="grupo">
    <div class="caja base-100">
        <table style="margin: 0 auto; width: 100%">
            <thead>
            <th class="text-center" style="width: 25px">#</th>
            <th style="width: 50px">Código</th>
            <th style="width: 170px">Cliente</th>
            <th style="width: 170px">Asesor</th>
            <th >Garantías</th>
            <th style="width: 65px">Monto</th>
            </thead>
            <tbody>
            <?php $i = 0;
            ?>
            @foreach($loans_warranty as $loan)
                <tr>
                    <td>{{$i + 1}}</td>
                    <td>{{$loan->code}}</td>
                    <td>
                        {{$loan->customer->customer->last_name}} {{$loan->customer->customer->name}}
                    </td>
                    <td>
                        {{$loan->employee->person->last_name}} {{$loan->employee->person->name}}
                    </td>
                    <td class="text-right">
                        @if(count($loan->artefactosA) > 0)
                            <table class="table table-bordered" style="width: 100%">
                                <tr style="background-color: lightgrey;">
                                    <th class="text-center" colspan="3">
                                        Artefactos
                                    </th>
                                </tr>
                                <tr style="background: green; color: white">
                                    <th class="text-center">#</th>
                                    <th class="text-center">Descripción</th>
                                    <th class="text-center" style="width: 130px">Tasación</th>
                                </tr>
                                <?php $y= 0;
                                $reference_total = 0;
                                ?>
                                @foreach($loan->artefactosA as $warranty)
                                    <tr>
                                        <td>{{$y + 1}}</td>
                                        <td>
                                            {{$warranty->Description}}
                                        </td>
                                        <td class="text-right">
                                            S/. {{$warranty->reference_price}}
                                        </td>
                                    </tr>
                                    <?php $y= $y + 1;
                                    $reference_total = $reference_total + $warranty->reference_price;
                                    ?>
                                @endforeach
                                <tr style="background-color: lightgrey;">
                                    <th colspan="2">TOTAL</th>
                                    <th class="text-right">S/. {{number_format($reference_total,2,'.','')}}</th>
                                </tr>
                            </table>
                            <br>
                        @endif
                        @if(count($loan->otrosA) > 0)
                            <table class="table table-bordered" style="width: 100%">
                                <tr style="background-color: lightgrey;">
                                    <th class="text-center" colspan="3">
                                        Otros
                                    </th>
                                </tr>
                                <tr style="background: green; color: white">
                                    <th class="text-center">#</th>
                                    <th class="text-center">Descripción</th>
                                    <th class="text-center" style="width: 130px">Tasación</th>
                                </tr>
                                <?php $y= 0;
                                $reference_total = 0;
                                ?>
                                @foreach($loan->otrosA as $warranty)
                                    <tr>
                                        <td>{{$y + 1}}</td>
                                        <td>
                                            {{$warranty->Description}}
                                        </td>
                                        <td class="text-right">
                                            S/. {{$warranty->reference_price}}
                                        </td>
                                    </tr>
                                    <?php $y= $y + 1;
                                    $reference_total = $reference_total + $warranty->reference_price;
                                    ?>
                                @endforeach
                                <tr style="background-color: lightgrey;">
                                    <th colspan="2">TOTAL</th>
                                    <th class="text-right">S/. {{number_format($reference_total,2,'.','')}}</th>
                                </tr>
                            </table>
                            <br>
                        @endif
                        @if(count($loan->joyasA) > 0)
                            <table class="table table-bordered" style="width: 100%">
                                <tr>
                                    <th class="text-center" colspan="5" style="background-color: lightgrey;">
                                        Joyas
                                    </th>
                                </tr>
                                <tr style="background: green; color: white">
                                    <th class="text-center">#</th>
                                    <th class="text-center">Descripción</th>
                                    <th class="text-center" style="width: 120px">Peso Bruto (gr)</th>
                                    <th class="text-center" style="width: 120px">Peso Neto (gr)</th>
                                    <th class="text-center" style="width: 110px">Tasación</th>
                                </tr>
                                <?php $y= 0;
                                $reference_total = 0;
                                $pb= 0;
                                $pn = 0;
                                ?>
                                @foreach($loan->joyasA as $warranty)
                                    <tr>
                                        <td>{{$y + 1}}</td>
                                        <td>
                                            {{$warranty->Description}}
                                        </td>
                                        <td class="text-right">
                                            {{$warranty->pb}}
                                        </td>
                                        <td class="text-right">
                                            {{$warranty->pn}}
                                        </td>
                                        <td class="text-right">
                                            S/. {{$warranty->reference_price}}
                                        </td>
                                    </tr>
                                    <?php $y= $y + 1;
                                    $reference_total = $reference_total + $warranty->reference_price;
                                    $pb = $pb + $warranty->pb;
                                    $pn = $pn + $warranty->pn;
                                    ?>
                                @endforeach
                                <tr style="background-color: lightgrey;">
                                    <th colspan="2">TOTAL</th>
                                    <th class="text-right"> {{number_format($pb,2,'.','')}}</th>
                                    <th class="text-right"> {{number_format($pn,2,'.','')}}</th>
                                    <th class="text-right">S/. {{number_format($reference_total,2,'.','')}}</th>
                                </tr>
                            </table>
                            @endif
                    </td>
                    <td class="text-right">
                        {{ number_format($loan->amount,2,'.','')}}
                    </td>
                </tr>
                <?php $i++ ;
                ?>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop
