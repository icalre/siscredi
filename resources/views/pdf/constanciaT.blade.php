@extends('pdf.layout')

@section('style-sheet')
    <style>
        body, html {
            width: 279.72px;
            font-size: 15px;
        }
    </style>
@stop
@section('content')
    <section class="content invoice" style="border: none">
        <!-- title grupo -->
	 <div class="grupo">
	 <div class="caja base-100 text-center">
	@if(isset($copy) && $copy == 1)
            <center>--COPIA--</center>
       	@endif
	</div>
	</div>
        <div class="grupo">
            <div class="caja base-100 text-center">
                        {{Html::image('images/logo_membretado.jpg')}}
                        <br/>
                        <strong>R.U.C. : 20539239504</strong>
                            <br/>
                <strong>Calle Torrez Paz #303 - Chiclayo</strong>
                <br>
                            <strong>
                              Fecha:{{date('d/m/Y H:i:s')}}
                            </strong>
                        <br/>
                        <!-- <strong>Pago de Constancia de No Adeudo</strong> -->
            </div><!-- /.col -->
        </div>
        <br>
        <div class="grupo">
            <div class="caja base-40">
                Cajero
            </div>
            <div class="caja base-60">
            : {{\Auth::user()->name}}
            </div>
        </div>
        <div class="grupo">
            <div class="caja base-40">
                Co. Cliente
            </div>
            <div class="caja base-60">
            : {{$customer->code}}
            </div>
        </div>
        <div class="grupo">
            <div class="caja base-40">
                Cliente
            </div>
            <div class="caja base-60">
            : <small>{{$customer->customer->customer_name}}</small>
            </div>
        </div>
        <br/>
        <?php $total = 0; ?>
        <?php $currency= '';?>
        <div class="grupo">
            <div class="caja base-40">
                Constancia
            </div>
            <div class="caja base-60">
            : <small>S/. 10.00</small>
            </div>
        </div>
       <div class="grupo">
            <div class="caja base-40">
                Total
            </div>
            <div class="caja base-60">
            : <small>S/. 10.00</small>
            </div>
        </div>
       <br>
	<br>
       <p class="text-center">
            <small>ESTE DOCUMENTO NO ES UN TICKET BAJO REGLAMENTO DE COMPROBANTES DE PAGO</small>
        </p>
        <br>
        <br>
        <div class="grupo">
            <div class="caja base-100 text-center">
                --
            </div>
        </div>
    </section>
@stop

