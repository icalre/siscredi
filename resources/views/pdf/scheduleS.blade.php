@extends('pdf.layout')

@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            @if(isset($data['code']))
                <strong style="font-size: 30px">Reporte de Pagos</strong>
            @else
                <strong style="font-size: 30px">Simulación</strong>
            @endif
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:s')}}
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
                <table style ="font-size: 12px; width: 100%">
                    <tr>
                        <th class="text-left" style="width: 25%">Monto de Credito</th>
                        <td>
                            : {{$data['currency_format']}} {{number_format($data['amount'],2,'.',',')}}
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">Costo Efectivo</th>
                        <td>
                            : {{$data['interest']}}% (Mensual)
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">Plazo ({{$data['period']}})</th>
                        <td>
                            : {{$data['quotas']}}
                        </td>
                    </tr>
                </table>
        </div>
    </div>
    <br>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table" style ="font-size: 12px; width: 100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 30px">#</th>
                    <th class="text-center">Vcto</th>
                    <th class="col-sm-2 text-center">Cuota</th>
                    <th class="col-sm-2 text-center">Capital</th>
                    <th class="col-sm-1 text-center">Interes</th>
                    <th class="col-sm-1 text-center">Mora</th>
                    <th class="col-sm-1 text-center">F.Pago</th>
                    <th class="col-sm-2 text-center">Situación</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $capital = 0;
                $interest = 0;
                $total = 0;
                $i = 1;
                ?>
                @foreach($data['payments'] as $payment)
                    <tr
                            style="
                            @if($i%2 != 0)
                            background-color: lightgrey;
                            @endif
                            @if($payment['state'] == 'Pagado')
                                   color: green;
                                    font-weight: 800;
                            @endif
                                    "
                    >
                        <td class="text-center">{{$i++}}</td>
                        <td>{{$payment['expiration']}}</td>
                        <td class="text-right">{{$data['currency_format']}} {{number_format($payment['amount'],2,'.',',')}}</td>
                        <td class="text-right">{{$data['currency_format']}} {{number_format($payment['capital'],2,'.',',')}}</td>
                        <td class="text-right">{{$data['currency_format']}} {{number_format($payment['interest'],2,'.',',')}}</td>
                        <td class="text-right" >
                            @if(isset($payment['mora']))
                                {{$payment['mora']}}
                            @endif
                        </td>
                        <td>
                            @if(isset($payment['date_p']))
                                {{$payment['date_p']}}
                            @endif
                        </td>
                        <td>
                            @if(isset($payment['state']))
                                {{$payment['state']}}
                            @endif
                        </td>
                    </tr>
                    <?php
                    $capital = $capital + $payment['capital'];
                    $interest = $interest + $payment['interest'];
                    $total = $total + $payment['amount'];
                    ?>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            @if($data['type_payment'] == 'Libre' && $capital <= $data['amount'] && isset($data['code']))
                <?php $index = count($data['payments']) - 1; ?>
                <p>
                    <strong>Saldo Capital : {{$data['currency_format']}} {{number_format($data['payments'][$index]['capital'],2,'.',',')}} </strong><br/>
                    <strong>Pago Minimo: {{$data['currency_format']}} {{number_format($data['payments'][$index]['interest'],2,'.',',')}} </strong>
                </p>
            @endif
        </div>
    </div>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <div class="grupo">
        <div class="caja base-50 text-center">
            ______________________________ <br/>
            CREDIPERU <br/>
        </div>
        <div class="caja base-50 text-center">
            ______________________________ <br/>
            CLIENTE <br/>
        </div>
    </div>
@stop
