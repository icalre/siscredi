@extends('pdf.layout')

@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            <strong style="font-size: 18px">Lista de Clientes </strong>
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:d')}}
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table style ="font-size: 12px; width: 70%;margin: 0 auto;">
                <tr>
                    <th class="text-left">Asesor</th>
                    <td>
                        : {{$employee->person->customer_name}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style ="font-size: 11px; width:70%; margin: 0 auto;">
                <thead>
                <tr style="background: deepskyblue">
                    <th style="width: 25px" class="text-center">#</th>
                    <th class="text-center">Cliente</th>
                </thead>
                <tbody>
                <?php $i = 1; ?>
                @foreach($employee->loans as $item)
                    <tr @if($i%2 != 0)
                        style="background-color: lightgrey"
                            @endif>
                        <td class="text-center">{{$i++}}</td>
                        <td>{{$item->customer->customer->customer_name}}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
