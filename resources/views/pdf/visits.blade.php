@extends('pdf.layout')
@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            <strong style="font-size: 18px">Vencimientos <br>del {{$date1}} al {{$date2}}</strong>
            @if(isset($enterprise->id))
            <br>
            <strong style="font-size: 18px">
                {{$enterprise->customer_name}}
            </strong>
                @endif
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:d')}}
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style ="font-size: 11px; width: 100%">
                <thead>
                <tr>
                    <th style="width: 25px" class="text-center">#</th>
                    <th class="text-center" style="width: 78px">Co. Crédito</th>
                    <th class="text-center">Cliente</th>
                    <th class="text-center">DNI</th>
                    <th class="text-center">Asesor</th>
                    <th class="text-center" style="width:82px" class="text-right">Monto</th>
                    <th class="text-center" style="width: 100px">Vcto</th>
                    <th class="text-center" style="width: 70px">Cuota</th>
                    <th style="width: 60px" class="text-center"></th>
                    <th class="text-center" style="width: 100px">Telf.</th>
                </thead>
                <tbody>
                <?php $i = 1;
                $total = 0;
                ?>
                @foreach($payments as $item)
                    <tr style="background: {{$item['color']}}">
                        <td class="text-center">{{$i++}}</td>
                        <td class="text-center">{{$item['code']}}</td>
                        <td>{{$item['customer']}}</td>
                        <td>{{$item['dni']}}</td>
                        <td>{{$item['dealer']}}</td>
                        <td class="text-right">{{$item['amount']}}</td>
                        <td class="text-center">{{$item['expiration']}}</td>
                        <td class="text-center">{{$item['number']}}</td>
                        <td class="text-center" style="color: red;">
                                {{$item['days']}}
                        </td>
                        <td class="text-center">
                            {{$item['phone']}}
                        </td>
                    </tr>
                    <?php $total = $total + $item['amount'] ; ?>
                @endforeach
                    <tr>
                        <th colspan="4">
                            Total
                        </th>
                        <td class="text-right">
                            {{number_format($total,2,'.','')}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop
