@extends('pdf.layout')

@section('content')
    <div class="grupo">
        <div class="caja base-20">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-60 text-center">
            <strong style="font-size: 18px">Caja del {{$date_start}} al {{$date_end}}</strong>
        </div>
        <div class="caja base-20 text-right">
            Fecha: {{date('d-m-Y H:i:d')}}
        </div>
    </div>
    <br>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table table-bordered" style="font-size: 11px; width: 100%;">
                <tbody>
                @foreach($cash_desk_details as $cash_desk_detail)
                    <tr>
                        <th colspan="3">
                            Fecha: {{$cash_desk_detail->created_at->format('d/m/Y')}}

                            <a href="{{route('cash-desk-detail-report-show', $cash_desk_detail->id)}}" target="_blank" style="float: right; margin: 2px">
                                Detalle
                            </a>
                        </th>
                    </tr>
                    <tr>
                        <th>
                            CUENTA
                        </th>
                        <th>
                            INICIAL
                        </th>
                        <th>
                            FINAL
                        </th>
                    </tr>
                    @foreach($cash_desk_detail->accountDetails as $accountDetail)
                        <tr>
                            <td>
                                {{$accountDetail->account->name}}
                            </td>
                            <td>
                                {{$accountDetail->initial_amount}}
                            </td>
                            <td>
                                {{$accountDetail->final_amount}}
                            </td>
                        </tr>
                    @endforeach
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection