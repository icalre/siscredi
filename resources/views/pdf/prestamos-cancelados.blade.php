@extends('pdf.layout')

@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            <strong style="font-size: 18px">PRESTAMOS CANCELADOS</strong> <br>
            <strong style="font-size: 18px">del {{$date_start}} al {{$date_end}}</strong>
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:d')}}
        </div>
    </div>
    <br>
<div class="grupo">
    <div class="caja base-100">
        <table style="margin: 0 auto; width: 100%">
            <thead>
            <th class="text-center" style="width: 25px">#</th>
            <th style="width: 50px">Código</th>
            <th style="width: 260px">Cliente</th>
            <th>Asesor</th>
            <th style="width: 80px">Intereses</th>
            <th style="width: 80px">Mora</th>
            <th style="width: 80px">Gastos</th>
            <th style="width: 80px">Monto</th>
            <th style="width: 65px">Fecha</th>
            </thead>
            <tbody>
            <?php $i = 0;
                $interest = 0;
                    $mora = 0;
                    $expenses = 0;
            ?>
            @foreach($loans as $loan)
                <tr>
                    <td>{{$i + 1}}</td>
                    <td>{{$loan->code}}</td>
                    <td>
                        {{$loan->customer->customer->last_name}} {{$loan->customer->customer->name}}
                    </td>
                    <td>
                        {{$loan->employee->person->last_name}} {{$loan->employee->person->name}}
                    </td>
                    <td class="text-right">
                        {{number_format($loan->amount_interest,2,'.',',')}}
                    </td>
                    <td class="text-right">
			{{number_format($loan->mora_amount,2,'.',',')}}
                    </td>
                    <td class="text-right">
                        {{number_format($loan->gastos_amount,2,'.',',')}}
                    </td>
                    <td class="text-right">
                        {{$loan->amount}}
                    </td>
                    <td class="text-center">
                        {{$loan->date_end}}
                    </td>
                </tr>
                <?php $i++ ;
                $interest = $interest + $loan->amount_interest;
                $mora = $mora + $loan->mora_amount;
                        $expenses = $expenses + $loan->gastos_amount;
                ?>
            @endforeach
            <tr>
                <th colspan="5">
                    Total
                </th>
                <th class="text-right">
                    {{$interest}}
                </th>
                <th class="text-right">
                     {{number_format($mora,2,'.',',')}}
                </th><th class="text-right">
                     {{number_format($expenses,2,'.',',')}}
                </th>

            </tr>
            </tbody>
        </table>
    </div>
</div>
@stop
