@extends('pdf.layout')
@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            <strong style="font-size: 30px">Reporte de Pago</strong>
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:s')}}
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table  style ="font-size: 12px; width: 100%;">
                @if(isset($loan->nro_contrato) && $loan->nro_contrato > 0)
                    <tr>
                        <th class="col-xs-4 text-left" style="width: 25%"> No. Contrato</th>
                        <td class="col-xs-8">
                            : {{$loan->nro_contrato}}
                        </td>
                    </tr>
                @endif
                <tr>
                    <th class="col-xs-4 text-left"  style="width: 25%"> No. Crédito</th>
                    <td class="col-xs-8">
                        : {{$loan->code}}
                    </td>
                </tr>
                    <tr>
                        <th class="text-left">Modalidad</th>
                        <td>
                            : {{$loan->modalidad}}
                        </td>
                    </tr>
                    @if($loan->modalidad == 'Descuento por Planilla')
                        <tr>
                            <th class="text-left">Empresa</th>
                            <td>
                                : {{$loan->enterprise->razon_social}}
                            </td>
                        </tr>
                    @endif
                <tr>
                    <th class="text-left">Tipo</th>
                    <td>
                        : Cuota {{$loan->type_payment}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Co. Cliente</th>
                    <td>
                        : {{$loan->customer->code}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Cliente</th>
                    <td>
                        : {{$loan->customer->customer->customer_name}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Telefono</th>
                    <td>
                        : {{$loan->customer->customer->telephone}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Actividad Economica</th>
                    <td>
                        : {{$loan->customer->customer->activity}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Asesor de Negocios</th>
                    <td>
                        : {{$loan->employee->person->customer_name}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Monto de Credito</th>
                    <td>
                        : {{$loan->currency_format}} {{number_format($loan->amount,2,'.',',')}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Fecha de Desembolso</th>
                    <td>
                        : {{$loan->desembolso}}
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Costo Efectivo</th>
                    <td>
                        : {{$loan->interest}}% (Mensual)
                    </td>
                </tr>
                <tr>
                    <th class="text-left">Plazo ({{$loan->period}})</th>
                    <td>
                        : {{$loan->quotas}}
                    </td>
                </tr>
                @if($loan->endorsement_option == 'Si')
                    <tr>
                        <th class="text-left">Aval</th>
                        <td>: {{$loan->endorsement->person->customer_name}}</td>
                    </tr>
                @endif
                @if($loan->warranty_option == 'Si')
                    <tr>
                        <th class="text-left">Garantia</th>
                        <td style="padding: 8px">
                            @if(count($loan->artefactosA) > 0)
                                <table class="table table-bordered" style="width: 100%">
                                    <tr style="background-color: lightgrey;">
                                        <th class="text-center" colspan="3">
                                            Artefactos
                                        </th>
                                    </tr>
                                    <tr style="background: green; color: white">
                                        <th class="text-center">#</th>
                                        <th class="text-center">Descripción</th>
                                        <th class="text-center" style="width: 130px">Tasación</th>
                                    </tr>
                                    <?php $y= 0;
                                    $reference_total = 0;
                                    ?>
                                    @foreach($loan->artefactosA as $warranty)
                                        <tr>
                                            <td>{{$y + 1}}</td>
                                            <td>
                                                {{$warranty->Description}}
                                            </td>
                                            <td class="text-right">
                                                S/. {{$warranty->reference_price}}
                                            </td>
                                        </tr>
                                        <?php $y= $y + 1;
                                        $reference_total = $reference_total + $warranty->reference_price;
                                        ?>
                                    @endforeach
                                    <tr style="background-color: lightgrey;">
                                        <th colspan="2">TOTAL</th>
                                        <th class="text-right">S/. {{number_format($reference_total,2,'.','')}}</th>
                                    </tr>
                                </table>
                                <br>
                            @endif
                            @if(count($loan->otrosA) > 0)
                                <table class="table table-bordered" style="width: 100%">
                                    <tr style="background-color: lightgrey;">
                                        <th class="text-center" colspan="3">
                                            Otros
                                        </th>
                                    </tr>
                                    <tr style="background: green; color: white">
                                        <th class="text-center">#</th>
                                        <th class="text-center">Descripción</th>
                                        <th class="text-center" style="width: 130px">Tasación</th>
                                    </tr>
                                    <?php $y= 0;
                                    $reference_total = 0;
                                    ?>
                                    @foreach($loan->otrosA as $warranty)
                                        <tr>
                                            <td>{{$y + 1}}</td>
                                            <td>
                                                {{$warranty->Description}}
                                            </td>
                                            <td class="text-right">
                                                S/. {{$warranty->reference_price}}
                                            </td>
                                        </tr>
                                        <?php $y= $y + 1;
                                        $reference_total = $reference_total + $warranty->reference_price;
                                        ?>
                                    @endforeach
                                    <tr style="background-color: lightgrey;">
                                        <th colspan="2">TOTAL</th>
                                        <th class="text-right">S/. {{number_format($reference_total,2,'.','')}}</th>
                                    </tr>
                                </table>
                                <br>
                            @endif
                            @if(count($loan->joyasA) > 0)
                                <table class="table table-bordered" style="width: 100%">
                                    <tr>
                                        <th class="text-center" colspan="5" style="background-color: lightgrey;">
                                            Joyas
                                        </th>
                                    </tr>
                                    <tr style="background: green; color: white">
                                        <th class="text-center">#</th>
                                        <th class="text-center">Descripción</th>
                                        <th class="text-center" style="width: 120px">Peso Bruto (gr)</th>
                                        <th class="text-center" style="width: 120px">Peso Neto (gr)</th>
                                        <th class="text-center" style="width: 110px">Tasación</th>
                                    </tr>
                                    <?php $y= 0;
                                    $reference_total = 0;
                                    $pb= 0;
                                    $pn = 0;
                                    ?>
                                    @foreach($loan->joyasA as $warranty)
                                        <tr>
                                            <td>{{$y + 1}}</td>
                                            <td>
                                                {{$warranty->Description}}
                                            </td>
                                            <td class="text-right">
                                                {{$warranty->pb}}
                                            </td>
                                            <td class="text-right">
                                                {{$warranty->pn}}
                                            </td>
                                            <td class="text-right">
                                                S/. {{$warranty->reference_price}}
                                            </td>
                                        </tr>
                                        <?php $y= $y + 1;
                                        $reference_total = $reference_total + $warranty->reference_price;
                                        $pb = $pb + $warranty->pb;
                                        $pn = $pn + $warranty->pn;
                                        ?>
                                    @endforeach
                                    <tr style="background-color: lightgrey;">
                                        <th colspan="2">TOTAL</th>
                                        <th class="text-right"> {{number_format($pb,2,'.','')}}</th>
                                        <th class="text-right"> {{number_format($pn,2,'.','')}}</th>
                                        <th class="text-right">S/. {{number_format($reference_total,2,'.','')}}</th>
                                    </tr>
                                </table>
                            @endif
                        </td>
                    </tr>
                @endif
                @if(isset($loan->person))
                    <tr>
                        <th class="text-left">Referido por</th>
                        <td>: {{$loan->person->customer_name}}</td>
                    </tr>
                @endif
                @if(isset($loan->observations))
                    <tr>
                        <th class="text-left">Observaciones</th>
                        <td>: {{$loan->observations}}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
    <br>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table class="table" style ="font-size: 12px; width: 100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 30px">#</th>
                    <th class="text-center">Vcto</th>
                    <th class="col-sm-2 text-center">Cuota</th>
                    <th class="col-sm-2 text-center">Capital</th>
                    <th class="col-sm-1 text-center">Interes</th>
                    <th class="col-sm-1 text-center">Mora</th>
                    <th class="col-sm-1 text-center">Gastos</th>
                    <th class="col-sm-1 text-center">F.Pago</th>
                    <th class="col-sm-2 text-center">Situación</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $capital = 0;
                $interest = 0;
                $total = 0;
                $i = 1;
                ?>
                @foreach($loan->payments as $payment)
                    <tr
                            style="
                            @if($i%2 != 0)
                            background-color: lightgrey;
                            @endif
                            @if($payment['state'] == 'Pagado')
                                   color: green;
                                    font-weight: 800;
                            @endif
                                    "
                    >
                        <td class="text-center">{{$i++}}</td>
                        <td>{{$payment->expiration}}</td>
                        <td class="text-right">{{$loan->currency_format}} {{number_format($payment->amount,2,'.',',')}}</td>
                        <td class="text-right">{{$loan->currency_format}} {{number_format($payment->capital,2,'.',',')}}</td>
                        <td class="text-right">{{$loan->currency_format}} {{number_format($payment->interest,2,'.',',')}}</td>
                        <td class="text-right" >
                            @if(isset($payment->mora))
                                {{number_format($payment->mora,2,'.',',')}}
                            @endif
                        </td>
                        <td class="text-right" >
                            {{number_format($payment->expenses,2,'.',',')}}
                        </td>
                        <td>
                            @if(isset($payment->date_p))
                                {{$payment->date_p}}
                            @endif
                        </td>
                        <td>
                            @if(isset($payment->state))
                                {{$payment->state}}
                            @endif
                        </td>
                    </tr>
                    <?php
                    $capital = $capital + $payment->capital;
                    $interest = $interest + $payment->interest;
                    $total = $total + $payment->amount;
                    ?>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            @if($loan->type_payment == 'Libre' && $capital <= $loan->amount && isset($loan->code))
                <?php $index = count($loan->payments) - 1; ?>
                <p>
                    <strong>Saldo Capital : {{$loan->currency_format}} {{number_format($loan->payments[$index]->capital,2,'.',',')}} </strong><br/>
                    <strong>Pago Minimo: {{$loan->currency_format}} {{number_format($loan->payments[$index]->interest,2,'.',',')}} </strong>
                </p>
            @endif
        </div>
    </div>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <div class="grupo">
        <div class="caja base-50 text-center">
            ______________________________ <br/>
            CREDIPERU <br/>
        </div>
        <div class="caja base-50 text-center">
            ______________________________ <br/>
            CLIENTE <br/>
        </div>
    </div>
@stop
