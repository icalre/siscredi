@extends('pdf.layout')

@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            <strong style="font-size: 18px">PRESTAMOS ANULADOS</strong> <br>
            <strong style="font-size: 18px">del {{$date_start}} al {{$date_end}}</strong>
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:d')}}
        </div>
    </div>
    <br>
<div class="grupo">
    <div class="caja base-100">
        <table style="margin: 0 auto; width: 100%">
            <thead>
            <th class="text-center" style="width: 25px">#</th>
            <th style="width: 50px">Código</th>
            <th style="width: 260px">Cliente</th>
            <th>Asesor</th>
            <th style="width: 80px">Monto</th>
            <th style="width: 65px">Fecha</th>
            </thead>
            <tbody>
            <?php $i = 0; ?>
            @foreach($loans as $loan)
                <tr>
                    <td>{{$i + 1}}</td>
                    <td>{{$loan->code}}</td>
                    <td>
                        {{$loan->customer->customer->last_name}} {{$loan->customer->customer->name}}
                    </td>
                    <td>
                        {{$loan->employee->person->last_name}} {{$loan->employee->person->name}}
                    </td>
                    <td class="text-right">
                        {{$loan->amount}}
                    </td>
                    <td class="text-center">
                        {{$loan->date_annulment}}
                    </td>
                </tr>
                <?php $i++; ?>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop
