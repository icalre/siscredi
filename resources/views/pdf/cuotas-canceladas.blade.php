@extends('pdf.layout')

@section('content')
    <div class="grupo">
        <div class="caja base-30">
            {{Html::image('images/logo_membretado.jpg')}}
        </div>
        <div class="caja base-40 text-center">
            <strong style="font-size: 18px">PAGO DE CUOTAS</strong> <br>
            <strong style="font-size: 18px">del {{$date_start}} al {{$date_end}}</strong>
        </div>
        <div class="caja base-30 text-right">
            Fecha: {{date('d-m-Y H:i:d')}}
        </div>
    </div>
    <br>
    <div class="grupo">
        <div class="caja base-100">
            <table style="margin: 0 auto; width: 100%;">
                <thead style="background: deepskyblue">
                    <th class="text-center" style="width: 25px">#</th>
                    <th style="width: 50px">Código</th>
                    <th style="width: 240px">Cliente</th>
                    <th style="width: 240px">Asesor</th>
                    <th style="width: 70px">DNI</th>
                    <th style="width: 70px">Capital</th>
                    <th style="width: 70px">Intereses</th>
                    <th style="width: 70px">Mora</th>
                    <th style="width: 70px">Gastos</th>
                    <th style="width: 70px">Cuota</th>
                    <th style="width: 70px"># Cuota</th>
                    <th style="width: 70px"></th>
                    <th style="width: 125px">F. Pago</th>
                </thead>
                <tbody>
                <?php $i = 0;
                $interest = 0;
                $mora = 0;
                $total = 0;
                $gastos = 0;
                $capital = 0;
                $efectivo = 0;
                $cuenta_corriente = 0;
                ?>
                @foreach($payments as $payment)
                    <tr>
                        <td>{{$i + 1}}</td>
                        <td>{{$payment->loan->code}}</td>
                        <td>
                            {{$payment->loan->customer->customer->last_name}} {{$payment->loan->customer->customer->name}}
                        </td>
                        <td>
                            {{$payment->loan->employee->person->last_name}} {{$payment->loan->employee->person->name}}
                        </td>
                        <td>{{$payment->loan->customer->customer->dni}}</td>
                        <td class="text-right">
                            {{number_format($payment->capital,2,'.',',')}}
                        </td>
                        <td class="text-right">
                            {{number_format($payment->interest,2,'.',',')}}
                        </td>
                        <td class="text-right">
                            {{number_format($payment->mora,2,'.',',')}}
                        </td>
                        <td class="text-right">
                            {{number_format($payment->expenses,2,'.',',')}}
                        </td>
                        <td class="text-right">
                            {{$payment->amount}}
                        </td>
                        <td class="text-center">
                            {{$payment->total_payments}}
                        </td>
                        <td class="text-right">
                            @if(isset($payment->account) && !empty($payment->account))
                                {{$payment->account->name}}

                                @if(!isset($totals[$payment->account->name]))
                                    <?php $totals[$payment->account->name] = 0  ?>
                                @endif

                                <?php $totals[$payment->account->name] = $totals[$payment->account->name] + $payment->amount?>
                            @endif
                        </td>
                        <td class="text-center">
                            {{$payment->date_p}}
                        </td>
                    </tr>
                    <?php $i++;
                    $interest = $interest + $payment->interest;
                    $mora = $mora + $payment->mora;
                    $capital = $capital + $payment->capital;
                    $total = $total + $payment->amount;
                    $gastos = $gastos + $payment->expenses;

                    if ($payment->type_payment == 'Efectivo') {
                        $efectivo = $efectivo + $payment->amount;

                    } else {
                        $cuenta_corriente = $cuenta_corriente + $payment->amount;

                    }
                    ?>
                @endforeach
                <tr>
                    <th colspan="4">
                        Total
                    </th>
                    <th class="text-right">
                        {{number_format($capital,2,'.',',')}}
                    </th>
                    <th class="text-right">
                        {{number_format($interest,2,'.',',')}}
                    </th>
                    <th class="text-right">
                        {{number_format($mora,2,'.',',')}}
                    </th>
                    <th class="text-right">
                        {{number_format($gastos,2,'.',',')}}
                    </th>
                    <th class="text-right">
                        {{number_format($total,2,'.',',')}}
                    </th>
                </tr>
                </tbody>
            </table>
            <br>
            <table>
                @foreach($totals as $key=>$total)
                    <tr>
                        <th colspan="3">
                            {{$key}}
                        </th>
                        <th class="text-right">
                            {{number_format($total,2,'.','')}}
                        </th>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@stop
