<h3>
    Wall
    <small>Gestión de Ingresos y gastos de wall</small>
</h3>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-6">
                        <table class="table table-bordered">
                            <tr>
                                <th colspan="2">
                                    Cuentas
                                </th>
                            </tr>
                            @foreach($extra_accounts  as $account)
                                <tr>
                                    <th>
                                        {{$account->name}}
                                    </th>
                                    <td>
                                        {{$account->amount}}
                                    </td>
                                </tr>
                            @endforeach

                        </table>
                    </div>
                </div>
            </div>
            <hr>
            <div class="panel-body" ng-show="showRegister">
                <div class="row">
                    <div class="col-sm-12">
                        <strong>
                            Registrar @{{ title }}
                        </strong>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        {!! Field::text('amount','',['placeholder'=>'0.00','class'=>'text-right','required'=>'required',
                    'ng-model'=>'item.amount', 'decimals'=>'2'],['required'],'loanForm')!!}
                    </div>
                    <div class="col-sm-3">
                        <label for="type_method">Cuenta</label>
                        <select name="type_method" id="type_method" class="form-control" chosen="chosen" ng-model="item.extra_account_id">
                            <option value="">Cuenta</option>
                            <option value="1">Efectivo</option>
                            <option value="2">BVVA</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        {!! Field::text('description','',[
                     'ng-model'=>'item.description', 'placeholder'=>'Descripción'],[],'loanForm')!!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <a href="#" class="btn btn-info pull-right" ng-click="saveItem()">
                            Guardar
                        </a>
                        <a style="margin-right: 5px" href="" class="btn btn-warning pull-right" ng-click="hideRegisterForm()">
                            Cancelar
                        </a>
                    </div>
                </div>
            </div>
            <div class="panel-body" ng-show="!showRegister">
                <div class="row">
                    <div class="col-sm-12">
                        <a href="#" class="btn btn-info" ng-click="changeItems(1)">
                            Ingresos
                        </a>
                        <a href="#" class="btn btn-warning" ng-click="changeItems(2)">
                            Gastos
                        </a>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-3">
                        <input type="text" id="date1" calendar="calendar" class="form-control"
                               ng-model="date_start" placeholder="Fecha Inicial">
                    </div>
                    <div class="col-sm-3">
                        <input type="text" id="date1" calendar="calendar" class="form-control"
                               ng-model="date_end"  placeholder="Fecha Final">
                    </div>
                    <div class="col-sm-2">
                        <a href="#" ng-click="getData()" class="btn btn-success">
                            Buscar
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a href="#" ng-click="showRegisterForm()" class="btn btn-info pull-right"
                           style="font-weight: 900">
                            <i class="icon-pencil"></i>
                            Registrar @{{ title }}
                        </a>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr class="bg-success">
                                    <th colspan="6" style="color: white" class="text-center">
                                        @{{ title }}
                                    </th>
                                </tr>
                                <tr class="bg-success">
                                    <th class="text-center" style="width: 25px; color: white">#</th>
                                    <th class="text-center" style="width: 150px; color: white">Fecha</th>
                                    <th class="text-center" style="width: 150px; color: white">Cuenta</th>
                                    <th class="text-center" style="width: 100px; color: white">Monto</th>
                                    <th class="text-center" style="color: white">Descripción</th>
                                    <th class="text-center" style="width: 70px; color: white"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="row in items track by $index" ng-class ="{'text-danger' : row.state == 0} ">
                                    <td class="text-center">
                                        @{{$index + 1}}
                                    </td>
                                    <td>
                                        @{{ row.created_at }}
                                    </td>
                                    <td>
                                        @{{ row.extra_account.name }}
                                    </td>
                                    <td class="text-right">
                                        @{{ row.amount }}
                                    </td>
                                    <td>
                                        @{{ row.description }}
                                    </td>
                                    <td>
                                        <a href="#" ng-click="annulledItem(row)" class="btn btn-danger" ng-hide="row.state == 0">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>