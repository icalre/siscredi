    {!!Form::token()!!}
    <div class="row">
        <div class="col-sm-3">
            {!!Field::select('site_id',[],'',[
              'chosen'=>'chosen',
              'required'=>'required',
              'data-placeholder'=>"Selecciona un local",
              'no-results-text'=>"'No encontrado'",
              'ng-model'=>"employee.site",
              'ng-options'=>"item.name for item in sites track by item.id"],
              ['required'],'areaForm')!!}
        </div>
        <div class="col-sm-3">
            {!! Field::select('area_id',[],'',['required'=>'required',
                        'chosen'=>'chosen',
                            'ng-model'=>'employee.area',
                            'ng-options'=>"item.name for item in employee.site.areas track by item.id"],['required'],'areaJobForm' )!!}
        </div>

        <div class="col-sm-3">
            {!! Field::select('job',[],'',['required'=>'required',
                        'chosen'=>'chosen',
                            'ng-model'=>'employee.area_job',
                            'ng-options'=>"item.name for item in employee.area.area_jobs track by item.id"],
                            ['required'],'areaJobForm' )!!}
        </div>
        <div class="col-sm-3">
            {!! Field::select('state',[],'',['required'=>'required',
                        'chosen'=>'chosen',
                            'ng-model'=>'employee.state',
                            'ng-options'=>"item.name for item in states track by item.id"],
                            ['required'],'areaJobForm' )!!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <label for="">Persona</label>
            <div angucomplete-alt
                 id="person"
                 placeholder="Buscar Persona"
                 pause="500"
                 selected-object="employee.person"
                 remote-url="app/api/people?q="
                 title-field="full_name"
                 minlength="3"
                 input-class="form-control form-control-small"
                 match-class="highlight"
                 field-required="true",
                 input-name="person_id",
                 initial-value="employee.person">
            </div>
        </div>
        <div class="col-sm-2">
            {!!Field::text('salary_amount','',['placeholder'=>'0.00','number'=>'number','class'=>'text-right',
               'ng-model'=>'employee.salary_amount', 'ng-change'=>'setSubSalaries()', 'decimals'=>'2'],
               [],'stockForm')!!}
        </div>
        <div class="col-sm-1">
            {!!Field::text('hours_day','',['placeholder'=>'0','number'=>'number','class'=>'text-right',
                                           'ng-change'=>'setSubSalaries()','ng-model'=>'employee.hours_day'],
                                           [],'stockForm')!!}
        </div>
        <div class="col-sm-1">
            {!!Field::text('salary_day','',['placeholder'=>'0.00','number'=>'number','class'=>'text-right',
                                           'ng-model'=>'employee.salary_day', 'decimals'=>'2', 'disabled' => 'disabled'],
                                           [],'stockForm')!!}
        </div>
        <div class="col-sm-1">
            {!!Field::text('salary_hour','',['placeholder'=>'0.00','number'=>'number','class'=>'text-right',
                                           'ng-model'=>'employee.salary_hour', 'decimals'=>'2', 'disabled'=> 'disabled'],
                                           [],'stockForm')!!}
        </div>
    </div>
