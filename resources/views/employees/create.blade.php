<h3>
    Registrar Trabajador
    <small>Registro de Trabajadores</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="employeeForm" class="form-validate" role="form" ng-submit="employeeForm.$valid && createEmployee()" novalidate>
        <div class="panel panel-default" ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('employees.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.employees" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </form>
    </div>
</div>