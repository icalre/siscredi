{!!Form::token()!!}
<div class="row">
    <div class="col-sm-6">
        {!! Field::text('razon_social','',['placeholder'=>'Ingrese su Razón Social','required'=>'required',
            'ng-model'=>'enterprise.razon_social'],['required'],'enterpriseForm')!!}
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        {!! Field::text('ruc','',['placeholder'=>'Ingrese su RUC','required'=>'required',
            'ng-model'=>'enterprise.ruc'],['required'],'enterpriseForm')!!}
    </div>
    <div class="col-sm-7">
        {!! Field::text('address', '',['placeholder'=>'Ingrese su dirección','required'=>'required',
            'ng-model'=>'enterprise.address'],['required'],'enterpriseForm') !!}
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        {!!Field::select('department_id',[],'',['required'=>'required',
         'chosen'=>'',
         'data-placeholder'=>"Selecciona un Departmamento",
         'no-results-text'=>"'No encontrado'",
         'ng-model'=>"enterprise.department",
         'width'=>"'100%'",
          'class'=>"chosen-select input-md",
         'ng-options'=>"item.departamento for item in departments track by item.id"],
         ['required'],'enterpriseForm')!!}
    </div>
    <div class="col-sm-4">

        {!!Field::select('province_id',[],'',['required'=>'required',
         'chosen'=>'',
         'data-placeholder'=>"Selecciona una Provincia",
         'no-results-text'=>"'No encontrado'",
         'ng-model'=>"enterprise.province",
         'width'=>"'100%'",
          'class'=>"chosen-select input-md",
         'ng-options'=>"item.provincia for item in enterprise.department.provinces track by item.id"],
         ['required'],'enterpriseForm')!!}
    </div>
    <div class="col-sm-4">

        {!!Field::select('district_id',[],'',['required'=>'required',
         'chosen'=>'',
         'data-placeholder'=>"Selecciona un Distrito",
         'no-results-text'=>"'No encontrado'",
         'ng-model'=>"enterprise.district",
         'width'=>"'100%'",
          'class'=>"chosen-select input-md",
         'ng-options'=>"item.distrito for item in enterprise.province.districts track by item.id"],
         ['required'],'enterpriseForm')!!}
    </div>
</div>
<br>
<div class="row">
    <div class="col-sm-3">
        {!! Field::text('telephone', '',['placeholder'=>'Ingrese su telefono',
            'ng-model'=>'enterprise.telephone'],[],'enterpriseForm') !!}
    </div>
    <div class="col-sm-3">
        {!! Field::text('phone_reference', '',['placeholder'=>'Ingrese su telefono',
            'ng-model'=>'enterprise.phone_reference'],[],'enterpriseForm') !!}
    </div>
    <div class="col-sm-6">
        {!! Field::email('email','',['placeholder'=>'Ingrese su email',
            'ng-model'=>'enterprise.email'],['email'],'enterpriseForm')!!}
    </div>
</div>
<div class="row">
    <div class="col-sm-2">
        {!! Field::text('activity', '',['placeholder'=>'Ingrese Actividad Economica',
            'ng-model'=>'enterprise.activity'],[],'enterpriseForm') !!}
    </div>
    <div class="col-sm-7">
        {!! Field::text('website','',['placeholder'=>'Ingrese su Sitio Web',
            'ng-model'=>'enterprise.website'],[],'enterpriseForm')!!}
    </div>
</div>
<div class="row">
    <div class="col-sm-7">
        <table class="table table-bordered">
            <tr>
                <th class="text-center" colspan="3" style="background-color: green; color: white">
                    REPRESENTANTE LEGAL
                    <a href="#" style="color: white" class="pull-right" ng-click="addPerson()">
                        <i class="fa fa-plus-circle"></i>
                    </a>
                </th>
            </tr>
            <tr>
                <th style="width: 65%" class="text-center">Nombre</th>
                <th class="text-center">Cargo</th>
                <th style="width: 1%" class="text-center"> </th>
            </tr>
            <tr ng-repeat="row in enterprise.people track by $index">
                <td>
                    <div angucomplete-alt
                         id="person_customer_@{{$index}}"
                         placeholder="Buscar Cliente"
                         pause="500"
                         selected-object="row.person"
                         remote-url="app/api/people?q="
                         title-field="customer_name"
                         minlength="3"
                         input-class="form-control form-control-small"
                         match-class="highlight"
                         input-name="person_id_customer_@{{$index}}",
                         initial-value="row.person">
                    </div>
                </td>
                <td>
                    {!! Field::text('cargo','',['placeholder'=>'Cargo',
            'ng-model'=>'row.cargo'],[],'enterpriseForm', 0)!!}
                </td>
                <td>
                    <a href="#" ng-click="removePerson($index)" style="color: red">
                        <i class="fa fa-times"></i>
                    </a>
                </td>
            </tr>
        </table>
    </div>
    @if(\Request::segment(3) != 'form-create')
    <div class="col-sm-4" ng-show="enterprise.signature.length > 0">
        <div class="pv-lg">
            <img src="@{{enterprise.signature}}"
                 alt="Contact"
                 class="center-block img-responsive img-circle img-thumbnail thumb96" />
        </div>
    </div>
        @endif
</div>

<div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="panel">
                    <div class="panel-heading">
                        <a href="#" ng-click="reset()" class="pull-right">
                            <small class="fa fa-refresh text-muted"></small>
                        </a>
                        <strong>Subir firma</strong>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <input id="fileInput" filestyle="" accept="image/*"
                                   type="file" data-class-button="btn btn-default"
                                   data-class-input="form-control" data-button-text="" class="form-control" required="required" />
                        </div>
                        <br/>
                        <div data-text="" class="imgcrop-preview">
                            <img ng-src="@{{myCroppedImage}}" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="panel">
                    <div class="panel-body">
                        <div class="imgcrop-area">
                            <img-crop image="myImage" result-image="myCroppedImage" area-type="@{{imgcropType}}"></img-crop>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>