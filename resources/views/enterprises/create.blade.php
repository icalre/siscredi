<h3>
    Registrar Empresa
    <small>Registro de Empresas</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="enterpriseForm" class="form-validate" role="form" ng-submit="enterpriseForm.$valid && createEnterprise()" novalidate>
        <div class="panel panel-default" ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('enterprises.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.enterprises" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </form>
    </div>
</div>