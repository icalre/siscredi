<div class="form-group">

    @if(\Lang::has('validation.attributes.' . $name) && $label_option == 1)
        {!! Form::label($name, $label) !!}
    @endif

    {!! $control !!}
    @if(count($angular_validation) > 0)
        @foreach($angular_validation as $error)
            @if($error == 'required')
                <span ng-show="{{$form_name}}.{{$name}}.$dirty &amp;&amp; {{$form_name}}.{{$name}}.$error.{{$error}}" class="text-danger">
                    Este campo es requerido.
                </span>
            @elseif($error == 'email')
                <span ng-show="{{$form_name}}.{{$name}}.$dirty &amp;&amp; {{$form_name}}.{{$name}}.$error.{{$error}}" class="text-danger">
                    Ingrese un correo válido
                </span>
            @endif
        @endforeach
    @endif
</div>

