    {!!Form::token()!!}
    <div class="row">
        <div class="col-sm-4">
            <label for="">Buscar Cliente</label>
            <div angucomplete-alt
                 id="person_customer"
                 placeholder="Buscar Cliente"
                 pause="500"
                 selected-object="addPerson"
                 remote-url="app/api/people?q="
                 title-field="customer_name"
                 minlength="3"
                 input-class="form-control form-control-small"
                 match-class="highlight"
                 input-name="person_id_customer"
             >
            </div>
        </div>
        <div class="col-sm-8">
            {!!Field::select('customers',[],[],['required'=>'required',
             'chosen'=>'',
             'multiple'=>'multiple',
             'no-results-text'=>"'No encontrado'",
             'ng-model'=>"sentMessage.people",
             'data-placeholder'=>'Clientes',
             'width'=>"'100%'",
              'class'=>"chosen-select input-md",
             'ng-options'=>"item.customer_name for item in people track by item.id"],
             ['required'],'sentMessageForm')!!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            {!!Field::select('type',[],'',['required'=>'required',
            'chosen'=>'',
            'data-placeholder'=>"Selecciona un Distrito",
            'no-results-text'=>"'No encontrado'",
            'ng-model'=>"sentMessage.type",
            'width'=>"'100%'",
             'class'=>"chosen-select input-md",
            'ng-options'=>"item.name for item in types track by item.value"],
            ['required'],'sentMessageForm')!!}
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10">
            {!! Field::textarea('message', '',['placeholder'=>'Ingrese el mensaje a enviar','required'=>'required',
                'ng-model'=>'sentMessage.message'],['required'],'sentMessageForm') !!}
        </div>
    </div>
