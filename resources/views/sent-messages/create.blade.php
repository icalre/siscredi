<h3>
    Enviar Mensaje de Texto
    <small>Envio de mensajes de Texto</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="sentMessageForm" class="form-validate" role="form"
              ng-submit="sentMessageForm.$valid && createSentMessage()" novalidate>
        <div class="panel panel-default" ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('sent-messages.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.sent-messages" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
        </div>
        </form>
    </div>
</div>