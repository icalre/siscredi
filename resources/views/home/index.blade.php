<!-- top navbar-->
<header class="topnavbar-wrapper">
    @include('home.partials.top-navbar')
</header>
<!-- sidebar-->
<aside  ng-controller="SidebarController" class="aside">
    @include('home.partials.sidebar')
</aside>
<!-- offsidebar-->
<aside  class="offsidebar">

</aside>
<!-- Main section-->
<section>
    <!-- Page content-->
    <div ui-view="" autoscroll="false" ng-class="app.viewAnimation" class="content-wrapper"></div>
</section>
<!-- Page footer-->
<footer >
    @include('home.partials.footer')
</footer>