<h3>
    Inicio
    <small>@{{ 'dashboard.WELCOME' | translate:{ appName: app.name } }}!</small>
</h3>
<!-- START widgets box-->
<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-12">
        <!-- START date widget-->
        <div class="panel widget">
            <div class="row row-table">
                <div class="col-xs-4 text-center bg-green pv-lg">
                    <!-- See formats: https://docs.angularjs.org/api/ng/filter/date-->
                    <now format="MMMM" class="text-sm text-uppercase"></now>
                    <br/>
                    <now format="d" class="h2 mt0"></now>
                </div>
                <div class="col-xs-8 pv-lg">
                    <now format="EEEE" class="text-uppercase"></now>
                    <br/>
                    <now format="h:mm" class="h2 mt0"></now>
                    <now format="a" class="text-muted text-sm"></now>
                </div>
            </div>
        </div>
    </div>
    @if(in_array('app/flujo', $sub_modules))
        <div class="col-lg-3 col-md-6 col-sm-12">
            <!-- START date widget-->
            <div class="panel widget">
                <div class="row row-table">
                    <div class="col-xs-4 text-center bg-purple pv-lg">
                        <em class="fa fa-money fa-3x"></em>
                    </div>
                    <div class="col-xs-8 pv-lg">
                        SALDO DE CAJA
                        <br/>
                        <span class="h4 mt0">
                        {{$total}}
                    </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12">
            <!-- START date widget-->
            <div class="panel widget">
                <div class="row row-table">
                    <div class="col-xs-4 text-center bg-warning pv-lg">
                        <em class="fa fa-dollar fa-3x"></em>
                    </div>
                    <div class="col-xs-8 pv-lg">
                        CAPITAL TOTAL
                        <br/>
                        <span class="h4 mt0">
                        {{number_format(Session::get('cash_flow') + Session::get('capital_pending'), 2,'.', '')}}
                    </span>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>

<div class="row">
    @if(in_array('app/flujo', $sub_modules))
        <div class="col-sm-4">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <!-- START date widget-->
                <div class="panel widget">
                    <div class="row row-table">
                        <div class="col-xs-4 text-center bg-info pv-lg">
                            <em class="icon-credit-card fa-2x"></em>
                        </div>
                        <div class="col-xs-8 pv-lg">
                            <a href="/app/home/pagadas" target="_blank">CUOTAS P.</a>

                            <br/>
                            <span class="h4 mt0">
                        @{{ targets.payment }}
                    </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <!-- START date widget-->
                <div class="panel widget">
                    <div class="row row-table">
                        <div class="col-xs-4 text-center bg-info pv-lg">
                            <em class="icon-credit-card fa-2x"></em>
                        </div>
                        <div class="col-xs-8 pv-lg">
                            <a href="/app/home/ingresos" target="_blank">
                                INGRESOS
                            </a>

                            <br/>
                            <span class="h4 mt0">
                        @{{ targets.cash_desk_incomes }}
                    </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <!-- START date widget-->
                <div class="panel widget">
                    <div class="row row-table">
                        <div class="col-xs-4 text-center bg-danger pv-lg">
                            <em class="icon-login fa-2x"></em>
                        </div>
                        <div class="col-xs-8 pv-lg">
                            <a href="/app/home/desembolsos" target="_blank">
                                DESEMBOLSOS
                            </a>
                            <br/>
                            <span class="h4 mt0">
                        @{{ targets.disbursement }}
                    </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <!-- START date widget-->
                <div class="panel widget">
                    <div class="row row-table">
                        <div class="col-xs-4 text-center bg-danger pv-lg">
                            <em class="icon-login fa-2x"></em>
                        </div>
                        <div class="col-xs-8 pv-lg">
                            <a href="/app/home/gastos" target="_blank">
                                GASTOS GENERALES
                            </a>
                            <br/>
                            <span class="h4 mt0">
                        @{{ targets.gastos }}
                    </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <!-- START date widget-->
                <div class="panel widget">
                    <div class="row row-table">
                        <div class="col-xs-4 text-center bg-danger pv-lg">
                            <em class="icon-login fa-2x"></em>
                        </div>
                        <div class="col-xs-8 pv-lg">
                            <a href="/app/home/egresos" target="_blank">
                                GASTOS ACTIVOS E INVERSIÓN
                            </a>
                            <br/>
                            <span class="h4 mt0">
                        @{{ targets.egresos }}
                    </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    @endif
</div>
@if(in_array('app/flujo', $sub_modules))
<div class="row">
    <div class="col-lg-12">
        <!-- START widget-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">Estadisticas {{date('Y')}}</div>
            </div>
            <div class="panel-wrapper">
                <div class="panel-body text-center">
                    <strong class="label label-info">Cantidades</strong> <br>
                    <flot dataset="chart.lineData" options="chart.lineOptions" height="250px"></flot>
                    <br>
                    <br>
                    <br>
                    <strong class="label label-warning">Montos</strong>
                    <flot dataset="chart.lineData2" options="chart.lineOptions" height="250px"></flot>
                    <br>
                    <br>
                    <br>
                    <strong class="label label-success">Capital</strong>
                    <flot dataset="chart.lineData3" options="chart.lineOptions2" height="250px"></flot>
                    <br>
                    <br>
                </div>
            </div>
        </div>
        <!-- END widget-->
    </div>
</div>
    @endif
