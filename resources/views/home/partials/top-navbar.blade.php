<!-- START Top Navbar-->
<nav role="navigation" class="navbar topnavbar">
    <!-- START navbar header-->
    <div class="navbar-header" style="background: white; height:80px">
        <a href="/" class="navbar-brand">
            <div class="brand-logo">
                <img src="images/logo_membretado.jpg" alt="App Logo" class="img-responsive"/>
            </div>
            <div class="brand-logo-collapsed">
                <img src="app-js/img/logo-single.png" alt="App Logo" class="img-responsive"/>
            </div>
        </a>
    </div>
    <!-- END navbar header-->
    <!-- START Nav wrapper-->
    <div class="nav-wrapper">
        <!-- START Left navbar-->
        <ul class="nav navbar-nav">
            <li>
                <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                <a href="" trigger-resize="" ng-click="app.layout.isCollapsed = !app.layout.isCollapsed"
                   ng-if="!app.layout.isCollapsedText" class="hidden-xs">
                    <em class="fa fa-navicon"></em>
                </a>
                <!-- Button to show/hide the sidebar on mobile. Visible on mobile only.-->
                <a href="" ng-click="app.asideToggled = !app.asideToggled" class="visible-xs sidebar-toggle">
                    <em class="fa fa-navicon"></em>
                </a>
            </li>
            <!-- START User avatar toggle-->
            <li>
                <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                <a href="" ng-click="toggleUserBlock()">
                    <em class="icon-user"></em>
                </a>
            </li>
            <!-- END User avatar toggle-->
            <!-- START lock screen-->
            <li>
                <a ng-click="logOut()" title="Cerrar Sesión">
                    <em class="icon-lock"></em>
                </a>
            </li>
            <!-- END lock screen-->
        </ul>
        <!-- END Left navbar-->
        <!-- START Right Navbar-->
        <ul class="nav navbar-nav navbar-right">
            @if(in_array('app/flujo', $sub_modules))

                <?php $total = 0; ?>

                @foreach($accounts as $account)
                    <li>
                        <a href="#">
                            {{$account->name}} : {{strpos($account->name, 'DOLARES') ? '$' : 'S/.'}} {{number_format($account->amount, 2,'.', '')}}
                        </a>
                    </li>
                    <?php $total = $total + $account->amount; ?>
                @endforeach
                <li>
                    <a href="#">
                        Total : S/. {{number_format($total, 2,'.', '')}}
                    </a>
                </li>
        @endif
        <!-- Fullscreen (only desktops)-->
            <li class="visible-lg">
                <a href="" toggle-fullscreen="toggle-fullscreen">
                    <em class="fa fa-expand"></em>
                </a>
            </li>
        </ul>
        <!-- END Right Navbar-->
    </div>
    <!-- END Nav wrapper-->
    <!-- START Search form-->
    <form role="search" action="search.html" class="navbar-form">
        <div class="form-group has-feedback">
            <input type="text" placeholder="@{{ 'topbar.search.PLACEHOLDER' | translate }}" class="form-control"/>
            <div search-dismiss="search-dismiss" class="fa fa-times form-control-feedback"></div>
        </div>
        <button type="submit" class="hidden btn btn-default">Submit</button>
    </form>
    <!-- END Search form-->
</nav>
<!-- END Top Navbar-->
