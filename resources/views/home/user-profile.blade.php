<div class="row" ng-hide="uploadPhotoForm">
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="#" class="btn btn-info pull-right" ng-click="updatePhotoForm()" >
                    Cambiar Foto <i class="fa fa-pencil"></i>
                </a>
            </div>
            <div class="panel-body text-center">
                <div class="pv-lg">
                    <img src="
                    @if($user->avatar)
                    {{\URL::to('/')}}/{{$user->avatar}}
                    @else
                         @if($user->person->gender == 'Male')
                            app-js/img/user/02.jpg
                         @else
                            app-js/img/user/09.jpg
                          @endif
                    @endif
                    "
                         alt="Contact"
                         class="center-block img-responsive img-circle img-thumbnail thumb96" />
                </div>
                <h3 class="m0 text-bold">
                    {{$user->person->name}} {{$user->person->last_name}}
                </h3>
                <div class="text-center">
                    <a href="#" ng-click="changePassword()" class="btn btn-primary">Cambiar Contraseña</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="h4 text-center">Información</div>
                <div class="row pv-lg">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <div class="col-sm-3" style="font-weight: 900">
                                    Nombre
                                </div>
                                <div class="col-sm-9">
                                    {{$user->person->name}} {{$user->person->last_name}}
                                </div>
                            </div>
                            <div class="form-group">
                                <div for="inputContact5" class="col-sm-3" style="font-weight: 900">DNI</div>
                                <div class="col-sm-9">
                                    {{$user->person->dni}}
                                </div>
                            </div>
                            <div class="form-group">
                                <div for="inputContact2" class="col-sm-3" style="font-weight: 900">Email</div>
                                <div class="col-sm-9">
                                    {{$user->person->email}}
                                </div>
                            </div>
                            <div class="form-group">
                                <div for="inputContact3" class="col-sm-3" style="font-weight: 900">Telefono</div>
                                <div class="col-sm-9">
                                   {{$user->person->telephone}}
                                </div>
                            </div>
                            <div class="form-group">
                                <div for="inputContact6" class="col-sm-3" style="font-weight: 900">Dirección</div>
                                <div class="col-sm-9">
                                    {{$user->person->address}}
                                </div>
                            </div>
                            <div class="form-group">
                                <div for="inputContact7" class="col-sm-3" style="font-weight: 900">Departamento</div>
                                <div class="col-sm-9">
                                    {{$user->person->department->departamento}}
                                </div>
                            </div>
                            <div class="form-group">
                                <div for="inputContact8" class="col-sm-3" style="font-weight: 900">Provincia</div>
                                <div class="col-sm-9">
                                    {{$user->person->province->provincia}}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3" style="font-weight: 900">Distrito</div>
                                <div class="col-sm-9">
                                    {{$user->person->district->distrito}}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3" style="font-weight: 900">Color</div>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <a href="#" ng-click="setColor()">
                                                <i id="user-color" class="fa fa-circle"
                                                   @if(!empty($user->color))
                                                   style="color: {{$user->color}}"
                                                   @endif
                                                ></i>
                                            </a>
                                        </div>
                                        <div class="col-sm-8" ng-show="changeColor">
                                            <div class="input-group">
                                                <input type="text" colorpicker="" ng-model="user.color" ng-change="setNewColor()" class="form-control" />
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-info" ng-click="updateColor()">Guardar</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div ng-show="uploadPhotoForm"  class="container-fluid">
    <form name="userPhotoForm" role="form" ng-submit="userPhotoForm.$valid && updatePhoto()" novalidate>
        {!!Form::token()!!}
    <div class="row">
        <div class="col-md-3">
            <div class="panel">
                <div class="panel-heading">
                    <a href="#" ng-click="reset()" class="pull-right">
                        <small class="fa fa-refresh text-muted"></small>
                    </a>Selecciona una imagen</div>
                <div class="panel-body">
                    <div class="form-group">
                        <input id="fileInput" filestyle="" accept="image/*"
                               type="file" data-class-button="btn btn-default"
                               data-class-input="form-control" data-button-text="" class="form-control" required="required" />
                    </div>
                    <br/>
                    <div data-text="" class="imgcrop-preview">
                        <img ng-src="@{{myCroppedImage}}" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel">
                <div class="panel-body">
                    <div class="imgcrop-area">
                        <img-crop image="myImage" result-image="myCroppedImage" area-type="@{{imgcropType}}"></img-crop>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <button type="button" ng-click="cancelUploadPhoto()" class="btn btn-default mr">Cancelar</button>
            <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
    </div>
    </form>
</div>


<script type="text/ng-template" id="dialogWithNestedConfirmDialogId">
    <div class="ngdialog-message">
        <h3 class="mt0">Cambiar Contraseña</h3>

        <form name="userForm" role="form" ng-submit="userForm.$valid && updatePassword()" novalidate>
        {!!Form::token()!!}
        <div class="row">
            <div class="col-sm-12">
                {!! Field::password('old-password',['placeholder'=>'Ingrese su contraseña actual','required'=>'required',
                    'ng-model'=>'user.old_password'],['required'],'userForm') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                {!! Field::password('password',['placeholder'=>'Ingrese nueva contraseña','required'=>'required',
                    'ng-model'=>'user.password'],['required'],'userForm') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                {!! Field::password('password_confirmation',['placeholder'=>'Repite nueva contraseña','required'=>'required',
                    'ng-model'=>'user.password_confirmation'],['required'],'userForm') !!}
            </div>
        </div>
        <button type="button" ng-click="closeThisDialog('Cancel')" class="btn btn-default mr">Cancelar</button>
            <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
    </div>
</script>