    {!!Form::token()!!}
    <div class="row">
        <div class="col-sm-3">
            {!!Field::select('modalidad',config('options.modalidades'),'',
            ['required'=>'required',
               'ng-model'=>'loan.modalidad', 'chosen'=>''],['required'],'loanForm' )!!}
        </div>
        <div class="col-sm-4" ng-show="loan.modalidad == 'Descuento por Planilla' ">
            <label for="">Empresa</label>
            <div angucomplete-alt
                 id="enterprise"
                 placeholder="Buscar Empresa"
                 pause="500"
                 selected-object="loan.enterprise"
                 remote-url="app/api/enterprises?q="
                 title-field="customer_name"
                 minlength="3"
                 input-class="form-control form-control-small"
                 match-class="highlight"
                 input-name="enterprise_id"
                 initial-value="loan.enterprise">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-5">
            <label for="">Cliente</label>
            <div angucomplete-alt
                 id="person_customer"
                 placeholder="Buscar Cliente"
                 pause="500"
                 selected-object="loan.customer"
                 remote-url="app/api/people?q="
                 title-field="customer_name"
                 minlength="3"
                 input-class="form-control form-control-small"
                 match-class="highlight"
                 field-required="true"
                 input-name="person_id_customer"
                 initial-value="loan.customer.customer">
            </div>
        </div>
        <div class="col-sm-5">
            <label for="">Asesor de Negocios</label>
            <div angucomplete-alt
                 id="person_employee"
                 placeholder="Buscar Asesor"
                 pause="500"
                 selected-object="loan.employee"
                 remote-url="app/api/employees?q="
                 title-field="person.customer_name"
                 minlength="3"
                 input-class="form-control form-control-small"
                 match-class="highlight"
                 input-name="person_id_employee"
                 initial-value="loan.employee">
            </div>
        </div>
        @if(in_array('app/a-loan', $sub_modules))
        <div class="col-sm-2">
            <br>
            <a style="margin-top: 5px" href="#" class="btn btn-info" ng-click="setNewEmployee()">
                <i class="icon-refresh"></i>
            </a>
        </div>
        @endif
    </div>
    <br>
    <div class="row">
        <div class="col-sm-2">
            {!! Field::text('amount','',['placeholder'=>'0.00','class'=>'text-right','required'=>'required',
                'ng-model'=>'loan.amount', 'decimals'=>'2'],['required'],'loanForm')!!}
        </div>
        <div class="col-sm-2">
            {!! Field::text('interest','',['placeholder'=>'0.00','class'=>'text-right','required'=>'required',
                'ng-model'=>'loan.interest', 'decimals'=>'2'],['required'],'loanForm')!!}
        </div>
        <div class="col-sm-2">
            {!!Field::select('currency',config('options.currencys'),'',
            ['required'=>'required',
               'ng-model'=>'loan.currency', 'chosen'=>''],['required'],'loanForm' )!!}
        </div>
        <div class="col-sm-2">
            {!!Field::select('period',config('options.periodos'),'',
            ['required'=>'required',
               'ng-model'=>'loan.period', 'chosen'=>''],['required'],'loanForm' )!!}
        </div>
        <div class="col-sm-2">
            {!!Field::select('type_payment',config('options.type_payments'),'',
            ['required'=>'required',
               'ng-model'=>'loan.type_payment', 'chosen'=>''],['required'],'loanForm' )!!}
        </div>
        <div class="col-sm-2">
            {!! Field::text('quotas','',['placeholder'=>'0','class'=>'text-right','required'=>'required',
                'ng-model'=>'loan.quotas', 'ng-model-options'=>'{debounce:1000}','ng-change'=>'setWeekQuota()'],['required'],'loanForm')!!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            {!!Field::select('endorsement_option',config('options.option_de'),'',
            ['required'=>'required',
               'ng-model'=>'loan.endorsement_option', 'chosen'=>''],['required'],'loanForm' )!!}
        </div>
        <div class="col-sm-4" ng-show="loan.endorsement_option == 'Si' " >
            <label for="">Persona</label>
            <div angucomplete-alt
                 id="person_aval"
                 placeholder="Buscar Cliente"
                 pause="500"
                 selected-object="loan.endorsement"
                 remote-url="app/api/people?q="
                 title-field="customer_name"
                 minlength="3"
                 input-class="form-control form-control-small"
                 match-class="highlight"
                 input-name="person_id_aval"
                 initial-value="loan.endorsement.person">
            </div>
        </div>
        <div class="col-sm-6">
            {!! Field::text('observations','',[
                 'ng-model'=>'loan.observations'],[],'loanForm')!!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-4">
                    {!!Field::select('warranty_option',config('options.option_de'),'',
                    ['required'=>'required',
                       'ng-model'=>'loan.warranty_option', 'chosen'=>''],['required'],'loanForm' )!!}
                </div>
                <div class="col-sm-8">
                    <div class="row" ng-show="loan.warranty_option == 'Si'">
                        <div class="col-sm-4">
                            <div class="checkbox c-checkbox" style="margin-top: 30px">
                                <label>
                                    <input type="checkbox"  value="" ng-model="loan.joyas" />
                                    <span class="fa fa-check"></span>Joyas</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="checkbox c-checkbox" style="margin-top: 30px">
                                <label>
                                    <input type="checkbox"  value=""  ng-model="loan.artefactos"/>
                                    <span class="fa fa-check"></span>Artefactos</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="checkbox c-checkbox" style="margin-top: 30px">
                                <label>
                                    <input type="checkbox"  value="" ng-model="loan.otros"/>
                                    <span class="fa fa-check"></span>Otros</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    {!!Field::select('effective_state',config('options.conditional'),'',
                    ['required'=>'required',
                       'ng-model'=>'loan.effective_state', 'chosen'=>''],[''],'loanForm' )!!}
                </div>
                <div class="col-sm-4" ng-show="loan.effective_state == 1">
                    {!! Field::text('amount','',['placeholder'=>'Ingrese un monto', 'ng-model'=>'loan.effective_warranty'],[],'loanForm')!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <label for="">Referido</label>
                    <div angucomplete-alt
                         id="person_refer"
                         placeholder="Buscar Cliente"
                         pause="500"
                         selected-object="loan.person"
                         remote-url="app/api/people?q="
                         title-field="customer_name"
                         minlength="3"
                         input-class="form-control form-control-small"
                         match-class="highlight"
                         input-name="person_id_refer"
                         initial-value="loan.person">
                    </div>
                </div>
                <div class="col-sm-4">
                    {!!Field::select('feed_option',config('options.option_de'),'',
                    ['required'=>'required',
                       'ng-model'=>'loan.feed_option', 'chosen'=>''],['required'],'loanForm' )!!}
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-4">
                    <label for="calendar">Ciclo de Pago</label>
                    <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </span>
                        <input ng-model="loan.date_start" id="calendar" required ="required" type="text" calendar="calendar" placeholder="DD-MM-YYYY"
                               data-inputmask="'mask': '99-99-9999'" class="form-control" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <br>
                    <a href="#" ng-click="generateSchedule()" ng-hide="loan.state" style="margin-top: 5px" class="btn btn-info">
                        Generar
                    </a>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-12 table-responsive">
                    <table class="table table-bordered table-striped" style="width: auto; margin: 0 auto">
                        <thead>
                        <tr>
                            <th style="width: 25px" class="text-center">#</th>
                            <th style="width: 120px" class="text-center">Vcto</th>
                            <th style="width: 150px" class=" text-center">Cuota</th>
                            <th style="width: 150px" class="text-center">Capital</th>
                            <th style="width: 120px" class="text-center">Interes</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="item in loan.payments track by $index">
                            <td>@{{ $index + 1 }}</td>
                            <td>@{{item.expiration}}</td>
                            <td class="text-right">@{{item.amount|number:2}}</td>
                            <td class="text-right">@{{item.capital|number:2}}</td>
                            <td class="text-right">@{{item.interest|number:2}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <table class="table table-bordered" ng-show="loan.artefactos">
                <tr>
                    <th colspan="4" class="text-center bg-primary-dark">
                        ARTEFACTOS

                        <a class="pull-right" href="#" ng-click="addWarranty(loan.artefactosA)" style="color: green">
                            <i class="fa fa-plus"></i>
                        </a>
                    </th>
                </tr>
                <tr>
                    <th style="width: 30px" class="text-center">#</th>
                    <th class="text-center">Descripción</th>
                    <th class="text-center" style="width: 120px">Tasación</th>
                    <th style="width: 20px" class="text-center"></th>
                </tr>
                <tr ng-repeat="item in loan.artefactosA track by $index">
                    <td>
                        @{{$index+1}}
                    </td>
                    <td>
                        {!! Field::text('description','',['placeholder'=>'Descripción','required'=>'required',
                            'ng-model'=>'item.Description'],['required'],'loanForm', 0)!!}
                    </td>
                    <td>
                        {!! Field::text('reference_price','',['placeholder'=>'0.00','class'=>'text-right','required'=>'required',
            'ng-model'=>'item.reference_price', 'decimals'=>'2', 'ng-change'=>'total()'],['required'],'loanForm',0)!!}
                    </td>
                    <td>
                        <a ng-hide="loan.state" href="#" ng-click="removeWarranty(loan.artefactosA, $index)" class="btn btn-danger btn-small">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <th colspan="2" class="text-center">
                        TOTAL
                    </th>
                    <td class="text-right">
                        {!! Field::text('Tasación','',['placeholder'=>'0.00', 'disabled'=>'disabled','class'=>'text-right',
                'ng-model'=>'artefactos.tasacion', 'decimals'=>'2'],[],'loanForm',0)!!}
                    </td>
                </tr>
            </table>

            <table class="table table-bordered" ng-show="loan.otros">
                <tr>
                    <th colspan="4" class="text-center bg-primary-dark">
                        OTROS

                        <a class="pull-right" href="#" ng-click="addWarranty(loan.otrosA)" style="color: green">
                            <i class="fa fa-plus"></i>
                        </a>
                    </th>
                </tr>
                <tr>
                    <th style="width: 30px" class="text-center">#</th>
                    <th class="text-center">Descripción</th>
                    <th class="text-center" style="width: 120px">Tasación</th>
                    <th style="width: 20px" class="text-center"></th>
                </tr>
                <tr ng-repeat="item in loan.otrosA track by $index">
                    <td>
                        @{{$index+1}}
                    </td>
                    <td>
                        {!! Field::text('description','',['placeholder'=>'Descripción','required'=>'required',
                            'ng-model'=>'item.Description'],['required'],'loanForm', 0)!!}
                    </td>
                    <td>
                        {!! Field::text('reference_price','',['placeholder'=>'0.00','class'=>'text-right','required'=>'required',
            'ng-model'=>'item.reference_price', 'decimals'=>'2', 'ng-change'=>'total()'],['required'],'loanForm',0)!!}
                    </td>
                    <td >
                        <a ng-hide="loan.state" href="#" ng-click="removeWarranty(loan.otrosA,$index)" class="btn btn-danger btn-small">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <th colspan="2" class="text-center">
                        TOTAL
                    </th>
                    <td class="text-right">
                        {!! Field::text('Tasación','',['placeholder'=>'0.00', 'disabled'=>'disabled','class'=>'text-right',
                'ng-model'=>'otros.tasacion', 'decimals'=>'2'],[],'loanForm',0)!!}
                    </td>
                </tr>
            </table>

            <table class="table table-bordered" ng-show="loan.joyas">
                <tr>
                    <th colspan="6" class="text-center bg-primary-dark">
                        JOYAS
                        <a class="pull-right" href="#" ng-click="addWarranty(loan.joyasA)" style="color: green">
                            <i class="fa fa-plus"></i>
                        </a>
                    </th>
                </tr>
                <tr>
                    <th style="width: 30px" class="text-center">#</th>
                    <th class="text-center">Descripción</th>
                    <th class="text-center" style="width: 80px">Peso Bruto (gr) </th>
                    <th class="text-center" style="width: 80px">Peso Neto (gr)</th>
                    <th class="text-center" style="width: 120px">Tasación</th>
                    <th style="width: 20px" class="text-center"></th>
                </tr>
                <tr ng-repeat="item in loan.joyasA track by $index">
                    <td>
                        @{{$index+1}}
                    </td>
                    <td>
                        {!! Field::text('description','',['placeholder'=>'Descripción','required'=>'required',
                                'ng-model'=>'item.Description'],['required'],'loanForm', 0)!!}
                    </td>
                    <td>
                        {!! Field::text('pb','',['placeholder'=>'0.00','class'=>'text-right','required'=>'required',
                'ng-model'=>'item.pb', 'decimals'=>'2', 'ng-change'=>'total()'],['required'],'loanForm',0)!!}
                    </td>
                    <td>
                        {!! Field::text('pn','',['placeholder'=>'0.00','class'=>'text-right','required'=>'required',
                'ng-model'=>'item.pn', 'decimals'=>'2', 'ng-change'=>'total()'],['required'],'loanForm',0)!!}
                    </td>
                    <td>
                        {!! Field::text('reference_price','',['placeholder'=>'0.00','class'=>'text-right','required'=>'required',
                'ng-model'=>'item.reference_price', 'decimals'=>'2', 'ng-change'=>'total()'],['required'],'loanForm',0)!!}
                    </td>
                    <td>
                        <a ng-hide="loan.state" href="#" ng-click="removeWarranty(loan.joyasA, $index)" class="btn btn-danger btn-small">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <th colspan="2" class="text-center">
                        TOTAL
                    </th>
                    <td class="text-right">
                        {!! Field::text('pb','',['placeholder'=>'0.00', 'disabled'=>'disabled','class'=>'text-right',
                'ng-model'=>'joyas.pb', 'decimals'=>'2'],[],'loanForm',0)!!}
                    </td>
                    <td class="text-right">
                        {!! Field::text('pn','',['placeholder'=>'0.00', 'disabled'=>'disabled','class'=>'text-right',
                'ng-model'=>'joyas.pn', 'decimals'=>'2'],[],'loanForm',0)!!}
                    </td>
                    <td class="text-right">
                        {!! Field::text('Tasación','',['placeholder'=>'0.00', 'disabled'=>'disabled','class'=>'text-right',
                'ng-model'=>'joyas.tasacion', 'decimals'=>'2'],[],'loanForm',0)!!}
                    </td>
                </tr>
            </table>
        </div>
    </div>
