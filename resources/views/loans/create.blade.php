<h3>
    Registrar Prestamo
    <small>Registro de Prestamos</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="loanForm" class="form-validate" role="form" ng-submit="loanForm.$valid && createLoan()" novalidate>
        <div class="panel panel-default" ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('loans.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.loans" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </form>
    </div>
</div>