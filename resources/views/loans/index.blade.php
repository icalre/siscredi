<h3>
    Prestamos
    <a ui-sref="app.loans-create" class="pull-right btn btn-info">Registrar Prestamo</a>
    <small>Gestión de Prestamo</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-2">
                        <select chosen ="" name="loan_modalidad" id="loan_modalidad" ng-model="loan.modalidad" ng-change="loadPage(1)" style="width: 100%">
                            <option value="">Modalidad</option>
                            <option value="Convencional">Convencional</option>
                            <option value="Descuento por Planilla">Convenios Descuento por Planilla</option>
                            <option value="Independientes">Independientes</option>
                            <option value="Garantía de Joyas">Garantía de Joyas</option>
                            <option value="Garantía de Electrodomesticos">Garantía de Electrodomesticos</option>
                            <option value="Grupales">Grupales</option>
                            <option value="Compara de Saldos">Compara de Saldos</option>
                        </select>
                    </div>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-2">
                                <select chosen ="" name="loan_state" id="loan_state" ng-model="loan.state" ng-change="loadPage(1)" style="width: 100%">
                                    <option value="">Estado</option>
                                    <option value="Pre-Aprobado">Pre-Aprobado</option>
                                    <option value="Aprobado">Aprobado</option>
                                    <option value="Atrasado">Perdida</option>
                                    <option value="Perdida">C. Pesada</option>
                                    <option value="Perdida Cancelado">C. Pesada Cancelado</option>
                                    <option value="Garantía Ejecutada">Garantía Ejecutada</option>
                                    <option value="Garantía Ejecutada Cancelado">Garantía Ejecutada Cancelado</option>
                                    <option value="Vigente">Vigente</option>
                                    <option value="Aprobado">Aprobado</option>
                                    <option value="Pagado">Pagado</option>
                                    <option value="Anulado">Anulado</option>
                                    <option value="Cancelado">Cancelado</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select chosen ="" data-width="'100%'"  name="loan_warranty_option" id="loan_warranty_option" ng-model="loan.warranty_option" ng-change="loadPage(1)" style="width: 100%">
                                    <option value="">Garantia</option>
                                    <option value="No">No</option>
                                    <option value="Si">Si</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select chosen ="" data-width="'100%'" name="loan_endorsement_option" id="loan_endorsement_option" ng-model="loan.endorsement_option" ng-change="loadPage(1)" style="width: 100%">
                                    <option value="">Aval</option>
                                    <option value="No">No</option>
                                    <option value="Si">Si</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select chosen ="" data-width="'100%'" name="loan_type_payment" id="loan_type_payment" ng-model="loan.type_payment" ng-change="loadPage(1)" style="width: 100%">
                                    <option value="">Tipo</option>
                                    <option value="Fija">Fija</option>
                                    <option value="Libre">Libre</option>
                                </select>
                            </div>
                            <div class="col-sm-1">
                                <div class="form-group">
                                    <input type="text" decimals="2" ng-model="loan.interest"  ng-model-options="{ debounce: 1000 }" ng-change="loadPage(1)"
                                           placeholder="Interes" class="form-control text-right">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group input-group-sm pull-right">
                                    <input type="text" name="table_search"
                                           class="form-control"  placeholder="Buscar"
                                           ng-model="loan.search" ng-change="searchLoan()">
                                    <div class="input-group-btn">
                                        <button class="btn btn-sm btn-ifo disabled"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <select chosen ="" name="customer_type" id="customer_type" ng-model="loan.customer_type" ng-change="loadPage(1)" style="width: 100%">
                            <option value="">Tipo de Cliente</option>
                            <option value="Nuevo">Nuevo</option>
                            <option value="Renovación">Renovación</option>
                        </select>
                    </div>
                    <div class="col-sm-10">
                        <a href="/loans-report?type=@{{loan.modalidad}}&state=@{{loan.state}}&warranty=@{{loan.warranty_option}}&aval=@{{loan.endorsement_option}}&interest@{{loan.interest}}=&modalidad=@{{loan.modalidad }}&customer_type=@{{ loan.customer_type }}" target="_blank" class="pull-right btn btn-success">
                            Excel
                        </a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <!-- START table-responsive-->
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center" style="width: 30px">Código</th>
                            <th class="text-center">Cliente</th>
                            <th class="text-center">Asesor</th>
                            <th class="text-center" style="width: 100px">Monto</th>
                            <th class="text-center" style="width: 50px">Garantía</th>
                            <th class="text-center" style="width: 50px">Aval</th>
                            <th class="text-center" style="width: 70px">Estado</th>
                            <th style="width: 220px"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="row in loans track by $index" ng-class ="{'text-danger' : row.state == 'Anulado'
                        || row.state == 'Perdida' || row.state == 'Garantía Ejecutada'} ">
                            <td class="text-center">@{{row.code}}</td>
                            <td>@{{row.customer.customer.customer_name}}</td>
                            <td>@{{row.employee.person.customer_name}}</td>
                            <td class="text-right">@{{row.amount}}</td>
                            <td class="text-center">@{{row.warranty_option}}</td>
                            <td class="text-center">@{{row.endorsement_option}}</td>
                            <td class="text-center">@{{row.state_string}}</td>

                            <td class="text-center">
                                <a ui-sref="app.loans-show({id:row.id})" class="mb-sm btn btn-info">
                                    Visualizar
                                </a>
                                <a href="#" ng-click="dialogOption(row)" class="mb-sm btn btn-warning">
                                    Operaciones
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- END table-responsive-->
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="pagination pagination-sm pull-right" ng-show="paginate">
                            <li>
                                <a href="#" ng-show="paginate.current_page > 1" ng-click="previousPage()">«</a>
                            </li>
                            <li ng-repeat="i in paginate.pages track by $index " ng-hide="$index+1 < paginate.current_page-4 || $index+1 > paginate.current_page + 9" ng-class="{true:'active'}[paginate.current_page == $index+1]">
                                <a href="#" ng-click="loadPage($index+1)">
                                    @{{$index+1}}
                                </a>
                            </li>
                            <li>
                                <a href="#" ng-show="paginate.current_page < paginate.last_page" ng-click="nextPage()">»</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/ng-template" id="dialogWithNestedConfirmDialogId">
    <div class="ngdialog-message text-center">
        <h3 class="mt0">Elija Una Opcion</h3>
        <a href="#" ng-click="annulledLoan()" class="btn btn-danger" ng-hide="loan_selected.state == 'Cancelado'
                                                    || loan_selected.state == 'Perdida'
                                                    || loan_selected.state == 'Garantía Ejecutada'
                                                    || loan_selected.state == 'Perdida Cancelado'
                                                    || loan_selected.state == 'Garantía Ejecutada Cancelado'
                                                    || loan_selected.state == 'Anulado'
          ">Anular
        </a><br>
        @if(in_array('app/a-loan', $sub_modules))
            <a href="#" ng-click="acceptLoan()" class="btn btn-success" ng-hide="loan_selected.state != 'Pre-Aprobado'">
                Aprobar
            </a><br>
            <a href="#" ng-click="okLoan()" class="btn btn-success" ng-show="loan_selected.state == 'Atrasado'">
                Vigente
            </a><br>
            <a href="#" ng-click="losePrevLoan()" class="btn btn-info" ng-hide="
                loan_selected.state == 'Cancelado'
                || loan_selected.state == 'Perdida'
                || loan_selected.state == 'Garantía Ejecutada'
                || loan_selected.state == 'Perdida Cancelado'
                || loan_selected.state == 'Garantía Ejecutada Cancelado'
                || loan_selected.state == 'Anulado'
                || loan_selected.state == 'Atrasado'
            " style="background: #ae81ff">
                Perdida
            </a><br><br>
            <a href="#" ng-click="loseLoan()" class="btn btn-warning" ng-hide="
                loan_selected.state == 'Cancelado'
                || loan_selected.state == 'Perdida'
                || loan_selected.state == 'Garantía Ejecutada'
                || loan_selected.state == 'Perdida Cancelado'
                || loan_selected.state == 'Garantía Ejecutada Cancelado'
                || loan_selected.state == 'Anulado'
            ">
                Catera Pesada
            </a><br><br>
            <a href="#" ng-click="totalLoseLoan()" class="btn btn-warning" ng-hide="
                loan_selected.state == 'Cancelado'
                || loan_selected.state == 'Garantía Ejecutada'
                || loan_selected.state == 'Perdida Cancelado'
                || loan_selected.state == 'Garantía Ejecutada Cancelado'
                || loan_selected.state == 'Anulado'
            " style="background: #00a8c6">
                Perdida Total
            </a><br>
            <a href="#" ng-click="warrantyLoan()" class="btn btn-default bg-danger-light" ng-hide="
            loan_selected.state == 'Cancelado'
            || loan_selected.state == 'Perdida'
            || loan_selected.state == 'Garantía Ejecutada'
            || loan_selected.state == 'Perdida Cancelado'
            || loan_selected.state == 'Garantía Ejecutada Cancelado'
            || loan_selected.state == 'Anulado'
            || loan_selected.warranty_option == 'No'
        ">Garantía Ejecutada</a> <br>
        @endif
        <a href="app/loans/print-schedule?loan-id=@{{loan_selected.id}}" ng-click="printSchedule()" target="_blank" class="btn btn-info">
            Imprimir Cronograma
        </a>
    </div>
</script>