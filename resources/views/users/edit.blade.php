<h3>
    Editar Usuario
    <small>Edición de Usuarios</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="userForm" class="form-validate" ng-enter="preventEnterForm()" role="form" ng-submit="userForm.$valid && editUser()" novalidate>
        <div class="panel panel-default " ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('users.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.users" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </form>
    </div>
</div>