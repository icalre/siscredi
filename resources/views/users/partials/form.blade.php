    {!!Form::token()!!}
    <div class="row">
        <div class="col-sm-4">
            {!! Field::text('name','',['placeholder'=>'Ingrese un nombre','required'=>'required',
                'ng-model'=>'user.name'],['required'],'userForm')!!}
        </div>
        <div class="col-sm-7">
            <label for="person_value">
                Persona
            </label>
            <div angucomplete-alt
                 id="person"
                 placeholder="Buscar Persona"
                 pause="500"
                 selected-object="user.person"
                 remote-url="app/api/people?q="
                 title-field="full_name"
                 minlength="3"
                 input-class="form-control form-control-small"
                 match-class="highlight"
                 field-required="true",
                 input-name="person_id",
                 initial-value="user.person">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            {!! Field::password('password',['placeholder'=>'Ingrese una contraseña',
                'ng-model'=>'user.password'],[],'userForm') !!}
        </div>
        <div class="col-sm-4">
            {!! Field::password('password_confirmation',['placeholder'=>'Repite Contraseña',
                'ng-model'=>'user.password_confirmation'],[],'userForm') !!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">

            {!!Field::select('profile_id',[],'',['required'=>'required',
            'chosen'=>'',
            'data-placeholder'=>"Selecciona un Distrito",
            'no-results-text'=>"'No encontrado'",
            'ng-model'=>"user.profile",
            'width'=>"'100%'",
             'class'=>"chosen-select input-md",
            'ng-options'=>'item.name for item in profiles track by item.id'],
            ['required'],'userForm')!!}

        </div>
        <div class="col-sm-3">

            {!!Field::select('state',[],'',['required'=>'required',
            'chosen'=>'',
            'data-placeholder'=>"Selecciona un Distrito",
            'no-results-text'=>"'No encontrado'",
            'ng-model'=>"user.state",
            'width'=>"'100%'",
             'class'=>"chosen-select input-md",
            'ng-options'=>'item.name for item in states track by item.id'],
            ['required'],'userForm')!!}
        </div>
        <div class="col-sm-3">
            {!! Field::text('color','',['placeholder'=>'Color',
                'ng-model'=>'user.color', 'colorpicker'=>""],[],'userForm')!!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            {!!Field::select('administrative_option',[],'',['required'=>'required',
            'chosen'=>'',
            'data-placeholder'=>"Selecciona un Distrito",
            'no-results-text'=>"'No encontrado'",
            'ng-model'=>"user.administrative_option",
            'width'=>"'100%'",
             'class'=>"chosen-select input-md",
            'ng-options'=>'item.name for item in options track by item.id'],
            ['required'],'userForm')!!}

        </div>
        <div class="col-sm-5" ng-show="user.administrative_option == 1">
            {!! Field::password('code',['placeholder'=>'Ingrese un codigo',
                'ng-model'=>'user.code'],[],'userForm') !!}
        </div>
    </div>
