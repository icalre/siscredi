<div class="row" ng-show="panel == 'Flujo' ">
    <div class="col-sm-10">
        <div class="panel panel-info" ng-class="{'whirl standard' : loading}">
            <div class="panel-heading" uib-popover="Flujo de Caja en Linea a partir del 18-09-2016" popover-trigger="mouseenter">
                <h4>
                    Flujo de Caja en Linea
                    <a  class="btn btn-warning pull-right" ng-click="setPanel('Principal')" style="color: white" >
                        <i class="icon-action-undo"></i> Volver
                    </a>
                </h4>
            </div>
            <div class="panel-body" ng-show="report_response">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th class="bg-primary-light text-center" colspan="2" >
                                Flujo de Caja al <now style="font-size: 14px; font-weight: bold" format="d-MM-yyyy h:mm a" class="h2 mt0"></now>
                            </th>
                        </tr>
                        <tr class="bg-primary">
                            <th>Saldo Inicial (+) </th>
                            <td class="text-right" >@{{report.cash_desk_detail.initial_amount |number:2}} </td>
                        </tr>
                        <tr class="bg-primary">
                            <th>Pago de Cuotas (+) </th>
                            <td class="text-right" >@{{report.cash_desk_detail.total_pay |number:2}} </td>
                        </tr>
                        <tr class="bg-primary">
                            <th>
                                    Ingresos (+)
                            </th>
                            <td class="text-right">@{{report.cash_desk_detail.income_amount |number:2}} </td>
                        </tr>
                        <tr class="bg-warning">
                            <th>
                                    Desembolsos (-)
                            </th>
                            <td class="text-right">@{{report.cash_desk_detail.desembolsos |number:2}} </td>
                        </tr>
                        <tr class="bg-warning">
                            <th>
                                    Gastos (-)
                            </th>
                            <td class="text-right">@{{report.cash_desk_detail.expenses_amount |number:2}} </td>
                        </tr>
                        <tr class="bg-warning">
                            <th>
                                    Egresos (-)
                            </th>
                            <td class="text-right">@{{report.cash_desk_detail.expenses_amount_g |number:2}} </td>
                        </tr>
                        <tr class="bg-success">
                            <th>Saldo Final</th>
                            <td class="text-right">S/. @{{ report.cash_desk_detail.total_amount |number:2}} </td>
                        </tr>
                    </table>
                    <br>
                    <table class="table table-bordered">
                        <tr>
                            <th class="text-left">
                                Efectivo
                            </th>
                            <th>
                                S/. @{{ report.cash_desk_detail.efectivo |number:2}}
                            </th>
                        </tr>
                        <tr>
                            <th class="text-left">
                                Cuenta Corriente
                            </th>
                            <th>
                                S/. @{{ report.cash_desk_detail.cuenta_corriente |number:2}}
                            </th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>