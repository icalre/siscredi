<div class="row" ng-show="panel == 'Atrasadas' ">
    <div class="col-sm-10">
        <div class="panel panel-info" ng-class="{'whirl standard' : loading}">
            <div class="panel-heading">
                <h4>
                    Reporte Cuotas Atrasadas
                    <a  class="btn btn-warning pull-right" ng-click="setPanel('Principal')" style="color: white" >
                        <i class="icon-action-undo"></i> Volver
                    </a>

                    <a href="app/reports/print-atrasadas"
                       target="_blank"  class="btn btn-warning pull-right" style="margin-right: 10px; color: white" >
                        <i class="icon-printer"></i> Imprimir
                    </a>
                </h4>
            </div>
            <div class="panel-body" ng-show="report_response">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th style="width: 50px" class="text-center"> #</th>
                            <th style="width: 50px" class="text-center">
                                Codigo
                            </th>
                            <th class="text-center">Cliente</th>
                            <th class="text-center">Asesor</th>
                            <th class="text-center" style="width: 100px">Cuota</th>
                            <th class="text-center" style="width: 100px" >F. Vcto</th>
                            <th class="text-center" style="width: 60px" >Días</th>
                        </tr>
                        <tr ng-repeat="row in payments track by $index">
                            <td class="text-center">
                                @{{ $index + 1 }}
                            </td>
                            <td>
                                <a href="app/loans-show/@{{row.loan.id}}" target="_blank">
                                    @{{row.loan.code}}
                                </a>
                            </td>
                            <td>
                                @{{ row.loan.customer.customer.customer_name}}
                            </td>
                            <td>
                                @{{ row.loan.employee.person.customer_name }}
                            </td>
                            <td class="text-right">
                                @{{ row.amount }}
                            </td>
                            <td>
                                @{{row.expiration}}
                            </td>
                            <td class="text-center text-danger">
                                @{{row.days}}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>