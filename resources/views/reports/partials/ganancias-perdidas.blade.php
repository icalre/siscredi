<div class="row" ng-show="panel == 'GananciaP' ">
    <div class="col-sm-10">
        <div class="panel panel-info" ng-class="{'whirl standard' : loading}">
            <div class="panel-heading" uib-popover="Estado de ganancias y perdidas en linea a partir del 18-09-2016" popover-trigger="mouseenter">
                <h4>
                    Ganancias y Perdidas en Linea del @{{ date_start }} al @{{date_end}}
                    <a  class="btn btn-warning pull-right" ng-click="setPanel('Principal')" style="color: white" >
                        <i class="icon-action-undo"></i> Volver
                    </a>
                </h4>
            </div>
            <div class="panel-body" ng-show="report_response">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th>Intereses Ganados (+) </th>
                            <td class="text-right" >@{{report.interest |number:2}} </td>
                        </tr>
                        <tr>
                            <th>
                                <a href="app/reports/print-expenses?expenses=1&date-start=@{{date_start}}&date-end=@{{date_end}}" target="_blank">
                                    Gastos (-)
                                </a>
                            </th>
                            <td class="text-right">@{{report.expenses_amount |number:2}} </td>
                        </tr>
                        <tr>
                            <th>Utilidad</th>
                            <td class="text-right">S/. @{{report.interest - report.expenses_amount |number:2}} </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>