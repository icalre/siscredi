<div class="row" ng-show="panel == 'Garantia' ">
    <div class="col-sm-10">
        <div class="panel panel-info" ng-class="{'whirl standard' : loading}">
            <div class="panel-heading">
                <h4>
                    Reporte Prestamos Vigentes con Garantía
                    <a  class="btn btn-warning pull-right" ng-click="setPanel('Principal')" style="color: white" >
                        <i class="icon-action-undo"></i> Volver
                    </a>

                    <a href="app/reports/print-garantia-v"
                       target="_blank"  class="btn btn-warning pull-right" ng-click="setPanel('Principal')" style="margin-right: 10px; color: white" >
                        <i class="icon-printer"></i> Imprimir
                    </a>
                </h4>
            </div>
            <div class="panel-body" ng-show="report_response">
                <div class="table-responsive">
                    <table class="table table-bordered" ng-show="loans.length > 0">
                        <tr>
                            <th style="width: 50px" class="text-center"> #</th>
                            <th style="width: 100px" class="text-center">
                                Codigo
                            </th>
                            <th class="text-center">Cliente</th>
                            <th class="text-center" >Asesor</th>
                            <th class="text-center" >Garantías</th>
                            <th class="text-center" >Monto</th>
                        </tr>
                        <tr ng-repeat="row in loans track by $index">
                            <td class="text-center">
                                @{{ $index + 1 }}
                            </td>
                            <td>
                                <a href="app/loans-show/@{{row.id}}" target="_blank">
                                    @{{row.code}}
                                </a>
                            </td>
                            <td>
                                @{{ row.customer.customer.customer_name}}
                            </td>
                            <td>
                                @{{ row.employee.person.customer_name }}
                            </td>
                            <td class="text-right">
                                <table class="table table-bordered" ng-show="row.artefactosA.length > 0">
                                    <tr>
                                        <th class="text-center bg-primary" colspan="3">
                                            Artefactos
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center" >Descripción</th>
                                        <th class="text-center" style="width: 100px">Tasación</th>
                                    </tr>
                                    <tr ng-repeat="item in row.artefactosA track by $index"  ng-class ="{'text-green' : item.state == 'Rescatado'}">
                                        <td>@{{$index + 1}}</td>
                                        <td>
                                            @{{item.Description}}
                                        </td>
                                        <td class="text-right">
                                            @{{item.reference_price}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Total</th>
                                        <th class="text-right">
                                            @{{artefactos.tasacion | number:2}}
                                        </th>
                                    </tr>
                                </table>
                                <br>
                                <table class="table table-bordered" ng-show="row.otrosA.length > 0">
                                    <tr>
                                        <th class="text-center bg-primary" colspan="3">
                                            Otros
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">Descripción</th>
                                        <th class="text-center" style="width: 100px">Tasación</th>
                                    </tr>
                                    <tr ng-repeat="item in row.otrosA track by $index"  ng-class ="{'text-green' : item.state == 'Rescatado'}">
                                        <td>@{{$index + 1}}</td>
                                        <td>
                                            @{{item.Description}}
                                        </td>
                                        <td class="text-right" >
                                            @{{item.reference_price}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Total</th>
                                        <th class="text-right">
                                            @{{otros.tasacion | number:2}}
                                        </th>
                                    </tr>
                                </table>
                                <br>
                                <table class="table table-bordered" ng-show="row.joyasA.length > 0">
                                    <tr>
                                        <th class="text-center bg-primary" colspan="5">
                                            Joyas
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">Descripción</th>
                                        <th class="text-center" style="width: 60px">Peso Bruto (gr)</th>
                                        <th class="text-center" style="width: 60px">Peso Neto (gr)</th>
                                        <th class="text-center" style="width: 100px">Tasación</th>
                                    </tr>
                                    <tr ng-repeat="item in row.joyasA track by $index"  ng-class ="{'text-green' : item.state == 'Rescatado'}">
                                        <td>@{{$index + 1}}</td>
                                        <td>
                                            @{{item.Description}}
                                        </td>
                                        <td class="text-right">
                                            @{{item.pb | number:2}}
                                        </td>
                                        <td class="text-right">
                                            @{{item.pn | number:2}}
                                        </td>
                                        <td class="text-right">
                                            @{{item.reference_price| number:2}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Total</th>
                                        <th class="text-right">
                                            @{{joyas.pb}}
                                        </th>
                                        <th class="text-right">
                                            @{{joyas.pn}}
                                        </th>
                                        <th class="text-right">
                                            @{{joyas.tasacion}}
                                        </th>
                                    </tr>
                                </table>
                            </td>
                            <td class="text-right">
                                @{{row.amount|number:2}}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>