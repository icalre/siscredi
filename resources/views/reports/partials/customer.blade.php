<div class="row" ng-show="panel == 'Customers' ">
    <div class="col-sm-10">
        <div class="panel panel-info" ng-class="{'whirl standard' : loading}">
            <div class="panel-heading">
                <h4>
                    Clientes Prestamos Vigentes
                    <a  class="btn btn-warning pull-right" ng-click="setPanel('Principal')" style="color: white" >
                        <i class="icon-action-undo"></i> Volver
                    </a>

                    <a href="app/reports/print-customers?date-start=@{{date_start}}&date-end=@{{date_end}}"
                       target="_blank"  class="btn btn-warning pull-right" style="margin-right: 10px; color: white" >
                        <i class="icon-printer"></i> Imprimir
                    </a>
                </h4>
            </div>
            <div class="panel-body" ng-show="report_response">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th style="width: 50px" class="text-center"> #</th>
                            <th style="width: 100px" class="text-center">
                                Codigo
                            </th>
                            <th class="text-center">Cliente</th>
                            <th class="text-center">Asesor</th>
                        </tr>
                        <tr ng-repeat="row in customers track by $index">
                            <td class="text-center">
                                @{{ $index + 1 }}
                            </td>
                            <td>
                                <a>
                                    @{{row.customer.code}}
                                </a>
                            </td>
                            <td>
                                @{{ row.customer.customer.customer_name}}
                            </td>
                            <td>
                                @{{ row.employee.person.customer_name }}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>