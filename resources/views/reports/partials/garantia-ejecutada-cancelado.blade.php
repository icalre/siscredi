<div class="row" ng-show="panel == 'Garantia Ejecutada Cancelado' ">
    <div class="col-sm-12">
        <div class="panel panel-info" ng-class="{'whirl standard' : loading}">
            <div class="panel-heading">
                <h4>
                    Reporte Prestamos en Garantia Ejecutada Cancelado
                    <a  class="btn btn-warning pull-right" ng-click="setPanel('Principal')" style="color: white" >
                        <i class="icon-action-undo"></i> Volver
                    </a>

                    <a href="app/reports/print-grantia-ejecutada-cancelado"
                       target="_blank"  class="btn btn-warning pull-right" style="margin-right: 10px; color: white" >
                        <i class="icon-printer"></i> Imprimir
                    </a>
                </h4>
            </div>
            <div class="panel-body" ng-show="report_response">
                <div class="table-responsive">
                    <table class="table table-bordered" ng-show="loans.length > 0">
                        <tr>
                            <th style="width: 50px" class="text-center"> #</th>
                            <th style="width: 100px" class="text-center">
                                Codigo
                            </th>
                            <th class="text-center">Cliente</th>
                            <th class="text-center" >Asesor</th>
                            <th class="text-center" >Prestamo</th>
                            <th class="text-center" >Capital Perdida</th>
                            <th class="text-center" >Capital Pendiente</th>
                            <th class="text-center" >Fecha</th>
                        </tr>
                        <tr ng-repeat="row in loans track by $index">
                            <td class="text-center">
                                @{{ $index + 1 }}
                            </td>
                            <td>
                                <a href="app/loans-show/@{{row.id}}" target="_blank">
                                    @{{row.code}}
                                </a>
                            </td>
                            <td>
                                @{{ row.customer.customer.customer_name}}
                            </td>
                            <td>
                                @{{ row.employee.person.customer_name }}
                            </td>
                            <td class="text-right">
                                @{{row.amount|number:2}}
                            </td>
                            <td class="text-right">
                                @{{row.lose_capital|number:2}}
                            </td>
                            <td class="text-right">
                                @{{0|number:2}}
                            </td>
                            <td class="text-center">
                                @{{row.date_end}}
                            </td>
                        </tr>
                        <tr>
                            <th colspan="4">
                                Total
                            </th>
                            <th class="text-right">
                                @{{total_loan_amount | number:2}}
                            </th>
                            <th class="text-right">
                                @{{total_capital_lose | number:2}}
                            </th>
                            <th class="text-right">
                                @{{0| number:2}}
                            </th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>