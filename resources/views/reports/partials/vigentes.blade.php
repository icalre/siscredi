<div class="row" ng-show="panel == 'Vigentes' ">
    <div class="col-sm-10">
        <div class="panel panel-info" ng-class="{'whirl standard' : loading}">
            <div class="panel-heading">
                <h4>
                    Reporte Prestamos Vigentes
                    <a  class="btn btn-warning pull-right"  style="color: white" ng-click="setPanel('Principal')" >
                        <i class="icon-action-undo"></i> Volver
                    </a>

                    <a href="app/reports/print-vigentes?date-start=@{{date_start}}&date-end=@{{date_end}}"
                       target="_blank"  class="btn btn-warning pull-right" ng-click="setPanel('Principal')" style="margin-right: 10px; color: white" >
                        <i class="icon-printer"></i> Imprimir
                    </a>
                </h4>
            </div>
            <div class="panel-body" ng-show="report_response">
                <div class="table-responsive">
                    <table class="table table-bordered" ng-show="loans.length > 0">
                        <tr>
                            <th style="width: 50px" class="text-center"> #</th>
                            <th style="width: 100px" class="text-center">
                                Codigo
                            </th>
                            <th class="text-center">Cliente</th>
                            <th class="text-center" >Asesor</th>
                            <th class="text-center" >Monto</th>
                        </tr>
                        <tr ng-repeat="row in loans track by $index">
                            <td class="text-center">
                                @{{ $index + 1 }}
                            </td>
                            <td>
                                <a href="app/loans-show/@{{row.id}}" target="_blank">
                                    @{{row.code}}
                                </a>
                            </td>
                            <td>
                                @{{ row.customer.customer.customer_name}}
                            </td>
                            <td>
                                @{{ row.employee.person.customer_name }}
                            </td>
                            <td class="text-right">
                                @{{ row.amount }}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>