<h3>
    Reportes
</h3>

<div class="row" ng-show="panel == 'Principal' ">
    <div class="col-sm-10">
        <div class="panel panel-info" ng-class="{'whirl standard' : loading}">
            <div class="panel-heading">
                <h4>
                    Reporte General / @{{ date_start }} - @{{date_end}}
                    <a  class="pull-right" ng-click="clearRepo()" style="color: white" >
                        <i class="icon-action-undo"></i> Volver
                    </a>
                </h4>
            </div>
            <div class="panel-body" ng-hide="report_response">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fecha Inicial</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" id="date1" calendar="calendar" class="form-control"
                                       ng-model="date_start" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fecha Final</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" id="date2" calendar="calendar" class="form-control"
                                       ng-model="date_end" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <br/>
                        <a href="#" class="btn btn-info" ng-click="getReport()">Buscar</a>
                    </div>
                </div>
            </div>
            <div class="panel-body" ng-show="report_response">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th colspan="2" class="text-center">Periodo</th>
                        </tr>
                        <tr class="bg-primary">
                            <th>Capital Cancelado</th>
                            <td class="text-right" width="150px">@{{report.capital|number:2}}</td>
                        </tr>
                        <tr class="bg-primary">
                            <th>Capital Desembolsado</th>
                            <td class="text-right" width="150px">@{{report.desembolsos|number:2}}</td>
                        </tr>

                        <tr class="bg-primary">
                            <th>Interes Ganado</th>
                            <td class="text-right">@{{report.interest|number:2}}</td>
                        </tr>
                        <tr>
                            <th>
                                <a href="#" ng-click="setLoans('/app/reports/print-cancelados','Cancelados')">Prestamos Cancelados</a>
                            </th>
                            <td class="text-right" width="150px">@{{report.loans_cancelled_count}}</td>
                        </tr>
                        <tr>
                            <th>
                                <a href="#" ng-click="setLoans('/app/reports/print-anulados','Anulados')">Prestamos Anulados</a>
                            </th>
                            <td class="text-right" width="150px">@{{report.loans_annulment_count}}</td>
                        </tr>
                        <tr>
                            <th>
                                <a href="/print-lose-loans?date-start=@{{date_start}}&date-end=@{{date_end}}" target="_blank" > Prestamos Perdida</a>
                            </th>
                            <td class="text-right" width="150px">@{{report.loans_lose_period |number:2}}</td>
                        </tr>
                        <tr>
                            <th>
                                <a href="#" ng-click="setPayments('/app/reports/print-canceladas','Canceladas')">Pago de Cuotas</a>
                            </th>
                            <td class="text-right" width="150px">@{{report.payments_payed_count}}</td>
                        </tr>
                        <tr>
                            <th>
                                <a href="#" ng-click="setPayments('/app/reports/print-canceladas-sunat','Canceladas SUNAT')">Cuotas Canceladas SUNAT</a>
                            </th>
                            <td class="text-right" width="150px">@{{report.payments_payed_sunat_count}}</td>
                        </tr>
                        <tr>
                            <th>
                                <a href="/app/reports/print-incomes?incomes=1&date-start=@{{date_start}}&date-end=@{{date_end}}" target="_blank">
                                    Ingresos
                                </a>
                            </th>
                            <td class="text-right">@{{report.income_amount |number:2}} </td>
                        </tr>
                        <tr>
                            <th>
                                <a href="/app/reports/print-expenses?expenses=1&date-start=@{{date_start}}&date-end=@{{date_end}}" target="_blank"> Gastos Generales </a>
                            </th>
                            <td class="text-right" width="150px">@{{report.expenses_amount |number:2}}</td>
                        </tr>
                        <tr>
                            <th>
                                <a href="/app/reports/print-expenses-g?expenses=1&date-start=@{{date_start}}&date-end=@{{date_end}}" target="_blank">
                                    Gastos Activos e Inversión
                                </a>
                            </th>
                            <td class="text-right">@{{report.expenses_amount_g |number:2}} </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <a href="/disbursements-report?date-start=@{{date_start}}&date-end=@{{date_end}}" target="_blank">
                                    Desembolsos
                                </a>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <a href="/cash-desk-detail-report?date-start=@{{date_start}}&date-end=@{{date_end}}" target="_blank">
                                    Reporte de Cajas
                                </a>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <a href="/consolidate-report?expenses=1&date-start=@{{date_start}}&date-end=@{{date_end}}" target="_blank">
                                    Consolidado de pagos
                                </a>
                            </th>
                        </tr>
                    </table>
                    <br/>
                    <table class="table table-bordered">
                        <tr>
                            <th colspan="2" class="text-center">General</th>
                        </tr>
                        <tr class="bg-primary">
                            <th>Capital Vigente</th>
                            <td class="text-right">@{{report.capital_pending|number:2}}</td>
                        </tr>

                        <tr>
                            <th>
                                <a href="#" ng-click="setLoans('/app/reports/print-vigentes','Vigentes')">Prestamos Vigentes</a>
                            </th>
                            <td class="text-right" width="150px">@{{report.loans_process_count}}</td>
                        </tr>
                        <tr>
                            <th>
                                <a href="#" ng-click="setCustomers('/app/reports/print-customers')">Clientes / Prestamos Vigentes</a>
                            </th>
                            <td class="text-right" width="150px">@{{report.loan_customers_count}}</td>
                        </tr>

                        <tr>
                            <th>
                                <a href="#" ng-click="setLoans('/app/reports/print-garantia-v','Garantia')">Prestamos Vigentes con Garantía</a>
                            </th>
                            <td class="text-right" width="150px">@{{report.loans_warranty_count}}</td>
                        </tr>
                        <tr>
                            <th>
                                <a href="#" ng-click="setLoans('/app/reports/print-perdida','Perdida')">Prestamos Perdida</a>
                            </th>
                            <td class="text-right" width="150px">@{{report.loans_lose_count}}</td>
                        </tr>
                        <tr>
                            <th>
                                <a href="#" ng-click="setLoans('/app/reports/print-perdida-cancelado','Perdida Cancelado')">Prestamos Perdida Cancelados</a>
                            </th>
                            <td class="text-right" width="150px">@{{report.loans_lose_cancelled_count}}</td>
                        </tr>
                        <tr>
                            <th>
                                <a href="#" ng-click="setLoans('/app/reports/print-garantia-ejecutada','Garantia Ejecutada')">Prestamos Garantia Ejecutada</a>
                            </th>
                            <td class="text-right" width="150px">@{{report.loans_execute_warranty_count}}</td>
                        </tr>
                        <tr>
                            <th>
                                <a href="#" ng-click="setLoans('/app/reports/print-grantia-ejecutada-cancelado','Garantia Ejecutada Cancelado')">Prestamos Garantia Ejecutada Cancelados</a>
                            </th>
                            <td class="text-right" width="150px">@{{report.loans_execute_warranty_cancelled_count}}</td>
                        </tr>
                        <tr>
                            <th>
                                <a href="#" ng-click="setPayments('/app/reports/print-atrasadas','Atrasadas')">Cuotas Atrasadas</a>
                            </th>
                            <td class="text-right" width="150px">@{{report.payments_expired_count}}</td>
                        </tr>
                        <tr>
                            <th>
                                <a href="#" ng-click="setPanel('Flujo')" > Flujo de Caja en Linea</a>
                            </th>
                            <td class="text-right" width="150px">
                                @{{report.cash_desk_detail.total_amount |number:2}}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@include('reports.partials.cancelados')
@include('reports.partials.anulados')
@include('reports.partials.p-canceladas')
@include('reports.partials.p-canceladas-sunat')
@include('reports.partials.ganancias-perdidas')
@include('reports.partials.flujo')
@include('reports.partials.vigentes')
@include('reports.partials.customer')
@include('reports.partials.perdida')
@include('reports.partials.garantia')
@include('reports.partials.perdida-cancelado')
@include('reports.partials.garantia-ejecutada')
@include('reports.partials.garantia-ejecutada-cancelado')
@include('reports.partials.p-atrasadas')
