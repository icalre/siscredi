    {!!Form::token()!!}
    <div class="row">
        <div class="col-sm-6">
            {!! Field::text('razon_social','',['placeholder'=>'Ingrese su Razón Social','required'=>'required',
                'ng-model'=>'company.razon_social'],['required'],'companyForm')!!}
        </div>
        <div class="col-sm-6">
            {!! Field::text('nombre_comercial', '',['placeholder'=>'Ingrese su Nombre Comercial','required'=>'required',
                'ng-model'=>'company.nombre_comercial'],['required'],'companyForm') !!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            {!! Field::text('ruc','',['placeholder'=>'Ingrese su RUC','required'=>'required',
                'ng-model'=>'company.ruc'],['required'],'companyForm')!!}
        </div>
        <div class="col-sm-7">
            {!! Field::text('address', '',['placeholder'=>'Ingrese su dirección','required'=>'required',
                'ng-model'=>'company.address'],['required'],'companyForm') !!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            {!!Field::select('department_id',[],'',['required'=>'required',
             'chosen'=>'',
             'data-placeholder'=>"Selecciona un Departmamento",
             'no-results-text'=>"'No encontrado'",
             'ng-model'=>"company.department",
             'width'=>"'100%'",
              'class'=>"chosen-select input-md",
             'ng-options'=>"item.departamento for item in departments track by item.id"],
             ['required'],'companyForm')!!}
        </div>
        <div class="col-sm-4">

            {!!Field::select('province_id',[],'',['required'=>'required',
             'chosen'=>'',
             'data-placeholder'=>"Selecciona una Provincia",
             'no-results-text'=>"'No encontrado'",
             'ng-model'=>"company.province",
             'width'=>"'100%'",
              'class'=>"chosen-select input-md",
             'ng-options'=>"item.provincia for item in company.department.provinces track by item.id"],
             ['required'],'companyForm')!!}
        </div>
        <div class="col-sm-4">

            {!!Field::select('district_id',[],'',['required'=>'required',
             'chosen'=>'',
             'data-placeholder'=>"Selecciona un Distrito",
             'no-results-text'=>"'No encontrado'",
             'ng-model'=>"company.district",
             'width'=>"'100%'",
              'class'=>"chosen-select input-md",
             'ng-options'=>"item.distrito for item in company.province.districts track by item.id"],
             ['required'],'companyForm')!!}
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-4">
            {!! Field::text('telephone', '',['placeholder'=>'Ingrese su telefono',
                'ng-model'=>'company.telephone'],[],'companyForm') !!}
        </div>
        <div class="col-sm-7">
            {!! Field::email('email','',['placeholder'=>'Ingrese su email',
                'ng-model'=>'company.email'],['email'],'companyForm')!!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8">
            {!! Field::text('website','',['placeholder'=>'Ingrese su Sitio Web',
                'ng-model'=>'company.website'],[],'companyForm')!!}
        </div>
        <div class="col-sm-2">
            {!!Field::select('contract_option',config('options.states'),'',
            ['required'=>'required',
               'ng-model'=>'company.contract_option', 'chosen'=>''],['required'],'companyForm' )!!}
        </div>
        <div class="col-sm-2" ng-show="company.contract_option == 1">
            {!! Field::text('contract_number','',['placeholder'=>'Ingrese su Sitio Web',
                'ng-model'=>'company.contract_number'],[],'companyForm')!!}
        </div>
    </div>
