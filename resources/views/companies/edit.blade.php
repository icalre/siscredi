<h3>
    Editar Compañia
    <small>Edición de Compañia</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="companyForm" class="form-validate" ng-enter="preventEnterForm()" role="form" ng-submit="companyForm.$valid && editCompany()" novalidate>
        <div class="panel panel-default " ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('companies.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.companies" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </form>
    </div>
</div>