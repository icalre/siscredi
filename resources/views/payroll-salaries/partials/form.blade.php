    {!!Form::token()!!}
    <div class="row">
        <div class="col-sm-2">
            {!!Field::select('year_id',[],'',[
              'chosen'=>'chosen',
              'required'=>'required',
              'data-placeholder'=>"Selecciona un año",
              'no-results-text'=>"'No encontrado'",
              'ng-model'=>"payrollSalary.year",
              'ng-options'=>"item.name for item in years track by item.id"],
              ['required'],'payrollSalaryForm')!!}
        </div>
        <div class="col-sm-3">
            {!!Field::select('month_id',[],'',[
              'chosen'=>'chosen',
              'required'=>'required',
              'data-placeholder'=>"Selecciona un mes",
              'no-results-text'=>"'No encontrado'",
              'ng-model'=>"payrollSalary.month",
              'ng-options'=>"item.name for item in months track by item.id"],
              ['required'],'payrollSalaryForm')!!}
        </div>
    </div>