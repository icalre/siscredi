<h3>
    Registrar Planilla
    <small>Registro de Planilla</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="payrollSalaryForm" class="form-validate" role="form" ng-submit="payrollSalaryForm.$valid && createPayrollSalary()" novalidate>
        <div class="panel panel-default" ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('payroll-salaries.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.payroll-salaries" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </form>
    </div>
</div>