<h3>

    Pago Planilla
</h3>
<div class="row" ng-show="!payPanel">
    <div class="col-sm-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="text-center">
                    @{{payrollSalary.name}}
                </h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-1">

                    </div>
                    <div class="col-sm-10 table-responsive">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th style="width: 25px" class="text-center">#</th>
                                <th class="text-center">Nombre</th>
                                <th style="width: 120px" class="text-center">Sueldo</th>
                                <th style="width: 120px" class="text-center">Descuentos</th>
                                <th style="width: 120px" class="text-center">Neto</th>
                            </tr>
                            <tr ng-repeat="row in payrollSalary.details track by $index">
                                <td>@{{$index + 1}}</td>
                                <td>@{{ row.employee.person.customer_name }}</td>
                                <td class="text-right">@{{ row.amount}}</td>
                                <td class="text-right">@{{ row.discounts }}</td>
                                <td class="text-right">@{{ row.amount_pay}}</td>
                            </tr>
                            <tr>
                                <th colspan="2">TOTAL</th>
                                <th class="text-right">@{{ payrollSalary.amount_salaries }}</th>
                                <th class="text-right">@{{ payrollSalary.amount_discounts }}</th>
                                <th class="text-right">@{{ payrollSalary.amount_payed }}</th>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-1">
                        <a href="#" class="btn btn-success" ng-click="setPay()" ng-hide="payrollSalary.state == 'Cerrada' ">
                            Pagar
                        </a>
                        <br>
                        <br>
                        <a href="#" class="btn btn-danger" ng-click="closePayrollSalary()" ng-hide="payrollSalary.state == 'Cerrada'">
                            Cerrar
                        </a>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
            </div>
        </div>
    </div>
</div>
<div class="row" ng-show="payPanel">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-3">
                        {!!Field::select('asesor_id',[],'',['required'=>'required',
                             'chosen'=>'',
                             'ng-change'=>'getDiscountsEmployee()',
                             'data-placeholder'=>"Selecciona un trabajador",
                             'no-results-text'=>"'No encontrado'",
                             'ng-model'=>"payEmployee.employee",
                             'width'=>"'100%'",
                              'class'=>"chosen-select input-md",
                             'ng-options'=>"item.person_name for item in employees track by item.id"],
                             ['required'],'companyForm')!!}
                    </div>
                    <div class="col-sm-2">
                        {!! Field::text('amount','',['placeholder'=>'0.00','required'=>'required','ng-change'=>'setAmountPay()',
                            'ng-model'=>'payEmployee.amount','decimals'=>'2', 'class'=>'text-right'],['required'],'payEmployeeForm')!!}
                    </div>
                    <div class="col-sm-2">
                        {!! Field::text('discounts','',['placeholder'=>'0.00','required'=>'required','disabled'=>'disabled',
                            'ng-model'=>'payEmployee.discounts','ng-change'=>'setAmountPay()','decimals'=>'2', 'class'=>'text-right'],['required'],'payEmployeeForm')!!}
                    </div>
                    <div class="col-sm-2">
                        {!! Field::text('amount_pay','',['placeholder'=>'0.00','required'=>'required','disabled'=>'disabled',
                            'ng-model'=>'payEmployee.amount_pay','ng-change'=>'setAmountPay()','decimals'=>'2', 'class'=>'text-right'],['required'],'payEmployeeForm')!!}
                    </div>
                    <div class="col-sm-1">
                        <br>
                        <a href="#" class="btn btn-success" ng-click="createPayEmployee()">
                            <i class="fa fa-plus-circle"></i>
                            Pagar
                        </a>
                    </div>
                    <div class="col-sm-2 text-center">
                        <br>
                        <a href="#" ng-click="unSetPay()" class="btn btn-info" >
                            <i class="icon-action-undo"></i>
                            Volver
                        </a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-1">

                    </div>
                    <div class="col-sm-10 table-responsive">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th style="width: 25px" class="text-center">#</th>
                                <th class="text-center">
                                    Descripción
                                </th>
                                <th style="width: 120px" class="text-center">Fecha</th>
                                <th style="width: 120px" class="text-center">Monto</th>
                                <th style="width: 120px" class="text-center"></th>
                            </tr>
                            <tr ng-repeat="row in discounts track by $index">
                                <td>@{{$index + 1}}</td>
                                <td>@{{ row.description }}</td>
                                <td class="text-center">@{{ row.created_at}}</td>
                                <td class="text-right">@{{ row.amount }}</td>
                                <td class="text-center">
                                    <a href="#" class="text-fuchsia" ng-click="selectDiscount(row)" ng-show="row.state == 'Pendiente'">Descontar</a>
                                    <a href="#" ng-click="unSelectDiscount(row)" ng-show="row.state == 'Descontado'">
                                        <i class="fa fa-check-circle text-green" ></i>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-1">
                    </div>
                </div>
            </div>
            <div class="panel-footer">
            </div>
        </div>
    </div>
</div>