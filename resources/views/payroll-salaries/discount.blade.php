<h3>
    Descuentos
</h3>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-5">
                        {!!Field::select('asesor_id',[],'',['required'=>'required',
                             'chosen'=>'',
                             'ng-change'=>'getDiscounts()',
                             'data-placeholder'=>"Selecciona un trabajador",
                             'no-results-text'=>"'No encontrado'",
                             'ng-model'=>"employee",
                             'width'=>"'100%'",
                              'class'=>"chosen-select input-md",
                             'ng-options'=>"item.person_name for item in employees track by item.id"],
                             ['required'],'companyForm')!!}
                    </div>
                    <div class="col-sm-1">
                        <br>
                        <a href="#" class="btn btn-warning" ng-click="addDiscount()">
                            <i class="fa fa-plus-circle"></i>
                            Descuento
                        </a>
                    </div>
                    <div class="col-sm-2 text-center">
                        <br>
                        <a ui-sref="app.payroll-salaries" class="btn btn-info" >
                            <i class="icon-action-undo"></i>
                            Volver
                        </a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-1">

                    </div>
                    <div class="col-sm-10 table-responsive">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th style="width: 25px" class="text-center">#</th>
                                <th class="text-center">
                                    Descripción
                                </th>
                                <th style="width: 120px" class="text-center">Fecha</th>
                                <th style="width: 120px" class="text-center">Monto</th>
                                <th style="width: 120px" class="text-center">Estado</th>
                            </tr>
                            <tr ng-repeat="row in discounts track by $index">
                                <td>@{{$index + 1}}</td>
                                <td>@{{ row.description }}</td>
                                <td class="text-center">@{{ row.created_at}}</td>
                                <td class="text-right">@{{ row.amount }}</td>
                                <td class="text-center">@{{ row.state}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-1">
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <ul class="pagination pagination-sm pull-right" ng-show="paginate">
                            <li>
                                <a href="#" ng-show="paginate.current_page > 1" ng-click="previousPage()">«</a>
                            </li>
                            <li ng-repeat="i in paginate.pages track by $index " ng-hide="$index+1 < paginate.current_page-4 || $index+1 > paginate.current_page + 9" ng-class="{true:'active'}[paginate.current_page == $index+1]">
                                <a href="#" ng-click="loadPage($index+1)">
                                    @{{$index+1}}
                                </a>
                            </li>
                            <li>
                                <a href="#" ng-show="paginate.current_page < paginate.last_page" ng-click="nextPage()">»</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/ng-template" id="addDiscountId">
    <div class="ngdialog-message">
        <div class="row">
            <div class="col-sm-12 text-center">
                <strong>
                    @{{employee.person.customer_name}}
                </strong>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                {!! Field::text('description','',[
                 'ng-model'=>'discount.description'],[],'loanForm')!!}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                    {!! Field::text('amount','',['placeholder'=>'0.00','class'=>'text-right',
                        'ng-model'=>'discount.amount', 'decimals'=>'2'],[''],'loanForm')!!}
            </div>
            <div class="col-sm-3">
                <br>
                <a href="#" class="btn btn-info" ng-click="saveDiscount()">
                    Guardar
                </a>
            </div>
        </div>
    </div>
</script>