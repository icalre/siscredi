<h3>
    Editar Proveedor
    <small>Edición de Proveedores</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="supplierForm" class="form-validate" ng-enter="preventEnterForm()" role="form" ng-submit="supplierForm.$valid && editSupplier()" novalidate>
        <div class="panel panel-default " ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('suppliers.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.suppliers" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </form>
    </div>
</div>