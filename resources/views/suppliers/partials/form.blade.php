    {!!Form::token()!!}
    <div class="row">
        <div class="col-sm-3">
            {!!Field::select('supplier_type',config('options.supplier_types'),'',
            ['id'=>'supplier_type','required'=>'required',
               'ng-model'=>'supplier.type', 'chosen'=>''],['required'],'supplierForm' )!!}
        </div>
        <div class="col-sm-7" ng-show="supplier.type == 'Person'">
            <label for="">Cliente</label>
            <div angucomplete-alt
                 id="person"
                 placeholder="Buscar Persona"
                 pause="500"
                 selected-object="supplier.supplier"
                 remote-url="app/api/people?q="
                 title-field="customer_name"
                 minlength="3"
                 input-class="form-control form-control-small"
                 match-class="highlight"
                 input-name="person_id",
                 initial-value="supplier.supplier">
            </div>
        </div>
        <div class="col-sm-7" ng-show="supplier.type == 'Enterprise'">
            <label for="">Cliente</label>
            <div angucomplete-alt
                 id="person"
                 placeholder="Buscar Empresa"
                 pause="500"
                 selected-object="supplier.supplier"
                 remote-url="app/api/enterprises?q="
                 title-field="customer_name"
                 minlength="3"
                 input-class="form-control form-control-small"
                 match-class="highlight"
                 input-name="person_id",
                 initial-value="supplier.supplier">
            </div>
        </div>
    </div>
