<h3>
    Registrar Modulo
    <small>Registro de Modulos</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="moduleForm" class="form-validate" role="form" ng-submit="moduleForm.$valid && createModule()" novalidate>
        <div class="panel panel-default" ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('modules.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.modules" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </form>
    </div>
</div>