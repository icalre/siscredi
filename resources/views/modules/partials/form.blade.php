    {!!Form::token()!!}
    <div class="row">
        <div class="col-sm-4 has-feedback">
            {!! Field::text('name','',['placeholder'=>'Ingrese un nombre','required'=>'required',
                'ng-model'=>'module.name'],['required'],'moduleForm')!!}
        </div>
        <div class="col-sm-4 has-feedback">
            {!! Field::text('icon','',['placeholder'=>'Ingrese un nombre','required'=>'required',
                'ng-model'=>'module.icon'],['required'],'moduleForm')!!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-bordered">
                <tr>
                    <th class="text-center" colspan="6">
                        SUB MODULOS
                        <a href="#" class="mb-sm btn btn-success pull-right" ng-click="addSubModule()">Agregar</a>
                    </th>
                </tr>
                <tr>
                    <th class="text-center" style="width:  30px"> # </th>
                    <th class="text-center"> Nombre</th>
                    <th class="text-center" style="width: 170px">Uri</th>
                    <th style="width: 350px" class="text-center">Permisos</th>
                    <th style="width: 85px" class="text-center">Menu</th>
                    <th style="width: 50px"></th>
                </tr>
                <tr ng-repeat="row in module.sub_modules track by $index">
                    <td style="vertical-align: middle">
                        @{{$index + 1}}
                    </td>
                    <td style="vertical-align: middle">
                        {!! Field::text('sub_module_name_@{{$index}}','',['placeholder'=>'Ingrese un nombre','required'=>'required',
                'ng-model'=>'row.name', 'ui-keypress' =>'{13:"test($event)"}'],['required'],'moduleForm', 0)!!}
                    </td>
                    <td style="vertical-align: middle">
                        {!! Field::text('uri_@{{$index}}','',['placeholder'=>'Ingrese un nombre','required'=>'required',
                'ng-model'=>'row.uri'],['required'],'moduleForm', 0)!!}
                    </td>
                    <td style="vertical-align: middle">
                        <ui-select multiple="" ng-model="row.profiles" theme="bootstrap">
                            <ui-select-match placeholder="Seleccione perfiles">
                                @{{$item.name}}
                            </ui-select-match>
                            <ui-select-choices repeat="item in profiles | propsFilter: {name: $select.search}">
                                <div ng-bind-html="item.name | highlight: $select.search"></div>
                            </ui-select-choices>
                        </ui-select>
                    </td>
                    <td>

                        <ui-select ng-model="row.link_menu" theme="bootstrap" >
                            <ui-select-match placeholder="Seleccione">@{{$select.selected.name}}</ui-select-match>
                            <ui-select-choices repeat="item in options | propsFilter: {name: $select.search}">
                                <div ng-bind-html="item.name | highlight: $select.search"></div>
                            </ui-select-choices>
                        </ui-select>

                    </td>
                    <td>
                        <a href="#" ng-click="removeSubModule($index)">
                            <i class="fa fa-times text-danger"></i>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </div>