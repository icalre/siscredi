<h3>
    Negocios
</h3>

<div class="row" ng-hide="panel_selected != ''">
    <div class="col-sm-11">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="btn-group">
                    <a class="btn btn-default" ng-click="setPanel('Customer')" ng-class="{true: 'bg-primary-dark'}[panel_selected == 'Customer']">
                        Clientes
                    </a>
                    <a class="btn btn-default" ng-click="setPanel('Loan')" ng-class="{true: 'bg-primary-dark'}[panel_selected == 'Loan']">
                        Prestamos
                    </a>
                    <a class="btn btn-default" ng-click="setPanel('Convenios')" ng-class="{true: 'bg-primary-dark'}[panel_selected == 'Loan']">
                        Convenios
                    </a>
                    <a class="btn btn-default" ng-click="setPanel('Simulacion')" ng-class="{true: 'bg-primary-dark'}[panel_selected == 'Simulacion']">
                        Simulación
                    </a>
                    <a class="btn btn-default" ng-click="setPanel('Comisiones')" ng-class="{true: 'bg-primary-dark'}[panel_selected == 'Comisiones']">
                        Comisiones
                    </a>
                    @if(in_array('app/comissions', $sub_modules))
                    <a class="btn btn-default" ng-click="setPanel('ListaClientes')" ng-class="{true: 'bg-primary-dark'}[panel_selected == 'ListaClientes']">
                        Lista de Clientes
                    </a>
                    @endif
                    <a class="btn btn-default" ng-click="setPanel('Vencimientos')" ng-class="{true: 'bg-primary-dark'}[panel_selected == 'Vencimientos']">
                        Vencimientos
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

@include('business.partials.customer')
@include('business.partials.loan')
@include('business.partials.simulacion')
@include('business.partials.comisiones')
@if(in_array('app/comissions', $sub_modules))
    @include('business.partials.customers')
@endif
@include('business.partials.vistsits')
@include('business.partials.convenios')