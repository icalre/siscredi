<div class="row" ng-show="panel_selected == 'Simulacion'">
    <div class="col-sm-5">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4>
                    Simulación
                    <a  class="pull-right" style="color: white" ng-click="setPanel('')">
                        <i class="icon-action-undo"></i> Volver
                    </a>
                </h4>
            </div>
            <div class="panel-body">
                <form name="generateSheduleForm" role="form" ng-submit="generateSheduleForm.$valid && generateSchedule()" novalidate>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon">Monto</span>
                                    <input type="text" class="form-control text-right" placeholder="0.00" ng-model="loan_simulate.amount" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon">Costo Efectivo</span>
                                    <input type="text" class="form-control text-right" placeholder="0.00" ng-model="loan_simulate.interest" >
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="currency">Moneda</label>
                                    <select chosen="" id="currency" required="required" ng-model="loan_simulate.currency" class="form-control" name="currency">
                                        <option value="" selected="selected">Seleccione</option>
                                        <option value="Soles">Soles</option>
                                        <option value="Dolares">Dolares</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="period">Periodo</label>
                                    <select chosen="" id="period" required="required" ng-model="loan_simulate.period"
                                            class="form-control" name="period" ng-change="setWeekQuota()">
                                        <option value="" selected="selected">Seleccione</option>
                                        <option value="Semanal">Semanal</option>
                                        <option value="Quincenal">Quincenal</option>
                                        <option value="Mensual">Mensual</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="type_payment">Cuota</label>
                                    <select chosen="" id="type_payment" required="required" ng-model="loan_simulate.type_payment"
                                            class="form-control" name="type_payment" ng-change="setWeekQuota()">
                                        <option value="" selected="selected">Seleccione</option>
                                        <option value="Fijo">Fijo</option>
                                        <option value="Libre">Libre</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon"># Cuotas</span>
                                    <input type="text" class="form-control text-right" placeholder="0" ng-model="loan_simulate.quotas"
                                           ng-change="setWeekQuota()" ng-model-options="{ debounce: 500 }">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input ng-change="setDateDisbursement()" placeholder="Ciclo" id="date" calendar="calendar" type="text" class="form-control" data-inputmask="'mask': '99-99-9999'" ng-model="loan_simulate.date_start" required="">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i> Desembolso
                                    </div>
                                    <input placeholder="Desembolso" id="date" calendar="calendar" type="text" class="form-control" data-inputmask="'mask': '99-99-9999'" ng-model="loan_simulate.date_disbursement" required="">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary margin pull-right">Generar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-7">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4>
                    Cronograma
                    <a href="#" ng-click="printSimulated()" class="pull-right" style="color: white">
                        <i class="icon-printer"></i> Imprimir
                    </a>
                </h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thread>
                            <tr>
                                <th class="text-center" style="width: 25px;">#</th>
                                <th class="text-center">Vcto</th>
                                <th class="text-center">Cuota</th>
                                <th class="text-center">Capital</th>
                                <th class="text-center">Interes</th>
                            </tr>
                        </thread>
                        <tbody>
                        <tr ng-repeat="item in loan_simulate.payments track by $index" ng-class ="{'text-green' : item.state == 'Pagado'}">
                            <td>@{{$index + 1}}</td>
                            <td class="text-center">@{{item.expiration}}</td>
                            <td class="text-right">@{{item.amount | number:2}}</td>
                            <td class="text-right">@{{item.capital | number:2}}</td>
                            <td class="text-right">@{{item.interest | number:2}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>