<div class="row" ng-show="panel_selected == 'Comisiones'">
    <div class="col-sm-5">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4>
                    Comisiones
                    <a  class="pull-right" style="color: white" ng-click="setPanel('')">
                        <i class="icon-action-undo"></i> Volver
                    </a>
                </h4>
            </div>
            <div class="panel-body">
                @if(in_array('app/comissions', $sub_modules))
                    <div class="row">
                        <div class="col-sm-10">
                            {!!Field::select('employee_id',[],'',['required'=>'required',
                             'chosen'=>'',
                             'data-placeholder'=>"Selecciona un trabajador",
                             'no-results-text'=>"'No encontrado'",
                             'ng-model'=>"employee",
                             'width'=>"'100%'",
                              'class'=>"chosen-select input-md",
                             'ng-options'=>"item.person_name for item in employees track by item.id"],
                             ['required'],'companyForm')!!}
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Fecha Inicial</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" id="date1" calendar="calendar" class="form-control" data-inputmask="'mask': '99-99-9999'"
                                       ng-model="date1" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>Fecha Final</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" id="date2" calendar="calendar" class="form-control" data-inputmask="'mask': '99-99-9999'"
                                       ng-model="date2" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2" style="position: relative">
                        <a style="position: absolute;left:-10px; top: 25px " target="_blank" href="app/business/comissions?employee=@{{employee.id}}&date1=@{{date1}}&date2=@{{date2}}"
                           class="btn btn-info"
                        >Buscar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>