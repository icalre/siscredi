<div class="row" ng-show="panel_selected == 'ListaClientes'">
    <div class="col-sm-7">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4>
                    Lista de Clientes
                    <a  class="pull-right" style="color: white" ng-click="setPanel('')">
                        <i class="icon-action-undo"></i> Volver
                    </a>
                </h4>
            </div>
            <br>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th class="text-center" style="width: 30px">
                                    #
                                </th>
                                <th class="text-center">
                                    Asesor
                                </th>
                                <th class="text-center" style="width: 120px">
                                    # Clientes
                                </th>
                            </tr>
                            <?php $i=0 ?>
                            @foreach($employees as $employee)
                                <tr>
                                    <td>
                                        {{$i+1}}
                                    </td>
                                    <td>
                                        {{$employee->person->customer_name}}
                                    </td>
                                    <td class="text-right">
                                        <a href="app/business/print-customer?employee={{$employee->id}}" target="_blank">
                                            {{$employee->loan_counts}}
                                        </a>

                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
