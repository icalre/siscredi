<div class="row" ng-show="panel_selected == 'Loan'">
    <div class="col-sm-5">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4>
                    Créditos
                    <a   class="pull-right" style="color: white" ng-click="setPanel('')">
                        <i class="icon-action-undo"></i> Volver
                    </a>
                </h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="input-group">
                                <input ng-enter="searchLoan()" type="text" ng-model="loan.code"
                                       class="form-control" placeholder="Código">
                                <div class="input-group-btn">
                                    <a href="#" class="btn btn-info" ng-click="searchLoan()">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th colspan="2" class="text-center">
                                Datos de Crédito
                                <a uib-popover="Historial" popover-trigger="mouseenter"  class="pull-right" href="#" ng-click="dialogOpen()">
                                    <i class="icon-speech"></i>
                                </a>
                            </th>
                        </tr>
                        <tr ng-show="loan.nro_contrato > 0">
                            <th style="width: 40%">Nº Contrato</th>
                            <td>: @{{loan.nro_contrato}}</td>
                        </tr>
                        <tr>
                            <th style="width: 40%">Nº Préstamo</th>
                            <td>: @{{loan.code}}</td>
                        </tr>
                        <tr>
                            <th>Modalidad</th>
                            <td>: @{{loan.modalidad}}</td>
                        </tr>
                        <tr ng-show="loan.modalidad == 'Descuento por Planilla'">
                            <th>Empresa</th>
                            <td>: @{{loan.enterprise.razon_social}}</td>
                        </tr>
                        <tr>
                            <th>Tipo</th>
                            <td>: Cuota @{{loan.type_payment}}</td>
                        </tr>
                        <tr>
                            <th>Co. Cliente</th>
                            <td>: @{{loan.customer.code}}</td>
                        </tr>
                        <tr>
                            <th>Cliente</th>
                            <td>: @{{loan.customer.customer.customer_name}}</td>
                        </tr>
                        <tr>
                            <th>Asesor</th>
                            <td>: @{{loan.employee.person.customer_name}}</td>
                        </tr>
                        <tr>
                            <th>Monto (@{{loan.currency}})</th>
                            <td>: @{{loan.amount}}</td>
                        </tr>
                        <tr>
                            <th>Fecha de Desembolso</th>
                            <td>: @{{loan.desembolso}}</td>
                        </tr>
                        <tr>
                            <th>Fecha de Cancelación</th>
                            <td>: @{{loan.date_end}}</td>
                        </tr>
                        <tr>
                            <th>Costo Efectivo</th>
                            <td>: @{{loan.interest}}%</td>
                        </tr>
                        <tr>
                            <th>Plazo (@{{loan.period}})</th>
                            <td>: @{{loan.quotas}}</td>
                        </tr>
                        <tr>
                            <th>Estado</th>
                            <td>: @{{loan.state}}</td>
                        </tr>
                        <tr ng-show="loan.warranty_option='Si'">
                            <th>Garantía</th>
                            <td>
                                : <br>
                                <table class="table table-bordered" ng-show="loan.artefactosA.length > 0">
                                    <tr>
                                        <th class="text-center bg-primary" colspan="3">
                                            Artefactos
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center" >Descripción</th>
                                        <th class="text-center" style="width: 100px">Tasación</th>
                                    </tr>
                                    <tr ng-repeat="item in loan.artefactosA track by $index"  ng-class ="{'text-green' : item.state == 'Rescatado'}">
                                        <td>@{{$index + 1}}</td>
                                        <td>
                                            @{{item.Description}}
                                        </td>
                                        <td class="text-right">
                                            @{{item.reference_price}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Total</th>
                                        <th class="text-right">
                                            @{{artefactos.tasacion | number:2}}
                                        </th>
                                    </tr>
                                </table>
                                <br>
                                <table class="table table-bordered" ng-show="loan.otrosA.length > 0">
                                    <tr>
                                        <th class="text-center bg-primary" colspan="3">
                                            Otros
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">Descripción</th>
                                        <th class="text-center" style="width: 100px">Tasación</th>
                                    </tr>
                                    <tr ng-repeat="item in loan.otrosA track by $index"  ng-class ="{'text-green' : item.state == 'Rescatado'}">
                                        <td>@{{$index + 1}}</td>
                                        <td>
                                            @{{item.Description}}
                                        </td>
                                        <td class="text-right" >
                                            @{{item.reference_price}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Total</th>
                                        <th class="text-right">
                                            @{{otros.tasacion | number:2}}
                                        </th>
                                    </tr>
                                </table>
                                <br>
                                <table class="table table-bordered" ng-show="loan.joyasA.length > 0">
                                    <tr>
                                        <th class="text-center bg-primary" colspan="5">
                                            Joyas
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">Descripción</th>
                                        <th class="text-center" style="width: 60px">Peso Bruto (gr)</th>
                                        <th class="text-center" style="width: 60px">Peso Neto (gr)</th>
                                        <th class="text-center" style="width: 100px">Tasación</th>
                                    </tr>
                                    <tr ng-repeat="item in loan.joyasA track by $index"  ng-class ="{'text-green' : item.state == 'Rescatado'}">
                                        <td>@{{$index + 1}}</td>
                                        <td>
                                            @{{item.Description}}
                                        </td>
                                        <td class="text-right">
                                            @{{item.pb | number:2}}
                                        </td>
                                        <td class="text-right">
                                            @{{item.pn | number:2}}
                                        </td>
                                        <td class="text-right">
                                            @{{item.reference_price| number:2}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Total</th>
                                        <th class="text-right">
                                            @{{joyas.pb}}
                                        </th>
                                        <th class="text-right">
                                            @{{joyas.pn}}
                                        </th>
                                        <th class="text-right">
                                            @{{joyas.tasacion}}
                                        </th>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr ng-show="endorsement_option='Si'">
                            <th>Aval</th>
                            <td>: @{{loan.endorsement.person.last_name}} @{{loan.endorsement.person.name}}</td>
                        </tr>
                        <tr>
                            <th>Referido</th>
                            <td>: @{{loan.person.last_name}} @{{loan.person.name}}</td>
                        </tr>
			<tr>
                            <th>Observaciones</th>
                            <td>: @{{loan.observations}}</td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-7">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4>
                    Cronograma
                    <a href="app/business/print-schedule?loan-id=@{{loan.id}}" target="_blank"  class="pull-right" style="color: white" ng-click="setPanel('')">
                        <i class="icon-printer"></i> Imprimir
                    </a>
                </h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thread>
                            <tr>
                                <th class="text-center" style="width: 25px;">#</th>
                                <th class="text-center">Vcto</th>
                                <th class="text-center">Cuota</th>
                                <th class="text-center">Capital</th>
                                <th class="text-center">Interes</th>
                                <th class="text-center">Gastos</th>
                                <th class="text-center" ng-hide="loan.type_payment == 'Libre'">Mora</th>
                                <th class=" text-center">F. Pago</th>
                                <th class=" text-center">Estado</th>
                                <th style="width: 25px;"></th>
                            </tr>
                        </thread>
                        <tbody>
                        <tr ng-repeat="item in loan.payments track by $index" ng-class ="{'text-green' : item.state == 'Pagado'}">
                            <td>@{{$index + 1}}</td>
                            <td class="text-center">@{{item.expiration}}</td>
                            <td class="text-right">@{{item.amount | number:2}}</td>
                            <td class="text-right">@{{item.capital | number:2}}</td>
                            <td class="text-right">@{{item.interest | number:2}}</td>
                            <td class="text-right">@{{item.expenses | number:2}}</td>
                            <td class="text-right" ng-hide="loan.type_payment == 'Libre'" >@{{item.mora | number:2}}</td>
                            <td class="text-center">@{{item.date_p}}</td>
                            <td class="text-center">@{{item.state}}</td>
                            <td class="text-center">
                                <span class="badge bg-danger">@{{item.score}}</span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br>
                    <p class="text-center">
                        <span class="badge bg-danger">Su score es de: @{{score| number:2}}</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/ng-template" id="dialogWithNestedConfirmDialogId">
    <div class="ngdialog-message text-center">
        <br>
        <br>
        <table class="table table-bordered">
            <tr>
                <th colspan="2" class="bg-primary">
                    HISTORAL

                    <a style="color: white" href="app/print-historial?loan-id=@{{loan.id}}" class="pull-right" target="_blank">
                        <i class="icon-printer"></i>
                    </a>
                </th>
            </tr>
            <tr>
                <th style="width: 100px">
                    Fecha
                </th>
                <th>
                    Comentario
                </th>
            </tr>
            <tr ng-repeat="row in loan.comments track by $index">
                <td>
                    @{{row.date}}
                </td>
                <td>
                    @{{row.comment}}
                </td>
            </tr>
        </table>
    </div>
</script>
