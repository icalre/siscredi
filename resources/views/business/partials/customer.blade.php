<div class="row" ng-show="panel_selected == 'Customer'">
    <div class="col-sm-5">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4>
                    Clientes
                    <a  class="pull-right" style="color: white" ng-click="setPanel('')">
                        <i class="icon-action-undo"></i> Volver
                    </a>
                </h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="input-group">
                                <input ng-enter="searchCustomers()" type="text" ng-model="person.q" class="form-control" placeholder="DNI o Nombre">
                                <div class="input-group-btn">
                                    <a href="#" class="btn btn-info" ng-click="searchCustomers()">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <div class="table-responsive" ng-show="people.length > 0">
                    <table class="table table-bordered">
                        <tr>
                            <th style="width: 100px">Codigo</th>
                            <th>Cliente</th>
                        </tr>
                        <tr ng-repeat="row in people track by $index">
                            <td class="text-center">
                                <a href="#" ng-click="setCustomer(row)">
                                    <span class="label label-info">
                                        @{{row.customer.code}}
                                    </span>
                                </a>
                            </td>
                            <td>
                                @{{row.customer_name}}
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive"  ng-show="person.id">
                            <table class="table table-bordered">
                                <tr>
                                    <th colspan="2">
                                        Datos Cliente

                                        <a href="app/business/person-info?person-id=@{{person.id}}"
                                           target="_blank" class="btn btn-info pull-right">
                                            Ver Ficha
                                        </a>
                                    </th>
                                </tr>
                                <tr>
                                    <th>Código</th>
                                    <td>
                                        @{{person.customer.code}}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Nombre</th>
                                    <td>
                                        @{{person.customer_name}}

                                        <a uib-popover="Comentarios" popover-trigger="mouseenter"  class="pull-right" href="#" ng-click="dialogCommentsOpen()">
                                            <i class="icon-speech"></i>
                                        </a>

                                        <a   href="app/business/billing?customer-id=@{{person.customer.id}}"
                                           target="_blank" class="pull-right" style="; margin-right:5px; color: #9c3328; font-weight: 900">
                                            <i class="icon-book-open"></i>
                                        </a>
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <table class="table table-bordered">
                                <tr>
                                    <th class="text-center" style="width: 20px">#</th>
                                    <th class="text-center">Código</th>
                                    <th class="text-center">Monto</th>
                                    <th class="text-center">Moneda</th>
                                    <th class="text-center">Estado</th>
                                    <th class="text-center">Fecha</th>
                                </tr>
                                <tr ng-repeat="item in person.customer.loans track by $index">
                                    <td class="text-center">
                                        @{{ $index + 1 }}
                                    </td>
                                    <td class="text-center">
                                        <a href="#" ng-click="setLoan(item)">
                                            <span class="label label-info">
                                                @{{item.code}}
                                            </span>
                                        </a>

                                    </td>
                                    <td class="text-right">
                                        @{{ item.amount }}
                                    </td>
                                    <td>
                                        @{{ item.currency }}
                                    </td>
                                    <td>
                                        @{{ item.state }}
                                    </td>
                                    <td>
                                        @{{ item.date_start }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/ng-template" id="dialogCommentsId">
    <div class="ngdialog-message">
    <br>
    <br>
    <table class="table table-bordered">
        <tr>
            <th colspan="2" class="bg-primary text-center">
                COMENTARIOS
                <a style="color: white" href="app/print-comments?customer-id=@{{person.customer.id}}" class="pull-right" target="_blank">
                    <i class="icon-printer"></i>
                </a>
            </th>
        </tr>
        <tr>
            <th style="width: 100px">
                Fecha
            </th>
            <th>
                Comentario
            </th>
        </tr>
        <tr ng-repeat="row in person.customer.comments track by $index">
            <td>
                @{{row.created_at}}
            </td>
            <td>
                @{{row.comment}}
            </td>
        </tr>
    </table>
</div>
</script>