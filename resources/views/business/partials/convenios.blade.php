<div class="row" ng-show="panel_selected == 'Convenios'">
    <div class="col-sm-9">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4>
                    Convenios
                    <a  class="pull-right" style="color: white" ng-click="setPanel('')">
                        <i class="icon-action-undo"></i> Volver
                    </a>
                </h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <div angucomplete-alt
                                 id="person"
                                 placeholder="Buscar Empresa"
                                 pause="500"
                                 selected-object="searchLoanEnterprise"
                                 remote-url="app/api/enterprises?q="
                                 title-field="customer_name"
                                 minlength="3"
                                 input-class="form-control form-control-small"
                                 match-class="highlight"
                                 input-name="person_id",
                                 initial-value="loan.enterprise"
                                 text-no-results = "'No Encontrado'"
                            >
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <div class="table-responsive" ng-show="people.length > 0">
                    <table class="table table-bordered">
                        <tr>
                            <th style="width: 100px">Codigo</th>
                            <th>Cliente</th>
                        </tr>
                        <tr ng-repeat="row in people track by $index">
                            <td class="text-center">
                                <a href="#" ng-click="setCustomer(row)">
                                    <span class="label label-info">
                                        @{{row.customer.code}}
                                    </span>
                                </a>
                            </td>
                            <td>
                                @{{row.customer_name}}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <th class="text-center" style="width: 20px">#</th>
                                    <th class="text-center">Código</th>
                                    <th class="text-center">Cliente</th>
                                    <th class="text-center">Monto</th>
                                    <th class="text-center">Moneda</th>
                                    <th class="text-center">Estado</th>
                                    <th class="text-center">Fecha</th>
                                </tr>
                                <tr ng-repeat="item in enterprise_loans track by $index">
                                    <td class="text-center">
                                        @{{ $index + 1 }}
                                    </td>
                                    <td class="text-center">
                                        <a href="#" ng-click="setLoan(item)">
                                            <span class="label label-info">
                                                @{{item.code}}
                                            </span>
                                        </a>

                                    </td>
                                    <td >
                                        @{{ item.customer.customer.customer_name }}
                                    </td>
                                    <td class="text-right">
                                        @{{ item.amount }}
                                    </td>
                                    <td>
                                        @{{ item.currency }}
                                    </td>
                                    <td>
                                        @{{ item.state }}
                                    </td>
                                    <td>
                                        @{{ item.date_start }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>