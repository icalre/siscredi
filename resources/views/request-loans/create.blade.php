<h3>
    Registrar Propuesta de Crédito
    <small>Registro de Propuesta de Crédito</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="requestLoanForm" class="form-validate" role="form" ng-submit="requestLoanForm.$valid && createRequestLoan()" novalidate>
        <div class="panel panel-default" ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('request-loans.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.request-loans" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </form>
    </div>
</div>