<h3>
    Prestamo
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="loanForm" class="form-validate" ng-enter="preventEnterForm()" role="form" ng-submit="loanForm.$valid && editLoan()" novalidate>
        <div class="panel panel-default " ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('loans.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.loans" class="btn btn-warning">Volver</a>
            </div>
        </div>
        </form>
    </div>
</div>