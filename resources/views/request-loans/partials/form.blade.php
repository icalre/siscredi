    {!!Form::token()!!}
    <div class="row">
        <div class="col-sm-5">
            <label for="">Cliente</label>
            <div angucomplete-alt
                 id="person_customer"
                 placeholder="Buscar Cliente"
                 pause="500"
                 selected-object="requestLoan.person"
                 remote-url="app/api/people?q="
                 title-field="customer_name"
                 minlength="3"
                 input-class="form-control form-control-small"
                 match-class="highlight"
                 field-required="true"
                 input-name="person_id_customer"
                 initial-value="requestLoan.customer.customer">
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-2">
            {!! Field::text('amount','',['placeholder'=>'0.00','class'=>'text-right','required'=>'required',
                'ng-model'=>'requestLoan.amount', 'decimals'=>'2'],['required'],'requestLoanForm')!!}
        </div>
        <div class="col-sm-2">
            {!! Field::text('interest','',['placeholder'=>'0.00','class'=>'text-right','required'=>'required',
                'ng-model'=>'requestLoan.interest', 'decimals'=>'2'],['required'],'requestLoanForm')!!}
        </div>
        <div class="col-sm-2">
            {!!Field::select('period',config('options.periodos'),'',
            ['required'=>'required',
               'ng-model'=>'requestLoan.period', 'chosen'=>''],['required'],'requestLoanForm' )!!}
        </div>
        <div class="col-sm-2">
            {!!Field::select('type_payment',config('options.type_payments'),'',
            ['required'=>'required',
               'ng-model'=>'requestLoan.type_payment', 'chosen'=>''],['required'],'requestLoanForm' )!!}
        </div>
        <div class="col-sm-2">
            {!! Field::text('quotas','',['placeholder'=>'0','class'=>'text-right','required'=>'required',
                'ng-model'=>'requestLoan.quotas', 'ng-model-options'=>'{debounce:1000}','ng-change'=>'setWeekQuota()'],['required'],'requestLoanForm')!!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-10">
            {!! Field::textarea('sugerencia','',[
                 'ng-model'=>'requestLoan.observation'],[],'requestLoanForm')!!}
        </div>
    </div>
