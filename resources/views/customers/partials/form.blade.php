    {!!Form::token()!!}
    <div class="row">
        <div class="col-sm-3">
            {!!Field::select('customer_type',config('options.customer_types'),'',
            ['id'=>'customer_type','required'=>'required',
               'ng-model'=>'customer.type', 'chosen'=>''],['required'],'customerForm' )!!}
        </div>
        <div class="col-sm-7" ng-show="customer.type == 'Person'">
            <label for="">Cliente</label>
            <div angucomplete-alt
                 id="person"
                 placeholder="Buscar Persona"
                 pause="500"
                 selected-object="customer.customer"
                 remote-url="app/api/people?q="
                 title-field="customer_name"
                 minlength="3"
                 input-class="form-control form-control-small"
                 match-class="highlight"
                 input-name="person_id",
                 initial-value="customer.customer">
            </div>
        </div>
        <div class="col-sm-7" ng-show="customer.type == 'Enterprise'">
            <label for="">Cliente</label>
            <div angucomplete-alt
                 id="person"
                 placeholder="Buscar Empresa"
                 pause="500"
                 selected-object="customer.customer"
                 remote-url="app/api/enterprises?q="
                 title-field="customer_name"
                 minlength="3"
                 input-class="form-control form-control-small"
                 match-class="highlight"
                 input-name="person_id",
                 initial-value="customer.customer">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            {!!Field::select('customer_type',config('options.rate'),'',
            ['id'=>'rate','required'=>'required',
               'ng-model'=>'customer.rate', 'chosen'=>''],['required'],'customerForm' )!!}
        </div>
    </div>
