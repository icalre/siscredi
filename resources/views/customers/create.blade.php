<h3>
    Registrar Cliente
    <small>Registro de Clientes</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="customerForm" class="form-validate" role="form" ng-submit="customerForm.$valid && createCustomer()" novalidate>
        <div class="panel panel-default" ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('customers.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.customers" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </form>
    </div>
</div>