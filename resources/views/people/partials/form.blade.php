    {!!Form::token()!!}
    <div class="row">
        <div class="col-sm-4">
            <label for="dni">DNI</label>
            <div class="input-group">
                <input placeholder="Ingrese un dni" required="required" ng-model="person.dni" numbers-only="numbers-only"
                       class="form-control" name="dni" type="text" value="" style="">
                 <span class="input-group-btn">
                    <button class="btn btn-default" type="button" ng-click="consultReniec()">
                        <i class="icon-user"></i>
                    </button>
                 </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            {!! Field::text('name','',['placeholder'=>'Ingrese un nombre','required'=>'required',
                'ng-model'=>'person.name'],['required'],'personForm')!!}
        </div>
        <div class="col-sm-7">
            {!! Field::text('last_name', '',['placeholder'=>'Ingrese Apellidos','required'=>'required',
                'ng-model'=>'person.last_name'],['required'],'personForm') !!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-5">
            {!! Field::text('address', '',['placeholder'=>'Ingrese una dirección','required'=>'required',
                'ng-model'=>'person.address'],['required'],'personForm') !!}
        </div>
        <div class="col-sm-3">
            {!! Field::text('telephone', '',['placeholder'=>'Ingrese un telefono',
                'ng-model'=>'person.telephone'],[],'personForm') !!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            {!!Field::select('department_id',[],'',['required'=>'required',
             'chosen'=>'',
             'data-placeholder'=>"Selecciona un Departmamento",
             'no-results-text'=>"'No encontrado'",
             'ng-model'=>"person.department",
             'width'=>"'100%'",
              'class'=>"chosen-select input-md",
             'ng-options'=>"item.departamento for item in departments track by item.id"],
             ['required'],'personForm')!!}
        </div>
        <div class="col-sm-4">

            {!!Field::select('province_id',[],'',['required'=>'required',
             'chosen'=>'',
             'data-placeholder'=>"Selecciona una Provincia",
             'no-results-text'=>"'No encontrado'",
             'ng-model'=>"person.province",
             'width'=>"'100%'",
              'class'=>"chosen-select input-md",
             'ng-options'=>"item.provincia for item in person.department.provinces track by item.id"],
             ['required'],'personForm')!!}
        </div>
        <div class="col-sm-4">

            {!!Field::select('district_id',[],'',['required'=>'required',
             'chosen'=>'',
             'data-placeholder'=>"Selecciona un Distrito",
             'no-results-text'=>"'No encontrado'",
             'ng-model'=>"person.district",
             'width'=>"'100%'",
              'class'=>"chosen-select input-md",
             'ng-options'=>"item.distrito for item in person.province.districts track by item.id"],
             ['required'],'personForm')!!}
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-3">
            {!! Field::text('date_of_birth','',['placeholder'=>'dd-mm-yyyy',
            'masked'=>"",'data-inputmask'=>"'mask': '99-99-9999'",'required'=>'required',
                'ng-model'=>'person.date_of_birth'],['required'],'personForm')!!}
        </div>
        <div class="col-sm-2">
            {!!Field::select('gender',[],'',['required'=>'required',
             'chosen'=>'',
             'data-placeholder'=>"Selecciona un Distrito",
             'no-results-text'=>"'No encontrado'",
             'ng-model'=>"person.gender",
             'width'=>"'100%'",
              'class'=>"chosen-select input-md",
             'ng-options'=>"item.name for item in genders track by item.id"],
             ['required'],'personForm')!!}
        </div>
        <div class="col-sm-6">
            {!! Field::email('email','',['placeholder'=>'Ingrese un correo',
                'ng-model'=>'person.email'],['email'],'personForm')!!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            {!!Field::select('file',config('options.conditional'),'',
            ['required'=>'required',
               'ng-model'=>'person.file', 'chosen'=>''],['required'],'loanForm' )!!}
        </div>
        <div class="col-sm-4">
            {!! Field::text('business_address', '',['placeholder'=>'Ingrese una dirección',
                'ng-model'=>'person.business_address'],[],'personForm') !!}
        </div>
        <div class="col-sm-4">
            {!! Field::text('phone_reference', '',['placeholder'=>'Ingrese una dirección',
                'ng-model'=>'person.phone_reference'],[],'personForm') !!}
        </div>
        <div class="col-sm-2">
            {!! Field::text('activity', '',['placeholder'=>'Ingrese Actividad Economica',
                'ng-model'=>'person.activity'],[],'personForm') !!}
        </div>
    </div>