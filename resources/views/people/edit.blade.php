<h3>
    Editar Persona
    <small>Edición de Personas</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="moduleForm" class="form-validate" ng-enter="preventEnterForm()" role="form" ng-submit="moduleForm.$valid && editPerson()" novalidate>
        <div class="panel panel-default " ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('people.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.people" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </form>
    </div>
</div>