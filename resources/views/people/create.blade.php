<h3>
    Registrar Persona
    <small>Registro de Personas</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="personForm" class="form-validate" role="form" ng-submit="personForm.$valid && createPerson()" novalidate>
        <div class="panel panel-default" ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('people.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.people" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </form>
    </div>
</div>