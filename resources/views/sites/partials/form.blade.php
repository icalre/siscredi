    {!!Form::token()!!}
    <div class="row">
        <div class="col-sm-4">
            {!!Field::select('company_id',[],'',['required'=>'required',
             'chosen'=>'',
             'data-placeholder'=>"Selecciona una compañia",
             'no-results-text'=>"'No encontrado'",
             'ng-model'=>"site.company",
             'width'=>"'100%'",
              'class'=>"chosen-select input-md",
             'ng-options'=>"item.nombre_comercial for item in companies track by item.id"],
             ['required'],'companyForm')!!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            {!! Field::text('name','',['placeholder'=>'Ingrese un nombre','required'=>'required',
                'ng-model'=>'site.name'],['required'],'profileForm')!!}
        </div>
        <div class="col-sm-7">
            {!! Field::text('address', '',['placeholder'=>'Ingrese una descripción','required'=>'required',
                'ng-model'=>'site.address'],['required'],'profileForm') !!}
        </div>
    </div>
