<h3>
    Registrar Local
    <small>Registro de Locales</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="siteForm" class="form-validate" role="form" ng-submit="siteForm.$valid && createSite()" novalidate>
        <div class="panel panel-default" ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('sites.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.sites" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </form>
    </div>
</div>