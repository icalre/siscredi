<h3>
    Registrar Area
    <small>Registro de Areas</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="areaForm" class="form-validate" role="form" ng-submit="areaForm.$valid && createArea()" novalidate>
        <div class="panel panel-default" ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('areas.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.areas" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </form>
    </div>
</div>