    {!!Form::token()!!}
    <div class="row">
        <div class="col-sm-4">
            {!! Field::text('name','',['placeholder'=>'Ingrese un nombre','required'=>'required',
                'ng-model'=>'area.name'],['required'],'areaForm')!!}
        </div>
        <div class="col-sm-7">
            {!! Field::text('description', '',['placeholder'=>'Ingrese una descripción','required'=>'required',
                'ng-model'=>'area.description'],['required'],'areaForm') !!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            {!!Field::select('sites',[],[],['required'=>'required',
             'chosen'=>'',
             'multiple'=>'multiple',
             'no-results-text'=>"'No encontrado'",
             'ng-model'=>"area.sites",
             'data-placeholder'=>'Selecionar Locales',
             'width'=>"'100%'",
              'class'=>"chosen-select input-md",
             'ng-options'=>"item.name for item in sites track by item.id"],
             ['required'],'companyForm')!!}
        </div>
    </div>
