<h3>
    Caja
    <small>Desembolsos</small>
</h3>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-info" ng-class="{'whirl standard' : loading}">
            <div class="panel-heading">
                <h4>Desembolsos
                    <a  class="pull-right" style="color: white" ui-sref="app.cash-desk-details">
                        <i class="icon-action-undo"></i> Volver
                    </a>
                </h4>
            </div>
            <div class="panel-body">
                <br>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr class="bg-success" >
                            <th class="text-center" style="width: 25px; color: white">#</th>
                            <th class="text-center" style="width: 120px; color: white">C. Crédito</th>
                            <th class="text-center" style="color: white">Cliente</th>
                            <th class="text-center" style="width: 100px; color: white">Monto</th>
                            <th class="text-center" style="color: white; width: 150px">Fecha</th>
                            <th class="text-center" style="width: 120px; color: white"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="row in disbursements track by $index" ng-class ="{'text-danger' : row.state == 'Anulled'}">
                            <td class="text-center" >
                                @{{$index + 1}}
                            </td>
                            <td>
                                @{{row.loan.code }}
                            </td>
                            <td>
                                @{{ row.loan.customer.customer.customer_name }}
                            </td>
                            <td class="text-right">
                                @{{ row.amount }}
                            </td>
                            <td>
                                @{{ row.created_at }}
                            </td>
                            <td>
                                <a  href="#" ng-click="rePrint(row)" title="Re Imprimir" class="btn btn-warning">
                                    <i class="icon-printer"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="3" >Total</th>
                            <th class="text-right">@{{total_disbursements | number:2}}</th>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>