<h3>
    Caja
</h3>
@if($cash_desk->state == 0)
    <div class="row">
        <div class="col-sm-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>
                        Abrir Caja
                    </h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-8">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 25px">#</th>
                                    <th>Cuenta</th>
                                    <th>Monto</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($accounts as $index=>$account)
                                    <tr>
                                        <td>
                                            {{$index + 1}}
                                        </td>
                                        <td>
                                          {{$account->name}}
                                        </td>
                                        <td>
                                            {{$account->amount}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <br>
                            <a href="#" ng-click="openCashDesk()" class="btn btn-info" style="margin-top: 5px">
                                Abrir Caja
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif($cash_desk->state == 1)
    <div class="row">
        <div class="col-sm-9">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="btn-group">
                        <a class="btn btn-info" ng-click="setPanel('Loan')"
                           ng-class="{true: 'bg-primary-dark'}[panel_selected == 'Loan']">Prestamos</a>
                        <a class="btn btn-info" ng-click="setPanel('No Adeudo')"
                           ng-class="{true: 'bg-primary-dark'}[panel_selected == 'No Adeudo']">No Adeudo</a>
                        <a class="btn btn-info" ui-sref="app.cash-desk-details-deposits">Pago de Cuotas</a>
                        <a class="btn btn-info" ui-sref="app.cash-desk-details-incomes">Ingresos</a>
                        <a class="btn btn-warning" ui-sref="app.cash-desk-details-expenses">Gastos Generales</a>
                        <a class="btn btn-warning" ui-sref="app.cash-desk-details-expenses-g">Gastos activos e inversión</a>
                        <a class="btn btn-warning" ui-sref="app.cash-desk-details-disbursements">Desembolsos</a>
                        <a class="btn btn-warning" ui-sref="app.cash-desk-details-close">Cierre de Caja</a>
                    </div>
                    <div class="pull-right" ng-show="consignments.length > 0">
                        <a href="#" ng-click="openDialogR()">
                            <em class="fa fa-exclamation-circle fa-2x text-danger"></em>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" ng-show="panel_selected == 'No Adeudo' ">
        <div class="col-sm-8">
            <div class="panel panel-info">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="type_method">Cuenta</label>
                            <select name="type_method" id="type_method" class="form-control" chosen="chosen" ng-model="account_id">
                                <option value="">Cuenta</option>
                                <option value="1">Efectivo</option>
                                <option value="2">BVVA</option>
                                <option value="3">BCP</option>
                                <option value="4">BCP DOLARES</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <label style="width:100%">Cliente</label>
                    <div angucomplete-alt
                         id="person"
                         placeholder="Buscar Persona"
                         pause="500"
                         selected-object="customer"
                         remote-url="app/api/people?q="
                         title-field="full_name"
                         minlength="3"
                         input-class="form-control form-control-small"
                         match-class="highlight"
                         field-required="true" ,
                         input-name="person_id">
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <button type="button" ng-click="printConstancia()" class="btn btn-info">Imprimir
                                    </button>
                                    <button type="button" ng-click="setPanel('')" class="btn btn-danger">Cancelar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" ng-show="panel_selected == 'Loan' ">
        <div class="col-sm-8">
            <div class="panel panel-info">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="text" ng-enter="searchLoan()" ng-model="loan.code" class="form-control">
                                <span class="input-group-btn">
                                <a ng-click="getPendingPayments()" class="btn btn-success">
                                    Adelantos
                                </a>
                                <a href="#" ng-click="loanDisbursement()" class="btn btn-warning">
                                    Desembolso
                                </a>
                                <a ng-click="breakWarranty()" class="btn btn-default bg-yellow-dark">
                                    Liberar Garantía
                                </a>
                                <a ng-click="setPanel('')" class="btn btn-danger">
                                    Cerrar
                                </a>
                            </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-sm-4">
                            <strong>@{{ loan.customer.customer.customer_name }}</strong>
                        </div>
                        <div class="col-sm-3" ng-show="loan.type_payment == 'Fija' && loan.state == 'Vigente'"
                             ng-click="getCancellation()">
                            <a href="#" class="btn btn-sm btn-primary">Simulador de Pago Total</a>
                        </div>
                        <div class="col-sm-2" ng-show="loan.state == 'Vigente'" ng-click="setLoanExpense(loan)">
                            <a href="#" class="btn btn-sm btn-primary">Agregar Gasto</a>
                        </div>
                        <div class="col-sm-3" ng-show="loan.state == 'Perdida' ||loan.state == 'Garantía Ejecutada'">
                            <span class="pull-right badge bg-danger">@{{loan.state}}</span>
                        </div>
                    </div>
                    <div class="row" ng-show="loan.customer.customer.customer_name">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="type_method">Cuentas</label>
                                <select name="type_method" id="type_method" class="form-control" chosen="chosen" ng-model="type_payment">
                                    <option value="">Tipo de Pago</option>
                                    <option value="1">Efectivo</option>
                                    <option value="2">BVVA</option>
                                    <option value="3">BCP</option>
                                    <option value="4">BCP DOLARES</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-3" ng-show="loan.expense_amount > 0">
                            <table class="table table-bordered">
                                <tr>
                                    <th ng-class="{'bg-yellow' : expenses_select}" ng-click="setExpense()">
                                        Gastos: @{{loan.expense_amount|number:2}}
                                    </th>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <br>
                    <div class="row" ng-show="loan.state == 'Vigente' || loan.state == 'Atrasado' ">
                        <div class="col-sm-12" ng-show="loan.type_payment == 'Fija'">
                            <table class="table table-bordered table-striped">
                                <thread>
                                    <tr>
                                        <th class="col-sm-1 text-center">#</th>
                                        <th class="col-sm-3 text-center">Vcto</th>
                                        <th class="col-sm-2 text-center">Cuota</th>
                                        <th class="col-sm-2 text-center">Capital</th>
                                        <th class="col-sm-2 text-center">Interes</th>
                                        <th class="col-sm-2 text-center">Mora</th>
                                        <th class="col-sm-1 text-center"></th>
                                    </tr>
                                </thread>
                                <tbody>
                                <tr ng-repeat="item in payments track by $index"
                                    ng-class="{'text-green' : item.state == 'Pagado'}">
                                    <td>@{{$index + 1}}</td>
                                    <td class="text-center">@{{item.expiration}}</td>
                                    <td class="text-right">@{{item.amount|number:2}}</td>
                                    <td class="text-right">@{{item.capital|number:2}}</td>
                                    <td class="text-right">@{{item.interest|number:2}}</td>
                                    <td class="text-right" ng-class="{'bg-yellow' : item.pay_mora}">
                                        <span ng-click="setMora(item)"
                                              ng-show="item.mora >= 0 && !item.pay_mora">@{{item.mora|number:2}}</span>
                                        <input type="text" ng-model="item.mora" class="form-control" ng-change="setAmountPay(item)" ng-show="item.mora >= 0 && item.pay_mora">
                                    </td>
                                    <td>
                                        <a href="#" class="text-pink" ng-click="selectPayment(item)"
                                           ng-show="item.cobrar && item.state == 'Pendiente'">Cobrar</a>
                                        <a href="#" ng-click="unSelectPayment(item)"
                                           ng-show="!item.cobrar && item.state != 'Pagado'">
                                            <i class="fa fa-check-circle text-green"></i>
                                        </a>
                                        <span class="text-green" ng-show="item.state == 'Pagado'">Pagado</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th colspan="2">Total</th>
                                    <th class="text-right">@{{loan.total.amount|number:2}}</th>
                                    <th class="text-right">@{{loan.total.capital|number:2}}</th>
                                    <th class="text-right">@{{loan.total.interest|number:2}}</th>
                                    <th class="text-right">@{{loan.total.mora|number:2}}</th>
                                    <th class="text-right"></th>
                                </tr>
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-sm-12">
                                    <br/>
                                    <a href="#" class="btn btn-info" ng-click="payF()" ng-show="!adelanto">Cobrar</a>
                                    <a href="#" class="btn btn-info" ng-click="payE()" ng-show="adelanto">Cobrar</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12"
                             ng-show="loan.type_payment == 'Libre' || loan.state =='Perdida' || loan.state == 'Garantía Ejecutada'">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label for="capital">Capital</label>
                                        <input ng-model-options="{ debounce: 1000 }" ng-change="amountL()" decimals="2"
                                               class="form-control text-right" type="text" id="capital" name="capital"
                                               ng-model="payment.capital"/>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label for="interest">Interes</label>
                                        <input decimals="2" disabled class="form-control text-right" type="text"
                                               id="interest" name="interest" ng-model="payment.interest"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label for="amount">Total</label>
                                        <input ng-model-options="{ debounce: 1000 }" ng-change="setCapitalL()"
                                               decimals="2" class="form-control text-right" type="text" id="amount"
                                               name="amount" ng-model="payment.amount"/>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <br/>
                                    <a href="#" class="btn btn-info" ng-click="payL()">Cobrar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" ng-show="loan.state == 'Perdida' ||loan.state == 'Garantía Ejecutada'">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="comment">Comentario</label>
                                        <input class="form-control" type="text" id="comment" name="comment"
                                               ng-model="paymentDocument.comment"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label for="capital">Capital</label>
                                        <input class="form-control text-right" type="text" id="capital" name="capital"
                                               ng-model="payment.capital"
                                               ng-model-options="{ debounce: 1000 }" decimals="2"
                                               ng-change="amountL()"/>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label for="interest">Interes</label>
                                        <input class="form-control text-right" type="text" id="interest" name="interest"
                                               ng-model-options="{ debounce: 1000 }" decimals="2" ng-change="amountL()"
                                               ng-model="payment.interest"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label for="amount">Total</label>
                                        <input ng-model-options="{ debounce: 1000 }" decimals="2"
                                               ng-change="setCapitalL()" class="form-control text-right"
                                               type="text" id="amount" name="amount" ng-model="payment.amount"/>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <br/>
                                    <a href="#" class="btn btn-info" ng-click="payCapital()">Cobrar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/ng-template" id="panel-loan-expense">
        <div class="ngdialog-message text-center">
            <h3 class="mt0">Agregar Gasto</h3>
            <div class="row">
                <div class="col-sm-5 text-left">
                    {!! Field::text('amount','',['placeholder'=>'0.00','class'=>'text-right','required'=>'required',
                    'ng-model'=>'loanExpense.amount', 'decimals'=>'2'],['required'],'loanForm')!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-left">
                    {!! Field::text('description','',['placeholder'=>'Descripción','required'=>'required',
                    'ng-model'=>'loanExpense.description'],['required'],'loanForm')!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <a href="#" class="btn btn-info pull-right" ng-click="sendLoanExpense()">Guardar</a>
                </div>
            </div>
        </div>
    </script>

    <script type="text/ng-template" id="remesas-dialog">
        <div class="ngdialog-message">
            <br>
            <br>
            <table class="table table-bordered" style="width: 245px; margin: 0 auto">
                <tr>
                    <th colspan="3" class="bg-primary text-center">
                        REMESAS
                    </th>
                </tr>
                <tr>
                    <th style="width: 25px" class="text-center">
                        #
                    </th>
                    <th style="width: 100px" class="text-center">
                        Fecha
                    </th>
                    <th style="width: 120px" class="text-center">
                        Monto
                    </th>
                </tr>
                <tr ng-repeat="row in consignments track by $index">
                    <td class="text-center">
                        @{{ $index + 1 }}
                    </td>
                    <td class="text-center">
                        @{{row.pivot.date}}
                    </td>
                    <td class="text-right">
                        @{{row.pivot.amount}}
                    </td>
                </tr>
            </table>
            <br>
            <div class="row">
                <div class="col-sm-9"></div>
                <div class="col-sm-3">
                    <a href="#" class="btn btn-sm btn-info" ng-click="setConsignment()">
                        Aceptar
                    </a>
                </div>
            </div>
        </div>
    </script>
@endif
