<h3>
    Caja
    <small>Ingresos</small>
</h3>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-info" ng-class="{'whirl standard' : loading}">
            <div class="panel-heading">
                <h4>Ingresos
                    <a class="pull-right" style="color: white" ui-sref="app.cash-desk-details">
                        <i class="icon-action-undo"></i> Volver
                    </a>
                </h4>
            </div>
            <div class="panel-body" ng-hide="panel== 'Create' || panel == 'Edit' ">
                <div class="row">
                    <div class="col-sm-12">
                        <a href="#" ng-click="setPanel('Create')" class="btn btn-info pull-right"
                           style="font-weight: 900">
                            <i class="icon-pencil"></i>
                            Registrar
                        </a>
                    </div>
                </div>
                <br>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr class="bg-success">
                            <th class="text-center" style="width: 25px; color: white">#</th>
                            <th class="text-center" style="width: 100px; color: white">Tipo</th>
                            <th class="text-center" style="width: 100px; color: white">Fecha</th>
                            <th class="text-center" style="width: 100px; color: white">Monto</th>
                            <th class="text-center" style="color: white">Descripción</th>
                            <th class="text-center" style="width: 120px; color: white"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="row in incomes track by $index">
                            <td class="text-center">
                                @{{$index + 1}}
                            </td>
                            <td>
                                @{{row.type }}
                            </td>
                            <td>
                                @{{ row.created_at }}
                            </td>
                            <td class="text-right">
                                @{{ row.amount }}
                            </td>
                            <td>
                                @{{ row.description }}
                            </td>
                            <td>
                                <a href="#" ng-click="setEdit(row)" class="btn btn-warning">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="#" ng-click="deleteIncome(row)" class="btn btn-danger">
                                    <i class="fa fa-remove"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center" colspan="3">
                                Total
                            </th>
                            <th class="text-right">
                                @{{total_incomes | number:2}}
                            </th>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel-body" ng-show="panel == 'Create'">
                <div class="row">
                    <div class="col-sm-2">
                        {!!Field::select('type',config('options.crediperu_options'),'',
                        ['required'=>'required',
                           'ng-model'=>'income.type', 'chosen'=>''],['required'],'IncomeForm' )!!}
                    </div>
                    <div class="col-sm-2">
                        {!! Field::text('amount','',['placeholder'=>'0.00','class'=>'text-right','required'=>'required',
                    'ng-model'=>'income.amount', 'decimals'=>'2'],['required'],'loanForm')!!}
                    </div>
                    <div class="col-sm-2">
                        <label for="type_method">Cuenta</label>
                        <select name="type_method" id="type_method" class="form-control" chosen="chosen" ng-model="income.account_id">
                            <option value="">Cuenta</option>
                            <option value="1">Efectivo</option>
                            <option value="2">BVVA</option>
                            <option value="3">BCP</option>
                            <option value="4">BCP DOLARES</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        {!! Field::text('description','',[
                     'ng-model'=>'income.description', 'placeholder'=>'Descripción'],[],'loanForm')!!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <a href="#" class="btn btn-info pull-right" ng-click="saveIncome()">
                            Guardar
                        </a>
                        <a style="margin-right: 5px" href="" class="btn btn-warning pull-right" ng-click="setPanel('')">
                            Cancelar
                        </a>
                    </div>
                </div>
            </div>

            <div class="panel-body" ng-show="panel == 'Edit'">
                <div class="row">
                    <div class="col-sm-2">
                        {!!Field::select('type',config('options.crediperu_options'),'',
                        ['required'=>'required',
                           'ng-model'=>'income.type', 'chosen'=>''],['required'],'IncomeForm' )!!}
                    </div>
                    <div class="col-sm-2">
                        {!! Field::text('amount','',['placeholder'=>'0.00','class'=>'text-right','required'=>'required',
                    'ng-model'=>'income.amount', 'decimals'=>'2'],['required'],'loanForm')!!}
                    </div>
                    <div class="col-sm-2">
                        <label for="type_method">Cuenta</label>
                        <select name="type_method" id="type_method" class="form-control" chosen="chosen" ng-model="income.account_id">
                            <option value="">Cuenta</option>
                            <option value="1">Efectivo</option>
                            <option value="2">BVVA</option>
                            <option value="3">BCP</option>
                            <option value="4">BCP DOLARES</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        {!! Field::text('description','',[
                     'ng-model'=>'income.description'],[],'loanForm')!!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <a href="#" class="btn btn-info pull-right" ng-click="editIncome()">
                            Guardar
                        </a>
                        <a style="margin-right: 5px" href="#" class="btn btn-warning pull-right"
                           ng-click="setPanel('')">
                            Cancelar
                        </a>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
