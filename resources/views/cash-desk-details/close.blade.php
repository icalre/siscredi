<h3>
    Caja
    <small>Cerrar Caja</small>
</h3>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-info" ng-class="{'whirl standard' : loading}">
            <div class="panel-heading">
                <h4>Cerrar Caja
                    <a  class="pull-right" style="color: white" ui-sref="app.cash-desk-details">
                        <i class="icon-action-undo"></i> Volver
                    </a>
                </h4>
            </div>
            <div class="panel-body">
                <div class="pull-right" ng-show="consignments.length > 0">
                    <a href="#" ng-click="openDialogR()">
                        <em class="fa fa-exclamation-circle fa-2x text-danger"></em>
                    </a>
                </div>
                <br>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Monto Inicial <span class="pull-right">(+)</span></th>
                            <th style="width: 200px" class="text-right">@{{cashDeskDetail.initial_amount}}</th>
                        </tr>
                        <tr>
                            <th><a ui-sref="app.cash-desk-details-deposits">Pago de Cuotas</a> <span class="pull-right">(+)</span></th>
                            <th style="width: 200px" class="text-right">@{{cashDeskDetail.deposit_amount}}</th>
                        </tr>
                        <tr>
                            <th><a ui-sref="app.cash-desk-details-incomes">Ingresos</a> <span class="pull-right">(+)</span></th>
                            <th style="width: 200px" class="text-right">@{{cashDeskDetail.income_amount}}</th>
                        </tr>
                        <tr>
                            <th><a ui-sref="app.cash-desk-details-disbursements">Desembolsos</a><span class="pull-right">(-)</span></th>
                            <th style="width: 200px" class="text-right">@{{cashDeskDetail.disbursement_amount}}</th>
                        </tr>
                        <tr>
                            <th><a ui-sref="app.cash-desk-details-expenses">Gastos</a><span class="pull-right">(-)</span></th>
                            <th style="width: 200px" class="text-right">@{{cashDeskDetail.expenses_amount}}</th>
                        </tr>
                        <tr style="border-bottom: 2px #000000 solid">
                            <th><a ui-sref="app.cash-desk-details-expenses-g">Egresos</a><span class="pull-right">(-)</span></th>
                            <th style="width: 200px" class="text-right">@{{cashDeskDetail.expenses_amount_g}}</th>
                        </tr>
                        <tr>
                            <th>Total</th>
                            <th style="width: 200px" class="text-right">@{{cashDeskDetail.total_amount | number:2}}</th>
                        </tr>
                        <tr>
                            <th>Arqueo</th>
                            <th style="width: 200px" class="text-right">
                                <div class="form-group">
                                    <input type="text" class="form-control text-right"
                                           placeholder="0.00"
                                           ng-model="cashDeskDetail.arching"
                                           ng-model-options="{debounce: 1000}"
                                           decimals="2"
                                           ng-change="difference()"/>
                                </div>
                            </th>
                        </tr>
                        <tr style="border-bottom: 2px #000000 solid">
                            <th>Diferencia</th>
                            <th style="width: 200px" class="text-right">@{{cashDeskDetail.difference | number:2}}</th>
                        </tr>
                        <tr>
                            <th>Saldo Efectivo</th>
                            <th style="width: 200px" class="text-right">@{{cashDeskDetail.efectivo}}</th>
                        </tr>
                        <tr>
                            <th>Saldo Cuenta Corriente</th>
                            <th style="width: 200px" class="text-right">@{{cashDeskDetail.cuenta_corriente | number:2}}</th>
                        </tr>

                    </table>
                    <br/>
                    <br/>
                    <a href="#" class="btn btn-info" ng-click="close()">Cerrar Caja</a>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/ng-template" id="remesas-dialog">
    <div class="ngdialog-message">
        <br>
        <br>
        <table class="table table-bordered" style="width: 245px; margin: 0 auto">
            <tr>
                <th colspan="3" class="bg-primary text-center">
                    REMESAS
                </th>
            </tr>
            <tr>
                <th style="width: 25px" class="text-center">
                    #
                </th>
                <th style="width: 100px" class="text-center">
                    Fecha
                </th>
                <th style="width: 120px" class="text-center">
                    Monto
                </th>
            </tr>
            <tr ng-repeat="row in consignments track by $index">
                <td class="text-center">
                    @{{ $index + 1 }}
                </td>
                <td class="text-center">
                    @{{row.pivot.date}}
                </td>
                <td class="text-right">
                    @{{row.pivot.amount}}
                </td>
            </tr>
        </table>
        <br>
        <div class="row">
            <div class="col-sm-9"></div>
            <div class="col-sm-3">
                <a href="#" class="btn btn-sm btn-info" ng-click="setConsignment()">
                    Aceptar
                </a>
            </div>
        </div>
    </div>
</script>