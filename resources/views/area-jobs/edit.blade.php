<h3>
    Editar Area
    <small>Edición de Area</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="areaJobForm" class="form-validate" ng-enter="preventEnterForm()" role="form" ng-submit="areaJobForm.$valid && editAreaJob()" novalidate>
        <div class="panel panel-default " ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('area-jobs.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.area-jobs" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </form>
    </div>
</div>