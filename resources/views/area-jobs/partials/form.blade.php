    {!!Form::token()!!}
    <div class="row">
        <div class="col-sm-4">
            {!! Field::select('area_id',[],'',['required'=>'required',
                        'chosen'=>'chosen',
                            'ng-model'=>'areaJob.area',
                            'ng-options'=>"item.name for item in areas track by item.id"],['required'],'areaJobForm' )!!}
        </div>
        <div class="col-sm-4">
            {!! Field::text('name','',['placeholder'=>'Ingrese un nombre','required'=>'required',
                'ng-model'=>'areaJob.name'],['required'],'areaJobForm')!!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            {!! Field::text('description', '',['placeholder'=>'Ingrese una descripción','required'=>'required',
                'ng-model'=>'areaJob.description'],['required'],'areaJobForm') !!}
        </div>
    </div>
