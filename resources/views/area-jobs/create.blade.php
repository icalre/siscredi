<h3>
    Registrar Perfil
    <small>Registro de Perfiles</small>
</h3>
<div class="row">
    <div class="col-sm-12">
        <form name="areaJobForm" class="form-validate" role="form" ng-submit="areaJobForm.$valid && createAreaJob()" novalidate>
        <div class="panel panel-default" ng-class="{'whirl standard' : loading}">
            <div class="panel-body">
                @include('area-jobs.partials.form')
            </div>
            <div class="panel-footer">
                <a ui-sref="app.area-jobs" class="btn btn-warning">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </form>
    </div>
</div>