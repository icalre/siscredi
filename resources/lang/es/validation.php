<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'El campo :attribute debe ser aceptado.',
    'active_url'           => 'El campo :attribute no es una URL válida.',
    'after'                => 'El campo :attribute debe ser una fecha posterior a :date.',
    'alpha'                => 'El campo :attribute sólo puede contener letras.',
    'alpha_dash'           => 'El campo :attribute sólo puede contener letras, números y guiones (a-z, 0-9, -_).',
    'alpha_num'            => 'El campo :attribute sólo puede contener letras y números.',
    'array'                => 'El campo :attribute debe ser un array.',
    'before'               => 'El campo :attribute debe ser una fecha anterior a :date.',
    'between'              => [
        'numeric' => 'El campo :attribute debe ser un valor entre :min y :max.',
        'file'    => 'El archivo :attribute debe pesar entre :min y :max kilobytes.',
        'string'  => 'El campo :attribute debe contener entre :min y :max caracteres.',
        'array'   => 'El campo :attribute debe contener entre :min y :max elementos.',
    ],
    'boolean'              => 'El campo :attribute debe ser verdadero o falso.',
    'confirmed'            => 'El campo confirmación de :attribute no coincide.',
    'date'                 => 'El campo :attribute no corresponde con una fecha válida.',
    'date_format'          => 'El campo :attribute no corresponde con el formato de fecha :format.',
    'different'            => 'Los campos :attribute y :other han de ser diferentes.',
    'digits'               => 'El campo :attribute debe ser un número de :digits dígitos.',
    'digits_between'       => 'El campo :attribute debe contener entre :min y :max dígitos.',
    'email'                => 'El campo :attribute no corresponde con una dirección de e-mail válida.',
    'filled'               => 'El campo :attribute es obligatorio.',
    'exists'               => 'El campo :attribute no existe.',
    'image'                => 'El campo :attribute debe ser una imagen.',
    'in'                   => 'El campo :attribute debe ser igual a alguno de estos valores :values',
    'integer'              => 'El campo :attribute debe ser un número entero.',
    'ip'                   => 'El campo :attribute debe ser una dirección IP válida.',
    'json'                 => 'El campo :attribute debe ser una cadena de texto JSON válida.',
    'max'                  => [
        'numeric' => 'El campo :attribute debe ser :max como máximo.',
        'file'    => 'El archivo :attribute debe pesar :max kilobytes como máximo.',
        'string'  => 'El campo :attribute debe contener :max caracteres como máximo.',
        'array'   => 'El campo :attribute debe contener :max elementos como máximo.',
    ],
    'mimes'                => 'El campo :attribute debe ser un archivo de tipo :values.',
    'min'                  => [
        'numeric' => 'El campo :attribute debe tener al menos :min.',
        'file'    => 'El archivo :attribute debe pesar al menos :min kilobytes.',
        'string'  => 'El campo :attribute debe contener al menos :min caracteres.',
        'array'   => 'El campo :attribute no debe contener más de :min elementos.',
    ],
    'not_in'               => 'El campo :attribute seleccionado es invalido.',
    'numeric'              => 'El campo :attribute debe ser un numero.',
    'regex'                => 'El formato del campo :attribute es inválido.',
    'required'             => 'El campo :attribute es obligatorio',
    'required_if'          => 'El campo :attribute es obligatorio cuando el campo :other es :value.',
    'required_with'        => 'El campo :attribute es obligatorio cuando :values está presente.',
    'required_with_all'    => 'El campo :attribute es obligatorio cuando :values está presente.',
    'required_without'     => 'El campo :attribute es obligatorio cuando :values no está presente.',
    'required_without_all' => 'El campo :attribute es obligatorio cuando ningún campo :values están presentes.',
    'same'                 => 'Los campos :attribute y :other deben coincidir.',
    'size'                 => [
        'numeric' => 'El campo :attribute debe ser :size.',
        'file'    => 'El archivo :attribute debe pesar :size kilobytes.',
        'string'  => 'El campo :attribute debe contener :size caracteres.',
        'array'   => 'El campo :attribute debe contener :size elementos.',
    ],
    'string'               => 'El campo :attribute debe contener solo caracteres.',
    'timezone'             => 'El campo :attribute debe contener una zona válida.',
    'unique'               => 'El elemento :attribute ya está en uso.',
    'url'                  => 'El formato de :attribute no corresponde con el de una URL válida.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => array(
        'name' => 'nombre',
        'last_name'=>'apellidos',
        'email'     => 'correo Electronico',
        'password'  => 'contraseña',
        'password_confirmation' => 'repita su Contraseña',
        'profile_id' => 'perfil',
        'telephone'=>'telefono',
        'department'=>'departamento',
        'province'=>'provincia',
        'district'=>'distrito',
        'date_of_birth'=>'fecha de Nacimiento',
        'address'=>'dirección',
        'department_id'=> 'Departamento',
        'province_id'=>'Provincia',
        'district_id'=>'Distrito',
        'description'=>'Descripción',
        'user'=>'usuario',
        'state'=>'estado',
        'gender'=>'sexo',
        'person_id'=>'persona',
        'ruc'=>'RUC',
        'dni'=>'DNI',
        'razon_social'=>'razon Social',
        'nombre_comercial'=>'nombre Comercial',
        'website'=>'sitio Web',
        'customer_type'=>'tipo de Cliente',
        'area_id'=>'area',
        'area'=>'area',
        'job'=>'cargo',
        'sites'=>'locales',
        'site_id'=>'local',
        'alias'=>'alias',
        'unit_id'=>'unidad de Medida',
        'insumo_type_id'=>'tipo de Insumo',
        'product_type_id'=>'tipo de Producto',
        'stock_option'=>'control de Stock',
        'recipe_option'=>'receta',
        'commercial_permission'=>'permiso Publicitario',
        'health_license'=>'licencia Sanitaria',
        'operating_license'=>'licencia de Funcionamiento',
        'permit_fumigation'=>'Permiso de Fumigacion',
        'supplier_type'=>'Tipo de Proveedor',
        'department_store_id'=>'almacenes',
        'amount_total'=>'Monto Total',
        'document_type_id'=>'tipo de Documento',
        'serial'=>'serie',
        'number'=>'número',
        'taxable_amount'=>'base Imponible',
        'sub_total'=>'sub Total',
        'igv'=>'IGV',
        'payment_state'=>'estado',
        'taxable_option'=>'Op. Gravada',
        'detraccion'=>'detraccion',
        'percepcion'=>'percepcion',
        'retencion'=>'retencion',
        'otros'=>'otros',
        'purchase_type'=>'tipo de Compra',
        'purchase_expiration'=>'Fecha de Pago',
        'date_of_issue'=>'fecha de Emisión',
        'date_of_cancellation'=>'fecha de Cancelado',
        'amount'=>'monto',
        'expiration'=>'Vencimiento',
        'site_supplier_id'=>'local',
        'lot_option'=>'Lotes',
        'lot_quantity'=>'Cant / Unid',
        'date_start'=>'fecha de Inicio',
        'state_sell'=>'venta',
        'combination_option'=>'combinacion',
        'flavors_option'=>'sabores',
        'flavors_quantity'=>'Cantidad Sabores',
        'date_end'=>'fecha Fin',
        'days'=>'Dias',
        'taxable_amount_igv'=>'BI + IGV',
        'no_taxable_amount'=>'Op. no gravadas',
        'nro_constancia_detraccion'=>'Nro. Const. Detrac',
        'date_constancia_detraccion'=>'F. E. Const. Det.',
        'exchange_rate'=>'Tipo de Cambio',
        'lounge_id'=>'salon',
        'cash_desk_id'=>'caja',
        'initial_amount'=>'Monto Inicial',
        'printer_name'=>'Impresora Nombre',
        'printer_option'=>'Impresora',
        'administrative_option'=>'codigo Administrativo',
        'code'=>'codigo',
        'type'=>'tipo',
        'purchase_code'=>'Codigo de Compra',
        'employee_id'=>'Empleado',
        'discounts'=>'Descuentos',
        'amount_pay'=>'Monto a Pagar',
        'screen_option'=>'pantalla',
        'refer_id'=>'referido por',
        'old-password'=>'contraseña Anterior',
        'salary_amount'=>'Sueldo',
        'hours_day'=>'Hora / Día',
        'salary_day'=>'P. Día',
        'salary_hour'=>'P. Hora',
        'turn'=>'Turno',
        'purchase_description'=>  'concepto',
        'icon'=>'Icono',
        'sub_module_name'=>'nombre de Sub Modulo',
        'uri'=>'uri',
        'company_id'=>'Compañia',
        'job_id'=>'Cargo',
        'area_job_id'=>'Cargo',
        'customer_id'=>'Cliente',
        'color'=>'Color',
        'interest'=>'Costo Efectivo',
        'type_payment'=>'Cuotas',
        'currency'=>'Moneda',
        'period'=>'Periodo',
        'quotas'=>'Cuotas',
        'observations'=>'Observaciones',
        'endorsement_option'=>'Aval',
        'warranty_option'=>'Garantía',
        'reference_price'=>'tasación',
        'pb'=>'Peso Bruto',
        'pn'=>'Peso Neto',
        'contract_option'=>'Activar Contratos',
        'contract_number'=>'Numero de Contrato',
        'file'=>'Fail',
        'business_address'=>'Dirección Negocio/Trabajo',
        'phone_reference'=>'Telefono Referencia',
        'activity'=>'Actividad Economica',
        'modalidad'=>'Modalidad',
        'enterprise_id'=>'Empresa',
        'asesor_id'=>'Asesor',
        'feed_option'=>'Pro-Rateo',
        'customers'=>'Clientes',
        'sugerencia'=>'Sugerencias',
        'corriente'=>'Cuenta Corriente',
        'effective_state'=>'Previsión',
        'loan_id'=>'Código de Prestamo'
    ),

];
