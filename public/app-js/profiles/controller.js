/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('profiles.controllers',[])
        .controller('ProfileController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert){
                    $scope.paginate = {};
                    $scope.profiles = []
                    $scope.profile = {}
                    $scope.loading = false

                    var id = $stateParams.id;


                    $scope.loadPage = function(page){
                        globalService.getter('app/profiles/all?page='+page).then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.profiles = data.data;
                            $scope.track = Number($scope.paginate.per_page)*(Number($scope.paginate.current_page) - 1);
                        });
                    }
                    $scope.nextPage = function() {
                        if ($scope.paginate.current_page < $scope.paginate.last_page) {
                            $scope.paginate.current_page++;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.previousPage = function() {
                        if ($scope.paginate.current_page > 1) {
                            $scope.paginate.current_page --;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.allPeople = function () {
                        globalService.getter('app/profiles/all').then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.profiles = data.data;
                        });
                    }

                    if(id || $location.url() == '/app/profiles-create')
                    {

                        if(id){
                            globalService.getter('app/profiles/find/'+id).then(function (data) {
                                $scope.profile = data;
                            });
                        }
                    }else{
                        $scope.message = globalService.getMessage();
                        $scope.allPeople();
                    }


                    $scope.createProfile = function(){
                        $scope.loading = true
                        globalService.poster('app/profiles-create', $scope.profile).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.profiles');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.editProfile = function(){
                        $scope.loading = true
                        globalService.putter('app/profiles-edit/'+id, $scope.profile).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage(data.data + ' - Se modifico correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.profiles');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.deleteProfile = function(row){
                        var r = confirm('Estas Seguro de Eliminar este registro');
                        if(r === true)
                        {
                            globalService.deleter('app/profiles-delete/'+row.id).then(
                                function(data){
                                    if(data.response){
                                        globalService.setMessage(data.data + ' - Se elimino correctamente','success');
                                        $scope.allPeoples()
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                    $scope.preventEnterForm = function () {
                        return false;
                    }


                    $scope.searchProfile = function(){
                        if($scope.profile.search.length >= 3){
                            globalService.getter('app/api/profiles?q='+$scope.profile.search).then(function (data) {
                                $scope.profiles = data;
                                $scope.paginate = {};
                            });
                        }else if($scope.profile.search.length == 0){
                            $scope.loadPage(1);
                        }
                    }
                }
            ]
        );
})();
