/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('customers.controllers',[])
        .controller('CustomerController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert){
                    $scope.paginate = {};
                    $scope.customers = []
                    $scope.customer = {}
                    $scope.loading = false
                    $scope.page_correlative = 25;
                    $scope.sum_cor_page = 0;
                    $scope.customer.rate = 'Bueno'

                    var id = $stateParams.id;


                    $scope.loadPage = function(page){

                        var type = '';
                        var rate = '';
                        if($scope.customer.type)
                        {
                            type = $scope.customer.type;
                        }

                        if($scope.customer.rate_filter)
                        {
                            rate = $scope.customer.rate_filter;
                        }

                        globalService.getter('app/customers/all?type='+type+'&rate='+rate+'&page='+page).
                            then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.customers = data.data;
                            $scope.track = Number($scope.paginate.per_page)*(Number($scope.paginate.current_page) - 1);
                            if(page >  1)
                            {
                                $scope.sum_cor_page = Number($scope.page_correlative) *(Number(page) - 1)
                            }

                            console.log($scope.sum_cor_page)
                        });
                    }
                    $scope.nextPage = function() {
                        if ($scope.paginate.current_page < $scope.paginate.last_page) {
                            $scope.paginate.current_page++;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.previousPage = function() {
                        if ($scope.paginate.current_page > 1) {
                            $scope.paginate.current_page --;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.allPeople = function () {
                        globalService.getter('app/customers/all').then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.customers = data.data;
                        });
                    }

                    if(id || $location.url() == '/app/customers-create')
                    {

                        if(id){
                            globalService.getter('app/customers/find/'+id).then(function (data) {
                                $scope.customer = data;
                            });
                        }
                    }else{
                        $scope.message = globalService.getMessage();
                        $scope.allPeople();
                    }


                    $scope.createCustomer = function(){
                        $scope.loading = true
                        globalService.poster('app/customers-create', $scope.customer).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.customers');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.editCustomer = function(){
                        $scope.loading = true
                        globalService.putter('app/customers-edit/'+id, $scope.customer).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage(data.data + ' - Se modifico correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.customers');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.deleteCustomer = function(row){
                        var r = confirm('Estas Seguro de Eliminar este registro');
                        if(r === true)
                        {
                            globalService.deleter('app/customers-delete/'+row.id).then(
                                function(data){
                                    if(data.response){
                                        globalService.setMessage(data.data + ' - Se elimino correctamente','success');
                                        $scope.allPeoples()
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                    $scope.preventEnterForm = function () {
                        return false;
                    }

                    $scope.filterCustomer = function(){
                            $scope.loadPage(1);
                    }


                    $scope.searchCustomer = function(){

                        var type = '';
                        if($scope.customer.type)
                        {
                            type = $scope.customer.type;
                        }


                        if($scope.customer.search.length >= 3){
                            globalService.getter('app/api/customers?type='+type+'&q='+$scope.customer.search).then(function (data) {
                                $scope.customers = data;
                                $scope.paginate = {};
                            });
                        }else if($scope.customer.search.length == 0){
                            $scope.loadPage(1);
                        }
                    }
                }
            ]
        );
})();
