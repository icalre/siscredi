/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('business.controllers',[])
        .controller('BusinessController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert','$window','ngDialog',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert,$window,ngDialog){
                    $scope.panel_selected = ''
                    $scope.loan = {}
                    $scope.person = {}
                    $scope.people = []
                    $scope.loan_simulate = {}
                    $scope.employees = []
                    $scope.employee = {}
                    $scope.date1 = ''
                    $scope.date2 = ''
                    $scope.enterprise = {}

                    $scope.enterprise_loans = []

                    $scope.otros = {tasacion: 0}
                    $scope.artefactos = {tasacion: 0}
                    $scope.joyas = {pb:0 , pn:0 , tasacion: 0}

                    $scope.loan.joyasA = []
                    $scope.loan.artefactosA = []
                    $scope.loan.otrosA = []

                    $scope.setDateDisbursement = function(){
                        $scope.loan_simulate.date_disbursement = $scope.loan_simulate.date_start;
                    }

                    $scope.setPanel = function (panel) {
                        $scope.panel_selected = panel
                    }

                    globalService.getter('app/api/employees').then(function (data) {
                        $scope.employees = data;
                        $scope.paginate = {};
                    });

                    $scope.searchCustomers = function () {
                        $scope.people = []
                        globalService.getter('app/api/people?q='+$scope.person.q).then(function (data) {
                            $scope.people = data;
                            $scope.paginate = {};
                        });
                    }

                    $scope.setCustomer = function (row) {
                        $scope.people = []
                        $scope.person = row
                    }

                    $scope.total = function () {

                        $scope.joyas.pb = 0 ;
                        $scope.joyas.pn = 0 ;
                        $scope.joyas.tasacion = 0 ;
                        $scope.artefactos.tasacion = 0;
                        $scope.otros.tasacion = 0;

                        $scope.loan.joyasA.filter(
                            function (row) {
                                $scope.joyas.pb = Number($scope.joyas.pb) + Number(row.pb) ;
                                $scope.joyas.pn = Number($scope.joyas.pn) + Number(row.pn) ;
                                $scope.joyas.tasacion = Number($scope.joyas.tasacion) + Number(row.reference_price) ;
                            }
                        )

                        $scope.loan.artefactosA.filter(
                            function (row) {
                                $scope.artefactos.tasacion = Number($scope.artefactos.tasacion) + Number(row.reference_price) ;
                            }
                        )

                        $scope.loan.otrosA.filter(
                            function (row) {
                                $scope.otros.tasacion = Number($scope.otros.tasacion) + Number(row.reference_price) ;
                            }
                        )
                    }

                    $scope.dialogOpen = function (row) {
                        $scope.dialog =  ngDialog.open({
                            template: 'dialogWithNestedConfirmDialogId',
                            className: 'ngdialog-theme-default',
                            scope: $scope
                        })
                    }

                    $scope.dialogCommentsOpen = function (row) {
                        $scope.dialog =  ngDialog.open({
                            template: 'dialogCommentsId',
                            className: 'ngdialog-theme-default',
                            scope: $scope
                        })
                    }

                    $scope.setWeekQuota = function()
                    {
                        $scope.payments = [];
                        if($scope.loan_simulate.period == 'Semanal')
                        {
                            if(Number($scope.loan_simulate.quotas) > 0)
                            {
                                var valid = Number($scope.loan_simulate.quotas) % 4;

                                if(valid > 0)
                                {
                                    alert('El numero de cuotas tiene que se multiplo de 4, 4-8-12');
                                    $scope.loan_simulate.quotas = '';
                                }
                            }
                        }
                    }

                    $scope.searchLoan = function () {

                        globalService.getter('app/business/loan/'+$scope.loan.code).then(function (data) {
                            $scope.loan = data;
                            $scope.loan.joyasA = []
                            $scope.loan.artefactosA = []
                            $scope.loan.otrosA = []
                            var i = 0;
                            var sum = 0;
                            var flag_score = 0;


                            $scope.loan.payments.filter(function(data){

                                if(data.state == 'Pagado' && data.score < 0 && flag_score == 0)
                                {
                                    flag_score++;
                                    sum = sum + Number(0);
                                }

                                if(data.state == 'Pagado' && data.score >= 0)
                                {
                                    i++;
                                    sum = sum + Number(data.score);
                                    flag_score = 0;
                                }
                            });

                            $scope.score = Number(sum) / Number(i);


                            $scope.loan.warranties.filter(function (item) {

                                if(item.type == 'Artefacto')
                                {
                                    $scope.loan.artefactosA.push(item)
                                }else  if(item.type == 'Otro')
                                {
                                    $scope.loan.otrosA.push(item)
                                }else  if(item.type == 'Joya')
                                {
                                    $scope.loan.joyasA.push(item)

                                }
                            })

                            if($scope.loan.joyasA.length > 0)
                            {
                                $scope.loan.joyas = true
                            }

                            if($scope.loan.otrosA.length > 0)
                            {
                                $scope.loan.otros = true
                            }

                            if($scope.loan.artefactosA.length > 0)
                            {
                                $scope.loan.artefactos = true
                            }


                            $scope.total();
                        })
                    }

                    $scope.setLoan = function (item) {
                        $scope.loan = item
                        $scope.searchLoan()
                        $scope.setPanel('Loan')
                    }

                    $scope.generateSchedule = function(){
                        if($scope.generateSheduleForm.$valid){
                            globalService.poster('app/api/generate-schedule',$scope.loan_simulate).then(function (data) {
                                if(data.response)
                                {
                                    $scope.loan_simulate.payments = data.quotas;
                                }
                            });
                        }else{
                            SweetAlert.swal('ERROR', 'No has completado los campos requeridos', 'error');
                        }
                    }

                    $scope.printSimulated = function () {

                        globalService.poster('app/business/print-simulated-schedule',$scope.loan_simulate).then(function (data) {
                            if(data.response)
                            {
                                $scope.loan_simulate.code = data.code;
                                $window.open('app/business/print-simulated-schedule?loan-id='+$scope.loan_simulate.code)
                            }
                        });


                    }

                    $scope.searchLoanEnterprise = function (selected) {
                        if(selected)
                        {
                            globalService.getter('app/business/enterprise-loans?id='+selected.originalObject.id).then(
                                function (data) {
                                    $scope.enterprise_loans = data.loans
                                }
                            )
                        }else {
                            console.log('cleared');
                        }
                    }

                }
            ]
        )


})();
