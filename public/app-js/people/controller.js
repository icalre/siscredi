/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('people.controllers',[])
        .controller('PersonController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert){
                    $scope.paginate = {};
                    $scope.people = [];
                    $scope.person = {};
                    $scope.departments = [];
                    $scope.loading = false;
                    $scope.genders = [
                        {id: 1, value: 'Male', name: 'Hombre'},
                        {id: 2, value: 'Female', name: 'Mujer'}
                    ];

                    var id = $stateParams.id;

                    $scope.consultReniec = function () {
                        globalService.getter('app/api/reniec?dni='+$scope.person.dni).then(function (data) {
                            if(data.response)
                            {
                                $scope.person.name = data.data.nombres;
                                $scope.person.last_name = data.data.apellidoPaterno+' '+data.data.apellidoMaterno;
                            }else {
                                alert('DNI no valido');
                            }
                        });
                    }


                    $scope.loadPage = function(page){
                        globalService.getter('app/people/all?page='+page).then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.people = data.data;
                            $scope.track = Number($scope.paginate.per_page)*(Number($scope.paginate.current_page) - 1);
                        });
                    }
                    $scope.nextPage = function() {
                        if ($scope.paginate.current_page < $scope.paginate.last_page) {
                            $scope.paginate.current_page++;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.previousPage = function() {
                        if ($scope.paginate.current_page > 1) {
                            $scope.paginate.current_page --;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.allPeople = function () {
                        globalService.getter('app/people/all').then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.people = data.data;
                        });
                    }

                    if(id || $location.url() == '/app/people-create')
                    {
                        globalService.getter('app/api/departments').then(function (data) {
                            $scope.departments = data;
                        });

                        if(id){
                            globalService.getter('app/people/find/'+id).then(function (data) {
                                $scope.person = data;
                                $scope.genders.filter(function (row) {
                                    if(row.value == $scope.person.gender)
                                    {
                                        $scope.person.gender = row
                                    }
                                })
                            });
                        }
                    }else{
                        $scope.message = globalService.getMessage();
                        $scope.allPeople();
                    }


                    $scope.createPerson = function(){
                        $scope.loading = true
                        globalService.poster('app/people-create', $scope.person).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.people');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.editPerson = function(){
                        $scope.loading = true
                        globalService.putter('app/people-edit/'+id, $scope.person).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage(data.data + ' - Se modifico correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.people');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.deletePerson = function(row){
                        var r = confirm('Estas Seguro de Eliminar este registro');
                        if(r === true)
                        {
                            globalService.deleter('app/people-delete/'+row.id).then(
                                function(data){
                                    if(data.response){
                                        globalService.setMessage(data.data + ' - Se elimino correctamente','success');
                                        $scope.allPeoples()
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                    $scope.preventEnterForm = function () {
                        return false;
                    }

                    $scope.resetP = function () {
                        $scope.person.province = '';
                    }

                    $scope.resetD = function () {
                        $scope.person.district = '';
                    }

                    $scope.searchPerson = function(){
                        if($scope.person.search.length >= 3){
                            globalService.getter('app/api/people?q='+$scope.person.search).then(function (data) {
                                $scope.people = data;
                                $scope.paginate = {};
                            });
                        }else if($scope.person.search.length == 0){
                            $scope.loadPage(1);
                        }
                    }
                }
            ]
        );
})();
