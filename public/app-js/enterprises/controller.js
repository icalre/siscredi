/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('enterprises.controllers',[])
        .controller('EnterpriseController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert){
                    $scope.paginate = {};
                    $scope.enterprises = []
                    $scope.departments = []
                    $scope.enterprise = {}
                    $scope.loading = false
                    $scope.enterprise.people = []
                    $scope.myImage        = '';
                    $scope.myCroppedImage = '';
                    $scope.imgcropType    = 'circle';

                    var id = $stateParams.id;

                    $scope.addPerson = function () {
                        var item = {}

                        $scope.enterprise.people.push(item);
                    }

                    $scope.removePerson = function (index) {
                        $scope.enterprise.people.splice(index,1);
                    }


                    $scope.loadPage = function(page){
                        globalService.getter('app/enterprises/all?page='+page).then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.enterprises = data.data;
                            $scope.track = Number($scope.paginate.per_page)*(Number($scope.paginate.current_page) - 1);
                        });
                    }
                    $scope.nextPage = function() {
                        if ($scope.paginate.current_page < $scope.paginate.last_page) {
                            $scope.paginate.current_page++;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.previousPage = function() {
                        if ($scope.paginate.current_page > 1) {
                            $scope.paginate.current_page --;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.allPeople = function () {
                        globalService.getter('app/enterprises/all').then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.enterprises = data.data;
                        });
                    }

                    if(id || $location.url() == '/app/enterprises-create')
                    {
                        globalService.getter('app/api/departments').then(function (data) {
                            $scope.departments = data;
                        });

                        if(id){
                            globalService.getter('app/enterprises/find/'+id).then(function (data) {
                                $scope.enterprise = data;
                                var array_temp = $scope.enterprise.people
                                $scope.enterprise.people = []
                                array_temp.filter(
                                    function (item) {

                                        var new_item = {
                                            person: item,
                                            cargo: item.pivot.cargo
                                        }

                                        $scope.enterprise.people.push(new_item)
                                    }
                                )
                            });
                        }
                    }else{
                        $scope.message = globalService.getMessage();
                        $scope.allPeople();
                    }


                    $scope.createEnterprise = function(){
                        $scope.loading = true
                        globalService.poster('app/enterprises-create', $scope.enterprise).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');

                                    if($scope.myImage == '')
                                    {
                                        SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                        $state.go('app.enterprises');
                                    }else{

                                        $scope.updatePhoto();
                                    }


                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.editEnterprise = function(){
                        $scope.loading = true
                        globalService.putter('app/enterprises-edit/'+id, $scope.enterprise).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage(data.data + ' - Se modifico correctamente', 'success');
                                    if($scope.myImage == '')
                                    {
                                        SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                        $state.go('app.enterprises');
                                    }else{

                                        $scope.updatePhoto();
                                    }
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.deleteEnterprise = function(row){
                        var r = confirm('Estas Seguro de Eliminar este registro');
                        if(r === true)
                        {
                            globalService.deleter('app/enterprises-delete/'+row.id).then(
                                function(data){
                                    if(data.response){
                                        globalService.setMessage(data.data + ' - Se elimino correctamente','success');
                                        $scope.allPeoples()
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                    $scope.preventEnterForm = function () {
                        return false;
                    }


                    $scope.searchEnterprise = function(){
                        if($scope.enterprise.search.length >= 3){
                            globalService.getter('app/api/enterprises?q='+$scope.enterprise.search).then(function (data) {
                                $scope.enterprises = data;
                                $scope.paginate = {};
                            });
                        }else if($scope.enterprise.search.length == 0){
                            $scope.loadPage(1);
                        }
                    }



                    //upload crop

                    $scope.reset = function() {
                        $scope.myImage        = '';
                        $scope.myCroppedImage = '';
                        $scope.imgcropType    = 'circle';
                    };

                    $scope.reset();

                    var handleFileSelect=function(evt) {
                        var file=evt.currentTarget.files[0];
                        var reader = new FileReader();
                        reader.onload = function (evt) {
                            $scope.$apply(function(/*$scope*/){
                                $scope.myImage=evt.target.result;
                            });
                        };
                        if(file)
                            reader.readAsDataURL(file);
                    };

                    angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);

                    $scope.base64ToBlob = function (base64Data, contentType) {
                        contentType = contentType || '';
                        var sliceSize = 1024;
                        var byteCharacters = atob(base64Data);
                        var bytesLength = byteCharacters.length;
                        var slicesCount = Math.ceil(bytesLength / sliceSize);
                        var byteArrays = new Array(slicesCount);

                        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
                            var begin = sliceIndex * sliceSize;
                            var end = Math.min(begin + sliceSize, bytesLength);

                            var bytes = new Array(end - begin);
                            for (var offset = begin, i = 0 ; offset < end; ++i, ++offset) {
                                bytes[i] = byteCharacters[offset].charCodeAt(0);
                            }
                            byteArrays[sliceIndex] = new Uint8Array(bytes);
                        }
                        return new Blob(byteArrays, { type: contentType });
                    }

                    $scope.updatePhoto = function () {

                        globalService.postFile('app/enterprises/upload-photo',$scope.base64ToBlob($scope.myCroppedImage.replace('data:image/png;base64,',''), 'image/jpeg')).then(
                            function (data) {
                                if(data.response)
                                {
                                    $scope.uploadPhotoForm = false

                                    $scope.reset();

                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.enterprises');
                                }else{
                                    $scope.errors = data;
                                }
                            }
                        )
                    }

                }
            ]
        );
})();
