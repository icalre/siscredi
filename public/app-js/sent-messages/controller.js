/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('sentMessages.controllers',[])
        .controller('SentMessageController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert){
                    $scope.paginate = {};
                    $scope.sentMessages = []
                    $scope.sentMessage = {}
                    $scope.loading = false
                    $scope.people = []
                    $scope.sentMessage.people = []
                    $scope.sentMessage.message = ''
                    $scope.types = [
                        {
                            value:1, name: 'SMS'
                        },
                        {
                            value: 2, name: 'WHATSAPP'
                        }
                    ];

                    var id = $stateParams.id;


                    $scope.loadPage = function(page){
                        var message = ''

                        if($scope.sentMessage.message.length >= 3)
                        {
                            message = $scope.sentMessage.message
                        }

                        globalService.getter('app/sent-messages/all?message='+message+'&page='+page).then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.sentMessages = data.data;
                            $scope.track = Number($scope.paginate.per_page)*(Number($scope.paginate.current_page) - 1);
                        });
                    }

                    $scope.nextPage = function() {
                        if ($scope.paginate.current_page < $scope.paginate.last_page) {
                            $scope.paginate.current_page++;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.previousPage = function() {
                        if ($scope.paginate.current_page > 1) {
                            $scope.paginate.current_page --;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.allPeople = function () {
                        globalService.getter('app/sent-messages/all').then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.sentMessages = data.data;
                        });
                    }

                    if(id || $location.url() == '/app/sent-messages-create')
                    {

                        if(id){
                            globalService.getter('app/sent-messages/find/'+id).then(function (data) {
                                $scope.sentMessage = data;
                            });
                        }else{
                            $scope.sentMessage.message = sessionStorage.getItem('message');
                        }
                    }else{
                        sessionStorage.setItem('message', '');
                        $scope.message = globalService.getMessage();
                        $scope.allPeople();
                    }

                    $scope.addPerson = function (selected) {

                        var new_item = {}
                        var exist = false

                        if(selected)
                        {
                            new_item = selected.originalObject
                            $scope.people.filter(
                                function (row) {
                                    if(row.id == new_item.id)
                                    {
                                        exist = true
                                        console.log('exist')
                                    }
                                }
                            )

                            if(!exist)
                            {
                                $scope.people.push(new_item)
                            }

                            $scope.$broadcast('angucomplete-alt:clearInput', 'person_customer');

                            if(!$scope.sentMessage.people)
                            {
                                $scope.sentMessage.people = []
                            }


                            $scope.sentMessage.people.filter(
                                function (item) {
                                    if(item.id == new_item.id)
                                    {
                                        alert('Ya se ha agregado a este cliente');
                                        return false
                                    }
                                }
                            )

                            $scope.sentMessage.people.push(new_item)
                        }



                    }


                    $scope.createSentMessage = function(){
                        $scope.loading = true
                        globalService.poster('app/sent-messages-create', $scope.sentMessage).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.sent-messages');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }




                    $scope.preventEnterForm = function () {
                        return false;
                    }


                    $scope.searchSentMessage = function(){
                        $scope.loadPage(1);
                    }

                    $scope.replicateMessage = function (row) {
                        sessionStorage.setItem('message', row.message);
                        $state.go('app.sent-messages-create');
                    }
                }
            ]
        );
})();
