/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('requestLoans.controllers',[])
        .controller('RequestLoanController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','ngDialog','SweetAlert',
                function($scope,$sce, $stateParams,$location,globalService, $state, ngDialog,SweetAlert){
                    $scope.paginate = {};
                    $scope.requestLoans = []
                    $scope.requestLoan = {}
                    $scope.loading = false
                    $scope.dialog = {}

                    var id = $stateParams.id;

                    $scope.setWeekQuota = function()
                    {
                        $scope.payments = [];
                        if($scope.requestLoan.period == 'Semanal')
                        {
                            if(Number($scope.requestLoan.quotas) > 0)
                            {
                                var valid = Number($scope.requestLoan.quotas) % 4;

                                if(valid > 0)
                                {
                                    alert('El numero de cuotas tiene que se multiplo de 4, 4-8-12');
                                    $scope.requestLoan.quotas = '';
                                }
                            }
                        }
                    }


                    $scope.loadPage = function(page){

                        globalService.getter('app/request-loans/all?page='+page).then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.requestLoans = data.data;
                            $scope.track = Number($scope.paginate.per_page)*(Number($scope.paginate.current_page) - 1);
                        });
                    }

                    $scope.nextPage = function() {
                        if ($scope.paginate.current_page < $scope.paginate.last_page) {
                            $scope.paginate.current_page++;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.previousPage = function() {
                        if ($scope.paginate.current_page > 1) {
                            $scope.paginate.current_page --;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.allRequestLoans = function () {
                        globalService.getter('app/request-loans/all').then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.requestLoans = data.data;
                        });
                    }

                    if(id || $location.url() == '/app/request-loans-create')
                    {

                        if(id){

                        }
                    }else{
                        $scope.requestLoan.modalidad = ''
                        $scope.message = globalService.getMessage();
                        $scope.allRequestLoans();
                    }


                    $scope.createRequestLoan = function() {
                        $scope.loading = true

                        globalService.poster('app/request-loans-create', $scope.requestLoan).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.request-loans');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }
                    

                    $scope.preventEnterForm = function () {
                        return false;
                    }
                    


                }
            ]
        );
})();
