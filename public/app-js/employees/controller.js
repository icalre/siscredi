/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('employees.controllers',[])
        .controller('EmployeeController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert){
                    $scope.paginate = {};
                    $scope.employees = []
                    $scope.employee = {}
                    $scope.loading = false
                    $scope.sites = []

                    $scope.states = [
                        {id:1, value: 0, name: 'Inactivo'},
                        {id:2, value: 1, name:'Activo'}
                    ]

                    globalService.getter('app/api/sites').then(function (data) {
                        $scope.sites = data;
                        $scope.employee.site = $scope.sites[0];
                    });

                    var id = $stateParams.id;


                    $scope.loadPage = function(page){
                        var area_id = '';
                        var site_id = '';
                        var job_id = '';
                        if($scope.employee.site)
                        {
                            site_id = $scope.employee.site.id;
                        }

                        if($scope.employee.area)
                        {
                            area_id = $scope.employee.area.id;
                        }

                        if($scope.employee.area_job)
                        {
                            job_id = $scope.employee.area_job.id;
                        }

                        globalService.getter('app/employees/all?page='+page
                            +'&site='+ site_id +'&area='+ area_id +'&job='+ job_id
                        ).then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.employees = data.data;
                            $scope.track = Number($scope.paginate.per_page)*(Number($scope.paginate.current_page) - 1);
                        });
                    }
                    $scope.nextPage = function() {
                        if ($scope.paginate.current_page < $scope.paginate.last_page) {
                            $scope.paginate.current_page++;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.previousPage = function() {
                        if ($scope.paginate.current_page > 1) {
                            $scope.paginate.current_page --;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.allPeople = function () {
                        globalService.getter('app/employees/all').then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.employees = data.data;
                        });
                    }

                    if(id || $location.url() == '/app/employees-create')
                    {

                        if(id){
                            globalService.getter('app/employees/find/'+id).then(function (data) {
                                $scope.employee = data;
                            });
                        }
                    }else{
                        $scope.message = globalService.getMessage();
                        $scope.allPeople();
                    }


                    $scope.createEmployee = function(){
                        $scope.loading = true
                        globalService.poster('app/employees-create', $scope.employee).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.employees');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.editEmployee = function(){
                        $scope.loading = true
                        globalService.putter('app/employees-edit/'+id, $scope.employee).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage(data.data + ' - Se modifico correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.employees');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.deleteEmployee = function(row){
                        var r = confirm('Estas Seguro de Eliminar este registro');
                        if(r === true)
                        {
                            globalService.deleter('app/employees-delete/'+row.id).then(
                                function(data){
                                    if(data.response){
                                        globalService.setMessage(data.data + ' - Se elimino correctamente','success');
                                        $scope.allPeoples()
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                    $scope.preventEnterForm = function () {
                        return false;
                    }


                    $scope.searchEmployee = function(){
                        if($scope.employee.search.length >= 3){
                            globalService.getter('app/api/employees?q='+$scope.employee.search).then(function (data) {
                                $scope.employees = data;
                                $scope.paginate = {};
                            });
                        }else if($scope.employee.search.length == 0){
                            $scope.loadPage(1);
                        }
                    }

                    $scope.setSubSalaries = function () {
                        $scope.employee.salary_day = Number($scope.employee.salary_amount) / 30;

                        $scope.employee.salary_hour = Number($scope.employee.salary_day) / Number($scope.employee.hours_day);
                    }

                    $scope.filterSite = function () {
                        $scope.loadPage(1)
                    }

                    $scope.filterArea = function () {
                        $scope.loadPage(1)
                    }

                    $scope.filterAreaJob = function () {
                        $scope.loadPage(1)
                    }

                }
            ]
        );
})();
