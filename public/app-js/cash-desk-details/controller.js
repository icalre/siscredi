/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('cash-desk-details.controllers',[])
        .controller('CashDeskDetailController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert','$window','ngDialog','$timeout',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert,$window,ngDialog,$timeout){
                    $scope.cash_desk_details = {};
                    $scope.cash_desk = {};
                    $scope.loan = {};
                    $scope.customer = {};
                    $scope.panel_selected = '';
                    $scope.disbursement = {};
                    $scope.payment = {};
                    $scope.payments = [];
                    $scope.adelanto = false;
                    $scope.index_quota = -1;
                    $scope.paymentDocument={};
                    $scope.paymentDocument.amount = 0;
                    $scope.paymentDocumentDetails = [];
                    $scope.dialog = {};
                    $scope.loanExpense = {};
                    $scope.expenses_select = false;
                    $scope.consignments = [];
                    $scope.type_payment = '1';
                    $scope.account_id = null;

                    var getCashDeskDetail = function () {
                        $scope.consignments = []
                        globalService.getter('app/cash-desk-details/find').then(
                            function (data) {
                                $scope.cash_desk_details = data;
                                $scope.consignments = data.get_consignments;
                            }
                        )
                        $timeout(getCashDeskDetail, 60000);
                    }

                    if($location.url() == '/app/cash-desk-details')
                    {
                        $timeout( getCashDeskDetail, 60000);
                    }



                    $scope.openDialogR = function () {
                        $scope.dialog =  ngDialog.open({
                            template: 'remesas-dialog',
                            className: 'ngdialog-theme-default',
                            scope: $scope
                        })
                    }

                    $scope.setConsignment = function () {
                        $scope.dialog.close();

                        globalService.poster('app/cash-desk-details/accept-consignment', $scope.consignments).then(
                            function (data) {
                                getCashDeskDetail();
                                if(data.response)
                                {
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                }else{
                                    SweetAlert.swal('ERROR', 'Operación no completada.', 'error');
                                }
                            }
                        )
                    }


                    $scope.getCancellation = function(){
                        globalService.getter('app/cash-desk-details/cancellation-total?loan-id='+$scope.loan.id).then(function(data)
                        {
                            console.log('succes');
                            var data_print = data.data;
                            data_print.method = data.action;
                            globalService.poster('https://print-module.test/print',
                                data_print).then(
                                function(data){
                                }
                            );
                        });
                    }

                    $scope.openCashDesk = function () {
                        globalService.poster('app/cash-desk-details/open', $scope.cash_desk_details).then(
                            function (data) {
                                if(data.response)
                                {
                                    $window.location.reload();
                                }
                            }
                        )
                    }

                    $scope.getTotal = function(){
                        var total = {};
                        total.capital = 0;
                        total.interest = 0;
                        total.amount = 0;
                        $scope.index_quota = -1;
                        for(var i = 0; i < $scope.payments.length; i++){
                            if($scope.index_quota == -1  && $scope.payments[i].state == 'Pendiente')
                            {
                                $scope.index_quota = i;
                            }
                            var item = $scope.payments[i];
                            $scope.payments[i].cobrar = true;
                            total.capital = Number(total.capital) +  Number(item.capital);
                            total.interest = Number(total.interest) +  Number(item.interest);
                            total.amount = Number(total.amount) +  Number(item.amount);
                        }

                        return total;
                    }

                    $scope.getTotal2 = function(){
                        var total = {};
                        total.capital = 0;
                        total.interest = 0;
                        total.amount = 0;
                        $scope.index_quota = -1;

                        for(var i = 0; i < $scope.payments.length; i++){

                            if($scope.index_quota == -1  && $scope.payments[i].state == 'Pendiente')
                            {
                                $scope.index_quota = i;
                            }
                            $scope.payments[i].cobrar = true;
                            $scope.payments[i].interest = 0.00;
                            $scope.payments[i].amount = $scope.payments[i].capital;
                            var item = $scope.payments[i];

                            total.capital = Number(total.capital) +  Number(item.capital);
                            total.interest = Number(total.interest) +  Number(item.interest);
                            total.amount = Number(total.amount) +  Number(item.amount);
                        }
                        return total;
                    }

                    $scope.totalAmount = function(){
                        var total = {};
                        total.capital = 0;
                        total.interest = 0;
                        total.amount = 0;
                        total.mora = 0;
                        for(var i = 0; i < $scope.paymentDocumentDetails.length; i++){
                            var item = $scope.paymentDocumentDetails[i];
                            total.capital = Number(total.capital) +  Number(item.capital);
                            total.interest = Number(total.interest) +  Number(item.interest);
                            total.amount = Number(total.amount) +  Number(item.amount);


                            if(item.pay_mora)
                            {
                                total.mora = Number(total.mora) + Number(item.mora)

                            }
                        }

                        if($scope.expenses_select)
                        {
                            total.amount = Number(total.amount) +  Number($scope.loan.expense_amount);
                        }

                        $scope.loan.total = total;
                    }


                    $scope.searchLoan = function () {
                        $scope.type_payment = '1';

                        globalService.getter('app/cash-desk-details/loan?code='+$scope.loan.code)
                            .then(
                                function (data) {
                                    if(data.response)
                                    {
                                        $scope.loan = data.loan;
                                        $scope.loan.expense_amount = 0;
                                        if($scope.loan.state == 'Cancelado' || $scope.loan.state == 'Anulado')
                                        {
                                            alert('El Prestamo esta '+ $scope.loan.state);
                                            return false;
                                        }

                                        if($scope.loan.type_payment == 'Libre' && ($scope.loan.state =='Vigente' || $scope.loan.state =='Atrasado' ) ){
                                            $scope.payment =data.payments[0];
                                        }else if($scope.loan.type_payment == 'Fija' && ($scope.loan.state =='Vigente' || $scope.loan.state =='Atrasado' )){
                                            $scope.payments = data.loan.payments;
                                            $scope.loan.total = $scope.getTotal();
                                            console.log($scope.payments)
                                        }else if($scope.loan.state =='Perdida' || $scope.loan.state == 'Garantía Ejecutada'){
                                            $scope.payment =data.payments[0];
                                        }

                                        $scope.loan.expenses.filter(
                                            function (row) {
                                                $scope.loan.expense_amount = Number($scope.loan.expense_amount) + Number(row.amount)
                                            }
                                        );
                                    }else{
                                    }
                                }
                            )
                    }

                    $scope.setPanel = function (panel) {
                        $scope.loan = {}
                        $scope.panel_selected = panel
                    }

                    $scope.loanDisbursement = function () {
                        $scope.disbursement.loan_id = $scope.loan.id;
                        $scope.disbursement.amount = $scope.loan.amount;
                        $scope.disbursement.type_payment = $scope.type_payment;

                        globalService.poster('app/cash-desk-details/disbursement', $scope.disbursement).then(
                            function (data) {
                                if(data.response)
                                {
                                    globalService.poster('https://print-module.test/print',
                                        {date: data.data.date,
                                            method: data.action,
                                            loan: data.data.loan,
                                            disbursement: data.data.disbursement
                                        }).then(
                                        function(data){
                                        }
                                    );
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                }else{
                                    SweetAlert.swal('ERROR', 'Operación no completada.', 'error');
                                }
                            }
                        )
                    };

                    $scope.selectPayment = function(item){
                        var index = $scope.payments.indexOf(item);

                        if(index == $scope.index_quota)
                        {
                            $scope.index_quota = $scope.index_quota + 1;
                        }else{
                            alert('Seleccione en orden ascendente!!');
                            return false;
                        }

                        item.cobrar = false;
                        console.log(item)
                        $scope.paymentDocumentDetails.push(item);
                        $scope.paymentDocument.amount = Number($scope.paymentDocument.amount) + Number(item.amount);
                        $scope.totalAmount();
                    }

                    $scope.unSelectPayment = function(item){
                        var index = $scope.payments.indexOf(item) + 1;
                        if(index == $scope.index_quota)
                        {
                            $scope.index_quota = $scope.index_quota - 1;
                        }else{
                            alert('Seleccione en orden descendente!!');
                            return false;
                        }
                        item.cobrar = true;
                        $scope.paymentDocument.amount = Number($scope.paymentDocument.amount) - Number(item.amount);
                        var index = $scope.paymentDocumentDetails.indexOf(item);
                        $scope.paymentDocumentDetails.splice(index, 1);
                        $scope.totalAmount();
                    }

                    $scope.setMora = function(item){
                        if(!item.cobrar)
                        {
                            return false
                        }

                        if(item.mora <= 0)
                        {
                            return false
                        }

                        if(item.pay_mora)
                        {
                            item.pay_mora  = false
                            item.amount = Number(item.amount) - Number(item.mora)

                        }else if(!item.pay_mora){
                            item.pay_mora  = true
                            item.amount = Number(item.amount) + Number(item.mora)
                        }
                    }

                    $scope.setAmountPay = function(item){
                        item.amount = Number(item.mora) + Number(item.capital) + Number(item.interest);
                    }

                    $scope.payF = function(){

                        $scope.paymentDocument.paymentDocumentDetails = $scope.paymentDocumentDetails;
                        $scope.paymentDocument.customer_id = $scope.loan.customer_id;
                        $scope.paymentDocument.expense_amount = 0;
                        $scope.paymentDocument.type_payment = $scope.type_payment;

                        if($scope.expenses_select)
                        {
                            $scope.paymentDocument.expense_amount = $scope.loan.expense_amount;
                            $scope.paymentDocument.amount = Number($scope.paymentDocument.amount) + Number($scope.loan.expense_amount);
                        }


                        globalService.poster('app/cash-desk-details/payF',$scope.paymentDocument).then(
                            function(data){
                                if(data.response){
                                    var data_print = data.data;
                                    data_print.method = data.action;
                                    globalService.poster('https://print-module.test/print',
                                        data_print).then(
                                        function(data){
                                        }
                                    );

                                    $scope.setPanel('');
                                    $scope.loan = {}
                                    $scope.paymentDocument={};
                                    $scope.paymentDocument.amount = 0;
                                    $scope.paymentDocumentDetails = [];
                                }
                            }
                        );
                    }

                    $scope.payE = function(){

                        $scope.paymentDocument.paymentDocumentDetails = $scope.paymentDocumentDetails;
                        $scope.paymentDocument.customer_id = $scope.loan.customer_id;
                        $scope.paymentDocument.type_payment = $scope.type_payment;

                        globalService.poster('app/cash-desk-details/payE',$scope.paymentDocument).then(
                            function(data){
                                if(data.response){
                                    var data_print = data.data;
                                    data_print.method = data.action;
                                    globalService.poster('https://print-module.test/print',
                                        data_print).then(
                                        function(data){
                                        }
                                    );
                                    $scope.setPanel('');
                                    $scope.loan = {}
                                    $scope.paymentDocument={};
                                    $scope.paymentDocument.amount = 0;
                                    $scope.paymentDocumentDetails = [];
                                }
                            }
                        );
                    }


                    $scope.setCapitalL = function () {
                        $scope.payment.capital = Number($scope.payment.amount) - Number($scope.payment.interest)

                        if($scope.payment.capital < 0)
                        {
                            $scope.payment.capital = 0
                            $scope.payment.amount = $scope.payment.interest
                        }

                        if($scope.expenses_select)
                        {
                            $scope.payment.amount = Number($scope.payment.amount) +  Number($scope.loan.expense_amount);
                        }
                    }

                    $scope.amountL = function () {

                        if($scope.payment.capital == '')
                        {
                            $scope.payment.capital = 0
                        }

                        $scope.payment.amount = Number($scope.payment.capital) + Number($scope.payment.interest)

                        if($scope.expenses_select)
                        {
                            $scope.payment.amount = Number($scope.payment.amount) +  Number($scope.loan.expense_amount);
                        }

                    }


                    $scope.payL = function(){
                        $scope.paymentDocument.customer_id = $scope.loan.customer_id;
                        $scope.payment.amount = Number($scope.payment.capital) + Number($scope.payment.interest);
                        $scope.paymentDocument.amount = $scope.payment.amount;
                        $scope.paymentDocument.payment = $scope.payment;
                        $scope.paymentDocument.type_payment = $scope.type_payment

                        $scope.paymentDocument.expense_amount = 0;

                        if($scope.expenses_select)
                        {
                            $scope.paymentDocument.expense_amount = $scope.loan.expense_amount;
                            $scope.paymentDocument.amount = Number($scope.paymentDocument.amount) + Number($scope.loan.expense_amount);
                        }

                        globalService.poster('app/cash-desk-details/payL',$scope.paymentDocument).then(
                            function(data){
                                if(data){
                                    var data_print = data.data;
                                    data_print.method = data.action;
                                    globalService.poster('https://print-module.test/print',
                                        data_print).then(
                                        function(data){
                                        }
                                    );

                                    $scope.setPanel('');
                                    $scope.loan = {}
                                    $scope.paymentDocument={};
                                    $scope.paymentDocument.amount = 0;
                                    $scope.paymentDocumentDetails = [];
                                }
                            }
                        );
                    }

                    $scope.payCapital = function(){
                        var r = confirm("¿Desea amortizar el prestamo con "+$scope.payment.amount+"?");

                        if(r) {
                            $scope.paymentDocument.customer_id = $scope.loan.customer_id;
                            $scope.payment.amount = Number($scope.payment.capital) + Number($scope.payment.interest);
                            $scope.paymentDocument.amount = $scope.payment.amount;
                            $scope.paymentDocument.payment = $scope.payment;
                            $scope.paymentDocument.type_payment = $scope.type_payment;


                            $scope.paymentDocument.expense_amount = 0;

                            if($scope.expenses_select)
                            {
                                $scope.paymentDocument.expense_amount = $scope.loan.expense_amount;
                                $scope.paymentDocument.amount = Number($scope.paymentDocument.amount) + Number($scope.loan.expense_amount);
                            }

                            globalService.poster('app/cash-desk-details/payC', $scope.paymentDocument).then(
                                function (data) {
                                    if (data) {
                                        var data_print = data.data;
                                        data_print.method = data.action;
                                        globalService.poster('https://print-module.test/print',
                                            data_print).then(
                                            function(data){
                                            }
                                        );
                                        $scope.setPanel('');
                                        $scope.loan = {}
                                        $scope.paymentDocument={};
                                        $scope.paymentDocument.amount = 0;
                                        $scope.paymentDocumentDetails = [];
                                    }
                                }
                            );
                        }
                    }

                    $scope.setExpense = function () {
                        if($scope.expenses_select)
                        {
                            $scope.expenses_select = false
                        }else{
                            $scope.expenses_select = true
                            if($scope.loan.type_payment == 'Libre'
                                || $scope.loan.state =='Perdida'
                                || $scope.loan.state == 'Garantía Ejecutada')
                            {
                                $scope.amountL();
                            }
                        }

                        $scope.totalAmount()
                    }


                    $scope.getPendingPayments = function(){
                        $scope.paymentDocument={};
                        $scope.paymentDocument.amount = 0;
                        $scope.paymentDocumentDetails = [];
                        $scope.index_quota = -1;
                        $scope.payments = [];

                        globalService.getter('app/cash-desk-details/get-pending-payments?code='+$scope.loan.code)
                            .then(function(data){
                                if(data.response)
                                {
                                    $scope.loan = data.loan;
                                    $scope.paymentDocument.customer_id = $scope.loan.customer_id;
                                    if($scope.loan.type_payment == 'Fija'){
                                        $scope.payments = data.loan.payments;
                                        $scope.adelanto = true;
                                        $scope.loan.total = $scope.getTotal2();
                                    }
                                }else{
                                    alert('Cobrar la cuota mas cercana para poder adelantar cuotas.');
                                }
                            });
                    }

                    $scope.printConstancia = function () {
                        if($scope.customer.originalObject)
                        {
                            globalService.getter('app/cash-desk-details/constancia?customer-id='+$scope.customer.originalObject.id+'&pay=1&account-id='+$scope.account_id).then(function(data)
                            {
                                $scope.setPanel('');

                                if(data.response)
                                {
                                    alert('El Cliente aun presenta deuda');
                                }else{
                                    $window.open('app/cash-desk-details/constancia?&customer-id='+$scope.customer.originalObject.id);
                                }
                            });
                        }else{
                            alert('Seleccione un cliente');

                            return false;
                        }
                    }

                    $scope.breakWarranty = function()
                    {
                        globalService.getter('/app/cash-desk-details/free-warranty?code='+$scope.loan.code).then(function(data)
                        {
                            if(data['response'])
                            {
                                alert('Operación Completada Correctamente');
                                $scope.setPanel('');
                            }else{
                                alert('Operación no Completada');
                                $scope.setPanel('');
                            }
                        });
                    }

                    $scope.setLoanExpense = function (loan) {
                        $scope.loanExpense = {}
                        $scope.dialog =  ngDialog.open({
                            template: 'panel-loan-expense',
                            className: 'ngdialog-theme-default',
                            scope: $scope
                        })
                    }

                    $scope.sendLoanExpense = function () {
                        $scope.loanExpense.loan_id = $scope.loan.id;

                        globalService.poster('app/cash-desk-details/create-loan-expense', $scope.loanExpense).then(
                            function (data) {
                                $scope.dialog.close();
                                if(data['response'])
                                {
                                    alert('Operación Completada Correctamente');
                                    $scope.setPanel('');
                                }else{
                                    alert('Operación no Completada');
                                    $scope.setPanel('');
                                }
                            }
                        )
                    }

                }
            ]
        )

        .controller('CashDeskDetailExpenseController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert','$window',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert,$window){
                    $scope.expenses = []
                    $scope.expense = {}
                    $scope.panel = ''
                    $scope.loading = false
                    $scope.total_expenses = 0
                    $scope.employees = []

                    globalService.getter('app/api/employees').then(function (data) {
                        $scope.employees = data;
                    });

                    $scope.setPanel = function (panel) {
                        if(panel != 'Edit')
                        {
                            $scope.expense = {}
                        }
                        $scope.panel = panel
                    }

                    $scope.setEdit = function (row) {
                        $scope.expense = row;
                        $scope.setPanel('Edit')
                    }

                    $scope.allExpenses = function () {
                        $scope.total_expenses = 0

                        globalService.getter('app/cash-desk-details/expenses').then(
                            function (data) {
                                $scope.expenses = data

                                $scope.expenses.filter(
                                    function (row) {
                                        $scope.total_expenses = Number($scope.total_expenses) + Number(row.amount)
                                    }
                                )
                            }
                        )
                    }

                    $scope.allExpenses()

                    $scope.saveExpense = function () {
                        $scope.loading = true
                        $scope.expense.sub_type = 0;
                        globalService.poster('app/cash-desk-details/expense-create', $scope.expense).then(
                            function (data) {
                                $scope.loading = false
                                if(data.response){
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $scope.allExpenses()
                                    $scope.setPanel('')
                                    $scope.expense = {}
                                    $scope.expense.amount = ''
                                    $state.go('app.cash-desk-details-expenses');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        )
                    }

                    $scope.editExpense = function () {
                        $scope.loading = true
                        globalService.putter('app/cash-desk-details/expense-edit', $scope.expense).then(
                            function (data) {
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $scope.allExpenses()
                                    $scope.setPanel('')
                                    $state.go('app.cash-desk-details-expenses');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        )
                    }

                    $scope.deleteExpense = function (row) {

                        var r = confirm('Estas Seguro de Eliminar este registro');
                        if(r === true)
                        {
                            $scope.loading = true
                            globalService.deleter('app/cash-desk-details/expense-delete/'+row.id).then(
                                function(data){
                                    $scope.loading = false
                                    if(data.response) {
                                        globalService.setMessage('Se registro correctamente', 'success');
                                        SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                        $scope.allExpenses()
                                        $scope.setPanel('')
                                        $state.go('app.cash-desk-details-expenses');
                                    }else{
                                        var message = '';
                                        SweetAlert.swal('Operacion No Completada', message, 'error');
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                }
                ]
        )

        .controller('CashDeskDetailExpenseGController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert','$window',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert,$window){
                    $scope.expenses = []
                    $scope.expense = {}
                    $scope.panel = ''
                    $scope.loading = false
                    $scope.total_expenses = 0

                    $scope.setPanel = function (panel) {
                        if(panel != 'Edit')
                        {
                            $scope.expense = {}
                        }
                        $scope.panel = panel
                    }

                    $scope.setEdit = function (row) {
                        $scope.expense = row;
                        $scope.setPanel('Edit')
                    }

                    $scope.allExpenses = function () {
                        $scope.total_expenses = 0

                        globalService.getter('app/cash-desk-details/expenses-g').then(
                            function (data) {
                                $scope.expenses = data

                                $scope.expenses.filter(
                                    function (row) {
                                        $scope.total_expenses = Number($scope.total_expenses) + Number(row.amount)
                                    }
                                )
                            }
                        )
                    }

                    $scope.allExpenses()

                    $scope.saveExpense = function () {
                        $scope.loading = true
                        $scope.expense.sub_type = 1;
                        globalService.poster('app/cash-desk-details/expense-create', $scope.expense).then(
                            function (data) {
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $scope.allExpenses()
                                    $scope.setPanel('')
                                    $scope.expense = {}
                                    $scope.expense.amount = ''
                                    $state.go('app.cash-desk-details-expenses-g');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        )
                    }

                    $scope.editExpense = function () {
                        $scope.loading = true
                        globalService.putter('app/cash-desk-details/expense-edit', $scope.expense).then(
                            function (data) {
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $scope.allExpenses()
                                    $scope.setPanel('')
                                    $state.go('app.cash-desk-details-expenses-g');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        )
                    }

                    $scope.deleteExpense = function (row) {

                        var r = confirm('Estas Seguro de Eliminar este registro');
                        if(r === true)
                        {
                            $scope.loading = true
                            globalService.deleter('app/cash-desk-details/expense-delete/'+row.id).then(
                                function(data){
                                    $scope.loading = false
                                    if(data.response) {
                                        globalService.setMessage('Se registro correctamente', 'success');
                                        SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                        $scope.allExpenses()
                                        $scope.setPanel('')
                                        $state.go('app.cash-desk-details-expenses');
                                    }else{
                                        var message = '';
                                        data.errors.filter(
                                            function (item) {
                                                message += '-' + item;
                                                message += '\n'
                                            }
                                        )
                                        SweetAlert.swal('Operacion No Completada', message, 'error');
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                }
            ]
        )

        .controller('CashDeskDetailIncomeController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert','$window',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert,$window){

                    $scope.incomes = []
                    $scope.income = {}
                    $scope.panel = ''
                    $scope.loading = false
                    $scope.total_incomes = 0

                    $scope.setPanel = function (panel) {
                        $scope.panel = panel
                    }

                    $scope.setEdit = function (row) {
                        $scope.income = row;
                        $scope.setPanel('Edit')
                    }

                    $scope.allIncomes = function () {
                        globalService.getter('app/cash-desk-details/incomes').then(
                            function (data) {
                                $scope.incomes = data

                                $scope.incomes.filter(
                                    function (row) {
                                        $scope.total_incomes = Number($scope.total_incomes) + Number(row.amount)
                                    }
                                )
                            }
                        )
                    }

                    $scope.allIncomes()

                    $scope.saveIncome = function () {
                        $scope.loading = true
                        globalService.poster('app/cash-desk-details/income-create', $scope.income).then(
                            function (data) {
                                $scope.loading = false
                                if(data.response) {
                                    $scope.income = {}
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $scope.allIncomes()
                                    $scope.setPanel('')
                                    $state.go('app.cash-desk-details-incomes');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        )
                    }

                    $scope.editIncome = function () {
                        $scope.loading = true
                        globalService.putter('app/cash-desk-details/income-edit', $scope.income).then(
                            function (data) {
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $scope.allIncomes()
                                    $scope.setPanel('')
                                    $state.go('app.cash-desk-details-incomes');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        )
                    }

                    $scope.deleteIncome = function (row) {
                        var r = confirm('Estas Seguro de Eliminar este registro');
                        if(r === true)
                        {
                            $scope.loading = true
                            globalService.deleter('app/cash-desk-details/income-delete/'+row.id).then(
                                function(data){
                                    $scope.loading = false
                                    if(data.response) {
                                        globalService.setMessage('Se registro correctamente', 'success');
                                        SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                        $scope.allIncomes()
                                        $scope.setPanel('')
                                        $state.go('app.cash-desk-details-incomes');
                                    }else{
                                        var message = '';
                                        SweetAlert.swal('Operacion No Completada', message, 'error');
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }
                }
            ]
        )

        .controller('CashDeskDetailDepositController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert','$window',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert,$window){
                    $scope.loading = false
                    $scope.deposit = {}
                    $scope.deposits = []
                    $scope.total_deposits = 0

                    $scope.allDeposits = function () {
                        globalService.getter('app/cash-desk-details/deposits').then(
                            function (data) {
                                $scope.deposits = data

                                $scope.deposits.filter(
                                    function (row) {
                                        if(row.state == 'Correct')
                                        {
                                            $scope.total_deposits = Number($scope.total_deposits) + Number(row.amount)
                                        }
                                    }
                                )

                                console.log($scope.total_deposits);
                            }
                        )
                    }

                    $scope.allDeposits()

                    $scope.rePrint = function (row) {
                        var r = confirm('Estas Seguro que desea Re Imprimir?');
                        if(r === true)
                        {
                            $scope.loading = true
                            globalService.poster('app/cash-desk-details/re-print-deposit',row).then(
                                function(data){
                                    $scope.loading = false
                                    if(data.response) {
                                        var data_print = data.data;
                                        data_print.method = data.action;
                                        globalService.poster('https://print-module.test/print',
                                            data_print).then(
                                            function(data){
                                            }
                                        );
                                        globalService.setMessage('Se registro correctamente', 'success');
                                        SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                        $scope.allDeposits()
                                        $state.go('app.cash-desk-details-deposits');
                                    }else{
                                        var message = '';
                                        SweetAlert.swal('Operacion No Completada', message, 'error');
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                    $scope.annulledDeposit = function (row) {
                        var r = confirm('Estas Seguro de Extornar este pago');
                        if(r === true)
                        {
                            $scope.loading = true
                            globalService.poster('app/cash-desk-details/annulled-deposit',row).then(
                                function(data){
                                    $scope.loading = false
                                    if(data.response) {
                                        var data_print = data.data;
                                        data_print.method = data.action;
                                        globalService.poster('https://print-module.test/print',
                                            data_print).then(
                                            function(data){
                                            }
                                        );
                                        globalService.setMessage('Se registro correctamente', 'success');
                                        SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                        $scope.allDeposits()
                                        $state.go('app.cash-desk-details-deposits');
                                    }else{
                                        var message = '';
                                        SweetAlert.swal('Operacion No Completada', message, 'error');
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                }
            ]
        )

        .controller('CashDeskDetailDisbursementController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert','$window',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert,$window){
                    $scope.loading = false
                    $scope.disbursements = []
                    $scope.total_disbursements = 0

                    $scope.allDisbursements = function () {
                        globalService.getter('app/cash-desk-details/disbursements').then(
                            function (data) {
                                $scope.disbursements = data

                                $scope.disbursements.filter(
                                    function (row) {
                                        $scope.total_disbursements= Number($scope.total_disbursements) + Number(row.amount)
                                    }
                                )
                            }
                        )
                    }

                    $scope.allDisbursements()

                    $scope.rePrint = function (row) {
                        var r = confirm('Estas Seguro que desea Re Imprimir?');
                        if(r === true)
                        {
                            $scope.loading = true
                            globalService.poster('app/cash-desk-details/re-print-disbursement',row).then(
                                function(data){
                                    $scope.loading = false
                                    if(data.response) {
                                        var data_print = data.data;
                                        data_print.method = data.action;
                                        globalService.poster('https://print-module.test/print',
                                            data_print).then(
                                            function(data){
                                            }
                                        );
                                        globalService.setMessage('Se registro correctamente', 'success');
                                        SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                        $scope.allDisbursements()
                                        $state.go('app.cash-desk-details-disbursements');
                                    }else{
                                        var message = '';
                                        SweetAlert.swal('Operacion No Completada', message, 'error');
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                }
            ]
        )

        .controller('CashDeskDetailCloseController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert','$window','ngDialog',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert,$window,ngDialog){

                    $scope.cashDeskDetail = {}
                    $scope.consignments = []

                    $scope.getDetail = function () {
                        globalService.getter('app/cash-desk-details/find').then(
                            function (data) {
                                $scope.cashDeskDetail = data
                                $scope.consignments = data.get_consignments
                            }
                        )
                    }

                    $scope.getDetail();

                    $scope.openDialogR = function () {
                        $scope.dialog =  ngDialog.open({
                            template: 'remesas-dialog',
                            className: 'ngdialog-theme-default',
                            scope: $scope
                        })
                    }

                    $scope.setConsignment = function () {
                        $scope.dialog.close();

                        globalService.poster('app/cash-desk-details/accept-consignment', $scope.consignments).then(
                            function (data) {
                                $scope.getDetail();
                                if(data.response)
                                {
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                }else{
                                    SweetAlert.swal('ERROR', 'Operación no completada.', 'error');
                                }
                            }
                        )
                    }

                    $scope.difference = function () {
                        $scope.cashDeskDetail.difference = Number($scope.cashDeskDetail.arching) - Number($scope.cashDeskDetail.total_amount)
                    }


                    $scope.close = function () {
                        $scope.difference();
                        var r = confirm('Estas Seguro que desea cerrar con una diferencia de: ' + $scope.cashDeskDetail.difference.toFixed(2));
                        if(r === true)
                        {
                            if($scope.consignments.length > 0){
                                alert('Tienes que aceptar las remesas para poder cerrar caja.');
                                return false;
                            }

                            $scope.loading = true

                            globalService.poster('app/cash-desk-details/close',$scope.cashDeskDetail).then(
                                function(data){
                                    $scope.loading = false
                                    if(data.response) {
                                        globalService.setMessage('Se registro correctamente', 'success');
                                        SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                        $window.open('/cash-desk-detail-report/'+$scope.cashDeskDetail.id);
                                        window.location.href = "/app/cash-desk-details";
                                    }else{
                                        var message = '';
                                        SweetAlert.swal('Operacion No Completada', message, 'error');
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                }
            ]
        )
})();
