/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('payrollSalaries.controllers',[])
        .controller('PayrollSalaryController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert','ngDialog',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert,ngDialog){
                    $scope.paginate = {};
                    $scope.loading = false
                    $scope.years = []
                    $scope.months = []
                    $scope.payrollSalary = {}
                    $scope.payrollSalaries = []
                    $scope.employees = []
                    $scope.employee = {}
                    $scope.discounts = []
                    $scope.discount = {}
                    $scope.dialog = {}
                    $scope.payPanel = false

                    $scope.payEmployee = {};
                    $scope.payEmployee.amount = 0;
                    $scope.pendingDiscounts = [];
                    $scope.payEmployee.salary_discounts = [];

                    $scope.total_discounts = 0;

                    var id = $stateParams.id;


                    $scope.allPayrollSalary= function () {
                        globalService.getter('app/payroll-salaries/all').then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.payrollSalaries = data.data;
                        });
                    }

                    globalService.getter('app/api/years').then(function (data) {
                        $scope.years = data
                    });

                    globalService.getter('app/api/months').then(function (data) {
                        $scope.months = data
                    });

                        globalService.getter('app/api/employees').then(function (data) {
                            $scope.employees = data;
                            $scope.paginate = {};
                        });

                        $scope.getPayrollSalary = function () {
                            globalService.getter('app/payroll-salaries/find/'+id).then(function (data) {
                                $scope.payrollSalary = data;
                            });
                        }

                    if(id || $location.url() == '/app/payroll-salaries-create')
                    {
                        if(id){
                            $scope.getPayrollSalary();
                        }
                    }else{
                        $scope.message = globalService.getMessage();
                        $scope.allPayrollSalary();
                    }

                    $scope.createPayrollSalary = function(){
                        $scope.loading = true
                        globalService.poster('app/payroll-salaries-create', $scope.payrollSalary).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.payroll-salaries');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.addDiscount = function () {

                        if(!$scope.employee.id)
                        {
                            alert('No se ha seleccionado un asesor');
                            return false;
                        }

                        $scope.dialog =  ngDialog.open({
                            template: 'addDiscountId',
                            className: 'ngdialog-theme-default',
                            scope: $scope
                        })
                    }

                    $scope.getDiscounts = function () {

                        if(!$scope.employee.id)
                        {
                            $scope.discounts = []
                            return false;
                        }

                        globalService.getter('app/payroll-salaries/discounts?employee='+$scope.employee.id).then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.discounts = data.data;
                        });

                    }
                    
                    $scope.getDiscountsEmployee = function () {

                        if(!$scope.payEmployee.employee.id)
                        {
                            $scope.discounts = []
                            return false;
                        }

                        globalService.getter('app/payroll-salaries/employe-discounts?employee='+$scope.payEmployee. employee.id).then(function (data) {
                            $scope.discounts = data;
                            $scope.pendingDiscounts = data;
                        });
                    }

                    $scope.saveDiscount = function () {
                        $scope.discount.employee_id = $scope.employee.id
                        $scope.discount.state = 1
                        $scope.discount.type = 'Descuento'

                        globalService.poster('app/payroll-salaries-discount', $scope.discount).then(
                            function(data){
                                if(data.response) {
                                    $scope.dialog.close();
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $scope.getDiscounts();

                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.setPay = function () {
                        $scope.payPanel = true
                    }

                    $scope.unSetPay = function () {
                        $scope.payPanel = false
                    }



                    $scope.setDiscountsAmount = function()
                    {
                        $scope.payEmployee.discounts = 0;

                        $scope.pendingDiscounts.filter(function(data)
                        {
                            if(data.state == 'Descontado')
                            {
                                $scope.payEmployee.discounts = $scope.payEmployee.discounts + Number(data.amount);
                            }
                        });

                        $scope.payEmployee.salary_discounts.filter(function(data)
                        {
                            if(data.state == 'Descontado')
                            {
                                $scope.payEmployee.discounts = $scope.payEmployee.discounts + Number(data.amount);
                            }
                        });
                    }

                    $scope.selectDiscount = function(row)
                    {
                        row.state = 'Descontado';
                        $scope.setDiscountsAmount();
                        $scope.setAmountPay();
                    }

                    $scope.unSelectDiscount = function(row)
                    {
                        row.state = 'Pendiente';
                        $scope.setDiscountsAmount();
                        $scope.setAmountPay();
                    }

                    $scope.setAmountPay = function()
                    {
                        $scope.setDiscountsAmount();
                        $scope.payEmployee.amount_pay = Number($scope.payEmployee.amount) - Number($scope.payEmployee.discounts);
                    }

                    $scope.createPayEmployee = function()
                    {
                        if(!$scope.payEmployee.employee)
                        {
                            $scope.discounts = []
                            $scope.pendingDiscounts = [];
                            alert('No se ha seleccionado asesor.');
                            return false;
                        }

                        $scope.setDiscountsAmount();
                        $scope.setAmountPay();

                        var r = confirm('Estas Seguro de pagar S/.'+ Number($scope.payEmployee.amount_pay).toFixed(2) + ' a ' + $scope.payEmployee.employee.person.customer_name);

                        if(r === true)
                        {
                            $scope.payEmployee.pendingDiscounts = $scope.pendingDiscounts;
                            $scope.payEmployee.payrollSalary = $scope.payrollSalary;

                            globalService.poster('app/payroll-salaries/pay-employee', $scope.payEmployee).then(
                                function(data){
                                    if(data.response) {
                                        globalService.setMessage('Se registro correctamente', 'success');
                                        SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                        $scope.employee = {};
                                        $scope.payEmployee = {};
                                        $scope.payEmployee.amount = 0;
                                        $scope.payEmployee.discounts = 0;
                                        $scope.payEmployee.amount_pay = 0;
                                        $scope.pendingDiscounts = [];
                                        $scope.payEmployee.salary_discounts = [];
                                        $scope.discounts = [];
                                        $scope.getPayrollSalary();

                                    }else{
                                        var message = '';
                                        data.errors.filter(
                                            function (item) {
                                                message += '-' + item;
                                                message += '\n'
                                            }
                                        )
                                        SweetAlert.swal('Operacion No Completada', message, 'error');
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                    $scope.closePayrollSalary = function () {

                        var r = confirm('Estas Seguro que deseas cerrar la planilla?');

                        if(r === true)
                        {
                            globalService.poster('app/payroll-salaries/close', $scope.payrollSalary).then(
                                function(data){
                                    if(data.response) {
                                        globalService.setMessage('Se registro correctamente', 'success');
                                        SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                        $state.go('app.payroll-salaries');
                                    }else{
                                        var message = '';
                                        data.errors.filter(
                                            function (item) {
                                                message += '-' + item;
                                                message += '\n'
                                            }
                                        )
                                        SweetAlert.swal('Operacion No Completada', message, 'error');
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                }
            ]
        );
})();
