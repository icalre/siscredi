/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('areaJobs.controllers',[])
        .controller('AreaJobController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert){
                    $scope.paginate = {};
                    $scope.areaJobs = []
                    $scope.areaJob = {}
                    $scope.loading = false
                    $scope.areas = []

                    var id = $stateParams.id;


                    $scope.loadPage = function(page){

                        var area_id = '';
                        if($scope.areaJob.area)
                        {
                            area_id = $scope.areaJob.area.id;
                        }

                        globalService.getter('app/area-jobs/all?page='+page+'&type='+ area_id).then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.areaJobs = data.data;
                            $scope.track = Number($scope.paginate.per_page)*(Number($scope.paginate.current_page) - 1);
                        });
                    }
                    $scope.nextPage = function() {
                        if ($scope.paginate.current_page < $scope.paginate.last_page) {
                            $scope.paginate.current_page++;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.previousPage = function() {
                        if ($scope.paginate.current_page > 1) {
                            $scope.paginate.current_page --;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.allPeople = function () {
                        globalService.getter('app/area-jobs/all').then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.areaJobs = data.data;
                        });
                    }

                    globalService.getter('app/api/areas').then(function (data) {
                        $scope.areas = data;
                    });

                    if(id || $location.url() == '/app/area-jobs-create')
                    {

                        if(id){
                            globalService.getter('app/area-jobs/find/'+id).then(function (data) {
                                $scope.areaJob = data;
                            });
                        }
                    }else{
                        $scope.message = globalService.getMessage();
                        $scope.allPeople();
                    }


                    $scope.createAreaJob = function(){
                        $scope.loading = true
                        globalService.poster('app/area-jobs-create', $scope.areaJob).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.area-jobs');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.editAreaJob = function(){
                        $scope.loading = true
                        globalService.putter('app/area-jobs-edit/'+id, $scope.areaJob).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage(data.data + ' - Se modifico correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.area-jobs');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.deleteAreaJob = function(row){
                        var r = confirm('Estas Seguro de Eliminar este registro');
                        if(r === true)
                        {
                            globalService.deleter('app/area-jobs-delete/'+row.id).then(
                                function(data){
                                    if(data.response){
                                        globalService.setMessage(data.data + ' - Se elimino correctamente','success');
                                        $scope.allPeoples()
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                    $scope.preventEnterForm = function () {
                        return false;
                    }


                    $scope.searchAreaJob = function(){
                        var area_id = '';
                        if($scope.area)
                        {
                            area_id = $scope.area.id;
                        }

                        if($scope.search.length >= 3){
                            globalService.getter('app/api/jobs?q='+$scope.search+'&type='+area_id).then(function (data) {
                                $scope.jobs = data;
                                $scope.paginate = {};
                            });
                        }else if($scope.search.length == 0){
                            $scope.loadPage(1);
                        }
                    }

                    $scope.filterArea = function()
                    {
                        $scope.loadPage(1);
                    }

                }
            ]
        );
})();
