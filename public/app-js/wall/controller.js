(function () {
    'use strict';
    angular.module('wall.controllers', [])
        .controller('WallController',
            ['$scope', '$sce', '$stateParams', '$location', 'globalService', '$state', 'SweetAlert', '$q',
                function ($scope, $sce, $stateParams, $location, globalService, $state, SweetAlert, $q) {
                    $scope.title = 'Ingresos';
                    $scope.items = [];
                    $scope.expenses = [];
                    $scope.incomes = [];
                    $scope.date_start = '';
                    $scope.date_end = '';
                    $scope.showRegister = false;
                    $scope.item = {};

                    $scope.changeItems = function (tab) {
                        if (tab == 1) {
                            $scope.title = 'Ingresos';
                            $scope.items = $scope.incomes;
                        }

                        if (tab == 2) {
                            $scope.title = 'Gastos';
                            $scope.items = $scope.expenses;
                        }
                    };

                    $scope.getData = function () {
                        globalService.getter('/wall-all?date_start=' + $scope.date_start + '&date_end=' + $scope.date_end).then(function (data) {
                            if (data.success) {
                                $scope.expenses = data.expenses;
                                $scope.incomes = data.incomes;

                                if ($scope.title == 'Gastos') {
                                    $scope.items = $scope.expenses;
                                } else {
                                    $scope.items = $scope.incomes;
                                }
                            }
                        });
                    };


                    $scope.showRegisterForm = function () {
                        $scope.showRegister = true;
                    };

                    $scope.hideRegisterForm = function () {
                        $scope.showRegister = false;
                    };

                    $scope.saveItem = function () {
                        var url = '/incomes';

                        if ($scope.title == 'Gastos') {
                            url = '/expenses';
                        }

                        globalService.poster(url, $scope.item).then(function (data) {
                            if (data.success) {
                                location.reload();
                            }
                        });
                    };

                    $scope.annulledItem = function (item) {
                        var r = confirm('Estas Seguro de anular este registro');

                        if (r === true) {
                            var url = '/incomes/' + item.id;

                            if ($scope.title == 'Gastos') {
                                url = '/expenses/' + item.id;
                            }

                            globalService.deleter(url).then(function (data) {
                                if (data.success) {
                                    location.reload();
                                }
                            });
                        }

                    };


                    $scope.changeDate = function () {
                        alert('hola');
                    };

                    $scope.getData();


                }
            ]
        );
})();