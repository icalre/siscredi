/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('users.controllers',[])
        .controller('UserController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert','$q',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert, $q){
                    $scope.paginate = {};
                    $scope.users = []
                    $scope.user = {}
                    $scope.loading = false
                    $scope.profiles = []
                    $scope.states = [
                        {id:1, value: 0, name: 'Inactivo'},
                        {id:2, value: 1, name:'Activo'}
                    ]

                    $scope.options = [
                        {id:1, value: 0, name: 'No'},
                        {id:2, value: 1, name: 'Si'}
                    ]

                    $scope.user.state = {id:2, value: 1, name:'Activo'}

                    $scope.user.administrative_option = {id:1, value: 0, name: 'No'}

                    var id = $stateParams.id;


                    $scope.loadPage = function(page){
                        globalService.getter('app/users/all?page='+page).then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.users = data.data;
                            $scope.track = Number($scope.paginate.per_page)*(Number($scope.paginate.current_page) - 1);
                        });
                    }
                    $scope.nextPage = function() {
                        if ($scope.paginate.current_page < $scope.paginate.last_page) {
                            $scope.paginate.current_page++;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.previousPage = function() {
                        if ($scope.paginate.current_page > 1) {
                            $scope.paginate.current_page --;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.allPeople = function () {
                        globalService.getter('app/users/all').then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.users = data.data;
                        });
                    }

                    if(id || $location.url() == '/app/users-create')
                    {

                        globalService.getter('app/api/profiles').then(function (data) {
                            $scope.profiles = data;
                        });

                        if(id){
                            globalService.getter('app/users/find/'+id).then(function (data) {
                                $scope.user = data;
                            });
                        }
                    }else{
                        $scope.message = globalService.getMessage();
                        $scope.allPeople();
                    }


                    $scope.createUser = function(){
                        $scope.loading = true
                        globalService.poster('app/users-create', $scope.user).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.users');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.editUser = function(){
                        $scope.loading = true
                        globalService.putter('app/users-edit/'+id, $scope.user).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage(data.data + ' - Se modifico correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.users');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.deleteUser = function(row){
                        var r = confirm('Estas Seguro de Eliminar este registro');
                        if(r === true)
                        {
                            globalService.deleter('app/users-delete/'+row.id).then(
                                function(data){
                                    if(data.response){
                                        globalService.setMessage(data.data + ' - Se elimino correctamente','success');
                                        $scope.allPeoples()
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                    $scope.preventEnterForm = function () {
                        return false;
                    }


                    $scope.searchUser = function(){
                        if($scope.user.search.length >= 3){
                            globalService.getter('app/api/users?q='+$scope.user.search).then(function (data) {
                                $scope.users = data;
                                $scope.paginate = {};
                            });
                        }else if($scope.user.search.length == 0){
                            $scope.loadPage(1);
                        }
                    }

                }
            ]
        );
})();
