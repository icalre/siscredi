/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('reports.controllers',[])
        .controller('ReportsController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert','$window',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert,$window){
                    $scope.report = {}
                    $scope.date_start = ''
                    $scope.date_end = ''
                    $scope.loading = false
                    $scope.panel = 'Principal'
                    $scope.report_response = false;
                    $scope.total_interest = 0
                    $scope.total_mora = 0
                    $scope.payments = []
                    $scope.loans = []
                    $scope.total_payment_interes = 0
                    $scope.total_payment_mora = 0
                    $scope.total_payment_gasto= 0
                    $scope.flujo_amount = 0
                    $scope.customers = []
                    $scope.total_payment = 0
                    $scope.total_payment_capital = 0
                    $scope.total_capital_pending = 0;
                    $scope.total_capital_lose = 0;
                    $scope.total_loan_amount = 0;


                    $scope.setPanel = function (panel) {
                        $scope.panel = panel
                    }
                    
                    $scope.setLoans = function (uri, panel) {

                        globalService.getter(uri+'?date-start='+$scope.date_start +'&date-end='+$scope.date_end+'&json=1')
                            .then(
                                function (data) {
                                    $scope.loans = data;
                                    $scope.total_interest = 0;
                                    $scope.total_mora = 0;
                                    $scope.total_capital_pending = 0;
                                    $scope.total_capital_lose = 0
                                    $scope.total_loan_amount = 0;

                                    $scope.setPanel(panel)

                                    if($scope.panel == 'Cancelados' || $scope.panel == 'Perdida Cancelado' || $scope.panel == 'Garantia Ejecutada Cancelado')
                                    {
                                        $scope.loans.filter(
                                            function(data){
                                                $scope.total_interest = Number($scope.total_interest) + Number(data.amount_interest);
                                                $scope.total_mora = Number($scope.total_mora) + Number(data.mora_amount);
                                            }
                                        );
                                    }

                                    if($scope.panel == 'Perdida Cancelado' || $scope.panel == 'Garantia Ejecutada Cancelado')
                                    {
                                        $scope.loans.filter(
                                            function (data) {
                                                $scope.total_loan_amount = Number($scope.total_loan_amount)
                                                    + Number(data.amount);
                                                $scope.total_capital_lose = Number($scope.total_capital_lose) + Number(data.lose_capital);
                                            }
                                        )
                                    }

                                    if($scope.panel == 'Garantia Ejecutada' || $scope.panel == 'Perdida' )
                                    {
                                        $scope.loans.filter(
                                            function (data) {
                                                $scope.total_loan_amount = Number($scope.total_loan_amount)
                                                    + Number(data.amount);
                                                $scope.total_capital_pending = Number($scope.total_capital_pending)
                                                    + Number(data.capital_pending);
                                                $scope.total_capital_lose = Number($scope.total_capital_lose) + Number(data.lose_capital);
                                            }
                                        )
                                    }


                                });
                    }

                    $scope.paymentTotal = function(){
                        $scope.total_payment_interes = 0;
                        $scope.total_payment = 0
                        $scope.total_payment_mora = 0
                        $scope.total_payment_gasto= 0
                        $scope.total_payment_capital = 0

                        $scope.payments.filter(function(data){
                            $scope.total_payment_interes = Number($scope.total_payment_interes) + Number(data.interest);
                            $scope.total_payment_mora = Number($scope.total_payment_mora) + Number(data.mora);
                            $scope.total_payment_capital = Number($scope.total_payment_capital) + Number(data.capital);
                            $scope.total_payment_gasto = Number($scope.total_payment_gasto) + Number(data.expenses);
                            $scope.total_payment = Number($scope.total_payment) + Number(data.amount);
                        });
                    }

                    $scope.setPayments = function (uri, panel) {

                        globalService.getter(uri+'?date-start='+$scope.date_start +'&date-end='+$scope.date_end+'&json=1')
                            .then(
                                function (data) {
                                    $scope.payments = data;
                                    $scope.setPanel(panel);
                                    $scope.paymentTotal();
                                });
                    }

                    $scope.clearRepo = function () {
                        $scope.report_response = false;
                        $scope.report = {}
                    }

                    $scope.getReport = function () {
                        if($scope.date_start == '' || $scope.date_end == '' )
                        {
                            return false;
                        }
                        $scope.loading = true
                        globalService.getter('app/reports/get-data?date-start='+$scope.date_start +'&date-end='+$scope.date_end)
                        .then(
                            function (data) {
                                $scope.report = data
                                $scope.flujo_amount = Number($scope.report.cash_desk_detail.initial_amount)
                                    + Number($scope.report.total_pay) + Number($scope.report.income_amount_crediperu)
                                    - Number($scope.report.desembolsos)  - Number($scope.report.expenses_amount)
                                    - Number($scope.report.expenses_amount_g);

                                $scope.report_response = true;
                                $scope.loading = false;

                            }
                        )
                    }

                    $scope.setCustomers = function (uri) {
                        globalService.getter(uri+'?date-start='+$scope.date_start +'&date-end='+$scope.date_end+'&json=1')
                            .then(
                                function (data) {
                                    $scope.customers = data;
                                    $scope.setPanel('Customers');
                                });
                    }

                }
            ]
        )


})();
