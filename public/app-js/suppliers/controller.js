/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('suppliers.controllers',[])
        .controller('SupplierController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert){
                    $scope.paginate = {};
                    $scope.suppliers = []
                    $scope.supplier = {}
                    $scope.loading = false

                    var id = $stateParams.id;


                    $scope.loadPage = function(page){

                        var type = '';
                        if($scope.supplier.type)
                        {
                            type = $scope.supplier.type;
                        }

                        globalService.getter('app/suppliers/all?type='+type+'&page='+page).then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.suppliers = data.data;
                            $scope.track = Number($scope.paginate.per_page)*(Number($scope.paginate.current_page) - 1);
                        });
                    }
                    $scope.nextPage = function() {
                        if ($scope.paginate.current_page < $scope.paginate.last_page) {
                            $scope.paginate.current_page++;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.previousPage = function() {
                        if ($scope.paginate.current_page > 1) {
                            $scope.paginate.current_page --;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.allPeople = function () {
                        globalService.getter('app/suppliers/all').then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.suppliers = data.data;
                        });
                    }

                    if(id || $location.url() == '/app/suppliers-create')
                    {

                        if(id){
                            globalService.getter('app/suppliers/find/'+id).then(function (data) {
                                $scope.supplier = data;
                            });
                        }
                    }else{
                        $scope.message = globalService.getMessage();
                        $scope.allPeople();
                    }


                    $scope.createSupplier = function(){
                        $scope.loading = true
                        globalService.poster('app/suppliers-create', $scope.supplier).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.suppliers');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.editSupplier = function(){
                        $scope.loading = true
                        globalService.putter('app/suppliers-edit/'+id, $scope.supplier).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage(data.data + ' - Se modifico correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.suppliers');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.deleteSupplier = function(row){
                        var r = confirm('Estas Seguro de Eliminar este registro');
                        if(r === true)
                        {
                            globalService.deleter('app/suppliers-delete/'+row.id).then(
                                function(data){
                                    if(data.response){
                                        globalService.setMessage(data.data + ' - Se elimino correctamente','success');
                                        $scope.allPeoples()
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                    $scope.preventEnterForm = function () {
                        return false;
                    }

                    $scope.filterSupplier = function(){
                            $scope.loadPage(1);
                    }


                    $scope.searchSupplier = function(){

                        var type = '';
                        if($scope.supplier.type)
                        {
                            type = $scope.supplier.type;
                        }


                        if($scope.supplier.search.length >= 3){
                            globalService.getter('app/api/suppliers?type='+type+'&q='+$scope.supplier.search).then(function (data) {
                                $scope.suppliers = data;
                                $scope.paginate = {};
                            });
                        }else if($scope.supplier.search.length == 0){
                            $scope.loadPage(1);
                        }
                    }
                }
            ]
        );
})();
