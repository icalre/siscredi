/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('modules.controllers',[])
        .controller('ModuleController',
            ['$scope','$sce','$stateParams','$location','globalService','$state',
                function($scope,$sce, $stateParams,$location,globalService, $state){
                    $scope.modules = []
                    $scope.module = {}
                    $scope.module.sub_modules = []
                    $scope.profiles = []
                    $scope.options = [
                        {id: 1, value: 0, name: 'No'},
                        {id: 2, value: 1, name: 'Si'}
                    ]

                    var id = $stateParams.id;

                    $scope.allModules = function () {
                        globalService.getter('app/modules/all').then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.modules = data.data;
                        });
                    }

                    if(id || $location.url() == '/app/modules-create')
                    {
                        globalService.getter('app/api/profiles').then(function (data) {
                            $scope.profiles = data;
                        });

                        if(id){
                            globalService.getter('app/modules/find/'+id).then(function (data) {
                                $scope.module = data;
                                $scope.module.sub_modules = data.submodules;

                                $scope.module.sub_modules.filter(function (item) {
                                    $scope.options.filter(function (row) {
                                        if(row.value == item.link_menu)
                                        {
                                            item.link_menu = row
                                        }
                                    })
                                })
                            });
                        }
                    }else{
                        $scope.message = globalService.getMessage();
                        $scope.allModules();
                    }

                    $scope.addSubModule = function(){
                        var item = {
                            name : '',
                            uri:'',
                            //link_menu: {id: 2, value: 1, name: 'Si'}
                        };
                        $scope.module.sub_modules.push(item);
                    }

                    $scope.removeSubModule = function(item){
                        $scope.module.sub_modules.splice(item,1);
                    }

                    $scope.createModule = function(){
                        globalService.poster('app/modules-create', $scope.module).then(
                            function(data){
                                if(data.response) {
                                    globalService.setMessage(data.data + ' - Se registro correctamente', 'success');
                                    $state.go('app.modules');
                                }else{

                                }
                            }
                        );
                    }

                    $scope.editModule = function(){
                        globalService.putter('app/modules-edit/'+id, $scope.module).then(
                            function(data){
                                if(data.response) {
                                    globalService.setMessage(data.data + ' - Se modifico correctamente', 'success');
                                    $state.go('app.modules');
                                }else{

                                }
                            }
                        );
                    }

                    $scope.deleteModule = function(row){
                        var r = confirm('Estas Seguro de Eliminar este registro');
                        if(r === true)
                        {
                            globalService.deleter('app/modules-delete/'+row.id).then(
                                function(data){
                                    if(data.response){
                                        globalService.setMessage(data.data + ' - Se elimino correctamente','success');
                                        $scope.allModules()
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                    $scope.preventEnterForm = function () {
                        return false;
                    }
                }
            ]
        );
})();
