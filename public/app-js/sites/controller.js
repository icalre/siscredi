/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('sites.controllers',[])
        .controller('SiteController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert){
                    $scope.paginate = {};
                    $scope.sites = []
                    $scope.site = {}
                    $scope.loading = false
                    $scope.companies = []

                    var id = $stateParams.id;


                    $scope.loadPage = function(page){
                        globalService.getter('app/sites/all?page='+page).then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.sites = data.data;
                            $scope.track = Number($scope.paginate.per_page)*(Number($scope.paginate.current_page) - 1);
                        });
                    }
                    $scope.nextPage = function() {
                        if ($scope.paginate.current_page < $scope.paginate.last_page) {
                            $scope.paginate.current_page++;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.previousPage = function() {
                        if ($scope.paginate.current_page > 1) {
                            $scope.paginate.current_page --;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.allPeople = function () {
                        globalService.getter('app/sites/all').then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.sites = data.data;
                        });
                    }

                    if(id || $location.url() == '/app/sites-create')
                    {
                        globalService.getter('app/api/companies').then(function (data) {
                            $scope.companies = data;

                            if(id){
                                globalService.getter('app/sites/find/'+id).then(function (data) {
                                    $scope.site = data;
                                });
                            }else{
                                $scope.site.company = $scope.companies[0];
                            }

                        });
                    }else{
                        $scope.message = globalService.getMessage();
                        $scope.allPeople();
                    }


                    $scope.createSite = function(){
                        $scope.loading = true
                        globalService.poster('app/sites-create', $scope.site).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.sites');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.editSite = function(){
                        $scope.loading = true
                        globalService.putter('app/sites-edit/'+id, $scope.site).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage(data.data + ' - Se modifico correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.sites');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.deleteSite = function(row){
                        var r = confirm('Estas Seguro de Eliminar este registro');
                        if(r === true)
                        {
                            globalService.deleter('app/sites-delete/'+row.id).then(
                                function(data){
                                    if(data.response){
                                        globalService.setMessage(data.data + ' - Se elimino correctamente','success');
                                        $scope.allPeoples()
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                    $scope.preventEnterForm = function () {
                        return false;
                    }


                    $scope.searchSite = function(){
                        if($scope.site.search.length >= 3){
                            globalService.getter('app/api/sites?q='+$scope.site.search).then(function (data) {
                                $scope.sites = data;
                                $scope.paginate = {};
                            });
                        }else if($scope.site.search.length == 0){
                            $scope.loadPage(1);
                        }
                    }
                }
            ]
        );
})();
