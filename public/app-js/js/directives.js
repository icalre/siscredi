/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    var __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };
    angular.module('global.directives',[])

        .directive('ngEnter', function () {
            return function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
                    if(event.which === 13) {
                        scope.$apply(function (){
                            scope.$eval(attrs.ngEnter);
                        });

                        event.preventDefault();
                    }
                });
            };
        })

        .directive('loading', function () {
            return {
                restrict: 'E',
                replace:true,
                templateUrl: 'js/app/partials/screen-block.html',
                link: function (scope, element, attr) {
                    scope.$watch('loading', function (val) {
                        if (val)
                            $(element).show();
                        else
                            $(element).hide();
                    });
                }
            }
        })

        .directive("decimals", function ($filter) {
            return {
                restrict: "A", // Only usable as an attribute of another HTML element
                require: "?ngModel",
                scope: {
                    decimals: "@",
                    decimalPoint: "@"
                },
                link: function (scope, element, attr, ngModel) {
                    var decimalCount = parseInt(scope.decimals) || 2;
                    var decimalPoint = scope.decimalPoint || ".";
                    // Run when the model is first rendered and when the model is changed from code
                    ngModel.$render = function() {
                        if (ngModel.$modelValue != null && ngModel.$modelValue >= 0) {
                            if (typeof decimalCount === "number") {
                                element.val(Number(ngModel.$modelValue).toFixed(decimalCount).toString().replace(".", "."));
                            } else {
                                element.val(ngModel.$modelValue.toString().replace(".", "."));
                            }
                        }
                    }
                    // Run when the view value changes - after each keypress
                    // The returned value is then written to the model
                    ngModel.$parsers.unshift(function(newValue) {
                        if (typeof decimalCount === "number") {
                            var floatValue = parseFloat(newValue.replace(",", "."));
                            if (decimalCount === 0) {
                                return parseInt(floatValue);
                            }
                            return parseFloat(floatValue.toFixed(decimalCount));
                        }

                        return parseFloat(newValue.replace(",", "."));
                    });
                    // Formats the displayed value when the input field loses focus
                    element.on("change", function(e) {
                        var floatValue = parseFloat(element.val().replace(",", "."));
                        if (!isNaN(floatValue) && typeof decimalCount === "number") {
                            if (decimalCount === 0) {
                                element.val(parseInt(floatValue));
                            } else {
                                var strValue = floatValue.toFixed(decimalCount);
                                element.val(strValue.replace(".", decimalPoint));
                            }
                        }else{
                            element.val('');
                        }
                    });
                }
            }
        })


        .directive('onlyNumber', function () {
            return {
                restrict: 'EA',
                require: 'ngModel',
                link: function (scope, element, attrs, ngModel) {
                    scope.$watch(attrs.ngModel, function(newValue, oldValue) {
                        var spiltArray = String(newValue).split("");

                        if(attrs.allowNegative == "false") {
                            if(spiltArray[0] == '-') {
                                newValue = newValue.replace("-", "");
                                ngModel.$setViewValue(newValue);
                                ngModel.$render();
                            }
                        }

                        if(attrs.allowDecimal == "false") {
                            newValue = parseInt(newValue);
                            ngModel.$setViewValue(newValue);
                            ngModel.$render();
                        }

                        if(attrs.allowDecimal != "false") {
                            if(attrs.decimalUpto) {
                                var n = String(newValue).split(".");
                                if(n[1]) {
                                    var n2 = n[1].slice(0, attrs.decimalUpto);
                                    newValue = [n[0], n2].join(".");
                                    ngModel.$setViewValue(newValue);
                                    ngModel.$render();
                                }
                            }
                        }


                        if (spiltArray.length === 0) return;
                        if (spiltArray.length === 1 && (spiltArray[0] == '-' || spiltArray[0] === '.' )) return;
                        if (spiltArray.length === 2 && newValue === '-.') return;

                        /*Check it is number or not.*/
                        if (isNaN(newValue)) {
                            ngModel.$setViewValue(oldValue);
                            ngModel.$render();
                        }
                    });
                }
            }

        })


        .directive('numbersOnly', function(){
            return {
                require: 'ngModel',
                link: function(scope, element, attrs, modelCtrl) {
                    modelCtrl.$parsers.push(function (inputValue) {
                        // this next if is necessary for when using ng-required on your input.
                        // In such cases, when a letter is typed first, this parser will be called
                        // again, and the 2nd time, the value will be undefined
                        if (inputValue == undefined) return ''
                        var transformedInput = inputValue.replace(/[^0-9]/g, '');
                        if (transformedInput!=inputValue) {
                            modelCtrl.$setViewValue(transformedInput);
                            modelCtrl.$render();
                        }

                        return transformedInput;
                    });
                }
            };
        })

        .directive('calendar', function() {
            var linker = function(scope, element, attr) {
                element.pikaday({
                    format: 'DD-MM-YYYY'
                });
            };
            return {
                restrict: 'A',
                link: linker
            };
        })


})();