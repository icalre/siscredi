/**
 * Created by icalvay on 02/03/15.
 */
(function(){
    angular.module('global.services',[])
        .factory('globalService',['$http', '$q','$state', function($http, $q,$state){
            var message ={};
            var temp_data = {};
            function getter(uri)
            {
                var deferred = $q.defer();
                $http.get(uri)
                    .success(function (data) {
                        if(data == 403)
                        {
                            deferred.reject();
                            $state.go('403');
                        }else if(data == 401){
                            deferred.reject();
                            $state.go('login');
                        }else{
                            deferred.resolve(data);
                        }
                    });

                return deferred.promise;
            }

            function poster(uri,object)
            {
                var deferred = $q.defer();
                $http.post( uri, object )
                    .success(function (data)
                    {
                        if(data == 403)
                        {
                            deferred.reject();
                            $state.go('403');
                        }else if(data == 401){
                            deferred.reject();
                            $state.go('login');
                        }else{
                            deferred.resolve(data);
                        }
                    }
                );
                return deferred.promise;
            }

            function postFile(uri, file) {
                var fd = new FormData();
                fd.append('file', file);
                var deferred = $q.defer();
                $http.post( uri, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                })
                    .success(function (data)
                        {
                            if(data == 403)
                            {
                                deferred.reject();
                                $state.go('403');
                            }else if(data == 401){
                                deferred.reject();
                                $state.go('login');
                            }else{
                                deferred.resolve(data);
                            }
                        }
                    );
                return deferred.promise;
            }

            function putter(uri,object)
            {
                var deferred = $q.defer();
                $http.put(uri,object )
                    .success(function(data)
                    {
                        if(data == 403)
                        {
                            deferred.reject();
                            $state.go('403');
                        }else if(data == 401){
                            deferred.reject();
                            $state.go('login');
                        }else{
                            deferred.resolve(data);
                        }
                    }
                );
                return deferred.promise;
            }

            function deleter(uri)
            {
                var deferred = $q.defer();
                $http.delete(uri)
                    .success(function(data)
                    {
                        if(data == 403)
                        {
                            deferred.reject();
                            $state.go('403');
                        }else if(data == 401){
                            deferred.reject();
                            $state.go('login');
                        }else{
                            deferred.resolve(data);
                        }
                    }
                );
                return deferred.promise;
            }

            function setMessage(msg,type){
                message.msg = msg;
                message.type = type;
            }

            function setData(data)
            {
                temp_data = data;
            }

            function getData()
            {
                return temp_data;
            }

            function getMessage(){
                return message;
            }

            return {
                getter:getter,
                poster: poster,
                putter: putter,
                deleter: deleter,
                setMessage: setMessage,
                getMessage: getMessage,
                setData: setData,
                getData:getData,
                postFile: postFile
            }
        }])
        /*.factory('socketService', function ($rootScope) {
            var host = window.location.host.split(':');
            var socket = io.connect('http://'+host[0]+':3000/');
            return {
                on: function (eventName, callback) {
                    socket.on(eventName, function () {
                        var args = arguments;
                        $rootScope.$apply(function () {
                            callback.apply(socket, args);
                        });
                    });
                },
                emit: function (eventName, data, callback) {
                    socket.emit(eventName, data, function () {
                        var args = arguments;
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback.apply(socket, args);
                            }
                        });
                    })
                }
            };
        });*/
})();