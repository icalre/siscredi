/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    angular.module('home.controllers',[])
        .controller('HomeController',
            ['$scope','$sce','$routeParams','$location','globalService','$state','ngDialog','SweetAlert','$window','ChartData',
                function($scope,$sce, $routeParams,$location,globalService, $state,ngDialog,SweetAlert,$window){
                    $scope.targets = []
                    $scope.chart = {}

                    globalService.getter('app/flujo/targets-values').then(function (data) {
                        $scope.targets = data
                    });

                    globalService.getter('app/flujo/report-lines-numbers').then(function (data) {
                        $scope.chart.lineData = data
                    });

                    globalService.getter('app/flujo/report-lines-amounts').then(function (data) {
                        $scope.chart.lineData2 = data
                    });

                    globalService.getter('app/flujo/capital').then(function (data) {
                        $scope.chart.lineData3 = data
                    });

                    $scope.chart.lineOptions = {
                        series: {
                            lines: {
                                lineWidth: 2,
                                show: true,
                                fill: 0.01
                            },
                            points: {
                                show: true,
                                radius: 4
                            }
                        },
                        legend: {
                            noColumns: 0,
                            position: "sw",
                            margin:[0,-55]
                        },
                        grid: {
                            borderColor: '#8E959C',
                            borderWidth: 1,
                            hoverable: true,
                            backgroundColor: '#fcfcfc'
                        },
                        tooltip: true,
                        tooltipOpts: {
                            content: function (label, x, y) { return x + ' : ' + y; }
                        },
                        xaxis: {
                            show:true,
                            tickColor: '#eee',
                            mode: 'categories',
                        },
                        yaxis: {
                            show:true,
                            position: ($scope.app.layout.isRTL ? 'right' : 'left'),
                            tickColor: '#8E959C'
                        },
                        shadowSize: 0
                    };

                    $scope.chart.lineOptions2 = {
                        series: {
                            lines: {
                                lineWidth: 4,
                                show: true,
                                fill: 0.05
                            },
                            points: {
                                show: true,
                                radius: 4
                            }
                        },
                        legend: {
                            noColumns: 0,
                            position: "sw",
                            margin:[0,-55]
                        },
                        grid: {
                            borderColor: '#8E959C',
                            borderWidth: 1,
                            hoverable: true,
                            backgroundColor: '#fcfcfc'
                        },
                        tooltip: true,
                        tooltipOpts: {
                            content: function (label, x, y) { return x + ' : ' + y; }
                        },
                        xaxis: {
                            show:true,
                            tickColor: '#eee',
                            mode: 'categories',
                        },
                        yaxis: {
                            show:true,
                            position: ($scope.app.layout.isRTL ? 'right' : 'left'),
                            tickColor: '#8E959C'
                        },
                        shadowSize: 0
                    };
                }
            ]
        )
})();
