/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    angular.module('global.filters',[])
        .filter('startFrom', function() {
            return function(input, start) {
                var res = (start+1)%10;
                start = +start; //parse to int
                if(res == 0){
                    return input.slice(start);
                }else{
                    return input;
                }
            }
        });
})();