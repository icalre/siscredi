(function() {
    'use strict';
    angular
        .module('app.routes')
        .config(routesConfig);

    routesConfig.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider'];
    function routesConfig($stateProvider, $locationProvider, $urlRouterProvider, helper){

        // Set the following to true to enable the HTML5 Mode
        // You may have to set <base> tag in index and a routing configuration in your server
        $locationProvider.html5Mode(true);

        // defaults to dashboard
        $urlRouterProvider.otherwise('/app/home');

        //
        // Application Routes
        // -----------------------------------
        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'app/home/index',
                resolve: helper.resolveFor(
                    'fastclick',
                    'modernizr',
                    'icons',
                    'screenfull',
                    'animo',
                    'sparklines',
                    'slimscroll',
                    'classyloader',
                    'toaster',
                    'oitozero.ngSweetAlert',
                    'whirl'
                    )
            })
            .state('login', {
                url: '/login',
                title:"Login",
                templateUrl: 'app-js/pages/login.html',
                resolve: helper.resolveFor('fastclick', 'modernizr', 'icons','ngDialog','ngImgCrop','filestyle','angularFileUpload','oitozero.ngSweetAlert')
            })

            .state('403', {
                url: '/403',
                title:"No Autorizado",
                templateUrl: 'pages/403',
                resolve: helper.resolveFor('fastclick', 'modernizr', 'icons')
            })

            .state("app.home", {
                url:"/home",
                title:"Home",
                templateUrl:'app/home/ini',
                controller: 'HomeController',
                resolve:helper.resolveFor("flot-chart","flot-chart-plugins","weather-icons",'ngDialog')
            })
            .state("app.modules", {
                url:"/modules",
                title:"Modulos",
                templateUrl:'app-js/modules/views/index.html',
                controller: 'ModuleController'
            })
            .state("app.modules-create", {
                url:"/modules-create",
                title:"Crear Modulo",
                templateUrl:'app/modules/form-create',
                controller: 'ModuleController',
                resolve: helper.resolveFor("ui.select")
            })
            .state("app.modules-edit", {
                url:"/modules-edit/:id",
                title:"Crear Modulo",
                templateUrl:'app/modules/form-edit',
                controller: 'ModuleController',
                resolve: helper.resolveFor("ui.select")
            })

            .state("app.people", {
                url:"/people",
                title:"Personas",
                redirect:"/app/people",
                templateUrl:'app-js/people/views/index.html',
                controller: 'PersonController'
            })
            .state("app.people-create", {
                url:"/people-create",
                title:"Crear Persona",
                templateUrl:'app/people/form-create',
                controller: 'PersonController',
                resolve: helper.resolveFor('inputmask','localytics.directives')
            })
            .state("app.people-edit", {
                url:"/people-edit/:id",
                title:"Editar Persona",
                templateUrl:'app/people/form-edit',
                controller: 'PersonController',
                resolve: helper.resolveFor('inputmask','localytics.directives')
            })

            .state("app.profiles", {
                url:"/profiles",
                title:"Perfiles",
                redirect:"/app/profiles",
                templateUrl:'app-js/profiles/views/index.html',
                controller: 'ProfileController'
            })
            .state("app.profiles-create", {
                url:"/profiles-create",
                title:"Crear Perfil",
                templateUrl:'app/profiles/form-create',
                controller: 'ProfileController'
            })
            .state("app.profiles-edit", {
                url:"/profiles-edit/:id",
                title:"Editar Perfil",
                templateUrl:'app/profiles/form-edit',
                controller: 'ProfileController'
            })

            .state("app.sent-messages", {
                url:"/sent-messages",
                title:"Mensajes de Texto",
                redirect:"/app/sent-messages",
                templateUrl:'app-js/sent-messages/views/index.html',
                controller: 'SentMessageController'
            })
            .state("app.sent-messages-create", {
                url:"/sent-messages-create",
                title:"Enviar Mensaje",
                templateUrl:'app/sent-messages/form-create',
                controller: 'SentMessageController',
                resolve: helper.resolveFor('localytics.directives')
            })


            .state("app.users", {
                url:"/users",
                title:"Usuarios",
                redirect:"/app/users",
                templateUrl:'app-js/users/views/index.html',
                controller: 'UserController'
            })
            .state("app.users-create", {
                url:"/users-create",
                title:"Crear Usuario",
                templateUrl:'app/users/form-create',
                controller: 'UserController',
                resolve: helper.resolveFor('localytics.directives','colorpicker.module')
            })
            .state("app.users-edit", {
                url:"/users-edit/:id",
                title:"Editar Usuario",
                templateUrl:'app/users/form-edit',
                controller: 'UserController',
                resolve: helper.resolveFor('localytics.directives', 'colorpicker.module')
            })

            .state("app.companies", {
            url:"/companies",
            title:"Compañia",
            redirect:"/app/companies",
            templateUrl:'app-js/companies/views/index.html',
            controller: 'CompanyController'
        })
            .state("app.companies-create", {
                url:"/companies-create",
                title:"Crear Compañia",
                templateUrl:'app/companies/form-create',
                controller: 'CompanyController',
                resolve: helper.resolveFor('localytics.directives')
            })
            .state("app.companies-edit", {
                url:"/companies-edit/:id",
                title:"Editar Compañia",
                templateUrl:'app/companies/form-edit',
                controller: 'CompanyController',
                resolve: helper.resolveFor('localytics.directives')
            })

            .state("app.sites", {
                url:"/sites",
                title:"Locales",
                redirect:"/app/sites",
                templateUrl:'app-js/sites/views/index.html',
                controller: 'SiteController'
            })
            .state("app.sites-create", {
                url:"/sites-create",
                title:"Crear Local",
                templateUrl:'app/sites/form-create',
                controller: 'SiteController',
                resolve: helper.resolveFor('localytics.directives')
            })
            .state("app.sites-edit", {
                url:"/sites-edit/:id",
                title:"Editar Local",
                templateUrl:'app/sites/form-edit',
                controller: 'SiteController',
                resolve: helper.resolveFor('localytics.directives')
            })

            .state("app.areas", {
            url:"/areas",
            title:"Areas",
            redirect:"/app/areas",
            templateUrl:'app-js/areas/views/index.html',
            controller: 'AreaController'
        })
            .state("app.areas-create", {
                url:"/areas-create",
                title:"Crear Area",
                templateUrl:'app/areas/form-create',
                controller: 'AreaController',
                resolve: helper.resolveFor('localytics.directives')
            })
            .state("app.areas-edit", {
                url:"/areas-edit/:id",
                title:"Editar Local",
                templateUrl:'app/areas/form-edit',
                controller: 'AreaController',
                resolve: helper.resolveFor('localytics.directives')
            })

            .state("app.area-jobs", {
                url:"/area-jobs",
                title:"Cargos",
                redirect:"/app/area-jobs",
                templateUrl:'app-js/area-jobs/views/index.html',
                controller: 'AreaJobController',
                resolve: helper.resolveFor('localytics.directives')
            })
            .state("app.area-jobs-create", {
                url:"/area-jobs-create",
                title:"Crear Cargo",
                templateUrl:'app/area-jobs/form-create',
                controller: 'AreaJobController',
                resolve: helper.resolveFor('localytics.directives')
            })
            .state("app.area-jobs-edit", {
                url:"/area-jobs-edit/:id",
                title:"Editar Cargo",
                templateUrl:'app/area-jobs/form-edit',
                controller: 'AreaJobController',
                resolve: helper.resolveFor('localytics.directives')
            })

            .state("app.employees", {
                url:"/employees",
                title:"Trabajadores",
                redirect:"/app/employees",
                templateUrl:'app-js/employees/views/index.html',
                controller: 'EmployeeController',
                resolve: helper.resolveFor('localytics.directives')
            })
            .state("app.employees-create", {
                url:"/employees-create",
                title:"Crear Empleado",
                templateUrl:'app/employees/form-create',
                controller: 'EmployeeController',
                resolve: helper.resolveFor('localytics.directives')
            })
            .state("app.employees-edit", {
                url:"/employees-edit/:id",
                title:"Editar Cargo",
                templateUrl:'app/employees/form-edit',
                controller: 'EmployeeController',
                resolve: helper.resolveFor('localytics.directives')
            })

            .state("app.enterprises", {
                url:"/enterprises",
                title:"Empresas",
                redirect:"/app/enterprises",
                templateUrl:'app-js/enterprises/views/index.html',
                controller: 'EnterpriseController',
                resolve: helper.resolveFor('localytics.directives')
            })
            .state("app.enterprises-create", {
                url:"/enterprises-create",
                title:"Crear Empresa",
                templateUrl:'app/enterprises/form-create',
                controller: 'EnterpriseController',
                resolve: helper.resolveFor('localytics.directives','ngImgCrop','filestyle','angularFileUpload')
            })
            .state("app.enterprises-edit", {
                url:"/enterprises-edit/:id",
                title:"Editar Empresa",
                templateUrl:'app/enterprises/form-edit',
                controller: 'EnterpriseController',
                resolve: helper.resolveFor('localytics.directives','ngImgCrop','filestyle','angularFileUpload')
            })

            .state("app.customers", {
                url:"/customers",
                title:"Clientes",
                redirect:"/app/customers",
                templateUrl:'app-js/customers/views/index.html',
                controller: 'CustomerController',
                resolve: helper.resolveFor('localytics.directives')
            })
            .state("app.customers-create", {
                url:"/customers-create",
                title:"Crear Cliente",
                templateUrl:'app/customers/form-create',
                controller: 'CustomerController',
                resolve: helper.resolveFor('localytics.directives')
            })
            .state("app.customers-edit", {
                url:"/customers-edit/:id",
                title:"Editar Cliente",
                templateUrl:'app/customers/form-edit',
                controller: 'CustomerController',
                resolve: helper.resolveFor('localytics.directives')
            })

            .state("app.suppliers", {
                url:"/suppliers",
                title:"Proveedores",
                redirect:"/app/suppliers",
                templateUrl:'app-js/suppliers/views/index.html',
                controller: 'SupplierController',
                resolve: helper.resolveFor('localytics.directives')
            })
            .state("app.suppliers-create", {
                url:"/suppliers-create",
                title:"Crear Proveedor",
                templateUrl:'app/suppliers/form-create',
                controller: 'SupplierController',
                resolve: helper.resolveFor('localytics.directives')
            })
            .state("app.suppliers-edit", {
                url:"/suppliers-edit/:id",
                title:"Editar Proveedor",
                templateUrl:'app/suppliers/form-edit',
                controller: 'SupplierController',
                resolve: helper.resolveFor('localytics.directives')
            })

            .state('app.user-profile', {
                url: '/user-profile',
                title:"Perfil Usuario",
                templateUrl: '/app/home/user-profile',
                controller: 'LoginFormController',
                resolve: helper.resolveFor('ngDialog','ngImgCrop','filestyle','angularFileUpload','colorpicker.module')
            })

            .state("app.loans", {
                url:"/loans",
                title:"Prestamos",
                redirect:"/app/loans",
                templateUrl:'app/loans/index',
                controller: 'LoanController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.loans-create", {
                url:"/loans-create",
                title:"Crear Prestamo",
                templateUrl:'app/loans/form-create',
                controller: 'LoanController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.loans-show", {
                url:"/loans-show/:id",
                title:"Editar Loan",
                templateUrl:'app/loans/form-edit',
                controller: 'LoanController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.cash-desk-details", {
                url:"/cash-desk-details",
                title:"Caja",
                templateUrl:'app/cash-desk-details/index',
                controller: 'CashDeskDetailController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.cash-desk-details-expenses", {
                url:"/cash-desk-details-expenses",
                title:"Caja - Egresos",
                templateUrl:'app/cash-desk-details/expenses-view',
                controller: 'CashDeskDetailExpenseController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.cash-desk-details-expenses-g", {
                url:"/cash-desk-details-expenses-g",
                title:"Caja - Egresos",
                templateUrl:'app/cash-desk-details/expenses-g-view',
                controller: 'CashDeskDetailExpenseGController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.cash-desk-details-incomes", {
                url:"/cash-desk-details-incomes",
                title:"Caja - Ingresos",
                templateUrl:'app/cash-desk-details/incomes-view',
                controller: 'CashDeskDetailIncomeController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.cash-desk-details-disbursements", {
                url:"/cash-desk-details-disbursements",
                title:"Caja - Desembolsos",
                templateUrl:'app/cash-desk-details/disbursements-view',
                controller: 'CashDeskDetailDisbursementController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.cash-desk-details-deposits", {
                url:"/cash-desk-details-deposits",
                title:"Caja - Depositos",
                templateUrl:'app/cash-desk-details/deposits-view',
                controller: 'CashDeskDetailDepositController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.cash-desk-details-close", {
                url:"/cash-desk-details-close",
                title:"Caja - Cierre",
                templateUrl:'app/cash-desk-details/close-view',
                controller: 'CashDeskDetailCloseController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.business", {
                url:"/business",
                title:"Negocios",
                templateUrl:'app/business/index',
                controller: 'BusinessController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.reports", {
                url:"/reports",
                title:"Reportes",
                templateUrl:'app/reports/index',
                controller: 'ReportsController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })

            .state("app.request-loans", {
                url:"/request-loans",
                title:"Perfiles",
                redirect:"/app/profiles",
                templateUrl:'app-js/request-loans/views/index.html',
                controller: 'RequestLoanController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.request-loans-create", {
                url:"/request-loans-create",
                title:"Crear Perfil",
                templateUrl:'app/request-loans/form-create',
                controller: 'RequestLoanController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.black-list", {
                url:"/black-list",
                title:"Archivo Negativo",
                templateUrl:'app-js/black-list/views/index.html?v=1.0.0',
                controller: 'BlackListController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.payroll-salaries", {
                url:"/payroll-salaries",
                title:"Planillas",
                templateUrl:'app-js/payroll-salaries/views/index.html',
                controller: 'PayrollSalaryController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.payroll-salaries-create", {
                url:"/payroll-salaries-create",
                title:"Planillas",
                templateUrl:'app/payroll-salaries/form-create',
                controller: 'PayrollSalaryController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.payroll-salaries-pay", {
                url:"/payroll-salaries-pay/:id",
                title:"Planillas",
                templateUrl:'app/payroll-salaries/form-pay',
                controller: 'PayrollSalaryController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.payroll-salaries-discount", {
                url:"/payroll-salaries-discount",
                title:"Planillas",
                templateUrl:'app/payroll-salaries/form-discount',
                controller: 'PayrollSalaryController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })
            .state("app.wall", {
                url:"/wall",
                title:"Wall",
                templateUrl:'/wall/index',
                controller: 'WallController',
                resolve: helper.resolveFor('ngDialog','localytics.directives')
            })



    } // routesConfig

})();


