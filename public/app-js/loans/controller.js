/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('loans.controllers',[])
        .controller('LoanController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','ngDialog','SweetAlert',
                function($scope,$sce, $stateParams,$location,globalService, $state, ngDialog,SweetAlert){
                    $scope.paginate = {};
                    $scope.loans = []
                    $scope.loan = {}
                    $scope.loan.modalidad = 'Convencional'
                    $scope.loan.feed_option = 'Si'
                    $scope.loan_selected = {}
                    $scope.loading = false
                    $scope.dialog = {}

                    $scope.loan.joyasA = []
                    $scope.loan.artefactosA = []
                    $scope.loan.otrosA = []

                    $scope.otros = {tasacion: 0}
                    $scope.artefactos = {tasacion: 0}
                    $scope.joyas = {pb:0 , pn:0 , tasacion: 0}

                    var id = $stateParams.id;

                    $scope.setWeekQuota = function()
                    {
                        $scope.payments = [];
                        if($scope.loan.period == 'Semanal')
                        {
                            if(Number($scope.loan.quotas) > 0)
                            {
                                var valid = Number($scope.loan.quotas) % 4;

                                if(valid > 0)
                                {
                                    alert('El numero de cuotas tiene que se multiplo de 4, 4-8-12');
                                    $scope.loan.quotas = '';
                                }
                            }
                        }
                    }


                    $scope.loadPage = function(page){
                        var type = '';
                        var warranty = ''
                        var aval = ''
                        var interest = ''
                        var state = ''
                        var modalidad = ''
                        var customer_type = ''

                        if($scope.loan.type_payment)
                        {
                            type = $scope.loan.type_payment;
                        }

                        if($scope.loan.warranty_option)
                        {
                            warranty = $scope.loan.warranty_option;
                        }

                        if($scope.loan.endorsement_option)
                        {
                            aval = $scope.loan.endorsement_option
                        }

                        if($scope.loan.interest && $scope.loan.interest > 0)
                        {
                            interest = $scope.loan.interest
                        }

                        if($scope.loan.state)
                        {
                            state = $scope.loan.state
                        }

                        if($scope.loan.modalidad)
                        {
                            modalidad = $scope.loan.modalidad
                        }

                        if($scope.loan.customer_type)
                        {
                            customer_type = $scope.loan.customer_type
                        }

                        globalService.getter('app/loans/all?type='+type+'&state='+state+'&warranty='+warranty+'&aval='+aval
                            +'&interest='+interest+'&modalidad='+modalidad+'&customer_type='+customer_type+'&page='+page).then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.loans = data.data;
                            $scope.track = Number($scope.paginate.per_page)*(Number($scope.paginate.current_page) - 1);
                        });
                    }

                    $scope.nextPage = function() {
                        if ($scope.paginate.current_page < $scope.paginate.last_page) {
                            $scope.paginate.current_page++;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.previousPage = function() {
                        if ($scope.paginate.current_page > 1) {
                            $scope.paginate.current_page --;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.allLoans = function () {
                        globalService.getter('app/loans/all').then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.loans = data.data;
                        });
                    }

                    if(id || $location.url() == '/app/loans-create')
                    {

                        if(id){
                            globalService.getter('app/loans/find/'+id).then(function (data) {
                                $scope.loan = data;
                                $scope.loan.joyasA = []
                                $scope.loan.artefactosA = []
                                $scope.loan.otrosA = []

                                $scope.loan.warranties.filter(function (item) {

                                    if(item.type == 'Artefacto')
                                    {
                                        $scope.loan.artefactosA.push(item)
                                    }else  if(item.type == 'Otro')
                                    {
                                        $scope.loan.otrosA.push(item)
                                    }else  if(item.type == 'Joya')
                                    {
                                        $scope.loan.joyasA.push(item)

                                    }
                                })

                                if($scope.loan.joyasA.length > 0)
                                {
                                    $scope.loan.joyas = true
                                }

                                if($scope.loan.otrosA.length > 0)
                                {
                                    $scope.loan.otros = true
                                }

                                if($scope.loan.artefactosA.length > 0)
                                {
                                    $scope.loan.artefactos = true
                                }


                                $scope.total();
                            });
                        }
                    }else{
                        $scope.loan.modalidad = ''
                        $scope.message = globalService.getMessage();
                        $scope.allLoans();
                    }


                    $scope.createLoan = function() {
                        $scope.loading = true;

                        if ($scope.loan.modalidad == 'Descuento por Planilla')
                        {
                            if(!$scope.loan.enterprise)
                            {
                                alert('Seleccione una Empresa');
                                $scope.loading = false;
                                return false;
                            }
                        }

                        globalService.poster('app/loans-create', $scope.loan).then(
                            function(data){
                                $scope.loading = false;

                                if(data.response) {
                                    globalService.setMessage('Se registro correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.loans');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.editLoan = function(){
                        $scope.loading = true
                        globalService.putter('app/loans-edit/'+id, $scope.loan).then(
                            function(data){
                                $scope.loading = false
                                if(data.response) {
                                    globalService.setMessage(data.data + ' - Se modifico correctamente', 'success');
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    $state.go('app.loans');
                                }else{
                                    var message = '';
                                    data.errors.filter(
                                        function (item) {
                                            message += '-' + item;
                                            message += '\n'
                                        }
                                    )
                                    SweetAlert.swal('Operacion No Completada', message, 'error');
                                }
                            }
                        );
                    }

                    $scope.deleteLoan = function(row){
                        var r = confirm('Estas Seguro de Eliminar este registro');
                        if(r === true)
                        {
                            globalService.deleter('app/loans-delete/'+row.id).then(
                                function(data){
                                    if(data.response){
                                        globalService.setMessage(data.data + ' - Se elimino correctamente','success');
                                        $scope.allLoanss()
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }

                    $scope.preventEnterForm = function () {
                        return false;
                    }

                    $scope.filterLoan = function(){
                            $scope.loadPage(1);
                    }


                    $scope.searchLoan = function(){

                        var type = '';
                        var warranty = ''
                        var aval = ''
                        var interest = ''
                        var state = ''

                        if($scope.loan.type_payment)
                        {
                            type = $scope.loan.type_payment;
                        }

                        if($scope.loan.warranty_option)
                        {
                            warranty = $scope.loan.warranty_option;
                        }

                        if($scope.loan.endorsement_option)
                        {
                            aval = $scope.loan.endorsement_option
                        }

                        if($scope.loan.interest && $scope.loan.interest > 0)
                        {
                            interest = $scope.loan.interest
                        }

                        if($scope.loan.state)
                        {
                            state = $scope.loan.state
                        }


                        if($scope.loan.search.length >= 3){
                            globalService.getter('app/api/loans?type='+type+'&state='+state+'&warranty='+warranty+'&aval='+aval
                                +'&interest='+interest+'&q='+$scope.loan.search).then(function (data) {
                                $scope.loans = data;
                                $scope.paginate = {};
                            });
                        }else if($scope.loan.search.length == 0){
                            $scope.loadPage(1);
                        }
                    }

                    $scope.addWarranty = function(loanWarranty)
                    {
                        var item = {
                            Description:''
                        }

                        loanWarranty.push(item);
                    }

                    $scope.removeWarranty = function(loanWarranty,index)
                    {
                        loanWarranty.splice(index,1);
                        $scope.total();

                    }

                    $scope.total = function () {

                        $scope.joyas.pb = 0 ;
                        $scope.joyas.pn = 0 ;
                        $scope.joyas.tasacion = 0 ;
                        $scope.artefactos.tasacion = 0;
                        $scope.otros.tasacion = 0;

                        $scope.loan.joyasA.filter(
                            function (row) {
                                $scope.joyas.pb = Number($scope.joyas.pb) + Number(row.pb) ;
                                $scope.joyas.pn = Number($scope.joyas.pn) + Number(row.pn) ;
                                $scope.joyas.tasacion = Number($scope.joyas.tasacion) + Number(row.reference_price) ;
                            }
                        )

                        $scope.loan.artefactosA.filter(
                            function (row) {
                                $scope.artefactos.tasacion = Number($scope.artefactos.tasacion) + Number(row.reference_price) ;
                            }
                        )

                        $scope.loan.otrosA.filter(
                            function (row) {
                                $scope.otros.tasacion = Number($scope.otros.tasacion) + Number(row.reference_price) ;
                            }
                        )
                    }

                    $scope.generateSchedule = function(){
                        if($scope.loanForm.$valid){
                            globalService.poster('app/api/generate-schedule',$scope.loan).then(function (data) {
                                if(data.response)
                                {
                                    $scope.loan.payments = data.quotas;
                                }
                            });
                        }else{
                            SweetAlert.swal('ERROR', 'No has completado los campos requeridos', 'error');
                        }
                    }

                    $scope.dialogOption = function (row) {
                        $scope.loan_selected = row
                        $scope.dialog =  ngDialog.open({
                            template: 'dialogWithNestedConfirmDialogId',
                            className: 'ngdialog-theme-default',
                            scope: $scope
                        })
                    }


                    $scope.printSchedule = function () {
                        $scope.dialog.close();
                    }
                    
                    $scope.annulledLoan = function () {

                        var r = confirm("¿Realmente desea anular este Prestamo?");

                        if(r) {

                            globalService.poster('app/loans/annulled-loan', $scope.loan_selected).then(
                                function (data) {
                                    $scope.dialog.close();
                                    $scope.loadPage(1)
                                }
                            )
                        }
                    }

                    $scope.acceptLoan = function () {

                        var r = confirm("¿Realmente desea Aprobar este Prestamo?");

                        if(r) {

                            globalService.poster('app/loans/accept-loan', $scope.loan_selected).then(
                                function (data) {
                                    $scope.dialog.close();
                                    $scope.loadPage(1)
                                }
                            )
                        }
                    }

                    $scope.loseLoan = function () {
                        var r = confirm("¿Realmente desea pasar a Cartera Pesada este este Prestamo?");

                        if(r) {
                            globalService.poster('app/loans/lose-loan', $scope.loan_selected).then(
                                function (data) {
                                    $scope.dialog.close();
                                    $scope.loadPage(1)
                                }
                            )
                        }
                    }

                    $scope.warrantyLoan = function () {
                        var r = confirm("¿Realmente desea pasar a Garantía Ejecutada este Prestamo?");

                        if(r) {
                            globalService.poster('app/loans/warranty-loan', $scope.loan_selected).then(
                                function (data) {
                                    $scope.dialog.close();
                                    $scope.loadPage(1)
                                }
                            )
                        }
                    }


                    $scope.losePrevLoan = function () {
                        var r = confirm("¿Realmente desea pasar a Perdida este este Prestamo?");

                        if(r) {
                            globalService.poster('/loans-lose', $scope.loan_selected).then(
                                function (data) {
                                    $scope.dialog.close();
                                    $scope.loadPage(1);
                                }
                            )
                        }
                    }

                    $scope.okLoan = function () {
                        var r = confirm("¿Realmente desea pasar a Vigente este este Prestamo?");

                        if(r) {
                            globalService.poster('/loans-ok', $scope.loan_selected).then(
                                function (data) {
                                    $scope.dialog.close();
                                    $scope.loadPage(1);
                                }
                            )
                        }
                    }

                    $scope.totalLoseLoan = function () {
                        var r = confirm("¿Realmente desea pasar a Perdida Total este este Prestamo?");

                        if(r) {
                            globalService.poster('/loans-lose-total', $scope.loan_selected).then(
                                function (data) {
                                    $scope.dialog.close();
                                    $scope.loadPage(1);
                                }
                            )
                        }
                    };


                    $scope.setNewEmployee = function () {
                        var r = confirm("¿Seguro que desea cambiar el asesor de este credito?");

                        if(r) {
                            globalService.poster('app/loans/change-employee', $scope.loan).then(
                                function (data){
                                    if( data.response)
                                    {
                                        globalService.setMessage('Se registro correctamente', 'success');
                                        SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                    }
                                }
                            )
                        }
                    }


                }
            ]
        );
})();
