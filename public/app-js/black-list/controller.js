/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    'use strict';
    angular.module('black-list.controllers',[])
        .controller('BlackListController',
            ['$scope','$sce','$stateParams','$location','globalService','$state','SweetAlert',
                function($scope,$sce, $stateParams,$location,globalService, $state, SweetAlert){
                    $scope.paginate = {};
                    $scope.customers = []
                    $scope.customer = {}
                    $scope.loading = false
                    $scope.page_correlative = 25;
                    $scope.sum_cor_page = 0;
                    $scope.set_rate = false;

                    var id = $stateParams.id;


                    $scope.loadPage = function(page){
                        var type = '';
                        var rate = 'Malo';

                        globalService.getter('app/customers/all?type='+type+'&rate='+rate+'&page='+page).
                            then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.customers = data.data;
                            $scope.track = Number($scope.paginate.per_page)*(Number($scope.paginate.current_page) - 1);
                            if(page >  1)
                            {
                                $scope.sum_cor_page = Number($scope.page_correlative) *(Number(page) - 1)
                            }

                            console.log($scope.sum_cor_page)
                        });
                    }
                    $scope.nextPage = function() {
                        if ($scope.paginate.current_page < $scope.paginate.last_page) {
                            $scope.paginate.current_page++;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.previousPage = function() {
                        if ($scope.paginate.current_page > 1) {
                            $scope.paginate.current_page --;
                            $scope.loadPage($scope.paginate.current_page);
                        }
                    };

                    $scope.allPeople = function () {
                        globalService.getter('app/customers/all?rate=Malo').then(function (data) {
                            $scope.paginate = data;
                            $scope.paginate.pages = new Array($scope.paginate.last_page);
                            $scope.customers = data.data;
                        });
                    }

                    $scope.message = globalService.getMessage();
                    $scope.allPeople();



                    $scope.preventEnterForm = function () {
                        return false;
                    }

                    $scope.setCustomer = function (selected) {
                        $scope.set_rate = false;

                        if(selected)
                        {
                            var customers = []
                            var type = 'Person';
                            var rate = 'Malo';
                            var q = selected.originalObject.customer.customer_name

                            globalService.getter('app/api/customers?type='+type+'&rate='+rate+'&q='+q).then(function (data) {
                                customers = data;

                                if(customers.length > 0)
                                {
                                    $scope.customers = customers
                                    $scope.$broadcast('angucomplete-alt:clearInput', 'customer');
                                }else{
                                    $scope.allPeople();
                                    $scope.set_rate = true
                                    $scope.customer = selected.originalObject
                                }
                            })
                        }
                    }
                    
                    $scope.setCustomerBlackList = function () {
                        var r = confirm('Estas seguro de pasar este cliente a Archivo Negativo');
                        if(r === true)
                        {
                            globalService.poster('app/black-list/'+$scope.customer.id, $scope.customer).then(
                                function(data){
                                    if(data.response){
                                        globalService.setMessage(data.data + ' - Se elimino correctamente','success');
                                        $scope.$broadcast('angucomplete-alt:clearInput', 'customer');
                                        $scope.set_rate = false;
                                        $scope.customer = {}
                                        $scope.allPeople()
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }
                    
                    $scope.unSetCustomerBlackList = function (row) {
                        $scope.customer = row;
                        var r = confirm('Estas seguro de sacar este cliente del archivo negativo.');
                        if(r === true)
                        {
                            globalService.poster('app/black-list/d/'+$scope.customer.id, $scope.customer).then(
                                function(data){
                                    if(data.response){
                                        globalService.setMessage(data.data + ' - Se elimino correctamente','success');
                                        $scope.$broadcast('angucomplete-alt:clearInput', 'customer');
                                        $scope.set_rate = false;
                                        $scope.customer = {}
                                        $scope.allPeople()
                                    }
                                }
                            );
                        }else{
                            return false;
                        }
                    }


                }
            ]
        );
})();
