/**
 * Created by Ivan Calvay on 08/08/2016.
 */

/**
 * Created by icalvay on 03/03/15.
 */
(function(){
    angular.module('login.controllers',[])
        .controller('LoginFormController',
            ['$scope','$sce','$routeParams','$location','globalService','$state','ngDialog','SweetAlert','$window',
                function($scope,$sce, $routeParams,$location,globalService, $state,ngDialog,SweetAlert,$window){
                    $scope.user = {}
                    $scope.changePasswordDialog = {}
                    $scope.uploadPhotoForm = false
                    $scope.myImage        = '';
                    $scope.myCroppedImage = '';
                    $scope.imgcropType    = 'circle';
                    $scope.changeColor = false;

                    $scope.setColor = function () {
                        $scope.changeColor = true;
                    }


                    $scope.setNewColor = function () {
                        $('#user-color').css('color',$scope.user.color);
                    }

                    $scope.updateColor = function () {

                        globalService.poster('app/user/change-color', $scope.user).then(
                            function (data) {
                                if(data.response)
                                {
                                    $scope.user = {}
                                    $scope.changeColor = false;
                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                }else{
                                    $scope.errors = data;
                                }
                            }
                        )
                    }




                    $scope.reset = function() {
                        $scope.myImage        = '';
                        $scope.myCroppedImage = '';
                        $scope.imgcropType    = 'circle';
                    };

                    $scope.reset();

                    var handleFileSelect=function(evt) {
                        var file=evt.currentTarget.files[0];
                        var reader = new FileReader();
                        reader.onload = function (evt) {
                            $scope.$apply(function(/*$scope*/){
                                $scope.myImage=evt.target.result;
                            });
                        };
                        if(file)
                            reader.readAsDataURL(file);
                    };

                    angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);

                    globalService.getter('valid-session').then(
                        function (data) {
                            if(data.response)
                            {
                                if( $location.url() == '/login')
                                {
                                    $state.go('app.home');
                                }
                            }
                        }
                    );


                    $scope.login = function () {
                        globalService.poster('login', $scope.user).then(
                            function (data) {
                                if(data.response)
                                {
                                    $window.location.reload();
                                }else{
                                    $scope.user.authMsg = data.message;
                                }
                            }
                        )

                    }

                    $scope.base64ToBlob = function (base64Data, contentType) {
                        contentType = contentType || '';
                        var sliceSize = 1024;
                        var byteCharacters = atob(base64Data);
                        var bytesLength = byteCharacters.length;
                        var slicesCount = Math.ceil(bytesLength / sliceSize);
                        var byteArrays = new Array(slicesCount);

                        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
                            var begin = sliceIndex * sliceSize;
                            var end = Math.min(begin + sliceSize, bytesLength);

                            var bytes = new Array(end - begin);
                            for (var offset = begin, i = 0 ; offset < end; ++i, ++offset) {
                                bytes[i] = byteCharacters[offset].charCodeAt(0);
                            }
                            byteArrays[sliceIndex] = new Uint8Array(bytes);
                        }
                        return new Blob(byteArrays, { type: contentType });
                    }

                    $scope.changePassword = function () {
                        $scope.changePasswordDialog =  ngDialog.open({
                            template: 'dialogWithNestedConfirmDialogId',
                            className: 'ngdialog-theme-default',
                            scope: $scope
                        })
                    }

                    $scope.updatePassword = function () {
                        globalService.poster('app/user/change-password',$scope.user).then(
                            function (data) {
                                if(data.response)
                                {
                                    $scope.user = {}
                                    $scope.changePasswordDialog.close()

                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                }else{
                                    $scope.errors = data;
                                }
                            }
                        )
                    }

                    $scope.updatePhotoForm = function () {
                        $scope.uploadPhotoForm = true
                    }

                    $scope.cancelUploadPhoto = function () {
                        $scope.uploadPhotoForm = false
                        $scope.reset()
                    }

                    $scope.updatePhoto = function () {
                        if($scope.myImage == '')
                        {
                            SweetAlert.swal('Operacion No Completada', 'Tienes que Seleccionar una imagen', 'error');
                            return false;
                        }



                        globalService.postFile('app/user/upload-photo',$scope.base64ToBlob($scope.myCroppedImage.replace('data:image/png;base64,',''), 'image/jpeg')).then(
                            function (data) {
                                if(data.response)
                                {
                                    $scope.uploadPhotoForm = false

                                    $scope.reset();

                                    SweetAlert.swal('Operacion Completada!', 'Click OK para continuar!', 'success');
                                }else{
                                    $scope.errors = data;
                                }
                            }
                        )
                    }

                }
            ]
        )
})();
